---
fromCardId: '17'
toCardId: '20'
status: valid
---
A evaporação que ocorre na superfície do oceano aumenta se a água e o ar aquecerem. Isso cria mais nuvens e, em seguida, precipitação.
_FONTES: AR6 WG1 Box TS.6 p54 // AR6 WG1 FAQ 8.3 p50_
