---
fromCardId: '17'
toCardId: '20'
status: valid
---
L'evaporazione che avviene sulla superficie dell'oceano aumenta se l'acqua e l'aria si riscaldano. Ciò porta alla formazione di più nuvole che causeranno la pioggia.  
_FONTI: AR6 WG1 Box TS.6 p54 // AR6 WG1 FAQ 8.3 p50_
