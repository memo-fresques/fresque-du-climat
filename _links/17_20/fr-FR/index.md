---
fromCardId: '17'
toCardId: '20'
status: valid
---
L'évaporation qui a lieu à la
surface de l'océan augmente si l'eau
et l'air se réchauffent. Cela fait plus de
nuages qui feront ensuite de la pluie.  
_SOURCES: AR6 WG1 Box TS.6 p54 // AR6 WG1 FAQ 8.3 p50_
