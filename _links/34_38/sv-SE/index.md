---
fromCardId: '34'
toCardId: '38'
status: valid
---
Söndagen den 2 augusti 2020 förberedde Florida sig för passagen av Isaias, orkanen som dumpade skyfall på Bahamas dagen innan. Den kunde återfå styrka och testa räddningstjänsten i en av de stater som drabbats hårdast av Covid-19-epidemin i USA. Florida hade då mer än 480 000 fall sedan pandemins början, mer än New York, som länge var epicentrum för den amerikanska epidemin under våren 2020. På grund av vädret hade Florida varit tvungen att stänga testcenter för Covid-19, av vilka många var uppsatta i tält, i väntan på orkanens ankomst, även om testcenter i länet förblev öppna.
