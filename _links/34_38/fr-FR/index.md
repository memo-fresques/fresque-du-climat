---
fromCardId: '34'
toCardId: '38'
status: valid
---
Les cyclones peuvent avoir des conséquences catastrophiques sur les infrastructures et les vies humaines. Par exemple les cyclones Idai au Mozambique en 2019 a causé la mort de 602 personnes dans la ville de Beira. Le cyclone Kenneth, le mois suivant a affecté plus de 250 000 personnes et détruit 45 000 habitations. La détérioration des conditions sanitaires a permis la diffusion rapide du choléra.  
_SOURCES: Cross-Chapter Box DISASTER Case 1 p600 (p588)_
