---
fromCardId: '34'
toCardId: '38'
status: valid
---
I cicloni possono avere conseguenze catastrofiche sulle infrastrutture e sulle vite umane. Ad esempio, i cicloni Idai in Mozambico nel 2019 hanno causato la morte di 602 persone nella città di Beira. Il ciclone Kenneth, il mese successivo, ha colpito più di 250.000 persone e distrutto 45.000 abitazioni. Il deterioramento delle condizioni sanitarie ha permesso la rapida diffusione del colera.  
_FONTI: Cross-Chapter Box DISASTER Case 1 p600 (p588)_
