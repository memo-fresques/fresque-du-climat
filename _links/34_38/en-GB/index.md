---
fromCardId: '34'
toCardId: '38'
status: valid
---

Sunday, August 2, 2020, Florida is preparing for the passage of Isaias, the hurricane that dumped torrential rains on the Bahamas the day before. It could regain strength and test the emergency services of one of the states most affected by the Covid-19 epidemic in the United States. It has more than 480,000 cases detected since the start of the pandemic, surpassing New York, long the epicentre of the American epidemic in the spring, and second only to California, which has twice the population. Due to the weather, the state has had to close Covid-19 testing centres, many of which were set up in tents, in anticipation of the hurricane's arrival, although county testing centres remain open.
