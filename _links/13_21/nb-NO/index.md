---
fromCardId: '13'
toCardId: '21'
status: simplified
---

I den forenklede utgaven, lager vi en direkte kobling mellom kort 13 og kort 21 som da endrer rolle og blir jordens temperatur og ikke lenger bare atmosfærens.
Definisjonen av jordens temperatur er nettopp gjennomsnittet av lufttemperaturen, ved bakkenivå, på jordens overflate.
