---
fromCardId: '13'
toCardId: '21'
status: simplified
---

No caso da versão simplificada, faz-se uma ligação direta entre a carta 13 e a carta 21, que então muda de função e passa a ser a temperatura da terra e não apenas da atmosfera.
A definição da temperatura da terra é precisamente a temperatura do ar, ao nível do solo, em média na superfície da terra.
