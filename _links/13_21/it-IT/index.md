---
fromCardId: '13'
toCardId: '21'
status: simplified
---
Nel caso della versione semplificata, si stabilisce un collegamento diretto tra la carta 13 e la carta 21 che cambia il suo ruolo e diventa la temperatura della terra e non solo dell'atmosfera. La definizione della temperatura terrestre è proprio la temperatura dell'aria, al livello del suolo, in media sulla superficie terrestre.
