---
fromCardId: '13'
toCardId: '21'
status: simplified
---
En la versión simplificada (cuando no utilizamos las cartas : 10-aerosoles, 14-presupuesto energético, 15-forzamiento radiativo), se hace un enlace directo entre la carta 13 y la carta 21, en este caso la carta 21 se convierte en la temperatura de la tierra y no solo de la atmósfera.
La definición de la temperatura de la tierra, es precisamente la temperatura del aire, a nivel del suelo, en promedio sobre la superficie de la tierra.
