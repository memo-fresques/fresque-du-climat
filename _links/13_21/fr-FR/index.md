---
fromCardId: '13'
toCardId: '21'
status: simplified
---

Dans le cas de la version simplifiée, on fait un lien directement entre la carte 13 et la carte 21 qui change alors de rôle et devient la température de la terre et non plus de l'atmosphère uniquement.
La définition de la température de la terre, c'est justement la température de l'air, au niveau du sol, en moyenne sur la surface de la terre.
