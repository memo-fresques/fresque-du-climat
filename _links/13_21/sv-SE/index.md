---
fromCardId: '13'
toCardId: '21'
status: simplified
---
I den förenklade versionen gör vi en direkt länk mellan kort 13 och kort 21 som då ändrar roll och blir jordens temperatur och inte bara atmosfärens temperatur.
Definitionen av jordens temperatur är just lufttemperaturen vid marknivå, i genomsnitt över jordens yta.
