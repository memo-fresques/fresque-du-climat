---
fromCardId: '39'
toCardId: '38'
status: valid
---

Lorsque les réfugiés se déplacent, ils se retrouvent souvent placés dans des camps "de réfugiés" dans lesquels la concentration humaine et le manque d'infrastructures sanitaires favorisent l'apparition et la circulation des vecteurs de maladie.
