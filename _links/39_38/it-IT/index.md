---
fromCardId: '39'
toCardId: '38'
status: valid
---
Quando si spostano, i rifugiati spesso si ritrovano collocati in campi "profughi" in cui la concentrazione umana e la mancanza di infrastrutture sanitarie favoriscono la comparsa e la circolazione dei vettori di malattie.
