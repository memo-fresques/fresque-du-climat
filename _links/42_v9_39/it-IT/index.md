---
fromCardId: 42_v9
toCardId: '39'
status: valid
---
Le popolazioni svantaggiate, spesso le più colpite dalle disuguaglianze economiche e sociali, hanno generalmente meno risorse per affrontare gli impatti dei cambiamenti climatici. Possono essere più esposte agli eventi climatici estremi come tempeste, inondazioni e siccità, il che può portare a migrazioni di popolazione. 

**Fonti** : _AR6 WG2 FAQ 8.1 p1263 (p1251) // AR6 WG2 8.2.1.2 p1190 (pp1178)_
