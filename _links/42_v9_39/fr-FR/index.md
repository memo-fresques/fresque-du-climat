---
fromCardId: 42_v9
toCardId: '39'
status: valid
---
Les populations défavorisées, souvent les plus touchées par les inégalités économiques et sociales, ont généralement moins de ressources pour faire face aux impacts des changements climatiques. Elles peuvent être plus exposées aux événements climatiques extrêmes tels que les tempêtes, les inondations et les sécheresses, ce qui peut entraîner des mouvements de population. 

**Sources** : _AR6 WG2 FAQ 8.1 p1263 (p1251) // AR6 WG2 8.2.1.2 p1190 (pp1178)_
