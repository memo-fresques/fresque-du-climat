---
fromCardId: '2'
toCardId: '9'
status: optional
---
In realtà, l'industria è responsabile di tante emissioni di metano quanto l'agricoltura a causa delle emissioni fuggitive (le fughe di gas naturale nei gasdotti). È un punto poco conosciuto, quindi questa relazione non è considerata imprescindibile. 

L'industria emette anche HFC (fluidi refrigeranti).  
_FONTI: AR6 WG1 Figura 6.16 (1 year pulse) p887 (p870) // Emissioni di GHG dall'industria in Francia: https://www.citepa.org/wp-content/uploads/Citepa_Rapport-Secten-2022_Rapport-complet_v1.8.pdf
 p340_
