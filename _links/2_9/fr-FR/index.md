---
fromCardId: '2'
toCardId: '9'
status: optional
---
En réalité, l'industrie est responsable d'autant d'émissions de méthane que l'agriculture à cause des émissions fugitives (les fuites de gaz naturel dans les pipelines). C'est un point qui est peu connu, donc cette relation n'est pas considérée comme incontrournable. 

L'industrie émet également des HFC (fluides réfrigérants).  
_SOURCES: AR6 WG1 Figure 6.16 (1 year pulse) p887 (p870) // Emissions de GES par l'industrie en France : https://www.citepa.org/wp-content/uploads/Citepa_Rapport-Secten-2022_Rapport-complet_v1.8.pdf
 p340_
