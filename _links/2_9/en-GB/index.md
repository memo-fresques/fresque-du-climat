---
fromCardId: '2'
toCardId: '9'
status: optional
---

In fact, methane emissions from industry are as strong as emissions from agriculture because of fugitive emissions (natural gas leaks from pipelines). This is a point that is little known, so this relationship is not considered strongly relevant. Industry also emits HFCs (refrigerants).
