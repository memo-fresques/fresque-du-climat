---
fromCardId: '2'
toCardId: '9'
status: optional
---
I själva verket är industrier ansvariga för lika mycket metanutsläpp som jordbruket på grund av flyktiga utsläpp (läckage av naturgas i gasledningar). Detta är är inte så välkänt, så denna relation anses inte vara avgörande. 

Industrin släpper också ut HFC (köldmedia). 
_KÄLLOR: AR6 WG1 Figur 6.16 (1 års puls) s887 (s870) // Växthusgasutsläpp från industrin i Frankrike: https://www.citepa.org/wp-content/uploads/Citepa_Rapport-Secten-2022_Rapport-complet_v1.8.pdf
 s340_
