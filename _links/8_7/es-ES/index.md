---
fromCardId: '8'
toCardId: '7'
status: optional
---
La agricultura y ganadería no utiliza mucho combustible fósil, solo un poco de gasolina en los tractores. Sin embargo, la agricultura y la ganadería emite otros gases de efecto invernadero como el metano liberado en los eructos de los rumiantes y el cultivo de arroz, y el N2O procedente de los fertilizantes utilizados en la agricultura. Las emisiones de CO2 vinculadas a la agricultura corresponden al uso de combustibles fósiles, por lo que esta relación no es directa y, por lo tanto es incorrecta.
