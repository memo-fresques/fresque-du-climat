---
fromCardId: '8'
toCardId: '7'
status: optional
---
Jordbruket släpper inte ut mycket koldioxid förutom från avskogning. Dess koldioxidutsläpp kommer främst från andra växthusgaser.
