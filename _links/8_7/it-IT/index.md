---
fromCardId: '8'
toCardId: '7'
status: optional
---
L’agricoltura non utilizza molti combustibili fossili. Basta un po' di benzina da mettere nei trattori. Invece, emette altri gas serra, come il metano rilasciato dai rutti dei ruminanti e dalla coltivazione del riso o anche N2O proveniente in particolare dai fertilizzanti utilizzati. Le emissioni di CO2 legate all'agricoltura corrispondono all'utilizzo di combustibili fossili, questo legame non è diretto e quindi incorretto.
_FONTI: AR6 WG3 TS.3 p17 (p65) // AR6 WG3 Figura TS.6
AR6 WG3 Figura 7.3 p18 (p66)
p769 (p756)_
