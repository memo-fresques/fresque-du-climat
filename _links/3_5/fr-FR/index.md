---
fromCardId: '3'
toCardId: '5'
status: valid
---
L'utilisation des bâtiments utilise beaucoup d'énergies fossiles, de part l'utilisation d'électricité mais aussi des ressources fossiles pour le chauffage.  
_SOURCES: AR6 WG3 Figure 6.1 p630 (p617)_
