---
fromCardId: '3'
toCardId: '5'
status: valid
---
L'uso degli edifici impiega molti combustibili fossili, sia per l'uso di elettricità che per le risorse fossili necessarie al riscaldamento.
_FONTI: AR6 WG3 Figura 6.1 p630 (p617)_
