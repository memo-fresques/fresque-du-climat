---
fromCardId: '3'
toCardId: '5'
status: valid
---
El uso de los edificios utiliza muchos combustibles fósiles, para la producción  de la electricidad y para la calefacción.
_SOURCES: AR6 WG3 Figura 6.1 p630 (p617)_
