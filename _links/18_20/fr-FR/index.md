---
fromCardId: '18'
toCardId: '20'
status: invalid
---

La fonte de la banquise arctique, mais également la fonte des glaciers du Groenland peuvent amener, dans un futur lointain, à perturber la circulation thermohaline (qui donne le Gulf Stream). Mais attention, la carte "cycle de l'eau" ne fait pas du tout référence à la Circulation thermohaline.
