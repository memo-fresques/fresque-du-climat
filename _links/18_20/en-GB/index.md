---
fromCardId: '18'
toCardId: '20'
status: invalid
---

The melting of the Arctic ice pack, but also the melting of Greenland's glaciers may lead, in the distant future, to a disruption of the thermohaline circulation (which gives rise to the Gulf Stream). But beware, the "Water Cycle" card does not refer at all to the thermohaline circulation.
