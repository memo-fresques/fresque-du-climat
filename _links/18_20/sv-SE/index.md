---
fromCardId: '18'
toCardId: '20'
status: invalid
---
Smältningen av Arktis havsis, men även smältningen av Grönlands glaciärer kan i en avlägsen framtid störa den termohalina cirkulationen (som skapar Golfströmmen). Men observera att kortet 'Vattnets kretslopp' inte alls hänvisar till den termohalina cirkulationen.
