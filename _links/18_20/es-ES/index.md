---
fromCardId: '18'
toCardId: '20'
status: invalid
---

El deshielo del casquete polar ártico, pero también el deshielo de los glaciares de Groenlandia pueden llevar, en un futuro lejano, a perturbar la circulación termohalina (que da lugar a la Corriente del Golfo). Pero atención, el mapa "ciclo del agua" no hace en absoluto referencia a la Circulación termohalina.
