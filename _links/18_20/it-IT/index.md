---
fromCardId: '18'
toCardId: '20'
status: invalid
---
La fusione del ghiaccio marino dell'Artico e dei ghiacciai della Groenlandia potrebbe, in un futuro lontano, disturbare la circolazione termoalina (che produce la Corrente del Golfo). Ma attenzione, la carta del "ciclo dell'acqua" non si riferisce affatto alla circolazione termoalina.
