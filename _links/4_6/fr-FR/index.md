---
fromCardId: '4'
toCardId: '6'
status: optional
---

La construction de route nécessite parfois de déforester, mais l'aspect unidimensionnelle de la route la rend quasiment négligeable devant la déforestation lié à l'agriculture.  L'agriculture est responsable de 80 à 90% de la déforestation dans le monde.  
_SOURCES: Selon FAO 2022 FR entre 2000 et 2018: 90% http://www.fao.org/3/cb9360fr/cb9360fr.pdf 
p58_"
