---
fromCardId: '4'
toCardId: '6'
status: optional
---
La costruzione di strade a volte richiede la deforestazione, ma l'aspetto unidimensionale della strada la rende quasi trascurabile rispetto alla deforestazione legata all'agricoltura. L'agricoltura è responsabile dell'80-90% della deforestazione nel mondo. 
_FONTI: Secondo FAO, è pari al 90%. https://onuitalia.com/2021/11/08/deforestazione-3/
