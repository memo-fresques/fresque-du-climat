---
fromCardId: '4'
toCardId: '6'
status: optional
---
Vägbygge kräver ibland avskogning, men vägens endimensionella karaktär gör den nästan försumbar jämfört med avskogning relaterad till jordbruk. Jordbruket står för 80–90% av avskogningen i världen. 
_KÄLLOR: Enligt FAO 2022 SE mellan 2000 och 2018: 90% http://www.fao.org/3/cb9360fr/cb9360fr.pdf 
s58_"
