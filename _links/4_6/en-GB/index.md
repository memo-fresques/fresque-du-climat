---
fromCardId: '4'
toCardId: '6'
status: optional
---

Road construction sometimes requires deforestation, but the one-dimensional aspect of the road makes it almost negligible compared to deforestation linked to agriculture.
