---
fromCardId: '33'
toCardId: '32'
status: valid
---
Les évènements extrêmes peuvent détruire ou baisser les rendements agricoles. C'est le cas par exemple des cyclones et des submersions qui peuvent amener de l'eau salée sur les champs. Par exemple, le Viêtnam a subi les effets des inondations et de la salinisation du delta du Mékong. Les inondations et les sécheresses ont considérablement entraîné des pertes dans l'agriculture.  
_SOURCES: AR6 WG2 8.3.4.1 p1217 (p1206)_
