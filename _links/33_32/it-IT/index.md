---
fromCardId: '33'
toCardId: '32'
status: valid
---
Gli eventi estremi possono distruggere o ridurre le rese agricole. È il caso, ad esempio, dei cicloni e delle sommersioni che possono portare acqua salata nei campi. Ad esempio, il Vietnam ha subito gli effetti delle inondazioni e della salinizzazione del delta del Mekong. Le inondazioni e le siccità hanno causato notevoli perdite nel settore agricolo. 
_FONTI: AR6 WG2 8.3.4.1 p1217 (p1206)_
