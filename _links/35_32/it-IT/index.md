---
fromCardId: '35'
toCardId: '32'
status: optional
---
In generale, ciò che brucia bene è la foresta, non i campi di grano.  
_FONTI: AR6 WG2 TS.B.2.3 p14 (p48) // https://www.pioneer.com/us/agronomy/wildfires-crop-yields.html_
