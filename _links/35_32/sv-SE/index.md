---
fromCardId: '35'
toCardId: '32'
status: optional
---
Generellt sett är det skogen som brinner lätt – inte vetefälten.  
_KÄLLOR: AR6 WG2 TS.B.2.3 s14 (s48) // https://www.pioneer.com/us/agronomy/wildfires-crop-yields.html_
