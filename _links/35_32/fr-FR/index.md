---
fromCardId: '35'
toCardId: '32'
status: optional
---
En général, ce qui brûle bien, c'est la forêt, pas les champs de blé.  
_SOURCES: AR6 WG2 TS.B.2.3 p14 (p48) // https://www.pioneer.com/us/agronomy/wildfires-crop-yields.html_
