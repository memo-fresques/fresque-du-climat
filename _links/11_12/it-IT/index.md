---
fromCardId: '11'
toCardId: '12'
status: invalid
---
Anche se il cambiamento climatico e in particolare la perturbazione del ciclo dell'acqua ridurrà le rese agricole, l'eccesso di CO2 emesso dall'uomo compensa in parte aumentandole con il processo chiamato fertilizzazione. Le rese aumentano, ma non i nutrienti presenti nel suolo. In generale, l'apporto nutritivo sarà quindi inferiore, causando problemi di malnutrizione, in particolare nei paesi con scarso accesso a una varietà di cibi.
_FONTI: AR6 WG2 TS.C.3.4 p26 (p60) // AR6 WG2 TS.B.3 p14 (p48) // AR6 WG2 Figura TS.3 (b) 
p12 (p46) // AR6 WG2 Figura TS.6 FOOD-WATER (c) p41 (p75)_
