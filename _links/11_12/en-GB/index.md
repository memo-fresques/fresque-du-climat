---
fromCardId: '11'
toCardId: '12'
status: invalid
---

It can be argued that CO2 emissions take place before reaching carbon sinks. But the opposite link has our preference.
