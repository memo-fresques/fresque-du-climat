---
fromCardId: '11'
toCardId: '12'
status: invalid
---
Embora as alterações climáticas e, nomeadamente, a perturbação do ciclo da água reduzam os rendimentos agrícolas, o excesso de CO2 emitido pelo homem compensa parcialmente, aumentando os rendimentos agrícolas, o que se chama fertilização. Os rendimentos aumentam, mas não os nutrientes presentes nos solos. Globalmente, o aporte nutritivo será, portanto, menor, o que causará problemas de subnutrição, especialmente nos países que têm pouco acesso a uma alimentação variada.  
_FONTES: AR6 WG2 TS.C.3.4 p26 (p60) // AR6 WG2 TS.B.3 p14 (p48) // AR6 WG2 Figura TS.3 (b) 
p12 (p46) // AR6 WG2 Figura TS.6 ALIMENTAÇÃO-ÁGUA (c) p41 (p75)_
