---
fromCardId: '11'
toCardId: '12'
status: invalid
---
Bien que le changement climatique et notamment la perturbation du cycle de l'eau va baisser les rendements agricoles, le surplus de CO2 émis par l'homme compense en partie en augmentant les rendements agricoles, c'est ce qu'on appelle la fertilisation. Les rendements augmentent mais pas les nutriments présents dans les sols. Globalement, l'apport nutritif sera donc moindre ce qui posera des problèmes de sous-nutrition, en particulier dans les pays qui ont peu accès à une nourriture variée.  
_SOURCES: AR6 WG2 TS.C.3.4 p26 (p60) // AR6 WG2 TS.B.3 p14 (p48) // AR6 WG2 Figure TS.3 (b) 
p12 (p46) // AR6 WG2 Figure TS.6 FOOD-WATER (c) p41 (p75)_
