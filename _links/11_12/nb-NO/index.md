---
fromCardId: '11'
toCardId: '12'
status: invalid
---
Selv om klimaendringer og spesielt forstyrrelsen av vannkretsløpet vil redusere landbruksavlingene, kompenserer det overskytende CO2-utslippet fra mennesker delvis ved å øke landbruksavkastningen, dette kalles gjødsling. Avkastningen øker, men ikke næringsstoffene i jorda. Totalt sett vil næringstilførselen derfor være mindre, noe som vil føre til underernæringsproblemer, spesielt i land som har liten tilgang til variert mat.  
_KILDER: AR6 WG2 TS.C.3.4 s26 (s60) // AR6 WG2 TS.B.3 s14 (s48) // AR6 WG2 Figur TS.3 (b) 
s12 (s46) // AR6 WG2 Figur TS.6 MAT-VANN (c) s41 (s75)_
