---
fromCardId: '5'
toCardId: '40'
status: optional
---

Although conflicts may occur because of resources, they are not directly caused by climate change, rather by resource depletion or scarcity. The relationship can be made, but it is due more to politics than to the climate crisis.
