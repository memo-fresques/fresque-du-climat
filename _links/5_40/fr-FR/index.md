---
fromCardId: '5'
toCardId: '40'
status: optional
---

Bien que des conflits peuvent avoir lieu à cause des ressources, ce n'est pas directement causé par une perturbation du climat, mais bien par un appauvrissement des ressources. La relation peut-être faite, mais elle est plus politique que climatique. Mais en effet, un grand nombre de conflits armés sont liées à l'accès aux ressources, notamment aux énergies fossiles et en particulier le pétrole. Par exemple, l'invasion du Koweït par l'Irak en 1990 a été largement attribuée aux tensions sur les ressources pétrolières.   
_SOURCES: https://fr.wikipedia.org/wiki/G%C3%A9opolitique_du_p%C3%A9trole#:~:text=Guerre%20du%20Biafra,Guerre%20d'Irak_
