---
fromCardId: '5'
toCardId: '40'
status: optional
---
Aunque pueden ocurrir conflictos por los combustibles, la causa directa no es el cambio climático, sino más bien por el agotamiento de los recursos. La relación se puede establecer, pero es más política que climática. De hecho, un gran número de conflictos armados están vinculados al acceso a los combustibles, en particular a los combustibles fósiles y, concretamente al petróleo. Por ejemplo, la invasión iraquí de Kuwait en 1990 se atribuyó ampliamente a las tensiones por los recursos petroleros.   
_SOURCES: https://fr.wikipedia.org/wiki/G%C3%A9opolitique_du_p%C3%A9trole#:~:text=War%20of%20Biafra,War%20of Iraq_
