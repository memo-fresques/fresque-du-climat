---
fromCardId: '5'
toCardId: '40'
status: optional
---
Sebbene i conflitti possano verificarsi a causa delle risorse, non sono direttamente causati da un cambiamento climatico, ma piuttosto da un impoverimento delle risorse. La relazione può essere fatta, ma è più politica che climatica. Tuttavia, un gran numero di conflitti armati sono legati all'accesso alle risorse, in particolare ai combustibili fossili e al petrolio. Ad esempio, l'invasione del Kuwait da parte dell'Iraq nel 1990 è stata ampiamente attribuita alle tensioni sulle risorse petrolifere.   
_FONTI: https://it.wikipedia.org/wiki/Geopolitica_del_petrolio#:~:text=Guerra%20del%20Biafra,Guerra%20dell'Iraq_
