---
fromCardId: '5'
toCardId: '40'
status: optional
---
Även om konflikter kan uppstå på grund av resurser, är det inte direkt orsakat av klimatförändringarna, utan snarare av resursbrist. Sambandet kan göras, men det är mer politiskt än klimatrelaterat. Men det är faktiskt så att ett stort antal väpnade konflikter är kopplade till tillgången på resurser, särskilt fossila bränslen och i synnerhet olja. Till exempel har Iraks invasion av Kuwait 1990 till stor del tillskrivits spänningar kring oljeresurser.   
_KÄLLOR: https://fr.wikipedia.org/wiki/G%C3%A9opolitique_du_p%C3%A9trole#:~:text=Guerre%20du%20Biafra,Guerre%20d'Irak_
