---
fromCardId: '30'
toCardId: '32'
status: valid
---
23% des cultures sont irriguées, fournissant 34% de la production de calories. Parmi elles, 68% subissent des pénuries  d'eau 1 mois par an et 37% jusqu'à 5 mois par an. 454 millions d'hectares ont subit des pertes de rendement lié à la sécheresse entre 1983 et 2009.  
_SOURCES: AR6 WG2 TS.B.4.6 p16 (p50) // AR6 WG2 4.3.1 p584 (p596) // AR6 WG2 Figure 4.20 p621 (p633) // AR6 WG2 Table 4.4 p580 (p592)_
