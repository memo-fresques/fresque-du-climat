---
fromCardId: '42'
toCardId: '14'
status: valid
---
L'AMOC è una corrente oceanica che sprofonda nell'Artico a causa della sua alta densità, dovuta al suo raffreddamento e all'aumento della salinità. Così trasporta il calore e la CO2 verso gli strati profondi dell'oceano. Il suo rallentamento comporta una perdita di capacità di immagazzinare la CO2 e il calore.   
_FONTI: AR6 WG1 3.5.4.1 p500 // AR6 WG1 Cross-Chapter Box 5.3 p762_
