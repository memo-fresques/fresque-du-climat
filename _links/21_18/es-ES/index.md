---
fromCardId: '21'
toCardId: '18'
status: simplified
---
Esta relación es posible en la versión simplificada cuando no utilizamos las cartas : 10-aerosoles, 14-presupuesto energético, 15-forzamiento radiativo.
