---
fromCardId: '35'
toCardId: '38'
status: valid
---
Les incendies sont dangeureux pour les écosystèmes, les infrastructures mais aussi pour notre santé. Ils peuvent conduire à la mort de personnes ou induire des maladies respiratoires à cause de la fumée. Le méga-feu de 2017 au Chilie a causé 11 morts et on estime que 9.5 millions de personnes ont été exposés aux fumées et causeront 76 décès prématurés.  
_SOURCES: AR6 WG2 FAQ 2.3 p24 // AR6 WG2 Cross-Chapter Box DISASTER (case 5) p601 (p589)_
