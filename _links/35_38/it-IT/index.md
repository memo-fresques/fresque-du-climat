---
fromCardId: '35'
toCardId: '38'
status: valid
---
Gli incendi sono pericolosi per gli ecosistemi, le infrastrutture ma anche per la nostra salute. Possono portare alla morte di persone o indurre malattie respiratorie a causa del fumo. Il mega-incendio del 2017 in Cile ha causato 11 morti e si stima che 9,5 milioni di persone siano state esposte ai fumi, cosa che porterà a 76 decessi prematuri.  
_FONTI: AR6 WG2 FAQ 2.3 p24 // AR6 WG2 Cross-Chapter Box DISASTER (caso 5) p601 (p589)_
