---
fromCardId: '5'
toCardId: '3'
status: optional
---
I combustibili fossili consentono alle attività umane di esistere come le conosciamo. Ma nell’affresco la carta dei combustibili fossili è generalmente posta dopo le attività umane. Inoltre, la carta mostra le emissioni di CO2 legate all’utilizzo dei combustibili fossili.
