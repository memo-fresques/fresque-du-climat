---
fromCardId: '14'
toCardId: '21'
status: valid
---
El efecto invernadero adicional provoca un forzamiento radiativo positivo. El exceso de energía se distribuye en el océano (93%), la vegetación (5%), el hielo (3%) y la atmósfera (1%). Esta energía calienta los distintos compartimentos terrestres en los que se distribuye, aumentando la temperatura y, derritiendo el hielo. La temperatura de la superficie (GMST) aumentó 1,09°C en 2019 en comparación con el período 1850-1900, 0,88°C en la superficie del océano (agua) en comparación con 1,59°C en la tierra (aire).  
_FUENTES: AR6 WG1 TS3.1 (balance energético) p59 (p93) // AR6 WG1 Cuadro de sección transversal TS.1 (océano + tierra) p29 (p60) // AR6 WG1 Cuadro de sección transversal TS.1, Figura 1 ( c): temperatura p29 (p61) // AR6 WG1 Figura 2.11 (c) p333 (p316)_
