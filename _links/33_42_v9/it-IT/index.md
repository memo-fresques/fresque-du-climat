---
fromCardId: '33'
toCardId: 42_v9
status: valid
---
I disastri climatici possono esacerbare le disuguaglianze preesistenti accentuando le disparità economiche, sociali e ambientali tra le diverse popolazioni. Le politiche di gestione, adattamento e mitigazione del rischio devono tenere conto di queste disuguaglianze per promuovere una resilienza più equa. 

**Fonti:** _AR6 WG2 B.2.5 p25 (p13)_
