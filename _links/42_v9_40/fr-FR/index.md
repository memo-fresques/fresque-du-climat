---
fromCardId: 42_v9
toCardId: '40'
status: valid
---
Les inégalités économiques importantes, avec une concentration disproportionnée de richesse entre certaines élites et la population défavorisée, peuvent alimenter des tensions sociales. Ces tensions peuvent contribuer à l'émergence de mouvements de contestation ou à des revendications qui, dans certains cas, peuvent dégénérer en conflits armés. 
**Sources** :  _AR6 WG2 p1057 (p1045) // AR6 WG2 7.2.7.3 p1099 (p1087)_
