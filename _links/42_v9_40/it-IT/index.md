---
fromCardId: 42_v9
toCardId: '40'
status: valid
---
Significative disuguaglianze economiche, con una concentrazione sproporzionata della ricchezza nelle mani di alcune élite rispetto alla popolazione svantaggiata, possono alimentare le tensioni sociali. Queste tensioni possono contribuire all’emergere di movimenti o rivendicazioni di protesta che, in alcuni casi, possono degenerare in conflitti armati. 
**Fonti**: _AR6 WG2 p1057 (p1045) // AR6 WG2 7.2.7.3 p1099 (p1087)_
