---
fromCardId: '37'
toCardId: '40'
status: valid
---
La réduction des ressources en eau douce, les secheresses ou la sécurité alimentaire ont le potentiel d'exacerber des tensions déjà existentes, notamment dans les régions ou certains groupes dépendent de l'agriculture pour la production de nourriture.
Un réchauffement de 2°C et°C pourrait augmenter les risques de conflits de 13% et 26%. Cependant, d'autres facteurs sont considérés plus importants dans l'initiation de conflits, comme le manque de régulation des ressources naturelles, l'exclusion sociétale, des infrastructures pauvres ou d'anciens conflits.   
_SOURCES: AR6 WG2 5.12.4.1 (nourriture) p808 (p796)_
