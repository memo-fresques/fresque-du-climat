---
fromCardId: '9'
toCardId: '13'
status: valid
---
La CO2 è il gas serra antropico principale (75%) ma ce ne sono altri come il metano (18%), il protossido di azoto (4%) e i gas fluorurati (2%). Anche l'ozono è un gas serra ma non è prodotto direttamente dall'uomo. Tutti questi gas sono riscaldanti. L'"effetto serra" della carta 9 è da collegare a quello della carta 13.
_FONTI: AR6 WG3 TS.3 p11 (p59) // AR6 WG3 Figura TS.2 (a) : emissioni p11 (p59) // AR6 WG1 Figura TS.9 : concentrazioni e forzante nel tempo p36 (p68)_
