---
fromCardId: '9'
toCardId: '13'
status: valid
---
Bokstäverna "G" och "H" på kort 9 hänför sig till "GreenHouse" (växthus) på kort 13.
