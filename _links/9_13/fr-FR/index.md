---
fromCardId: '9'
toCardId: '13'
status: valid
---
Le CO2 est le gaz à effet de serre anthropique majoritaire (75%) mais il y en a d'autres comme le méthane (18%), le protoxyde d'azote (4%) et les gaz fluorés (2%). L'ozone est également un gaz à effet de serre mais il n'est pas produit directement par l'homme, ce sont des précurseurs qui sont produits. Tous ces gaz sont réchauffant. Le ""E"" et le ""S"" de la carte 9 sont à mettre en lien avec ""Effet de Serre"" de la carte 13.  
_SOURCES: AR6 WG3 TS.3 p11 (p59) // AR6 WG3 Figure TS.2 (a) : émissions p11 (p59) // AR6 WG1 Figure TS.9 : concentrations et forçage dans le temps p36 (p68)_
