---
fromCardId: '9'
toCardId: '13'
status: valid
---
El CO2 es el gas de efecto invernadero antropogénico mayoritario (75%) pero hay otros gases como el metano (18%), el óxido nitroso (4%) y los gases fluorados (2%). El ozono también es un gas de efecto invernadero no se emite directamente a la atmósfera, sino que se forma a partir de ciertos precursores (compuestos orgánicos volátiles no metánicos (COVNM), monóxido de carbono (CO), óxidos de nitrógeno (NOx), y en menor medida, metano (CH4)) que tienen su origen en los procesos de combustión (tráfico e industria). Todos estos gases son calentadores. La ""E"" y la ""I"" de la carta 9 están relacionadas con el ""Efecto Invernadero"" de la carta 13.  

