---
fromCardId: '33'
toCardId: '39'
status: valid
---
I principali fattori climatici che inducono migrazioni climatiche sono le siccità, le tempeste tropicali, i cicloni, le forti precipitazioni e le inondazioni.  
_FONTI: AR6 WG2 TS.B.6.1 p64 (p52) // AR6 WG2 Figura 7.7 p1096 (p1084)_
