---
fromCardId: '33'
toCardId: '39'
status: valid
---
Les principaux facteurs climatiques qui induisent des migrations climatiques sont les sécheresses, les tempêtes tropicales, les cyclones, les fortes précipitations et les inondations.  
_SOURCES: AR6 WG2 TS.B.6.1 p64 (p52) // AR6 WG2 Figure 7.7 p1096 (p1084)_
