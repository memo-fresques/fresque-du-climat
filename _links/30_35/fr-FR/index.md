---
fromCardId: '30'
toCardId: '35'
status: valid
---
La hausse de température et les sécheresses sont des conditions favorables aux incendies (wildfire). Avec le changement climatique, la saison des feux est plus longue et de plus haute magnitude comme par exemple en Californie, dans le bassin méditérannéen, au Canada...  
_SOURCES: AR6 WG2 2.4.4.2 : observed changes in Wildfire p255 (p243) // AR6 WG2 FAQ 2.3 p24_
