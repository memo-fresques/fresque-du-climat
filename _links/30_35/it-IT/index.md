---
fromCardId: '30'
toCardId: '35'
status: valid
---
L'aumento della temperatura e la siccità sono condizioni favorevoli agli incendi (wildfires). Con il cambiamento climatico, la stagione degli incendi è più lunga e più intensa, ad esempio in California, nel bacino del Mediterraneo, in Canada...  
_FONTI: AR6 WG2 2.4.4.2 : observed changes in Wildfire (cambiamenti osservati negli incendi) p255 (p243) // AR6 WG2 FAQ 2.3 p24_
