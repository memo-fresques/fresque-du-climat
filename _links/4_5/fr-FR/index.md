---
fromCardId: '4'
toCardId: '5'
status: valid
---
Les transports sont particulièrement dépendants du pétrole, ils utilisent en grande majorité des ressources fossiles.  
_SOURCES: AR6 WG3 Figure 6.1 p630 (p617)_
