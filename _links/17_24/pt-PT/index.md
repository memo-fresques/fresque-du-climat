---
fromCardId: '17'
toCardId: '24'
status: invalid
---
O aumento da temperatura da água não tem uma ligação direta com a Acidificação do oceano.
Mas ao aquecer, o oceano perde a sua capacidade de dissolver o CO2 atmosférico. Por outras palavras, o aumento da temperatura limita a capacidade do sumidouro de carbono oceânico e, por conseguinte, limita a acidificação do oceano. Em última análise, é mais lógico fazer uma ligação entre o aumento da temperatura da água e os sumidouros de carbono.
_FONTES: AR6 WG1 FAQ 3.3 p15 (p131)_
