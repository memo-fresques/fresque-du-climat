---
fromCardId: '17'
toCardId: '24'
status: invalid
---
La hausse de la température de l'eau n'a pas de lien direct avec l'acidification de l'océan.
Mais en se réchauffant, l'océan perd sa capacité à dissoudre le CO2 atmosphérique. Autrement dit, l'augmentation de température limite la capacité du puit de carbone océan et par là même limite l'acidification de l'océan. A la limite, il est plus logique de faire un lien entre la hausse de température de l'eau et les puits de carbone.  
_SOURCES: AR6 WG1 FAQ 3.3 p15 (p131)_
