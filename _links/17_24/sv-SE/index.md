---
fromCardId: '17'
toCardId: '24'
status: invalid
---
Ökningen av vattentemperaturen har ingen direkt koppling till havsförsurning.
Men när havet värms upp förlorar det sin förmåga att lösa upp atmosfäriskt CO2. Med andra ord begränsar temperaturökningen havets koldioxidupptagningsförmåga och därmed begränsar den också havsförsurningen. I själva verket är det mer logiskt att koppla ökningen av vattentemperaturen till koldioxidupptagningen (kolsänka).
_KÄLLOR: AR6 WG1 FAQ 3.3 s15 (s131)_
