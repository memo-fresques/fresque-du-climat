---
fromCardId: '17'
toCardId: '24'
status: invalid
---

The increase of water temperature is not linked to ocean acidification, at least for the time being. In the long term (over several centuries), as it heats up, the water will lose its capacity to dissolve atmospheric CO2 and will become less efficient as a carbon sink. So the increase in water temperature will inhibit, to some extent, ocean acidification.
