---
fromCardId: '17'
toCardId: '24'
status: invalid
---
L'aumento della temperatura dell'acqua non ha un legame diretto con l'acidificazione dell'oceano. Tuttavia, riscaldandosi, l'oceano perde la sua capacità di dissolvere la CO2 atmosferica. In altre parole, l'aumento della temperatura limita la capacità del pozzo di carbonio oceanico e quindi limita l'acidificazione dell'oceano. In ultima analisi, ha più senso stabilire un legame tra l'aumento della temperatura dell'acqua e i pozzi di carbonio. 
_FONTE: AR6 WG1 FAQ 3.3 p15 (p131)_
