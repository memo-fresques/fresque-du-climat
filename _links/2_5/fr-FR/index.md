---
fromCardId: '2'
toCardId: '5'
status: valid
---
L'industrie utilise beaucoup d'énergies fossiles notamment du gaz naturel et du charbon.  
_SOURCES: AR6 WG3 Figure 6.1 p630 (p617) https://www.weforum.org/agenda/2021/07/us-fossil-fuel-consumption-eia/_
