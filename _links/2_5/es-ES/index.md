---
fromCardId: '2'
toCardId: '5'
status: valid
---
La industria utiliza muchos combustibles fósiles, en particular gas natural y carbón.  
_SOURCES: AR6 WG3 Figura 6.1 p630 (p617) https://www.weforum.org/agenda/2021/07/us-fossil-fuel-consumption-eia/_
