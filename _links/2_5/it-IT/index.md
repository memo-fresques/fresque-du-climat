---
fromCardId: '2'
toCardId: '5'
status: valid
---
L'industria utilizza molti combustibili fossili, in particolare gas naturale e carbone.  
_FONTI: AR6 WG3 Figura 6.1 p630 (p617) https://www.weforum.org/agenda/2021/07/us-fossil-fuel-consumption-eia/_
