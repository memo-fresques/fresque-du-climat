---
fromCardId: '1'
toCardId: '27'
status: optional
---
Esta relación representa todo el impacto que la especie humana inflige sobre a la vida en el planeta tierra, como la destrucción de los habitats (por la deforestación). Es un tema aparte del cambio climático, pero es interesante hacer la conexión y la oportunidad de mencionar la "Fresque Océane". Si hablamos con un público mayor, podemos mencionar el tamaño de las latas de sardinas que ha disminuido porque ya no tienen tiempo de crecer antes de ser pescadas. El fenómeno se agrava por el calentamiento del agua que reduce el aporte de plancton.
  
_SOURCES: IPBES SPM.11 p34 // IPBES Figura SPM.2 p31_
