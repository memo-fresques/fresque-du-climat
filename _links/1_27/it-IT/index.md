---
fromCardId: '1'
toCardId: '27'
status: optional
---
Questo collegamento evidenzia tutte le degradazioni che l'umanità è in grado di infliggere alla vita marina, come ad esempio la distruzione degli habitat. Questo è fuori tema rispetto al cambiamento climatico, ma è comunque interessante fare il collegamento e l'occasione per citare la Fresque Océane/Ocean Collage (per chi parlasse inglese o francese). Se si parla a un pubblico anziano, si può ad esempio parlare della riduzione delle dimensioni delle scatolette di sardine perché non hanno più il tempo di crescere prima di essere pescate. Il fenomeno è aggravato dal riscaldamento dell'acqua che riduce l'apporto di plancton.
_FONTI: IPBES SPM.11 p34 // IPBES Figura SPM.2 p31_
