---
fromCardId: '1'
toCardId: '27'
status: optional
---
Med den här länken lyfter vi fram all negativ påverkan som människor tillfogar det marina livet, såsom plastföroreningar och överfiske. Det är irrelevant för klimatförändringarna, men det är intressant att göra kopplingen ändå.
