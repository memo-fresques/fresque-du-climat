---
fromCardId: '1'
toCardId: '27'
status: optional
---
Ved denne koblingen uttrykkes alle skadene som mennesket er i stand til å påføre livet på jorden, som for eksempel ødeleggelse av habitater. Det har ikke nødvendigvis noe med klimaendringer å gjøre, men det er interessant å gjøre koblingen likevel. Hvis man snakker til et godt voksent publikum, kan man nevne at størrelsen på sardinbokser minker fordi fisken ikke har tid til å vokse før den blir fanget. Fenomenet forverres av oppvarmingen av havet, som reduserer tilgangen på plankton.
