---
fromCardId: '1'
toCardId: '27'
status: optional
---
Com esta ligação, ilustramos todas as degradações que o Homem é capaz de infligir à vida terrestre, como a destruição de habitats (Desflorestação), por exemplo. Está fora do contexto das alterações climáticas, mas é interessante fazer a ligação na mesma e aproveitar a oportunidade para mencionar O Mural do Oceano. Se estivermos a falar com um público mais velho, podemos, por exemplo, falar sobre o tamanho das latas de sardinhas que diminuiu porque elas já não têm tempo de crescer antes de serem pescadas. O fenómeno é agravado pelo aquecimento da água, que reduz o fornecimento de plâncton.
  
_FONTES: IPBES SPM.11 p34 // IPBES Figura SPM.2 p31_
