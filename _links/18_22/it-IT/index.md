---
fromCardId: '18'
toCardId: '22'
status: invalid
---
È la trappola che viene tesa ai partecipanti nel primo set di carte. 
Per questo, non li incoraggiare subito a leggere il retro delle carte, ma aspetta che le abbiano posizionate prima di proporre loro di farlo. 
_FONTI: https://it.wikipedia.org/wiki/Principio_di_Archimede_
