---
fromCardId: '18'
toCardId: '22'
status: invalid
---

This it the trap in which participants can fall while playing with cards set n1. Therefore, do not ask them to read the cards from the beginning. Once they have put them in place, ask them to read the texts.
