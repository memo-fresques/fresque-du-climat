---
fromCardId: '18'
toCardId: '22'
status: invalid
---
Detta är fällan som är riggad för deltagarna i set 1.
För detta, uppmuntra dem inte att läsa baksidan av korten direkt, utan vänta tills de har placerat dem innan du föreslår att de läser dem. 
_KÄLLOR: https://fr.wikipedia.org/wiki/Pouss%C3%A9e_d%27Archim%C3%A8de_
