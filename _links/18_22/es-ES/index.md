---
fromCardId: '18'
toCardId: '22'
status: invalid
---
Es la trampa que se tiende a los participantes en el lote N°1. 
Para ello, no les incites de inmediato a leer el reverso de las cartas, sino espera a que las hayan colocado antes de proponerles que las lean.  
_FUENTES: https://es.wikipedia.org/wiki/Principio_de_Arqu%C3%ADmedes_
