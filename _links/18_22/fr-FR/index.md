---
fromCardId: '18'
toCardId: '22'
status: invalid
---
C'est le piège qui est tendu aux participants dans le lot N°1. 
Pour cela, ne les incitez pas tout de suite à lire de verso des cartes, mais attendez qu'ils les aient placées avant de leur proposer de les lire.  
_SOURCES: https://fr.wikipedia.org/wiki/Pouss%C3%A9e_d%27Archim%C3%A8de_
