---
fromCardId: '21'
toCardId: '32'
status: valid
---
Outre son effet sur les sécheresse, une augmentation de la temperature peut directement diminuer les rendements agricoles.  
_SOURCES: AR6 WG2 TS.B 3.1 p14 (p48) // AR6 WG1 FAQ 12.2 p69_
