---
fromCardId: '21'
toCardId: '32'
status: valid
---
Oltre al suo effetto sulla siccità, un aumento della temperatura può direttamente ridurre le rese agricole. 
_FONTI: AR6 WG2 TS.B 3.1 p14 (p48) // AR6 WG1 FAQ 12.2 p69_
