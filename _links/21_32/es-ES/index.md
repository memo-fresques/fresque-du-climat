---
fromCardId: '21'
toCardId: '32'
status: valid
---
Además de su efecto sobre las sequías, un aumento de la temperatura puede disminuir directamente los rendimientos agrícolas.  
_FUENTES: AR6 WG2 TS.B 3.1 p14 (p48) // AR6 WG1 FAQ 12.2 p69_
