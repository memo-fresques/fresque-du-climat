---
fromCardId: '21'
toCardId: '32'
status: valid
---
I nordliga länder kan en liten temperaturökning leda till bättre skördar, men i sydliga länder är det tvärtom: varje grad varmare ger en minskning av skörden.
