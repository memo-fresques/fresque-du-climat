---
fromCardId: '21'
toCardId: '32'
status: valid
---

In northern countries, a little increase in temperature can lead to better yields, but in southern countries, it's the contrary: Any degree warmer is a decrease in yields.
