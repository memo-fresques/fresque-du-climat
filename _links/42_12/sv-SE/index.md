---
fromCardId: '42'
toCardId: '12'
status: valid
---
CO2 absorberas vid havets yta. Den termohalina cirkulationen hjälper till att blanda vatten nära ytan och i djuphavet, vilket är nödvändigt för att ta upp CO2 i havet.
