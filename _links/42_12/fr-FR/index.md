---
fromCardId: '42'
toCardId: '12'
status: valid
---
L'AMOC est un courant océanique qui plonge dans l'arctique du fait de sa forte densité, elle-même dûe à son refroidissement et à l'augmentation de la salinité. Ainsi il transporte la chaleur et le CO2 vers les couches profondes de l'Océan. Son ralentissement entraîne une perte de capacité à stocker le CO2 et la chaleur.   
_SOURCES: AR6 WG1 3.5.4.1 p500 // AR6 WG1 Cross-Chapter Box 5.3 p762_
