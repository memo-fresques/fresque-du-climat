---
fromCardId: '12'
toCardId: '24'
status: valid
---
On peut lier ces deux cartes, la partie de nos émissions de CO2 qui va dans l'océan l'acidifie. On peut aussi lier directement la carte "émissions de CO2".  
