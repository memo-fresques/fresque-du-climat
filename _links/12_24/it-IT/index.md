---
fromCardId: '12'
toCardId: '24'
status: valid
---
È possibile collegare queste due carte, la parte delle nostre emissioni di CO2 che finisce nell'oceano lo acidifica. È anche possibile collegare direttamente la carta delle "emissioni di CO2".
