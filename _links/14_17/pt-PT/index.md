---
fromCardId: '14'
toCardId: '17'
status: valid
---
O efeito de estufa adicional provoca um forçamento radiativo positivo. O excesso de energia é distribuído no oceano (93%), na vegetação (5%), no gelo (3%) e na atmosfera (1%). Esta energia aquece os diferentes compartimentos terrestres nos quais é distribuída, aumenta a sua temperatura e, no caso do gelo, derrete-o. A temperatura média de superfície dos oceanos (SST) aumentou 0.88°C desde 1900 contra 1.59°C na Terra.  
_FONTES: AR6 WG1 TS3.1 (balanço energético) p59 (p93) // AR6 WG1 Cross-Section Box TS.1 (oceano + terra) p29 (p60) // AR6 WG1 Cross-Section Box TS.1, Figura 1 (c) : temperatura p29 (p61) // AR6 WG1 Figura 2.11 (c) p333 (p316)_
