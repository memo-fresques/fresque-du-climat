---
fromCardId: '14'
toCardId: '17'
status: valid
---
L'effetto serra addizionale provoca un forzante radiativo positivo. L'energia in eccesso si distribuisce nell'oceano (93%), nella vegetazione (5%), nel ghiaccio (3%) e nell'atmosfera (1%). Questa energia riscalda le diverse componenti terrestri in cui si distribuisce, aumenta la loro temperatura e, nel caso dei ghiacci, li fa sciogliere. La temperatura media della superficie degli oceani (SST) è aumentata di 0,88°C dal 1900, rispetto a 1,59°C sulla terraferma. 
_FONTI: AR6 WG1 TS3.1 (bilancio energetico) p59 (p93) // AR6 WG1 Cross-Section Box TS.1 (oceano + terra) p29 (p60) // AR6 WG1 Cross-Section Box TS.1, Figura 1 (c): temperatura p29 (p61) // AR6 WG1 Figura 2.11 (c) p333 (p316)_
