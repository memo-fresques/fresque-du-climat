---
fromCardId: '14'
toCardId: '17'
status: valid
---
Den ekstra drivhuseffekten forårsaker en positiv strålingspådriv. Overskuddsenergien fordeles i havet (93%), vegetasjonen (5%), isen (3%) og atmosfæren (1%). Denne energien varmer opp de stedene den fordeles i, øker temperaturen deres, og i isens tilfelle, får den til å smelte. Den gjennomsnittlige overflatetemperaturen i havene (SST) har økt med 0,88°C siden 1900 og med 1,59°C over land.  
_KILDER: AR6 WG1 TS3.1 (energibalanse) s59 (s93) // AR6 WG1 Tverrsnittsboks TS.1 (hav + land) s29 (s60) // AR6 WG1 Tverrsnittsboks TS.1, Figur 1 (c) : temperatur s29 (s61) // AR6 WG1 Figur 2.11 (c) s333 (s316)_
