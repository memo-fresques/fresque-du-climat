---
fromCardId: '14'
toCardId: '17'
status: valid
---
L'effet de serre additionnel provoque un forçage radiatif positif. Le surplus d'énergie se répartie dans l'océan (93%), la végétation (5%), la glace (3%) et l'atmosphère (1%). Cette énergie chauffe les différents compartiments terrestres dans lesquels elle se répartie, augmente leur température, et, dans le cas glaces, les fait fondre. La température moyenne de surface des océans (SST) au augmenté de 0.88°C depuis 1900 contre 1.59°C au dessus des terres.  
_SOURCES: AR6 WG1 TS3.1 (bilan énergétique) p59 (p93) // AR6 WG1 Cross-Section Box TS.1 (ocean + land) p29 (p60) // AR6 WG1 Cross-Section Box TS.1, Figure 1 (c) : température p29 (p61) // AR6 WG1 Figure 2.11 (c) p333 (p316)_
