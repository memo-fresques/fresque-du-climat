---
fromCardId: '12'
toCardId: '11'
status: valid
---
È possibile collegare queste due carte, la parte delle nostre emissioni di CO2 che va nell'atmosfera aumenta la concentrazione di CO2 nell'atmosfera. È anche possibile collegare direttamente la carta delle "emissioni di CO2". 
_FONTI: AR6 WG1 Box TS.5 p48 // AR5 WG1 Figura 6.8 (pozzi di carbonio) p503 // AR6 WG1 Figura SPM.7 (pozzi e scenari) p20_
