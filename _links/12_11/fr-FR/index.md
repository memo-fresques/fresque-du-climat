---
fromCardId: '12'
toCardId: '11'
status: valid
---
On peut lier ces deux cartes, la partie de nos émissions de CO2 qui va dans l'atmosphère augmente la concentration de CO2 dans l'atmosphère. On peut aussi lier directement la carte "émissions de CO2".  
_SOURCES: AR6 WG1 Box TS.5 p48 // AR5 WG1 Figure 6.8 (puits de carbone) p503 // AR6 WG1 Figure SPM.7 (puits et scénarios) p20_
