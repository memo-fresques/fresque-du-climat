---
fromCardId: '12'
toCardId: '11'
status: valid
---
Podemos ligar estas duas cartas, a parte das nossas emissões de CO2 que vai para a atmosfera aumenta a concentração de CO2 na atmosfera. Podemos também ligar diretamente a carta "emissões de CO2".  
_FONTES: AR6 WG1 Box TS.5 p48 // AR5 WG1 Figura 6.8 (sumidouros de carbono) p503 // AR6 WG1 Figura SPM.7 (sumidouros e cenários) p20_
