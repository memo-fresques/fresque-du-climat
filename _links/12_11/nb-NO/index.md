---
fromCardId: '12'
toCardId: '11'
status: valid
---
Disse to kortene kan kobles: delen av våre CO2-utslipp som går i atmosfæren øker konsentrasjonen av CO2 i atmosfæren. Det kan også gå en kobling til kortet "CO2-utslipp". 
_KILDER: AR6 WG1 Box TS.5 s48 // AR5 WG1 Figur 6.8 (karbonsluk) s503 // AR6 WG1 Figur SPM.7 (sluk og scenarier) s20_
