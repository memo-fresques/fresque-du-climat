---
fromCardId: '36'
toCardId: '30'
status: optional
---
Un’ondata di calore è un problema di temperatura, mentre una siccità è un problema di umidità. Detto questo, se fa caldo l'acqua evapora, cosa che può portare alla siccità. Le due cose sono spesso collegate.  
_FONTI: AR6 WG1 Box TS.6 p54 // AR6 WG1 FAQ 8.3 p50_
