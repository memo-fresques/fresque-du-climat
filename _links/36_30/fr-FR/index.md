---
fromCardId: '36'
toCardId: '30'
status: optional
---
Une canicule, c'est la température, une sécheresse, c'est l'humidité. 
Ceci étant dit, si il fait chaud, l'eau s'évapore et cela peut entraîner une sécheresse. Les deux vont de fait souvent ensemble.  
_SOURCES: AR6 WG1 Box TS.6 p54 // AR6 WG1 FAQ 8.3 p50_
