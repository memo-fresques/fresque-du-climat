---
fromCardId: '37'
toCardId: '39'
status: valid
---
La baisse des rendements agricole ou les perte de récoltes liée à des conditions extrêmes (sécheresse, cyclone, submersions) pose des problèmes de sécurité alimentaire (voir de famine) et peuvent être la cause de migrations.  
_SOURCES: AR6 WG2 7.2.6 p1091 (p1079) // AR6 WG2 Figure 7.7 p1096 (p1084)_
