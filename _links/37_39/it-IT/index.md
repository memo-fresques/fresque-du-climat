---
fromCardId: '37'
toCardId: '39'
status: valid
---
Il calo delle rese agricole o la perdita di raccolti legata a condizioni estreme (siccità, cicloni, inondazioni) pongono problemi di sicurezza alimentare (fino alla carestia) e possono essere causa di migrazioni.  
_FONTI: AR6 WG2 7.2.6 p1091 (p1079) // AR6 WG2 Figura 7.7 p1096 (p1084)_
