---
fromCardId: '20'
toCardId: '22'
status: invalid
---
No, più pioggia non farà traboccare l'oceano! È abbastanza raro vedere questo collegamento, ma non si sa mai. Se capitasse, chiedi ai partecipanti da dove viene la pioggia o da dove vengono le nuvole. 
_FONTI: AR6 WG2 Figura 4.2: schema del ciclo dell'acqua p577 (p565)_
