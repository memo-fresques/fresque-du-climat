---
fromCardId: '20'
toCardId: '22'
status: invalid
---
Eh non, plus de pluie ne va pas faire déborder l'océan ! Il est assez rare de voir ce lien, mais on ne sait jamais. Si c'est le cas, demandez aux particpants d'où vient la pluie, ou encore d'où viennent les nuages.  
_SOURCES: AR6 WG2 Figure 4.2 : schéma cycle de l'eau p577 (p565)_
