---
fromCardId: '20'
toCardId: '22'
status: invalid
---
¡Pues no, más lluvia no va a hacer que el océano se desborde! Es bastante raro ver este enlace, pero nunca se sabe. Si es el caso, pregunte a los participantes de dónde viene la lluvia, o de dónde vienen las nubes.  
_FUENTES: AR6 WG2 Figura 4.2: esquema ciclo del agua p577 (p565)_
