---
fromCardId: '40'
toCardId: '38'
status: valid
---

Les conflits armés dégradent les infrastructures et réduisent l'accès aux biens de premières nécessité (nourriture, eau, ...)
