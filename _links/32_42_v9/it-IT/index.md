---
fromCardId: '32'
toCardId: 42_v9
status: valid
---
Il calo delle rese agricole può avere implicazioni importanti sulle disuguaglianze, influenzando non solo i redditi degli agricoltori, ma anche l'accesso al cibo, all'occupazione e alle risorse. 

**Fonti**: _AR6 WG2 FAQ 8.1 p1263 (p1251) // AR6 WG2 8.2.1.2 p1190 (pp1178)
