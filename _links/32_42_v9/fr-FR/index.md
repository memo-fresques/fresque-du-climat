---
fromCardId: '32'
toCardId: 42_v9
status: valid
---
La baisse des rendements agricoles peut avoir des implications importantes sur les inégalités, affectant non seulement les revenus des agriculteurs, mais aussi l'accès à la nourriture, à l'emploi et aux ressources. 

**Source** : _AR6 WG2 FAQ 8.1 p1263 (p1251) // AR6 WG2 8.2.1.2 p1190 (pp1178)
