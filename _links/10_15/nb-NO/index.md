---
fromCardId: '10'
toCardId: '15'
status: valid
---
Aerosoler har to effekter. Noen reflekterer sollys direkte, slik som sulfataerosoler. Disse har derfor en avkjølende effekt. Noen kan derimot ha en oppvarmende effekt, som for eksempel sot. Når sot legger seg på snø, reduseres albedoen. Aerosoler kan også ha en effekt på skyer, både når det gjelder sammensetning og mengde. Totalt sett har aerosoler en sterk avkjølende effekt med en strålingspådriv på -1,3W/m².  
_KILDER: AR6 WG1 TS.3.1 s61 (s93) // AR6 WG1 6.2.1 s844 // AR6 WG1 Figur TS.15: Strålingspådriv s60 (s92)_
