---
fromCardId: '10'
toCardId: '15'
status: valid
---
Gli aerosol hanno due effetti. Alcuni sono in grado di riflettere direttamente la luce solare come nel caso degli aerosol di solfati. Hanno quindi in questo caso un effetto raffreddante. Alcuni possono invece avere un effetto riscaldante come il particolato che, quando si deposita sulla neve, ne riduce l'albedo. Gli aerosol possono anche avere un effetto sulle nuvole, in termini di composizione e quantità. Nel complesso, gli aerosol hanno una forte capacità raffreddante con un forzante radiativo di -1,3 W/m². 
_FONTI: AR6 WG1 TS.3.1 p61 (p93) // AR6 WG1 6.2.1 p844 // AR6 WG1 Figura TS.15: Radiative forcing (forzante radiativo) p60 (p92)_
