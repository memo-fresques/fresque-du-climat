---
fromCardId: '10'
toCardId: '15'
status: valid
---
Les aérosols ont deux effets. Certains sont capables de réfléchir directement la lumière du soleil comme c'est le cas des aérosols de sulfates. Ils ont donc dans ce cas un effet refroidissant. Certains peuvent à l'inverse avoir un effet réchauffant comme la suie qui, lorsqu'elle se dépose sur la neige diminue son albedo. Les aerosols peuvent aussi avoir un effet sur les nuages, en terme de composition et de quantité. Au total, les aérosols ont un fort pouvoir refroidissant avec un forçage radiatif de -1.3W/m².  
_SOURCES: AR6 WG1 TS.3.1 p61 (p93) // AR6 WG1 6.2.1 p844 // AR6 WG1 Figure TS.15 : Forçage radiatif p60 (p92)_
