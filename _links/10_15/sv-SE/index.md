---
fromCardId: '10'
toCardId: '15'
status: valid
---
Aerosoler har två effekter. Vissa kan direkt reflektera solljus, vilket är fallet med sulfataerosoler. I det här fallet har de därför en kylande effekt. Vissa kan omvänt ha en värmande effekt, som sot, som vid avsättning på snö minskar snöns albedo. Aerosoler kan också ha en effekt på moln, vad gäller sammansättning och kvantitet. Sammantaget har aerosoler en stark kyleffekt med en strålningskraft på -1,3 W/m².  
_KÄLLOR: AR6 WG1 TS.3.1 p61 (p93) // AR6 WG1 6.2.1 p844 // AR6 WG1 Figur TS.15: Radiative force p60 (p92)_
