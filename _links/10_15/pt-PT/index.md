---
fromCardId: '10'
toCardId: '15'
status: valid
---
Os aerossóis têm dois efeitos. Alguns são capazes de refletir diretamente a luz do sol, como é o caso dos aerossóis de sulfatos. Têm, portanto, um efeito de arrefecimento. Alguns podem, pelo contrário, ter um efeito de aquecimento, como a fuligem que, quando se deposita na neve, diminui o seu albedo. Os aerossóis também podem ter um efeito sobre as nuvens, em termos de composição e quantidade. No total, os aerossóis têm um forte poder de arrefecimento com um forçamento radiativo de -1.3W/m².  
_FONTE: AR6 WG1 TS.3.1 p61 (p93) // AR6 WG1 6.2.1 p844 // AR6 WG1 Figura TS.15: Forçamento radiativo p60 (p92)_
