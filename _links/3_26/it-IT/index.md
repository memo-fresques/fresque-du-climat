---
fromCardId: '3'
toCardId: '26'
status: optional
---
L'artificializzazione del suolo è anche responsabile delle inondazioni perché il suolo non è più drenante.  
_FONTI: AR6 WGI FAQ 8.1 p46_
