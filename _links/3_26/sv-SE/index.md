---
fromCardId: '3'
toCardId: '26'
status: optional
---
Markförstöring är också ansvarig för översvämningar eftersom marken inte längre dräneras.
_KÄLLOR: AR6 WGI FAQ 8.1 s46_
