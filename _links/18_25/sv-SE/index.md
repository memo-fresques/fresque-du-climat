---
fromCardId: '18'
toCardId: '25'
status: optional
---
Den stackars isbjörnen är en kraftfull symbol för klimatet. Om spelare gör denna koppling, föreslå att de ritar en björn!
