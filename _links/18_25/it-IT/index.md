---
fromCardId: '18'
toCardId: '25'
status: optional
---
La fusione della banchisa ha conseguenze sulla biodiversità che ne dipende, sia terrestre (orsi, pinguini, foche...) che marina. Gioca anche un ruolo nella conservazione di nutrienti come il ferro e nel passaggio della luce, regolando così la crescita stagionale del fitoplancton, alla base della catena alimentare di questo ecosistema. Anche l'ecosistema terrestre ne è influenzato, ad esempio con una maggiore crescita delle piante negli anni in cui la banchisa è ridotta. 
_FONTI: IPBES 2022 4.2.2.2.4 p704 // Banchisa: un ecosistema ricco
https://www.science.org/doi/10.1126/science.1235225_
