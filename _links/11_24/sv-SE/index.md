---
fromCardId: '11'
toCardId: '24'
status: invalid
---
Spelare identifierar ofta CO2-koncentrationen som en orsak till havsförsurning. Men det är mer logiskt att länka tillbaka till kolsänkor som orsaken.
