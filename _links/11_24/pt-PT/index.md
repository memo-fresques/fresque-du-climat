---
fromCardId: '11'
toCardId: '24'
status: invalid
---
Quanto maior for a concentração de CO2 atmosférico, mais os sumidouros de carbono podem absorver (mesmo que a proporção diminua), portanto, mais o oceano pode absorver CO2 e acidificar-se. Mas é mais lógico ligar a carta de Sumidouros de carbono ou das Emissões de CO2 à da Acidificação do oceano.
_FONTES: AR6 WG1 Caixa TS.5 p48 // AR5 WG1 Figura 6.8 (sumidouros de carbono) p503 // AR6 WG1 Figura SPM.7 (sumidouros e cenários) p20_
