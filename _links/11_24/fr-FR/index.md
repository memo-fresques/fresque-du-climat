---
fromCardId: '11'
toCardId: '24'
status: invalid
---
Plus la concentration en CO2 atmosphérique est grande et plus les puits de carbone peuvent en absorber (même si la proportion diminue), donc plus l'océan peut absorber du CO2 et s'acidifier. Mais il est plus logique de relier la carte puit de carbone ou emissions de CO2 à l'acidification de l'océan.  
_SOURCES: AR6 WG1 Box TS.5 p48 // AR5 WG1 Figure 6.8 (puits de carbone) p503 // AR6 WG1 Figure SPM.7 (puits et scénarios) p20_
