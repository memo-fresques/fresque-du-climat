---
fromCardId: '11'
toCardId: '24'
status: invalid
---

Players often identify CO2 concentration as a cause for ocean acidification. But it is more logical to link back to carbon sinks.
