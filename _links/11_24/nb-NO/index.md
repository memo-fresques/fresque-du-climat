---
fromCardId: '11'
toCardId: '24'
status: invalid
---
Jo høyere konsentrasjonen av atmosfærisk CO2 er, desto mer kan karbonlagrene absorbere (selv om andelen avtar), og jo mer CO2 kan havet absorbere og forsures. Men det er mer logisk å knytte kortet "karbonsluk" eller "CO2-utslipp" (kort 12 og 7) til "havforsuring" (kort 24).  
_KILDER: AR6 WG1 Box TS.5 s48 // AR5 WG1 Figur 6.8 (karbonlagre) s503 // AR6 WG1 Figur SPM.7 (lagre og scenarier) s20_
