---
fromCardId: '11'
toCardId: '24'
status: invalid
---
Maggiore è la concentrazione di CO2 nell'atmosfera, maggiore è la capacità dei pozzi di carbonio di assorbirla (anche se la proporzione diminuisce), quindi maggiore è la capacità dell'oceano di assorbire CO2 e acidificarsi. Tuttavia, ha più senso collegare la carta dei pozzi di carbonio o delle emissioni di CO2 all'acidificazione dell'oceano. 
_FONTI: AR6 WG1 Box TS.5 p48 // AR5 WG1 Figura 6.8 (pozzi di carbonio) p503 // AR6 WG1 Figura SPM.7 (pozzi e scenari) p20_
