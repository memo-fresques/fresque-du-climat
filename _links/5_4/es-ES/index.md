---
fromCardId: '5'
toCardId: '4'
status: optional
---
Las energías fósiles permiten que las actividades humanas existan tal como las conocemos actualmente. En general en el mural, se coloca la carta de los combustibles fósiles después de las actividades humanas. Además, la carta muestra las emisiones de CO2 relacionadas con el uso de los combustibles fósiles.
