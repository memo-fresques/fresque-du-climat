---
fromCardId: '34'
toCardId: '33'
status: valid
---
I cicloni possono provocare sommersioni. L'Asia è il continente più esposto a questo rischio, con un milione di persone esposte senza possibilità di adattamento entro la fine del secolo, secondo l'SSP 8.5.  
_FONTI: AR6 WG2 10.4.6.3.4 p1511 (p1499)_
