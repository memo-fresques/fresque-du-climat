---
fromCardId: '34'
toCardId: '33'
status: valid
---
Les cyclones peuvent provoquer des submersions. L'Asie est le continent le plus exposé à ce risque. En Asie, on estime qu'un millions de personnes y seront exposées sans adaptation d'ici la fin du siècle dans le RCP8.5.  
_SOURCES: AR6 WG2 10.4.6.3.4 p1511 (p1499)_
