---
fromCardId: 42_v9
toCardId: '38'
status: valid
---
Les inégalités limitent l'accès à la nourriture, aux soins, à l'éducation à l'emploi et au logement. 

**Sources** : _AR6 WG2 FAQ 8.1 p1263 (p1251) // AR6 WG2 8.2.1.2 p1190 (pp1178)_
