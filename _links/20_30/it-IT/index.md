---
fromCardId: '20'
toCardId: '30'
status: valid
---
La mancanza di pioggia e l'evaporazione sono le cause delle siccità. La perturbazione del ciclo dell'acqua è responsabile di entrambe. 
_FONTI: AR6 WG1 Box TS.6 p54 // AR6 WG1 FAQ 8.3 p50 // AR6 WG2 Figura 4.2: schema del ciclo dell'acqua p577 (p565)_
