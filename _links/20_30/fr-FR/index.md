---
fromCardId: '20'
toCardId: '30'
status: valid
---
Le manque de pluie et l'évaporation sont les causes des sécheresses. Dans les deux cas, c'est la perturbation du cycle de l'eau qui est en cause.  
_SOURCES: AR6 WG1 Box TS.6 p54 // AR6 WG1 FAQ 8.3 p50 // AR6 WG2 Figure 4.2 : schéma cycle de l'eau p577 (p565)_
