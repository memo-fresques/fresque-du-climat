---
fromCardId: '31'
toCardId: 42_v9
status: valid
---
Les ressources en eau douce jouent un rôle crucial dans le développement économique et social, et les disparités dans l'accès et la gestion de ces ressources peuvent exacerber les inégalités existantes. Une gestion durable et équitable de l'eau est donc essentielle pour promouvoir un développement équilibré et inclusif. 

**Sources :** _AR6 WG2 FAQ 8.1 p1263 (p1251) // AR6 WG2 8.2.1.2 p1190 (pp1178)_
