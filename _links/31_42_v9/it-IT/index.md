---
fromCardId: '31'
toCardId: 42_v9
status: valid
---
Le risorse di acqua dolce svolgono un ruolo cruciale nello sviluppo economico e sociale, e le disparità nell'accesso e nella gestione di queste risorse possono esacerbare le disuguaglianze esistenti. Una gestione sostenibile ed equa dell'acqua è quindi essenziale per promuovere uno sviluppo equilibrato e inclusivo. 

**Fonti :** _AR6 WG2 FAQ 8.1 p1263 (p1251) // AR6 WG2 8.2.1.2 p1190 (pp1178)_
