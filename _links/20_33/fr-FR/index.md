---
fromCardId: '20'
toCardId: '33'
status: valid
---

Dans les phénomènes de submersion, la baisse de pression atmosphérique joue un rôle important. Ces dépressions sont une manifestation de la perturbation du cycle de l'eau.
