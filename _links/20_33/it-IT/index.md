---
fromCardId: '20'
toCardId: '33'
status: valid
---
Nei fenomeni di sommersione, la diminuzione della pressione atmosferica gioca un ruolo importante. Queste depressioni sono una manifestazione della perturbazione del ciclo dell'acqua.
