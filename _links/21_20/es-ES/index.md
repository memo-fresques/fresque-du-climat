---
fromCardId: '21'
toCardId: '20'
status: valid
---
“El aumento de la temperatura en la Tierra aumenta la evaporación y altera el ciclo del agua. En el pasado hablábamos de calentamiento global y hoy hablamos de cambio climático (Climate Change), o incluso de cambio climático. 
Este cambio de vocabulario se materializa en esta relación que, por lo tanto, tiene una importancia fundamental. No dudes en hacer un comentario sobre este punto a los equipos.  
_FUENTES: AR6 WG2 4.1.2 p576 (p564)_"
