---
fromCardId: '21'
toCardId: '20'
status: valid
---
"La hausse de la température sur terre augmente l'évaporation et perturbe le cycle de l'eau. D'ailleur, autrefois, on parlait de réchauffement climatique (""Global Warming"") et aujourd'hui, on parle de changement climatique (Climate Change), voire de dérèglement climatique. 
Ce shift dans le vocabulaire est matérialisé par cette relation qui a donc une importance fondamentale. N'hésitez pas à faire une remarque sur ce point aux équipes.  
_SOURCES: AR6 WG2 4.1.2 p576 (p564)_"
