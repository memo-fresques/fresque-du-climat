---
fromCardId: '21'
toCardId: '20'
status: valid
---
L'aumento della temperatura del suolo aumenta l'evaporazione e perturba il ciclo dell'acqua. In passato si parlava di riscaldamento globale ("Global Warming") e oggi si parla di cambiamento climatico (Climate Change) o di alterazione del clima. Questo cambiamento nel vocabolario è evidenziato da questa relazione che ha quindi un'importanza fondamentale. Non esitare a fare un commento su questo punto ai gruppi. 
_FONTE: AR6 WG2 4.1.2 p576 (p564)_
