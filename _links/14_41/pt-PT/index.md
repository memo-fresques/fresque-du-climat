---
fromCardId: '14'
toCardId: '41'
status: optional
---
O efeito de estufa adicional provoca um forçamento radiativo positivo. O excesso de energia é distribuído no oceano (93%), na vegetação (5%), no gelo (3%) e na atmosfera (1%). Esta energia aquece os diferentes compartimentos terrestres nos quais é distribuída, aumenta a sua temperatura e, no caso do gelo, derrete-o. A temperatura do Permafrost aumentou e ele está a descongelar nalguns lugares, liberando CO2 e CH4. Pode-se também fazer uma ligação 21--> 41.  
_FONTES: AR6 WG1 Caixa 5.1 p745 (p728) // AR6 WG1 Figura 5.29 (feedbacks) p755 (p738)_
