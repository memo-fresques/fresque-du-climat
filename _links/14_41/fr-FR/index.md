---
fromCardId: '14'
toCardId: '41'
status: optional
---
L'effet de serre additionnel provoque un forçage radiatif positif. Le surplus d'énergie se répartie dans l'océan (93%), la végétation (5%), la glace (3%) et l'atmosphère (1%). Cette énergie chauffe les différents compartiments terrestres dans lesquels elle se répartie, augmente leur température, et, dans le cas glaces, les fait fondre. La température du Permafrost a augmenté et il dégèle à certains endroits relachant CO2 et CH4. On peut aussi faire un lien 21--> 41.  
_SOURCES: AR6 WG1 Box 5.1 p745 (p728) // AR6 WG1 Figure 5.29 (feedbacks) p755 (p738)_
