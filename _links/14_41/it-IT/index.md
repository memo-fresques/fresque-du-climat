---
fromCardId: '14'
toCardId: '41'
status: optional
---
L'effetto serra addizionale provoca un forzante radiativo positivo. L'energia in eccesso si distribuisce nell'oceano (93%), nella vegetazione (5%), nel ghiaccio (3%) e nell'atmosfera (1%). Questa energia riscalda le diverse componenti terrestri in cui si distribuisce, aumenta la loro temperatura e, nel caso dei ghiacci, li fa sciogliere. La temperatura del permafrost è aumentata e quest'ultimo si sta sciogliendo in alcune zone, rilasciando CO2 e CH4. È possibile stabilire un collegamento anche tra 21--> 41. 
_FONTI: AR6 WG1 Box 5.1 p745 (p728) // AR6 WG1 Figura 5.29 (feedback) p755 (p738)_
