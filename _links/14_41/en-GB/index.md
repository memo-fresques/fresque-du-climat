---
fromCardId: '14'
toCardId: '41'
status: optional
---

If we take the idea of energy conservation all the way, we can link the energy budget to the thawing of permafrost.
