---
fromCardId: '5'
toCardId: '10'
status: valid
---
Gli aerosol sono piccole particelle liquide o solide sospese nell'aria. Sono emessi dalla combustione di combustibili fossili, ma esistono anche allo stato naturale come le polveri del deserto, le ceneri vulcaniche o la fuliggine degli incendi.  
_FONTI: Glossario AR6 WG1	p2233 (p2216)_
