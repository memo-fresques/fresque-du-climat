---
fromCardId: '5'
toCardId: '10'
status: valid
---
Los aerosoles son pequeñas partículas líquidas o sólidas en suspensión en el aire. La combustión de energías fósiles es responsable de la emisión de los aerosoles, también existen aerosoles de forma natural, como el polvo del desierto, las cenizas volcánicas o el hollín de los incendios.  
_SOURCES: AR6 WG1 Glosario	p2233 (p2216)_
