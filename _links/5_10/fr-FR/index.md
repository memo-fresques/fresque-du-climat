---
fromCardId: '5'
toCardId: '10'
status: valid
---
Les aérosols sont de petites particules liquides ou solides en suspension dans l’air. Ils sont émis par la combustion des énergies fossiles mais il en existe également à l’état naturel comme les poussières du désert, les cendres volcaniques ou la suie des incendies.  
_SOURCES: AR6 WG1 Glossary	p2233 (p2216)_
