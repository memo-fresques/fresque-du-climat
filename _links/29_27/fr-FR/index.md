---
fromCardId: '29'
toCardId: '27'
status: valid
---
Le plancton dont font partis les ptéropodes (phytoplancton) et les coccolithophores (zooplancton)  est à la base de nombreux réseaux trophiques (chaînes alimentaires) et ont donc une grande importance pour la biodiversité marine.    
_SOURCES: https://www.annualreviews.org/doi/abs/10.1146/annurev.marine.010908.163834 // https://fr.wikipedia.org/wiki/Plancton_
