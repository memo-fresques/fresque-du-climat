---
fromCardId: '29'
toCardId: '27'
status: valid
---
Il plancton di cui fanno parte gli pteropodi (fitoplancton) e i coccolitofori (zooplancton) è alla base di numerose reti trofiche (catene alimentari) e ha quindi una grande importanza per la biodiversità marina.
_FONTI: https://www.annualreviews.org/doi/abs/10.1146/annurev.marine.010908.163834 // https://it.wikipedia.org/wiki/Plancton_
