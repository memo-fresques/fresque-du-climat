---
fromCardId: '21'
toCardId: '25'
status: valid
---
Les animaux et les plantes sont affectés par les changements de température  
_SOURCES: AR6 WG1 13.3.2.1 p1799_
