---
fromCardId: '42'
toCardId: '20'
status: valid
---
Non è semplice! I modelli indicano che il clima in Groenlandia e intorno all'Atlantico subirà delle variazioni, con una diminuzione delle precipitazioni alle latitudini medie, precipitazioni più intense ai tropici e in Europa e tempeste più forti nelle zone di tempeste nord atlantiche (North Atlantic storm track).  
_FONTI: AR6 WG1 FAQ 9.3 p56 // AR6 WG1 Box TS.3 p41_
