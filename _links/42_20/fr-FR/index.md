---
fromCardId: '42'
toCardId: '20'
status: valid
---
Pas simple! Les modèles indiquent que la météo au Groenland et autour de l'Atlantique seront affectés, avec une baisse des précipitations aux latitudes moyennes, de plus fortes précipitations dans les tropiques et l'Europe et de plus fortes tempêtes dans les zones de tempêtes nord atlantiques (North Atlantic storm track).  
_SOURCES: AR6 WG1 FAQ 9.3 p56 // AR6 WG1 Box TS.3 p41_
