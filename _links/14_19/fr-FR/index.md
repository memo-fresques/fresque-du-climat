---
fromCardId: '14'
toCardId: '19'
status: valid
---
L'effet de serre additionnel provoque un forçage radiatif positif. Le surplus d'énergie se répartie dans l'océan (93%), la végétation (5%), la glace (3%) et l'atmosphère (1%). Cette énergie chauffe les différents compartiments terrestres dans lesquels elle se répartie, augmente leur température, et, dans le cas glaces, les fait fondre. Les deux inlandsis, le Groënland et l'Antarctique fondent rapidement. Pour l'Antarctique cependant, l'ouest fond rapidement tandis que l'est font peu ou pas. Il n'est pas clair que l'humain est responsable de la fonte de l'Antarctique.  
_SOURCES: AR6 WG1 TS3.1 (bilan énergétique)
AR6 WG1 TS2.5 (fonte des inlandsis) p59 (p93)
p45 (p77) // AR6 WG1 Figure TS.13 (d) : bilan énergétique 
AR6 WG1 Figure TS.11 : fonte des inlandsis
AR6 WG1 Figure 9.16 : fonte des inlandsis p58
p43 (p75)
p1272 (p1255)_
