---
fromCardId: '14'
toCardId: '19'
status: valid
---
L'effetto serra addizionale provoca un forzante radiativo positivo. L'energia in eccesso si distribuisce nell'oceano (93%), nella vegetazione (5%), nel ghiaccio (3%) e nell'atmosfera (1%). Questa energia riscalda le diverse componenti terrestri in cui si distribuisce, aumenta la loro temperatura e, nel caso dei ghiacci, li fa sciogliere. Le due calotte glaciali, Groenlandia e Antartide, si stanno sciogliendo rapidamente. Tuttavia, per l'Antartide, l'ovest si sta sciogliendo rapidamente mentre l'est non si sta sciogliendo o si sta sciogliendo poco. Non è chiaro se l'uomo sia responsabile dello scioglimento dell'Antartide. 
_FONTI: AR6 WG1 TS3.1 (bilancio energetico)
AR6 WG1 TS2.5 (scioglimento delle calotte glaciali) p59 (p93)
p45 (p77) // AR6 WG1 Figura TS.13 (d): bilancio energetico
AR6 WG1 Figura TS.11: scioglimento delle calotte glaciali
AR6 WG1 Figura 9.16: scioglimento delle calotte glaciali p58
p43 (p75)
p1272 (p1255)_
