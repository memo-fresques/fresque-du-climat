---
fromCardId: '14'
toCardId: '19'
status: valid
---
O efeito de estufa adicional provoca um forçamento radiativo positivo. O excesso de energia é distribuído no oceano (93%), na vegetação (5%), no gelo (3%) e na atmosfera (1%). Esta energia aquece os diferentes compartimentos terrestres nos quais é distribuída, aumenta a sua temperatura e, no caso do gelo, derrete-o. As duas calotes polares, a Gronelândia e a Antártida, estão a derreter rapidamente. No entanto, na Antártida, a parte ocidental derrete rapidamente, enquanto a parte oriental derrete pouco ou nada. Não é claro que seja o Homem o responsável pelo derretimento da Antártida.  
_FONTES: AR6 WG1 TS3.1 (balanço energético)
AR6 WG1 TS2.5 (derretimento das calotes polares) p59 (p93)
p45 (p77) // AR6 WG1 Figura TS.13 (d) : balanço energético 
AR6 WG1 Figura TS.11 : derretimento das calotes polares
AR6 WG1 Figura 9.16 : derretimento das calotes polares p58
p43 (p75)
p1272 (p1255)_
