---
fromCardId: '40'
toCardId: '5'
status: optional
---

Conflicts are often linked to fossil fuels, but the link is more the other way round.
