---
fromCardId: '14'
toCardId: '18'
status: valid
---
O efeito de estufa adicional provoca um forçamento radiativo positivo. O excesso de energia é distribuído no oceano (93%), na vegetação (5%), no gelo (3%) e na atmosfera (1%). Esta energia aquece os diferentes compartimentos terrestres nos quais é distribuída, aumenta a sua temperatura e, no caso do gelo, derrete-o. O gelo marítimo no Ártico perdeu em superfície e em espessura. Está no seu ponto mais baixo dos últimos 1000 anos. Em contrapartida, o gelo marítimo da Antártida não derrete de forma significativa (forte variabilidade).  
_FONTES: AR6 WG1 TS3.1 (balanço energético) p59 (p93) // AR6 WG1 TS2.5 (derretimento dos glaciares) p44 (p76) // AR6 WG1 Figura TS.13 (d) : balanço energético p58 // AR6 WG1 Figura 9.13 : banquisa ártica p1265 (p1248)_
