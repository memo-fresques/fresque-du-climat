---
fromCardId: '14'
toCardId: '18'
status: valid
---
L'effetto serra addizionale provoca un forzante radiativo positivo. L'energia in eccesso si distribuisce nell'oceano (93%), nella vegetazione (5%), nel ghiaccio (3%) e nell'atmosfera (1%). Questa energia riscalda le diverse componenti terrestri in cui si distribuisce, aumenta la loro temperatura e, nel caso dei ghiacci, li fa sciogliere. La banchisa artica ha perso superficie e spessore. È al suo minimo storico degli ultimi 1000 anni. Al contrario, la banchisa antartica non si sta sciogliendo in modo significativo (forte variabilità). 
_FONTI: AR6 WG1 TS3.1 (bilancio energetico) p59 (p93) // AR6 WG1 TS2.5 (scioglimento dei ghiacciai) p44 (p76) // AR6 WG1 Figura TS.13 (d): bilancio energetico p58 // AR6 WG1 Figura 9.13: banchisa artica p1265 (p1248)_
