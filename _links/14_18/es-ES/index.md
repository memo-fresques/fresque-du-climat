---
fromCardId: '14'
toCardId: '18'
status: valid
---
El efecto invernadero adicional provoca un forzamiento radiativo positivo. El excedente de energía se distribuye en el océano (93%), la vegetación (5%), el hielo (3%) y la atmósfera (1%). Esta energía calienta los diferentes compartimentos terrestres en los que se distribuye, aumentando sus temperaturas y, derritiendo os hielos. El hielo marino ártico ha perdido en superficie y en grosor. Está en su nivel más bajo desde hace 1000 años. En cambio, el hielo marino antártico no se derrite de manera significativa (alta variabilidad).  
_FUENTES: AR6 WG1 TS3.1 (balance energético) p59 (p93) // AR6 WG1 TS2.5 (derretimiento de los glaciares) p44 (p76) // AR6 WG1 Figura TS.13 (d) : balance energético p58 // AR6 WG1 Figura 9.13 : hielo marino ártico p1265 (p1248)_
