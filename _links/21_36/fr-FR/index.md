---
fromCardId: '21'
toCardId: '36'
status: valid
---
Plus il fera chaud et plus la fréquence et l'intensité des canicules est importante.  
_SOURCES: AR6 WG1 11.1.4 p1540 (1523) // AR6 WG1 11.2.3 p1565 (p1548) // AR6 WG1 Figure TS.12 (a) p51_
