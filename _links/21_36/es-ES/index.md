---
fromCardId: '21'
toCardId: '36'
status: valid
---
Cuanto más calor haga, mayor será la frecuencia e intensidad de las olas de calor.  
_FUENTES: AR6 WG1 11.1.4 p1540 (1523) // AR6 WG1 11.2.3 p1565 (p1548) // AR6 WG1 Figura TS.12 (a) p51_
