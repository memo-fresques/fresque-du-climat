---
fromCardId: '21'
toCardId: '36'
status: valid
---
Più farà caldo e più la frequenza e l'intensità delle ondate di calore sarà significativa.  
_FONTI: AR6 WG1 11.1.4 p1540 (1523) // AR6 WG1 11.2.3 p1565 (p1548) // AR6 WG1 Figura TS.12 (a) p51_
