---
fromCardId: '24'
toCardId: '23'
status: valid
---
Se il pH diminuisce, la formazione di calcare diventa più difficile, in particolare per i gusci.  
_FONTI: IPBES 2.1.17.2 p190 // AR6 WG 3.2.3.1 p407 (p395) // https://www.annualreviews.org/doi/abs/10.1146/annurev.marine.010908.163834_
