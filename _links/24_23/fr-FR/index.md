---
fromCardId: '24'
toCardId: '23'
status: valid
---
Si le pH baisse, la formation de
calcaire devient plus difficile,
notamment pour les coquilles.  
_SOURCES: IPBES 2.1.17.2 p190 // AR6 WG 3.2.3.1 p407 (p395) // https://www.annualreviews.org/doi/abs/10.1146/annurev.marine.010908.163834_
