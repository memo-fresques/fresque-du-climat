---
fromCardId: '40'
toCardId: '1'
status: optional
---
È il ciclo di retroazione finale. Tutto questo prima o poi verrà regolamentato, ma non necessariamente in modo pacifico. I partecipanti spesso fanno questo collegamento e talvolta suggeriscono di realizzare un rotolo con l'affresco per mettere la fine e l'inizio uno accanto all'altro. 
È interessante notare che ci sono umani nelle carte iniziali e finali, ma non nel mezzo.  
_FONTI: The limits to Growth: https://www.donellameadows.org/wp-content/userfiles/Limits-to-Growth-digital-scan-version.pdf // Pubblicazioni di Earth4All: https://earth4all.life/publications/
