---
fromCardId: '40'
toCardId: '1'
status: optional
---
¡Es "el bucle del Club de Roma"! Todo terminará regulándose, pero no necesariamente de manera suave. Los participantes a menudo hacen esta conexión y a veces proponen conectar el final con el principio. 
Es interesante señalar que hay humanos en las cartas del principio y del final, pero no en el medio. 
_SOURCES: The limit to growth: https://www.donellameadows.org/wp-content/userfiles/Limits-to-Growth-digital-scan-version.pdf // Resumen de Earth4all en español: https://static1.squarespace.com/static/6253f8f13c707724ac00f7c1/t/636a2f8dbc9aea4165c62a5f/1667903376545/Earth4All_Exec_Summary_FR_Sep2022.pdf_
