---
fromCardId: '40'
toCardId: '1'
status: optional
---
Det är "Romklubbens loop"! Allt detta kommer att regleras till slut, men inte nödvändigtvis på ett mjukt sätt. Deltagarna gör ofta denna koppling och föreslår ibland att rulla ihop fresken för att sätta slutet och början kant i kant. 
Det är intressant att påpeka att det finns människor på korten i början och slutet, men inte i mitten. 
_KÄLLOR: The limit to growth: https://www.donellameadows.org/wp-content/userfiles/Limits-to-Growth-digital-scan-version.pdf // Sammanfattning av Earth4all på svenska: https://static1.squarespace.com/static/6253f8f13c707724ac00f7c1/t/636a2f8dbc9aea4165c62a5f/1667903376545/Earth4All_Exec_Summary_FR_Sep2022.pdf_
