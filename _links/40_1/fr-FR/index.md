---
fromCardId: '40'
toCardId: '1'
status: optional
---
C'est "la boucle du club de Rome" ! Tout cela finira bien par se réguler, mais pas forcément de manière soft. Les participants font souvent ce lien, et proposent parfois de faire un rouleau avec la fresque pour mettre bord à bord la fin et le début. 
Il est intéressant de faire remarquer qu'il y a des humains dans les cartes du début et celles de la fin, mais pas au milieu.  
_SOURCES: The limit to growth : https://www.donellameadows.org/wp-content/userfiles/Limits-to-Growth-digital-scan-version.pdf // Résumé de Earth4all en français : https://static1.squarespace.com/static/6253f8f13c707724ac00f7c1/t/636a2f8dbc9aea4165c62a5f/1667903376545/Earth4All_Exec_Summary_FR_Sep2022.pdf_
