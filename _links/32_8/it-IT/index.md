---
fromCardId: '32'
toCardId: '8'
status: invalid
---
La diminuzione delle rese agricole porta a un adattamento delle pratiche agricole. Fertilizzazione, irrigazione, uso di biotecnologie, cultivar (varietà selezionata), aratura, cambiamento della data di piantagione, diversificazione delle colture sono tutti modi per adattarsi ai cambiamenti climatici. Ma la funzione della carta agricoltura è soprattutto quella di rappresentare la causa delle emissioni di GHG. 
_FONTI: AR6 WG2 5.4.4 p754 (p742) // AR6 WG2 Figura 5.9: Adattamento e resa agricola p755 (p743)_
