---
fromCardId: '32'
toCardId: '8'
status: invalid
---
La baisse des rendements agricoles conduit à une adaptation des pratiques agricoles. Fertilisation, irrigation, utilisation de biotechnologies, cultivar (variété sélectionnée), labour, changement de date de plantaison, diversification des cultures sont autant de façon pour s'adapter aux changement climatique.  Mais la carte agriculture est plutôt là pour incarner les causes des émissions de GES.  
_SOURCES: AR6 WG2 5.4.4 p754 (p742) // AR6 WG2 Figure 5.9 : Adaptation et rendement agricole p755 (p743)_
