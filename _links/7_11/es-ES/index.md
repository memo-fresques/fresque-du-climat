---
fromCardId: '7'
toCardId: '11'
status: invalid
---
Las emisiones de CO2 que van a la atmósfera aumentan la concentración de CO2 en la atmósfera. También podemos hacer que la flecha de esta relación salga de los sumideros de carbono.  
_SOURCES: AR6 WG1 Box TS.5 p48 // AR5 WG1 Figura 6.8 p503 // AR6 WG1 Figura RRP.7 (pozos y escenarios) p20_
