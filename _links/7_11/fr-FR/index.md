---
fromCardId: '7'
toCardId: '11'
status: invalid
---
Les emissions de CO2 qui vont dans l'atmosphère augmentent la concentration en CO2 dans l'atmosphère. On peut également faire partir la flèche des puits de carbone.  
_SOURCES: AR6 WG1 Box TS.5 p48 // AR5 WG1 Figure 6.8 p503 // AR6 WG1 Figure SPM.7 (puits et scénarios) p20_
