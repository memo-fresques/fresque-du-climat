---
fromCardId: '37'
toCardId: '38'
status: valid
---
Le nombre de personnes touchées par la faim dans le monde a augmenté pour atteindre jusqu’à  828 millions en 2021.  
_SOURCES: https://www.who.int/fr/news/item/06-07-2022-un-report--global-hunger-numbers-rose-to-as-many-as-828-million-in-2021#:~:text=D'apr%C3%A8s%20un%20rapport%20de,millions%20de%20personnes%20en%202021_
