---
fromCardId: '37'
toCardId: '38'
status: valid
---
Il numero di persone colpite dalla fame nel mondo è aumentato fino a raggiungere gli 828 milioni nel 2021.  
_FONTI: https://www.who.int/news/item/06-07-2022-un-report--global-hunger-numbers-rose-to-as-many-as-828-million-in-2021
