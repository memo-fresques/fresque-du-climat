---
fromCardId: '5'
toCardId: '27'
status: optional
---
La extracción y producción de energía fósiles en el mar perturba localmente la biodiversidad marina. Además los accidentes de barcos que transportan petróleo pueden provocar mareas negras como la del superpetrolero Amoco Cadiz en 1978 cerca de las costas bretonas.
