---
fromCardId: '5'
toCardId: '27'
status: optional
---
L'extraction de ressources fossiles dans la mer perturbe localement la biodiversité marine. De plus, des accidents de bateaux transportant du pétrole peux provoquer des marées noires comme celle du supertanker Amoco Cadiz en 1978 prêt des côtes bretonnes.  
_SOURCES: https://www.science.org/doi/10.1126/science.1237261_
