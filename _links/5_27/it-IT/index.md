---
fromCardId: '5'
toCardId: '27'
status: optional
---
L'estrazione di risorse fossili nel mare disturba localmente la biodiversità marina. Inoltre, incidenti di petroliere possono provocare maree nere come quella della superpetroliera Amoco Cadiz nel 1978 vicino alle coste bretoni. 
_FONTI: https://www.science.org/doi/10.1126/science.1237261_
