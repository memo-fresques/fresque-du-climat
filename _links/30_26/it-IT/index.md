---
fromCardId: '30'
toCardId: '26'
status: valid
---
Un terreno secco e duro è meno in grado di assorbire forti piogge quando si verificano. Questo porta a un maggior deflusso nei laghi e nei fiumi.
_FONTI: AR6 WG1 FAQ 8.2 p48_
