---
fromCardId: '30'
toCardId: '26'
status: valid
---

See card n26: "If the soil is dried by drought it's worse because the water runs off."
