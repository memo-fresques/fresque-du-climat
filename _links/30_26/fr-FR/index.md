---
fromCardId: '30'
toCardId: '26'
status: valid
---
Un sol sec et dur est moins en mesure d'absorber de fortes pluies lorsqu'elles se produisent, ce qui entraîne
plus de ruissellement dans les lacs et les rivières.  
_SOURCES: AR6 WG1 FAQ 8.2 p48_
