---
fromCardId: '8'
toCardId: '10'
status: optional
---
In senso assoluto, è vero. L'uso di fertilizzanti azotati rilascia ammoniaca (NH3) che forma aerosol. Ma hanno un effetto di raffreddamento debole e influenzano poco il bilancio radiativo della Terra, a differenza della SO2 emessa dalla combustione di risorse fossili. 
_FONTI: AR6 WG1 6.6.2.1 p883 // AR6 WG1 Figura 6.16 (1 year pulse) p887_
