---
fromCardId: '8'
toCardId: '10'
status: optional
---
Besprutning av grödor ger visserligen aerosoler och luftföroreningar, men inte i samma utsträckning som ofullständig förbränning från kraftverk.
