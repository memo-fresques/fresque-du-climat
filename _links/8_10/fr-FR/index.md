---
fromCardId: '8'
toCardId: '10'
status: optional
---
Dans l'absolue c'est vrai. L'utilisation d'engrais azotés relâche de l'ammoniac (NH3) qui forme des aérosols. Mais ils ont un effet refroidissant faible et impact peu le bilan radiatif de la Terre contrairement au SO2 émis par la combustion de ressources fossiles.  
_SOURCES: AR6 WG1 6.6.2.1 p883 // AR6 WG1 Figure 6.16 (1 year pulse) p887_
