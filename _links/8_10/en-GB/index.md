---
fromCardId: '8'
toCardId: '10'
status: optional
---

Spraying crops does result in aerosols and air pollution, but not to the same extent as incomplete combustion from power plants.
