---
fromCardId: '27'
toCardId: '37'
status: valid
---
La biomassa globale degli oceani diminuirà dal 6 al 16% a seconda degli scenari, cosa che influenzerà la pesca e porrà problemi di sicurezza alimentare. Attenzione, il cambiamento climatico non è l'unico responsabile, la pesca eccessiva sta attualmente causando problemi maggiori nella perdita di biodiversità marina. 
_FONTI: AR6 WG2 TS.C.3.1 p23 (p57)_
