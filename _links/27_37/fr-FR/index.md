---
fromCardId: '27'
toCardId: '37'
status: valid
---
La biomasse globale des océans va diminuer de 6 à 16% selon les scenario ce qui va affecter la pêche et poser des problèmes de sécurité alimentaire. Attention, le changement climatique n'est pas le seul responsable, la surpêche pose actuellement des problèmes majeurs dans la perte de biodiversité marine.  
_SOURCES: AR6 WG2 TS.C.3.1 p23 (p57)_
