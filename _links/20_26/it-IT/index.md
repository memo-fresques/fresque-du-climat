---
fromCardId: '20'
toCardId: '26'
status: valid
---
L'aumento di episodi di piogge intense comporta un aumento della frequenza e della magnitudine delle esondazioni. 
_FONTI: AR6 WG1 TS.2.6 p52 // AR6 WGI FAQ 8.1 p46 // AR6 WG1 Tabella TS.5 p90 // AR6 WG2 Figura 4.2: schema del ciclo dell'acqua p577 (p565)_
