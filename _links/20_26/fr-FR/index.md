---
fromCardId: '20'
toCardId: '26'
status: valid
---
L'augmentation des évènements de fortes pluies induit une augmentation de la fréquence et de la magnitude des crues.  
_SOURCES: AR6 WG1 TS.2.6 p52 // AR6 WGI FAQ 8.1 p46 // AR6 WG1 Table TS.5 p90 // AR6 WG2 Figure 4.2 : schéma cycle de l'eau p577 (p565)_
