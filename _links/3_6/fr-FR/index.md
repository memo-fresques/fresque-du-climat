---
fromCardId: '3'
toCardId: '6'
status: optional
---

On pourrait dire que le chauffage au bois des bâtiments est responsable de déforestation mais ce n'est pas significatif. On mesure la surface de nos appartements en m² alors qu'on parle en ha quand on évoque l'agriculture. Ce ne sont pas les mêmes ordres de grandeur ! L'agriculture est responsable de 80 à 90% de la déforestation dans le monde.  
_SOURCES: Selon FAO 2022 FR entre 2000 et 2018: 90% http://www.fao.org/3/cb9360fr/cb9360fr.pdf 
p58_
