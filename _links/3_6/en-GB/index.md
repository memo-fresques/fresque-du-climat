---
fromCardId: '3'
toCardId: '6'
status: optional
---

It can be said that heating buildings with wood is in part responsible for deforestation, but it is not significant. Neither is building a major cause of deforestation. Agriculture is the main cause of deforestation, way in front of buildings and their usage.
