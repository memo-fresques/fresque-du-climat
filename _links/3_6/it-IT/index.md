---
fromCardId: '3'
toCardId: '6'
status: optional
---
Potremmo dire che riscaldare gli edifici con la legna è responsabile della deforestazione ma questo apporto non è significativo. Misuriamo la superficie dei nostri appartamenti in m² mentre quando parliamo di agricoltura parliamo di ettari. Non sono gli stessi ordini di grandezza! L’agricoltura è responsabile dall’80 al 90% della deforestazione mondiale.  
_FONTI: Secondo FAO, è pari al 90%. https://onuitalia.com/2021/11/08/deforestazione-3/
