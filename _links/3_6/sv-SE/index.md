---
fromCardId: '3'
toCardId: '6'
status: optional
---
Man kan säga att uppvärmning av byggnader med ved är ansvarigt för avskogning, men det är inte betydande. Vi mäter ytan på våra lägenheter i m² medan vi pratar om hektar när det handlar om jordbruk. Det är inte samma storleksordningar! Jordbruket är ansvarigt för 80–90% av avskogningen i världen. 
_KÄLLOR: Enligt FAO 2022 SE mellan 2000 och 2018: 90% http://www.fao.org/3/cb9360fr/cb9360fr.pdf 
s58_
