---
fromCardId: '33'
toCardId: '31'
status: optional
---
Les submersions apportent de l'eau salé sur les côtes et peuvent contaminer les nappes phréatiques. Par exemple, le Viêtnam a subi les effets des inondations et de la salinisation du delta du Mékong. Les inondations et les sécheresses ont considérablement entraîné des pertes dans l'agriculture.  
_SOURCES: AR6 WG2 Cross-Chapter Box SLR
AR6 WG2 8.3.4.1 (ex du Vietnam) p489 (p477)
p1218 (p1206) // AR6 WG2 8.3.4.1 (ex du Vietnam) p1218 (p1206)_
