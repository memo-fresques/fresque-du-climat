---
fromCardId: '33'
toCardId: '31'
status: optional
---
Le sommersioni portano acqua salata sulle coste e possono contaminare le falde acquifere. Ad esempio, il Vietnam ha subito gli effetti delle inondazioni e della salinizzazione del delta del Mekong. Le inondazioni e le siccità hanno causato notevoli perdite nel settore agricolo. 
_FONTI: AR6 WG2 Cross-Chapter Box SLR
AR6 WG2 8.3.4.1 (es. del Vietnam) p489 (p477)
p1218 (p1206) // AR6 WG2 8.3.4.1 (es. del Vietnam) p1218 (p1206)_
