---
fromCardId: '33'
toCardId: '31'
status: optional
---

If seawater rises, it can penetrate the water tables, which are freshwater reserves.
