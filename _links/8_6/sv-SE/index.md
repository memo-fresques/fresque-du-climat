---
fromCardId: '8'
toCardId: '6'
status: valid
---
Avskogning är relaterad till mellan 80 % och 90 % till jordbruket, så ja, det kan betraktas som en mänsklig aktivitet, som en konsekvens av jordbruket eller som båda.  
_KÄLLOR: Enligt FAO 2022 FR mellan 2000 och 2018: 90 % http://www.fao.org/3/cb9360fr/cb9360fr.pdf 
s58_
