---
fromCardId: '8'
toCardId: '6'
status: valid
---
La deforestazione è legata per l'80-90% all'agricoltura, quindi sì, può essere considerata un'attività umana, come una conseguenza dell'agricoltura o entrambe.
_FONTI: Secondo FAO, la deforestazione legata all'agricoltura è pari al 90%. https://onuitalia.com/2021/11/08/deforestazione-3/
