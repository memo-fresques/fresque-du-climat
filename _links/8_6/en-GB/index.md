---
fromCardId: '8'
toCardId: '6'
status: valid
---

Deforestation is due for 80% to agriculture. It can be considered as a human activity or as a consequence of agriculture of both.
