---
fromCardId: '8'
toCardId: '6'
status: valid
---
La déforestation est liée entre 80% et 90% à l'agriculture, donc oui, elle peut être considérée comme une activité humaine, comme une conséquence de l'agriculture ou les deux.  
_SOURCES: Selon FAO 2022 FR entre 2000 et 2018: 90% http://www.fao.org/3/cb9360fr/cb9360fr.pdf 
p58_
