---
fromCardId: '8'
toCardId: '12'
status: optional
---
Det är inte allvarligt om denna länk inte görs, men det är sant att jordbruk kan förbättra lagringskapaciteten genom fotosyntes. Det är principen om 1 för 1000 (om vi ökade markens förmåga att binda kol med bara 1/1000, skulle vi ha en betydande inverkan på CO2). Men i alla fall är de skogar som jordbruket ersätter mycket mer effektiva som kolsänkor.  
_KÄLLOR: AR6 WG1 5.2.1.1 s704 (s687)_
