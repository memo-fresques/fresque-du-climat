---
fromCardId: '8'
toCardId: '12'
status: optional
---
Non importa se questo collegamento non viene individuato, ma è vero che l'agricoltura può migliorare la capacità di stoccaggio attraverso la fotosintesi. Questo è il principio di 1 su 1000 (se aumentassimo la capacità del suolo di sequestrare carbonio anche solo di 1/1000, avremmo un impatto significativo sulla CO2). Ma in ogni caso, le foreste - che vengono sostituite dall’agricoltura - sono molto più efficaci come pozzo di carbonio.  
_FONTI: AR6 WG1 5.2.1.1 p704 (p687)_
