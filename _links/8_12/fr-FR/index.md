---
fromCardId: '8'
toCardId: '12'
status: optional
---
Ce n'est pas grave si ce lien n'est pas fait, mais il est vrai que l'agriculture peut améliorer la capacité de stockage via la photosynthèse. C'est le principe du 1 pour 1000 (si on augmentait ne serait-ce que de 1/1000 la capacité du sol à séquestrer du carbone, on aurait un impact important sur le CO2). Mais dans tous les cas, les forêts que l'agriculture remplace sont bien plus efficaces comme puit de carbone.  
_SOURCES: AR6 WG1 5.2.1.1 p704 (p687)_
