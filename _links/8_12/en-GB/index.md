---
fromCardId: '8'
toCardId: '12'
status: optional
---

It doesn't matter if this link is not made, but it is true that agriculture can improve storage capacity through photosynthesis. This is the 0.4% principle (if we increased the soil's capacity to sequester carbon by even 0,4%, we would have a significant impact on CO2).
