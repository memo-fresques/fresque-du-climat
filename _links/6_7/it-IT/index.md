---
fromCardId: '6'
toCardId: '7'
status: valid
---
La deforestazione consiste nel tagliare o bruciare alberi oltre la capacità di rigenerazione della foresta. Gli alberi vengono principalmente bruciati causando così emissioni di CO2 e contribuendo al 14% delle emissioni di CO2 nel mondo.  
_FONTI: AR6 WG1 Box TS.5 p48 // AR6 WG1 Figura 5.5 (a) p705 // https://tiikmpublishing.com/proceedings/index.php/iccc/article/view/570_
