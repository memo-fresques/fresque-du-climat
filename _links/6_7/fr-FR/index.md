---
fromCardId: '6'
toCardId: '7'
status: valid
---
La déforestation consiste à couper ou brûler des arbres au delà de la capacité de renouvellement de la forêt. Les arbres sont majoritairement brûlés entrainant ainsi des emissions de CO2. Ils contribuent ainsi à 14% des emissions de CO2 dans le monde.  
_SOURCES: AR6 WG1 Box TS.5 p48 // AR6 WG1 Figure 5.5 (a) p705 // https://tiikmpublishing.com/proceedings/index.php/iccc/article/view/570_
