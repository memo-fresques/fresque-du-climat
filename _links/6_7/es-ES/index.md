---
fromCardId: '6'
toCardId: '7'
status: valid
---
La deforestación consiste en talar o quemar árboles por encima de la capacidad de renovación del bosque. La mayoría de los árboles se queman, lo que genera emisiones de CO2. Contribuyen así al 14% de las emisiones de CO2 en todo el mundo.  
_SOURCES: AR6 WG1 Box TS.5 p48 // AR6 WG1 Figura 5.5 (a) p705 // https://tiikmpublishing.com/proceedings/index.php/iccc/article/view/570_
