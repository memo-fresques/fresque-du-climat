---
fromCardId: '1'
toCardId: '6'
status: optional
---

Deforestation can be considered either as a human activity, or as a consequence of agriculture, or both.
