---
fromCardId: '1'
toCardId: '6'
status: optional
---
Avskogning kan betraktas som en mänsklig aktivitet eller som en konsekvens av jordbruket eller som båda.
