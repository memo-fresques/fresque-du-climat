---
fromCardId: '6'
toCardId: '20'
status: optional
---
La deforestazione diminuisce l'evapotraspirazione e le precipitazioni. Può quindi avere conseguenze locali sul regime delle piogge.  
_FONTI: AR6 WG1 Box TS.6 p54 (p86) // FAO 2022 p17_
