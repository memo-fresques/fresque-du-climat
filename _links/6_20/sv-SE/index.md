---
fromCardId: '6'
toCardId: '20'
status: optional
---
Avskogning minskar evapotranspiration och nederbörd. Den kan därför ha lokala konsekvenser för nederbörden.  
_KÄLLOR: AR6 WG1 Box TS.6 s54 (s86) // FAO 2022 s17_
