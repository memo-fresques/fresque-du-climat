---
fromCardId: '6'
toCardId: '20'
status: optional
---
La déforestation diminue l'evapotranspiration et les précipitations. Elle peut donc avoir des conséquences locales sur le régime des pluies.  
_SOURCES: AR6 WG1 Box TS.6 p54 (p86) // FAO 2022 p17_
