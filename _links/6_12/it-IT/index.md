---
fromCardId: '6'
toCardId: '12'
status: optional
---
Le foreste assorbono sempre più CO2 man mano che aumenta la quantità di CO2 atmosferica. Bruciandole, liberiamo la CO2 immagazzinata negli alberi e distruggiamo un pozzo di carbonio che avrebbe potuto assorbire più CO2.  
_FONTI: AR6 WG1 5.2.1.4.1 p711 // AR6 WG2 Tabella 2.4 p287 (p275) // AR5 WG1 Figura 6.8 p503_
