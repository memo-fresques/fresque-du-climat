---
fromCardId: '6'
toCardId: '12'
status: optional
---
Les forêts absorbent de plus en plus de CO2 à mesure que la quantité de CO2 atmosphérique augmente. En les brulant, on relache le CO2 stocké dans les arbres et on détruit un puit de carbone qui aurait pu absorber plus de CO2.  
_SOURCES: AR6 WG1 5.2.1.4.1 p711 // AR6 WG2 Table 2.4 p287 (p275) // AR5 WG1 Figure 6.8 p503_
