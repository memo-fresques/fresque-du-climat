---
fromCardId: '6'
toCardId: '12'
status: optional
---

Participants often think that deforestation reduces carbon sinks. In reality, the impact is minimal because deforested areas represent a very small part of the total forest area. Moreover, a mature forest has reached its equilibrium and no longer absorbs carbon. Therefore, as mainly mature forests are deforested, this does not impact carbon sinks. On the other hand, the amount CO2 released is very high.
