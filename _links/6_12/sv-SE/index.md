---
fromCardId: '6'
toCardId: '12'
status: optional
---
Deltagarna tror ofta att avskogning minskar kolsänkor. I verkligheten är påverkan minimal eftersom avskogade områden utgör en mycket liten del av den totala skogsarealen. Dessutom har en mogen skog nått sin jämvikt och absorberar inte längre kol. Därför, eftersom huvudsakligen mogna skogar avskogas, påverkar detta inte kolsänkor. Å andra sidan är mängden CO2 som släpps ut mycket hög.
