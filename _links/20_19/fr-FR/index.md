---
fromCardId: '20'
toCardId: '19'
status: optional
---
C'est très technique, mais si on est pointilleux, la partie bleue du graphique de la carte 19 concernant l'Antarctique représente un gain de masse dû à une augmentation des précipitations. Les parties rouges représentent une perte de masse. Au total, l'Antarctique perd de la masse.  
_SOURCES: AR6 WG1 TS.2.5 p94 (p77) // AR6 WG2 Figure 4.2 : schéma cycle de l'eau p577 (p565)_
