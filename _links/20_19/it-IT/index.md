---
fromCardId: '20'
toCardId: '19'
status: optional
---
È molto tecnico, ma se vogliamo essere precisi, la parte blu del grafico della carta 19 riguardante l'Antartide rappresenta un guadagno di massa dovuto a un aumento delle precipitazioni. Le parti rosse rappresentano una perdita di massa. In totale, l'Antartide sta perdendo massa. 
_FONTI: AR6 WG1 TS.2.5 p94 (p77) // AR6 WG2 Figura 4.2: schema del ciclo dell'acqua p577 (p565)_
