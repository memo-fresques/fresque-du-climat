---
fromCardId: '20'
toCardId: '19'
status: optional
---
Det är lite tekniskt, men om man är noggrann, representerar den blå delen av diagrammet på kort 19 för Antarktis en massökning på grund av ökad nederbörd. De röda delarna representerar en massförlust. Totalt sett förlorar Antarktis massa. 
_KÄLLOR: AR6 WG1 TS.2.5 s94 (s77) // AR6 WG2 Figur 4.2: vattencykelschema s577 (s565)_
