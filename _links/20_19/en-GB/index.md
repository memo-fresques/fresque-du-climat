---
fromCardId: '20'
toCardId: '19'
status: optional
---

It's a bit technical, but the blue part of the map of Antarctica on card 19 represents a gain in mass due to an increase of precipitations. The red part represents a loss in mass. In total, Antarctica is losing mass.
