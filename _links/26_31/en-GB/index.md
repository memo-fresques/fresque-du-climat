---
fromCardId: '26'
toCardId: '31'
status: valid
---

Flooding can lead in some cases to contamination of freshwater resources and affect drinking water systems.
