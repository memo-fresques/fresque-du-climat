---
fromCardId: '26'
toCardId: '31'
status: valid
---
Les crues peuvent détruire les infrastructures et causer des morts. Les crues peuvent également poser des problèmes sanitaires liés à l'eau et sont associées à des maladies diarrhéiques comme le choléra.  
_SOURCES: AR6 WG2 TS.B.5.7 p63 (p51)_
