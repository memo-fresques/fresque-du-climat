---
fromCardId: '7'
toCardId: '12'
status: valid
---
"Entre 2010 y 2019, las emisiones antropogénicas de CO2 alcanzaron aproximadamente 11 PgC por año. De estas emisiones, el 46% se acumuló en la atmósfera, el 23% fue absorbido por el océano y el 31% por los ecosistemas terrestres. ""Las dos cartas se colocan uno al lado del otro para formar un mismo gráfico. 
Hay que conseguir que los participantes encuentren esta relación:
- Miren la carta 12. ¿Ven algo raro? 
- ¡Está escrito al revés!
- Exactamente. Hay que resolver un enigma. La solución está sobre la mesa.""  
_SOURCES: AR6 WG1 Box TS.5 p48 // AR5 WG1 Figura 6.8 p503 // AR6 WG1 Figura SPM.7 (sumideros y escenarios) p20_"
