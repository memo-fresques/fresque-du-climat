---
fromCardId: '7'
toCardId: '12'
status: valid
---
Tra il 2010 e il 2019, le emissioni antropiche di CO2 hanno raggiunto circa 11 PgC all'anno. Di queste emissioni, il 46% si è accumulato nell'atmosfera, il 23% è stato assorbito dall'oceano e il 31% dagli ecosistemi terrestri. Le due carte si mettono a specchio una dell'altra per formare un unico grafico. 
È questo che bisogna riuscire a far individuare al gruppo: 
- Guarda la carta 12. Cosa vedi di strano? 
- È scritta al contrario! 
- Esattamente. È un enigma da risolvere. La soluzione è sul tavolo.
_FONTI: AR6 WG1 Box TS.5 p48 // AR5 WG1 Figura 6.8 p503 // AR6 WG1 Figura SPM.7 (pozzi e scenari) p20_
