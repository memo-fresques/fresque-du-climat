---
fromCardId: '7'
toCardId: '12'
status: valid
---
"Entre 2010 et 2019, les emissions anthropiques de CO2 ont atteint environ 11 PgC par an. De ces emissions, 46% se sont accumulées dans l'atmosphère, 23% ont été absorbées par l'océan et 31% par les écosystèmes terrestres. ""Les deux cartes se mettent bord à bord pour faire un même graphique. 
C'est ce qu'il faut réussir à leur faire trouver : 
- Regardez la carte 12. Qu'est-ce que vous voyez de bizarre ? 
- c'est écrit à l'envers !
- exactement. C'est une énigme à résoudre. La solution est sur la table.""  
_SOURCES: AR6 WG1 Box TS.5 p48 // AR5 WG1 Figure 6.8 p503 // AR6 WG1 Figure SPM.7 (puits et scénarios) p20_"
