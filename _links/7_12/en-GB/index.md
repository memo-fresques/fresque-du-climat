---
fromCardId: '7'
toCardId: '12'
status: valid
---

These two cards can be put border-to-border in order to reconstitute the original graph.
Ask the participants: "Have a look at card 12. What do you notice ? - It's written upside down - Exact. This is an enigma. The answer to it is on the table."

When they have found that 12 and 7 go together, then explain the resulting graph: it shows where the CO2 comes from and where it goes to. This is why the outer curves are symetricals. Each year, the CO2 emitted by Human beings has to go somewhere. If it's not in the carbon sinks, then it's in the atmosphere.
