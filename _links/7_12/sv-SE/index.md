---
fromCardId: '7'
toCardId: '12'
status: valid
---
Dessa två kort kan läggas från kant-i-kant för att återskapa den ursprungliga grafen.
Fråga deltagarna: "Titta på kort 12. Är det något som ser konstigt ut? - Det är skrivet upp och ner - Exakt. Det här är en gåta. Svaret på det ligger på bordet."
När deltagarna förstår att kort 7 och 12 hör ihop på detta sätt, förklara då den resulterande kompletta grafen: vart koldioxiden kommer ifrån och vart den tar vägen. Det är därför de yttre kurvorna är symmetriska. Varje år behöver all koldioxid som har släppts ut ta vägen någonstans. Det som inte tas upp av haven eller av växter hamnar i atmosfären.
