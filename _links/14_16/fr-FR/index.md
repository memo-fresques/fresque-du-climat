---
fromCardId: '14'
toCardId: '16'
status: valid
---
L'effet de serre additionnel provoque un forçage radiatif positif. Le surplus d'énergie se répartie dans l'océan (93%), la végétation (5%), la glace (3%) et l'atmosphère (1%). Cette énergie chauffe les différents compartiments terrestres dans lesquels elle se répartie, augmente leur température, et, dans le cas glaces, les fait fondre. La quasi-totalité des glaciers ont perdu de la masse.  
_SOURCES: AR6 WG1 TS3.1 (bilan énergétique) p59 (p93) // AR6 WG1 TS2.5 (fonte des glaciers) p44 (p76) // AR6 WG1 Figure TS.13 (d) : bilan énergétique p58 // AR6 WG1 Figure 9.21 : fonte des glaciers p1294 (p1277)_
