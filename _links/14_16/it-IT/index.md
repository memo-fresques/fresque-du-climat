---
fromCardId: '14'
toCardId: '16'
status: valid
---
L'effetto serra addizionale provoca un forzante radiativo positivo. L'energia in eccesso si distribuisce nell'oceano (93%), nella vegetazione (5%), nel ghiaccio (3%) e nell'atmosfera (1%). Questa energia riscalda le diverse componenti terrestri in cui si distribuisce, aumenta la loro temperatura e, nel caso dei ghiacci, li fa sciogliere. Quasi tutti i ghiacciai hanno perso massa. 
_FONTI: AR6 WG1 TS3.1 (bilancio energetico) p59 (p93) // AR6 WG1 TS2.5 (scioglimento dei ghiacciai) p44 (p76) // AR6 WG1 Figura TS.13 (d): bilancio energetico p58 // AR6 WG1 Figura 9.21: scioglimento dei ghiacciai p1294 (p1277)_
