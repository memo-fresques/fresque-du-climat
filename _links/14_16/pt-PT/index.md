---
fromCardId: '14'
toCardId: '16'
status: valid
---
O efeito de estufa adicional provoca um forçamento radiativo positivo. O excesso de energia é distribuído no oceano (93%), na vegetação (5%), no gelo (3%) e na atmosfera (1%). Esta energia aquece os diferentes compartimentos terrestres nos quais é distribuída, aumenta a sua temperatura e, no caso do gelo, derrete-o. A quase totalidade dos glaciares perdeu massa.  
_FONTES: AR6 WG1 TS3.1 (balanço energético) p59 (p93) // AR6 WG1 TS2.5 (derretimento dos glaciares) p44 (p76) // AR6 WG1 Figura TS.13 (d) : balanço energético p58 // AR6 WG1 Figura 9.21 : derretimento dos glaciares p1294 (p1277)_
