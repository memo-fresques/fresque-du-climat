---
fromCardId: '14'
toCardId: '16'
status: valid
---
Den ekstra drivhuseffekten forårsaker en positiv strålingspådriv. Overskuddsenergien fordeles i havet (93%), vegetasjonen (5%), isen (3%) og atmosfæren (1%). Denne energien varmer opp de stedene den fordeles i, øker temperaturen deres, og i isens tilfelle, får den til å smelte. Nesten alle isbreer har mistet masse.  
_KILDER: AR6 WG1 TS3.1 (energibalanse) s59 (s93) // AR6 WG1 TS2.5 (isbreers smelting) s44 (s76) // AR6 WG1 Figur TS.13 (d) : energibalanse s58 // AR6 WG1 Figur 9.21 : isbreers smelting s1294 (s1277)_
