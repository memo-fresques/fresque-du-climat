---
fromCardId: 42_v9
toCardId: '37'
status: valid
---
L'aumento delle disuguaglianze può aggravare le conseguenze delle carestie, mentre le carestie, a loro volta, possono esacerbare le disuguaglianze preesistenti. Per spezzare questo circolo vizioso, sono essenziali misure volte a ridurre le disuguaglianze economiche e sociali, a rafforzare la resilienza delle comunità e a migliorare l'accesso alle risorse di base. 

**Fonti** : _AR6 WG2 FAQ 8.1 p1263 (p1251) // AR6 WG2 8.2.1.2 p1190 (pp1178)_ 
