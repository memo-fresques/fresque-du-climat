---
fromCardId: 42_v9
toCardId: '37'
status: valid
---
L'accroissement des inégalités peut aggraver les conséquences des famines, tandis que les famines, à leur tour, peuvent exacerber les inégalités préexistantes. Pour briser ce cercle vicieux, des mesures visant à réduire les inégalités économiques et sociales, à renforcer la résilience des communautés et à améliorer l'accès aux ressources de base sont essentielles. 

**Sources** : _AR6 WG2 FAQ 8.1 p1263 (p1251) // AR6 WG2 8.2.1.2 p1190 (pp1178)_ 
