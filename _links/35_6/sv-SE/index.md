---
fromCardId: '35'
toCardId: '6'
status: optional
---
Avskogning sker delvis genom att skogen bränns, som sedan kan urarta till en okontrollerad eldsvåda. Detta är vad som hände sommaren 2019 i Amazonas och Australien.
