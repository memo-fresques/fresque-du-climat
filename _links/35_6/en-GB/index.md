---
fromCardId: '35'
toCardId: '6'
status: optional
---

Deforestation is partly done by burning the forest, which can then degenerate into uncontrolled fire. This is what happened in the summer of 2019 in the Amazon and Australia.
