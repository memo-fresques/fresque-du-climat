---
fromCardId: '35'
toCardId: '6'
status: optional
---
Selon des travaux de 
recherche récents, 29 à 37 pour cent des pertes 
de forêt au niveau mondial entre 2003 et 2018 étaient associées à des 
incendies.  
_SOURCES: FAO 2022 p38_
