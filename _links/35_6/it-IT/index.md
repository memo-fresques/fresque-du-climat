---
fromCardId: '35'
toCardId: '6'
status: optional
---
Secondo ricerche recenti, il 29-37% delle perdite di foreste a livello mondiale tra il 2003 e il 2018 sono state associate a incendi.  
_FONTI: FAO 2022 p38_
