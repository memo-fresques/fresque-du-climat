---
fromCardId: '2'
toCardId: '6'
status: optional
---
Questo collegamento è possibile per le industrie che consumano legno. Tuttavia, bisogna fare attenzione, il legno utilizzato da una fabbrica in una foresta gestita in modo sostenibile non è considerato deforestazione.
