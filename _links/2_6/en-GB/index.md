---
fromCardId: '2'
toCardId: '6'
status: optional
---

This link is possible for wood-consuming industries. However, wood used by a factory in a sustainably managed forest would not be considered as deforestation.
