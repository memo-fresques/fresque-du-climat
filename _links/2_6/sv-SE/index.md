---
fromCardId: '2'
toCardId: '6'
status: optional
---
Den här länken är möjlig för industrier som använder trä. Men man måste vara försiktig – trä från en hållbart förvaltad skog anses inte innebära avskogning.
