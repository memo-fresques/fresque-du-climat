---
fromCardId: '23'
toCardId: '27'
status: optional
---
Kalkningsproblem påverkar inte bara rovsimsnäckor och coccoliter. De kan också påverka koraller till exempel. Så denna länk är helt acceptabel. 
_KÄLLOR: AR6 WG2 3.4.2.1 s424 (s412) // https://www.annualreviews.org/doi/abs/10.1146/annurev.marine.010908.163834_
