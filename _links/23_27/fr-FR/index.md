---
fromCardId: '23'
toCardId: '27'
status: optional
---
Les problèmes de calcification ne concernent pas seulement les ptéropodes et les coccolithophores. Ils peuvent aussi impacter le corail par exemple. Donc ce lien est tout à fait acceptable.  
_SOURCES: AR6 WG2 3.4.2.1 p424 (p412) // https://www.annualreviews.org/doi/abs/10.1146/annurev.marine.010908.163834_
