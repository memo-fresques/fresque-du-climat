---
fromCardId: '23'
toCardId: '27'
status: optional
---
I problemi di calcificazione non riguardano solo gli pteropodi e i coccolitofori. Possono anche influenzare il corallo, ad esempio. Quindi questo collegamento è del tutto accettabile.  
_FONTI: AR6 WG2 3.4.2.1 p424 (p412) // https://www.annualreviews.org/doi/abs/10.1146/annurev.marine.010908.163834_
