---
fromCardId: '23'
toCardId: '27'
status: optional
---

Calcification problems do not only affect pteropods and coccolithophores. Coral reefs are also affected, for example, so this link is acceptable.
