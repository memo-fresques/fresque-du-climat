---
fromCardId: '26'
toCardId: '33'
status: invalid
---
Non confondere "esondazioni" e "sommersioni". Le esondazioni sono i fiumi che straripano (è acqua dolce). Le sommersioni sono il mare che entra nelle terre (è acqua salata).
