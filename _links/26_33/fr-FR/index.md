---
fromCardId: '26'
toCardId: '33'
status: invalid
---

Ne pas confondre 'Crues' et 'Submersions'. Les crues, ce sont les rivières / fleuves qui débordent (c'est de l'eau douce). Les submersions, c'est la mer qui entre dans les terres (c'est de l'eau salée).
