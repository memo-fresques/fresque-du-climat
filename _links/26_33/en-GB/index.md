---
fromCardId: '26'
toCardId: '33'
status: invalid
---

Do not make any confusion between floods and submersions. Floods concern rivers (it's fresh water) while submersions concern sea water (salt water).
