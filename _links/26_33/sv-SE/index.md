---
fromCardId: '26'
toCardId: '33'
status: invalid
---
Blanda inte ihop 'Översvämningar' och 'Stigande havsnivå'. Översvämningar är när floder/vattendrag svämmar över (det är sötvatten). Stigande havsnivå är när havet tränger in på land (det är saltvatten).
