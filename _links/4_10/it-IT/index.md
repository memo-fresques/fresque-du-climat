---
fromCardId: '4'
toCardId: '10'
status: optional
---
L’uso di combustibili fossili per i trasporti lo rende un settore significativo per la formazione di aerosol.  
FONTI: AR6 WG1 6.2.1 p844 // AR6 WG1 Figura 6.16 (1 year pulse) p887_
