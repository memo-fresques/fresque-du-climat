---
fromCardId: '4'
toCardId: '10'
status: optional
---

L'utilisation de ressources fossiles pour le transport en fait un secteur important de formation d'aérosols.  
_SOURCES: AR6 WG1 6.2.1 p844 // AR6 WG1 Figure 6.16 (1 year pulse) p887_
