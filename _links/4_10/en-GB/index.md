---
fromCardId: '4'
toCardId: '10'
status: optional
---

Local pollution due to exhausts pipes is one of the main motivations to promote electric cars, far more than climate change. But EV are responsible for CO2 emissions due to power generation. These emissions depend a lot on the country where the car is rolling.
See the file about it.
