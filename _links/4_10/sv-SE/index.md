---
fromCardId: '4'
toCardId: '10'
status: optional
---
Lokala föroreningar på grund av avgaser från fordon är en av de främsta motiven för att främja elbilar, långt mer än klimatförändringar. Men elbilar är ansvariga för CO2-utsläpp från elproduktionen. Dessa utsläpp beror mycket på landet där bilen laddas.
