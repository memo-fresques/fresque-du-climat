---
fromCardId: '5'
toCardId: '1'
status: optional
---
Man kan fråga sig om mänskliga aktiviteter förbrukar fossila bränslen eller om fossila bränslen möjliggör mänskliga aktiviteter. Denna debatt bör inte ta tid och man kan gruppera de två korten vid behov. När man sjunger i sin trädgård, utför man en mänsklig aktivitet utan att förbränna fossila bränslen. När väderkvarnar eller vattenkvarnar producerade mjöl, använde de inte fossila bränslen. Lyckligtvis finns det fortfarande många mänskliga aktiviteter som inte orsakar förbränning av fossila bränslen.
