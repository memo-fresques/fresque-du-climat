---
fromCardId: '5'
toCardId: '1'
status: optional
---

What comes first? Do human activities cause the use of fossil fuels or do fossil fuels ensable human activities? Don't waste time on this and group the two cards together if necessary. Plenty of activities, happily, do not require burning fossil fuels, like windmills and watermills grinding grains for flour.
