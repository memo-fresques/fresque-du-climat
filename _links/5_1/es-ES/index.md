---
fromCardId: '5'
toCardId: '1'
status: optional
---
Podemos preguntarnos si las actividades humanas consumen combustibles fósiles o si los combustibles fósiles permiten las actividades humanas. Este debate no debe tomarnos tiempo y si es necesario se poden agrupar las dos cartas. Cuando canto en mi jardín, realizo una actividad humana sin energía fósil. Cuando los molinos de viento o de agua producían harina, no utilizaban combustibles fósiles. Afortunadamente, todavía quedan muchas actividades humanas sin combustibles fósiles.
