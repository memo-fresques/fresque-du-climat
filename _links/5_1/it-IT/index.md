---
fromCardId: '5'
toCardId: '1'
status: optional
---
Potremmo domandarci se le attività umane consumano energie fossili o se le energie fossili permettono le attività umane. Questo dibattito non deve prendere tempo e si possono raggruppare le due carte se necessario. Quando canto nel mio giardino, svolgo un'attività umana senza combustibili fossili. Quando i mulini a vento o ad acqua producevano farina, non utilizzavano combustibili fossili. Fortunatamente, rimangono molte attività umane non basate su energia fossile.
