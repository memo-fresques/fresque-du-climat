---
fromCardId: '26'
toCardId: '38'
status: valid
---
Översvämningar kan förstöra infrastruktur och orsaka dödsfall. Översvämningar kan också orsaka hälsoproblem relaterade till vatten och är förknippade med diarrésjukdomar som kolera.  
_KÄLLOR: AR6 WG2 TS.B.5.7 s63 (s51)_
