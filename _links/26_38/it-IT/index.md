---
fromCardId: '26'
toCardId: '38'
status: valid
---
Le esondazioni possono distruggere le infrastrutture e causare morti. Le esondazioni possono anche causare problemi sanitari legati all'acqua e sono associate a malattie diarroiche come il colera.  
_FONTI: AR6 WG2 TS.B.5.7 p63 (p51)_
