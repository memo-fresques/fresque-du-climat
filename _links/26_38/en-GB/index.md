---
fromCardId: '26'
toCardId: '38'
status: valid
---

Floods can lead to grave sanitary issues : sewers overflow, cholera threatens.
