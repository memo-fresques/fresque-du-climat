---
fromCardId: '10'
toCardId: '38'
status: valid
---
Gli aerosol rappresentano un grosso problema per la salute pubblica, causando malattie respiratorie, cardiovascolari, ecc. Sono una preoccupazione importante in Cina, ad esempio, dove l'uso del carbone è onnipresente. Anche se gli aerosol non sono gli unici a rientrare nella categoria delle "particelle fini", ogni anno 391.000 persone nei paesi dell'UE muoiono a causa dell'inquinamento dell'aria, responsabile anche di 1,1 milioni di morti premature in India e in Cina.   
_FONTE: https://link.springer.com/article/10.1007/s00114-009-0594-x_
