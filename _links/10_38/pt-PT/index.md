---
fromCardId: '10'
toCardId: '38'
status: valid
---

Os aerossóis representam grandes problemas de saúde pública. Doenças respiratórias, cardiovasculares... É uma preocupação importante na China, por exemplo, onde o uso de carvão é omnipresente. Embora os aerossóis não sejam os únicos a entrar na categoria de "Partículas finas", todos os anos morrem 391 000 pessoas nos países da UE devido à poluição do ar, a qual causa 1,1 milhões de mortes prematuras na Índia e na China.   
_FONTE: https://link.springer.com/article/10.1007/s00114-009-0594-x_
