---
fromCardId: '10'
toCardId: '38'
status: valid
---
Aerosoler utgör stora folkhälsoproblem. De leder till bl.a. andningssjukdomar och hjärt-kärlsjukdomar. Det är en stor oro i Kina till exempel där användningen av kol är mycket närvarande. Även om aerosoler inte är de enda som faller under kategorin "Fina partiklar", dör varje år 391 000 personer i EU-länderna av luftföroreningar, och det orsakar 1,1 miljoner förtida dödsfall i Indien och Kina.   
_KÄLLOR: https://link.springer.com/article/10.1007/s00114-009-0594-x_
