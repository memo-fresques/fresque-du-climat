---
fromCardId: '10'
toCardId: '38'
status: valid
---

 Les aérosols posent de gros problèmes de santé publique. Maladies respiratoires, cardiovasculaires... C'est une préoccupation majeure en Chine par exemple où l'utilisation de charbon est omniprésente. Bien que les aérosols ne soient pas les seuls à rentrer dans la catégorie "Particule fines", chaque année, 391 000 personnes dans les pays de l'UE décèdent de la pollution de l'air, et elle cause 1.1 millions décès prématurés en Inde et en Chine.   
_SOURCES: https://link.springer.com/article/10.1007/s00114-009-0594-x_
