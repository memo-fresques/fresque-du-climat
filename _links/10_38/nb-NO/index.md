---
fromCardId: '10'
toCardId: '38'
status: valid
---

 Aerosoler forårsaker store folkehelseproblemer. Luftveissykdommer, hjerte- og karsykdommer... Det er en stor utfordring i Kina for eksempel, hvor bruken av kull er omfattende. Selv om aerosoler ikke er de eneste fremmedstoffene i luften, dør hvert år 391 000 mennesker i EU-landene av luftforurensning, og den forårsaker 1,1 millioner for tidlige dødsfall i India og Kina.   
_KILDER: https://link.springer.com/article/10.1007/s00114-009-0594-x_
