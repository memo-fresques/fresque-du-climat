---
fromCardId: '10'
toCardId: '38'
status: valid
---

Although aerosols are not alone in the "fine particle" category, every year 391,000 people in EU countries die from air pollution, and it causes 1.1 million premature deaths in India and China. Air pollution such as soot caused by the burning of fossil fuels such as coal and oil was responsible for 8.7m deaths globally in 2018.
