---
fromCardId: '10'
toCardId: '38'
status: valid
---
Chociaż aerozole nie są jedynymi w kategorii „drobnych cząstek”, zanieczyszczenia powietrza powodują 391 000 zgonów w UE i 1,1 miliona przedwczesnych zgonów w Indiach i Chinach. Zanieczyszczenie powietrza, takie jak sadza spowodowana spalaniem paliw kopalnych, węgiel i ropa naftowa, było odpowiedzialne za 8,7 mln zgonów na całym świecie w 2018 r.
