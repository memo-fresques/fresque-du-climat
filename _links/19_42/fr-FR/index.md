---
fromCardId: '19'
toCardId: '42'
status: valid
---
La fonte des glaces arctiques a des conséquences sur l'AMOC, mais très peu sur le Gulfstream. Ce sont surtout les vents et la force de rotation de la Terre qui entrainent le Gulfstream. L'AMOC en revanche devrait ralentir notamment à cause de la fonte des glaces arctiques. Le moteur de l'amoc est la circulation thermohaline qui dépend de la température de l'eau et de la salinité. En se rapporchant des pôles, le courant devient plus froid et plus salé et donc plus dense, il va partir en profondeur et revenir vers le sud à environ 1500m de profondeur. La fonte du Groênland va adoucir ce courant alors moins dense, et réduire sa capacité à partir en profondeur ralentissant ainsi l'AMOC. Outre la fonte du Groënland, la fonte de la banquise (qui est en fait très peu riche en sel qui ne congèle pas ou peu) participe aussi à adoucir l'eau. Enfin, l'augmentation des pluies dans les mers nordique contribuera également à ce phénomène.  
_SOURCES: AR6 WG1 FAQ 9.3 p56 // AR6 WG1 FAQ 9.3, Figure 1 p57_
