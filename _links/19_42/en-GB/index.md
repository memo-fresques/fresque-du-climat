---
fromCardId: '19'
toCardId: '42'
status: valid
---

Freshwater coming from the melting of Greenland may threaten the thermohaline circulation.
