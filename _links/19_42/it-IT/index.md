---
fromCardId: '19'
toCardId: '42'
status: valid
---
Il disgelo dell'Artico ha conseguenze sull'AMOC, ma ha pochissimo effetto sulla Corrente del Golfo. Sono soprattutto i venti e la forza di rotazione della Terra a influenzare la Corrente del Golfo. L'AMOC, invece, dovrebbe rallentare significativamente a causa del disgelo dell'Artico. Il motore dell'AMOC è la circolazione termoalina, che dipende dalla temperatura dell'acqua e dalla salinità. Avvicinandosi ai poli, la corrente diventa più fredda e più salata e quindi più densa, scende in profondità e ritorna verso sud a circa 1500 metri di profondità. Il disgelo della Groenlandia renderà più dolci le acque di questa corrente, rendendola meno densa e riducendo la sua capacità di scendere in profondità, rallentando così l'AMOC. Oltre al disgelo della Groenlandia, il disgelo del ghiaccio marino (che in realtà è molto poco salato e quindi congela poco) contribuisce anche a rendere l'acqua più dolce. Infine, l'aumento delle piogge nei mari nordici contribuirà anche a questo fenomeno. 
_FONTI: AR6 WG1 FAQ 9.3 p56 // AR6 WG1 FAQ 9.3, Figura 1 p57_
