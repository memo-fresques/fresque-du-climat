---
fromCardId: '3'
toCardId: '10'
status: optional
---

Buildings emit few aerosols directly. The only significant emissions are chimney fires. In Chamonix, 85% of the fine particles present in the atmosphere come from wood heating.
