---
fromCardId: '3'
toCardId: '10'
status: optional
---
Byggnader släpper ut få aerosoler direkt. De enda betydande utsläppen är från öppna spisar. I Chamonix kommer 85% av de fina partiklarna i atmosfären från vedeldning. Indirekt, via elproduktion, finns det utsläpp av partiklar.
