---
fromCardId: '3'
toCardId: '10'
status: optional
---
Gli edifici emettono pochi aerosol in modo diretto. Le uniche emissioni significative sono i camini. A Chamonix, l'85% del particolato fine presente nell'atmosfera proviene dal riscaldamento a legna. In modo indiretto, viene emesso particolato tramite la produzione di elettricità.
