---
fromCardId: '18'
toCardId: '27'
status: optional
---
La fonte de la banquise a des conséquences sur la biodiversité qui en dépend, qu'elle soit terrestre (ours, pingouins, phoques...) ou marine. Elle joue également un rôle dans le stockage de nutriments comme le fer, et le passage de lumière régulant ainsi la croissance saisonnière du phytoplancton, à la base de la chaîne alimentaire de cet écosystème. L'écosystème terrestre est également touché avec par exemple une croissance des plantes accrues les années ou la banquise est plus petite.  
_SOURCES: IPBES 2022 4.2.2.2.4 p704 // Banquise : un écosystème riche
https://www.science.org/doi/10.1126/science.1235225_
