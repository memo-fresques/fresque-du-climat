---
fromCardId: '18'
toCardId: '27'
status: optional
---
El derretimiento del hielo marino impacta la biodiversidad que depende de él, ya sea terrestre (osos, pingüinos, focas...) o marina. También juega un papel en el almacenamiento de nutrientes como el hierro, y en el paso de la luz regulando así el crecimiento estacional del fitoplancton, la base de la cadena alimentaria de este ecosistema. El ecosistema terrestre también se ve afectado con, por ejemplo, un aumento del crecimiento de las plantas en los años en que el hielo marino es más pequeño.  
_FUENTES: IPBES 2022 4.2.2.2.4 p704 // Hielo marino: un ecosistema rico
https://www.science.org/doi/10.1126/science.1235225_
