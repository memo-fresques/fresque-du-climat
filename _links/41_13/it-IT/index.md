---
fromCardId: '41'
toCardId: '13'
status: optional
---
Possiamo collegare direttamente i feedback relativi al metano alla carta Emissioni altri gas serra o alla carta Effetto serra addizionale. Infatti, la carta degli altri gas serra rappresenta i gas serra di origine umana, mentre qui si tratta di gas serra che non sono emessi direttamente dall'uomo.
