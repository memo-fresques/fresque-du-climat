---
fromCardId: '41'
toCardId: '13'
status: optional
---

On peut relier directement les feedback liés au Méthane à la carte Autres GES ou à la carte Effet de Serre. En effet, la carte des autres GES représente les GES d'origine humaine, alors qu'ici, il s'agit de GES qui ne sont pas émis directement par l'homme.
