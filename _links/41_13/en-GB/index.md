---
fromCardId: '41'
toCardId: '13'
status: optional
---

The permafrost card can be linked either to Other GHGs or to Additional Greenhouse Effect. The Other GHGs card is about GHGs emitted by human activities, while the methane in permafrost is not of human origin.
