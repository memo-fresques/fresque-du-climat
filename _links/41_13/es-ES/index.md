---
fromCardId: '41'
toCardId: '13'
status: optional
---
Se podemos vincular directamente la liberación del Metano debida al permafrost con la cartaal mapa Otros GEI o al efecto Invernadero adicional. Si se crea esta relación  habría que clarificar que la tarjeta de otros GEI representa los GEI de origen humano, mientras que esta relación representa GEI que no son emitidos directamente por el hombre.
