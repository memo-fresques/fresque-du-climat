---
fromCardId: '41'
toCardId: '13'
status: optional
---
Vi kan direkt koppla kortet 'Permafrost' till kortet 'Andra växthusgaser' eller till kortet 'Växthuseffekten'. Faktum är att kortet om andra växthusgaser representerar växthusgaser av mänskligt ursprung, medan det här handlar om växthusgaser som inte släpps ut direkt av människan.
