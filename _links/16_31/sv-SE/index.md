---
fromCardId: '16'
toCardId: '31'
status: valid
---
Glaciärernas smältning hotar vattenförsörjningen. Faktum är att den relativa betydelsen av glaciärsmältvatten på sommaren kan vara betydande, och bidrar till exempel med 25 % av augustiflödena i avrinningsområden som dränerar de europeiska Alperna, med en yta på cirka 105 km2 och endast 1 % glaciärtäckning. Deras försvinnande skulle kunna vara katastrofalt för städer belägna i dalar som bevattnas av floder som rinner ner från de omgivande bergen och för sötvattensfaunan. Glaciärsmältvatten blir också viktigare under torka och värmeböljor. Till en början ger smältande glaciärer mer vatten tills de når en vattenpuckel. Men efter denna puckel har glaciärerna krympt för mycket och mängden vatten som tillförs minskar.   
_KÄLLOR: AR6 WG1 Box TS.6 s53 // SROCC FAQ 2.1, Figur 1: vattenpuckel s162_
