---
fromCardId: '16'
toCardId: '31'
status: valid
---
Lo scioglimento dei ghiacciai minaccia la fornitura di acqua. Infatti, l'importanza relativa delle acque derivate dalla fusione dei ghiacciai in estate può essere considerevole, contribuendo ad esempio al 25% delle portate di agosto nei bacini che drenano le Alpi europee, con una superficie di circa 105 km2 e solo l'1% di copertura glaciale. La loro scomparsa potrebbe essere catastrofica per le città situate nelle valli irrigate dai fiumi che scendono dalle montagne circostanti e per la fauna d'acqua dolce. L'acqua di fusione dei ghiacciai aumenta di importanza anche durante le siccità e le ondate di calore. Inizialmente, uno scioglimento maggiore dei ghiacciai porta maggiori quantità d'acqua fino a raggiungere un picco di acqua. Tuttavia, superato questo picco, i ghiacciai sono ormai troppo ridotti e la quantità di acqua fornita diminuisce. 
_FONTI: AR6 WG1 Box TS.6 p53 // SROCC FAQ 2.1, Figura 1: at peak water (picco d'acqua) p162_
