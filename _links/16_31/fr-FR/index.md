---
fromCardId: '16'
toCardId: '31'
status: valid
---

La fonte des glaciers menace l'approvisionnement en eau. En effet, l'importance relative des eaux de fonte des glaciers en été peut être considérable, contribuant par exemple à 25% des débits d'août dans les bassins drainant les Alpes européennes, avec une superficie d'environ 105 km2 et seulement 1% de couverture glaciaire. Leur disparition pourrait être catastrophique pour des villes situées dans les vallées arrosées par les rivières descendant des montagnes environnantes et pour la faune d'eau douce. L'eau de fonte des glaciers augmente également en importance pendant les sécheresses et les vagues de chaleur. Au départ, les glaciers qui fondent plus amènent plus d'eau jusqu'à ce qu'ils atteignent un pic d'eau. Cependant, passé ce pic, les glaciers ont trop rétrécit et la quantité d'eau fournie diminue.   
_SOURCES: AR6 WG1 Box TS.6 p53 // SROCC FAQ 2.1, Figure 1 : pic d'eau p162_
