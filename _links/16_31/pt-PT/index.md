---
fromCardId: '16'
toCardId: '31'
status: valid
---

O derretimento dos glaciares ameaça o abastecimento de água. De facto, a importância relativa das águas do degelo dos glaciares no verão pode ser considerável, contribuindo, por exemplo, com 25% dos caudais de agosto nas bacias que drenam os Alpes europeus, com uma área de cerca de 105 km2 e apenas 1% de cobertura glacial. O seu desaparecimento poderia ser catastrófico para cidades situadas nos vales irrigados pelos rios que descem das montanhas circundantes e para a fauna de água doce. A água do degelo dos glaciares também aumenta em importância durante as secas e as ondas de calor. Inicialmente, os glaciares que derretem mais, trazem mais água até atingirem um pico de água. No entanto, após esse pico, os glaciares encolhem demasiado e a quantidade de água fornecida diminui.   
_FONTE: AR6 WG1 Box TS.6 p53 // SROCC FAQ 2.1, Figura 1: pico de água p162_
