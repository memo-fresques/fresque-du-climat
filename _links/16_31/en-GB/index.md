---
fromCardId: '16'
toCardId: '31'
status: valid
---

Melting glaciers threaten water supplies. Indeed, the relative importance of glacier melt water in summer can be considerable, contributing for example to 25% of August flows in the basins draining the European Alps, with an area of about 105 km2 and only 1% glacial cover. Their disappearance could be catastrophic for cities located in valleys watered by rivers flowing down from the surrounding mountains and for freshwater fauna. Glacier meltwater also increases in importance during droughts and heat waves.
