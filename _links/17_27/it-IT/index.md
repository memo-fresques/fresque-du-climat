---
fromCardId: '17'
toCardId: '27'
status: valid
---
Un aumento della temperatura dell'acqua influisce sulla biodiversità marina. Può influire sul metabolismo, sulla dimensione del corpo, sul timing della migrazione... La biodiversità nelle acque dolci dovrebbe diminuire in modo proporzionale al grado di riscaldamento e al cambiamento delle precipitazioni. La temperatura dell'acqua è anche legata allo sbiancamento dei coralli. 
_FONTI: AR6 WG2 15.3.3.1.3 p2068 (p2056) // IPBES 2022 4.2.3.2 p713 (p650)_
