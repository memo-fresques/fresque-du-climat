---
fromCardId: '17'
toCardId: '27'
status: valid
---
Um aumento da temperatura da água afeta a biodiversidade marinha. Pode afetar o metabolismo, o tamanho do corpo, o tempo de migração... A biodiversidade em água doce deverá diminuir proporcionalmente em relação ao grau de aquecimento e à mudança de precipitações. A temperatura da água está também ligada ao branqueamento dos corais.  
_FONTES: AR6 WG2 15.3.3.1.3 p2068 (p2056) // IPBES 2022 4.2.3.2 p713 (p650)_
