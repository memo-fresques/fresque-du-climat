---
fromCardId: '17'
toCardId: '27'
status: valid
---
Une augmentation de la température de l'eau affecte la biodiversité marine. Elle peut affecter le métabolisme, la taille du corps, le timing de migration... La biodiversité en eau douce devrait diminuer proportionnellement au degré de réchauffement et au changement de précipitations. La température de l'eau est également liée au blanchissement des coraux.  
_SOURCES: AR6 WG2 15.3.3.1.3 p2068 (p2056) // IPBES 2022 4.2.3.2 p713 (p650)_
