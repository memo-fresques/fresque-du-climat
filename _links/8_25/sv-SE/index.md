---
fromCardId: '8'
toCardId: '25'
status: optional
---
Intensiv användning av bekämpningsmedel har negativa effekter på pollinatörer och andra land- och vattenlevande ryggradslösa arter, på groddjur, fåglar och ekosystem. Ingen koppling till klimatet, men intressant relation att göra. 
_KÄLLOR: AR6 WG2 13.3.1.4 s1849 (s1837) // https://www.sciencedirect.com/science/article/abs/pii/S0006320717301805?via%3Dihub_ 
