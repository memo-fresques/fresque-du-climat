---
fromCardId: '8'
toCardId: '25'
status: optional
---
L'uso intensivo di pesticidi ha impatti negativi sugli impollinatori e altre specie terrestri e acquatiche invertebrate, sugli anfibi, gli uccelli e gli ecosistemi. Nessun legame con il clima, ma è un collegamento interessante da fare. 
_FONTI: AR6 WG2 13.3.1.4 p1849 (p1837) // https://www.sciencedirect.com/science/article/abs/pii/S0006320717301805?via%3Dihub_
