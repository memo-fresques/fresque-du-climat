---
fromCardId: '8'
toCardId: '25'
status: optional
---
L'utilisation intensive de pesticides a des impacts négatifs sur les pollinisateurs et d'autres espèces terrestres et aquatiques invertébrés, sur les amphibiens, les oiseaux et les écosystèmes.  Pas de lien avec le climat, mais relation intéressante à faire.  
_SOURCES: AR6 WG2 13.3.1.4 p1849 (p1837) // https://www.sciencedirect.com/science/article/abs/pii/S0006320717301805?via%3Dihub_ 
