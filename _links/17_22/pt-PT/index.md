---
fromCardId: '17'
toCardId: '22'
status: valid
---
Desde 1900, o nível do mar subiu em média 20cm. Entre 1971 e 2018, cerca de metade da subida das águas foi devida à expansão térmica dos oceanos (50,4%). O derretimento dos glaciares é o segundo contribuinte com 22,2%, seguido pelo derretimento da Gronelândia (12,6%) e pelo derretimento da Antártida (7,1%). Embora os glaciares contribuam muito para a subida atual das águas, o seu derretimento integral faria subir o nível das águas apenas 0,3m, enquanto a Gronelândia e a Antártida têm o potencial de aumentar o nível das águas em 7m e 58m, respetivamente.  
_FONTES: AR6 WG1 Box TS.4 p45 // AR6 WG1 Cross-Chapter 9.1, Figura 1: contribuição para a subida das águas p1308 (p1291) // AR6 WG1 Box TS.4, Figura 1: cenários de subida das águas p46 (p78)_
