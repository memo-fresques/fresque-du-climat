---
fromCardId: '17'
toCardId: '22'
status: valid
---
El nivel del mar ha aumentado una media de 20 cm desde 1900. Entre 1971 y 2018, alrededor de la mitad del aumento del nivel del mar se debió a la expansión térmica de los océanos (50,4%). El deshielo de los glaciares es el segundo contribuyente con un 22,2%, seguido por el deshielo de Groenlandia (12,6%), el deshielo de la Antártida (7,1%). Aunque el deshielo de los glaciares contribuyen significativamente al aumento actual del nivel del mar, su deshielo total sólo elevaría los niveles de agua en 0,3 m, mientras que Groenlandia y la Antártida tienen el potencial de aumentar los niveles de agua en 7 m y 58 m respectivamente.  
_FUENTES: AR6 WG1 Box TS.4 p45 // AR6 WG1 Cross-Chapter 9.1, Figura 1: contribución al aumento de los niveles de agua p1308 (p1291) // AR6 WG1 Box TS.4, Figura 1: escenarios de aumento de agua p46 (p78)_
