---
fromCardId: '31'
toCardId: '40'
status: valid
---
La riduzione delle risorse di acqua dolce, la siccità o la sicurezza alimentare hanno il potenziale di esacerbare tensioni già esistenti, in particolare nelle regioni o in alcuni gruppi che dipendono dall'agricoltura per la produzione di cibo.
Un riscaldamento di 2°C e 3°C potrebbe aumentare i rischi di conflitti rispettivamente del 13% e 26%. Tuttavia, altri fattori sono considerati più importanti nello scoppio dei conflitti: la mancanza di regolamentazione delle risorse naturali, l'esclusione sociale, infrastrutture carenti o conflitti passati.   
_FONTI: AR6 WG2 4.3.6 (acqua, osservato) p605 (p593) // AR6 WG2 4.5.6 (acqua, proiezione) p630 (p618)_
