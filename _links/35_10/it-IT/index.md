---
fromCardId: '35'
toCardId: '10'
status: optional
---
Le analisi satellitari mostrano quantità di particolato tossico nel fumo che pone problemi di salute pubblica. Gli incendi emettono aerosol. Se questo collegamento non viene individuato non succede niente.  
_FONTI: AR6 WG3 7.3.1.4 : Fire regime changes (cambiamenti del regime degli incendi) p783 (p770)_
