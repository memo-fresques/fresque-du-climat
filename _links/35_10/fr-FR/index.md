---
fromCardId: '35'
toCardId: '10'
status: optional
---

Les analyses par satellite montrent dans les fumées des quantités de particules toxiques qui posent des problèmes de santé publique. Les incendies émettent des aérosols. Ce n'est pas grave si ce lien n'a pas été trouvé.  
_SOURCES: AR6 WG3 7.3.1.4 : Fire Regime changes p783 (p770)_
