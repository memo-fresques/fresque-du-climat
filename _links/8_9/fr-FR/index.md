---
fromCardId: '8'
toCardId: '9'
status: valid
---
L'agriculture n'utilise pas beaucoup d'énergies fossiles. Tout juste un peu d'essence pour mettre dans les tracteurs. En revanche, elle émet d'autres gaz à effet de serre comme le méthane rejeté par les rots des ruminants et la culture de riz ou encore du N2O provenant notament des engrais utilisés.  
_SOURCES: AR6 WG3 TS.3 p17 (p65) // AR6 WG3 Figure TS.6 p18 (p66) // AR6 WG3 Figure 7.3 p769 (p756)_
