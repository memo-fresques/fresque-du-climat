---
fromCardId: '34'
toCardId: '32'
status: valid
---
I cicloni possono distruggere le colture, come il ciclone Nargis in Myanmar che ha ridotto la produzione agricola del 19%.  
_FONTI: AR6 WG2 Tabella 5.14 p805 (p793)_
