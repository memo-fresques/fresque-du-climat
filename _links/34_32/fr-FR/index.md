---
fromCardId: '34'
toCardId: '32'
status: valid
---
Les cyclones peuvent détruire des cultures comme le cyclone Nargis à Myanmar qui a réduit la production agricole de 19%.  
_SOURCES: AR6 WG2 Table 5.14 p805 (p793)_
