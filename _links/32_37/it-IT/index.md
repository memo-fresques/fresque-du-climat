---
fromCardId: '32'
toCardId: '37'
status: valid
---
Un calo delle rese agricole può portare a malnutrizione o a carestie. Ad esempio, nel 2017, una siccità in Tanzania, Etiopia, Kenya e Somalia ha contribuito a una grave insicurezza alimentare.
_FONTE: AR6 WG2 Tabella 4.4 p592 (p580)_
