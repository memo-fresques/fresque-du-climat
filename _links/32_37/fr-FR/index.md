---
fromCardId: '32'
toCardId: '37'
status: valid
---
Une baisse des rendements agricoles peuvent conduire à une sous-nutrition voir à des famines. Par exemple en  2017, une sécheresse en Tanzanie, Ethiopie, Kenya et Somalie a contribué à une importante insécurité alimentaire.  
_SOURCES: AR6 WG2 Table 4.4 p592 (p580)_
