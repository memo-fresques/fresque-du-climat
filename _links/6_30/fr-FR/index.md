---
fromCardId: '6'
toCardId: '30'
status: optional
---
La déforestation peut être la cause directe de sécheresses car les arbres stockent beaucoup d'eau. Si on les coupe, ils ne jouent plus ce rôle de régulateurs de l'humidité.  
_SOURCES: AR6 WG1 Box TS.6 p54 (p86) // AR6 WG1 8.6.2.1 p1166 (p1149)_
