---
fromCardId: '6'
toCardId: '30'
status: optional
---
La deforestazione può essere la causa diretta delle siccità perché gli alberi immagazzinano molta acqua. Se vengono abbattuti, non svolgono più questo ruolo di regolatori dell'umidità.  
_FONTI: AR6 WG1 Box TS.6 p54 (p86) // AR6 WG1 8.6.2.1 p1166 (p1149)_
