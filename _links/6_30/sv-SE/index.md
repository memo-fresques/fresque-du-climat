---
fromCardId: '6'
toCardId: '30'
status: optional
---
Avskogning kan vara den direkta orsaken till torka eftersom träd lagrar mycket vatten. Om man hugger ner dem, spelar de inte längre rollen som fuktreglerare. 
_KÄLLOR: AR6 WG1 Box TS.6 s54 (s86) // AR6 WG1 8.6.2.1 s1166 (s1149)_
