---
fromCardId: '41'
toCardId: '9'
status: valid
---
Il permafrost è un suolo permanentemente ghiacciato. La materia organica che vi si è accumulata per migliaia di anni costituisce una riserva di 1700 PgC, 2 volte di più di quanto attualmente presente nell'atmosfera. In caso di scioglimento, la decomposizione di questa materia organica da parte di microrganismi potrebbe rilasciare CO2 e metano. Tuttavia, il tempismo e l'intensità di questo fenomeno rimangono incerti. I modelli attuali prevedono un rilascio relativamente basso di 18 PgC /°C (3,1 a 41) di aumento della temperatura entro il 2100. Attualmente emettiamo 12 PgC all'anno.  
_FONTI: AR6 WG1 Box 5.1 p745 (p728) // AR6 WG1 Figura 5.29 (feedback) p755 (p738)_
