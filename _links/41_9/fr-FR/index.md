---
fromCardId: '41'
toCardId: '9'
status: valid
---
Le permafrost est un sol gelé en permanence. La matière organiques qui s'y est accumulée pendant des milliers d'années constitue un stock de 1700 PgC, 2 fois plus que ce qu'il y a actuellement dans l'atmosphère. En cas de fonte, la décomposition de cette matière organique par des microorganismes pourrait relâcher du CO2 et du méthane. Cependant, le timing et l'intensité de ce phénomène reste incertain. Les modèles actuels prédisent un relargage relativement faible de 18 PgC /°C (3.1 à 41) d'augmentation de température d'ici 2100. On en émet actuellement 12PgC par an.  
_SOURCES: AR6 WG1 Box 5.1 p745 (p728) // AR6 WG1 Figure 5.29 (feedbacks) p755 (p738)_
