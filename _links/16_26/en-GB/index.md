---
fromCardId: '16'
toCardId: '26'
status: optional
---

It is possible, in certain circumstances of high heat, that very fast rapid melting of glaciers may cause flooding. But the real concern about these glaciers is that they are gradually disappearing, depriving downstream irrigation of a top-up in summer.
