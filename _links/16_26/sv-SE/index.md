---
fromCardId: '16'
toCardId: '26'
status: optional
---
Det är möjligt, under vissa omständigheter med hög värme, att den alltför snabba smältningen av glaciärerna orsakar översvämningar. Men det verkliga bekymret med dessa glaciärer är att de gradvis försvinner, vilket berövar bevattningen nedströms ett tillskott av vatten på sommaren.  
_KÄLLOR: AR6 WG1 Box TS.6 s53 // AR6 WG1 8.2.3.1 s1088 (s1071) // SROCC FAQ 2.1, Figur 1: vattenpiken s162_
