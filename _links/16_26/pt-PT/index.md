---
fromCardId: '16'
toCardId: '26'
status: optional
---
É possível, em certas circunstâncias de calor intenso, que o derretimento demasiado rápido dos glaciares provoque inundações. Mas a verdadeira preocupação em relação a esses glaciares é o facto de estarem a desaparecer gradualmente, privando a irrigação a jusante de água suplementar no verão.  
_FONTES: AR6 WG1 Box TS.6 p53 // AR6 WG1 8.2.3.1 p1088 (p1071) // SROCC FAQ 2.1, Figura 1: pico de água p162_
