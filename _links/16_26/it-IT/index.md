---
fromCardId: '16'
toCardId: '26'
status: optional
---
In alcune circostanze di forte calore, è possibile che il rapido scioglimento dei ghiacciai causi esondazioni. Ma la vera preoccupazione riguardo a questi ghiacciai è il fatto che stanno scomparendo gradualmente, privando l'irrigazione a valle di una fonte d'acqua in estate. 
_FONTI: AR6 WG1 Box TS.6 p53 // AR6 WG1 8.2.3.1 p1088 (p1071) // SROCC FAQ 2.1, Figura 1: peak water (picco d'acqua) p162_
