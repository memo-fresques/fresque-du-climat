---
fromCardId: '16'
toCardId: '26'
status: optional
---
Il est possible, dans certaines circonstances de forte chaleur, que la fonte trop rapide des glaciers provoque des crues. Mais le vrai sujet de préoccupation à propos des ces glaciers, c'est le fait qu'ils disparaissent progressivement, privant l'irrigation en aval d'un appoint d'eau en été.  
_SOURCES: AR6 WG1 Box TS.6 p53 // AR6 WG1 8.2.3.1 p1088 (p1071) // SROCC FAQ 2.1, Figure 1 : pic d'eau p162_
