---
fromCardId: '18'
toCardId: '15'
status: optional
---

When the ice pack melts, a white surface is replaced by a dark blue surface, which has a lower albedo and therefore absorbs more energy. This relationship is not essential but it does allow another feedback loop of the game to be put forward.
