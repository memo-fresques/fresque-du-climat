---
fromCardId: '18'
toCardId: '15'
status: optional
---
Cuando se derrite, el hielo marino que es blanco da paso a agua azul oscuro que tiene un albedo más bajo. El albedo es la capacidad de un cuerpo para reflejar la luz (un cuerpo negro tiene un albedo de 0, un espejo tiene un albedo de 1. La tierra tiene un albedo medio de 0,31). Así, la tierra absorbe más energía y se calienta. Es una retroalimentación, o bucle de amplificación. Esta relación no es indispensable, pero algunos participantes un poco expertos lo pensarán. 
_FUENTES: AR6 WG1 7.4.2.3 : albedo de la superficie (p970) // AR6 WG1 Figura 7.10 : retroalimentación p996 (979) // AR6 WG1 Tabla 7.10 : retroalimentación p995 (978)_
