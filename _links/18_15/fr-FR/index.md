---
fromCardId: '18'
toCardId: '15'
status: optional
---
Quand elle fond, la banquise qui est blanche laisse place à de l'eau bleu foncé qui a un albédo plus faible. L'albédo est la capacité d'un corps à  envoyer la lumière (un corps noir a un albédo de 0, un miroir a un albédo de 1. La terre a un albédo moyen de 0,31). Ainsi, la terre absorbe plus l'énergie et se réchauffe. C'est une rétroaction, ou boucle d'amplification. Cette relation n'est pas indispensable, mais certains participants un peu experts vont y penser.  
_SOURCES: AR6 WG1 7.4.2.3 : surface albedo (p970) // AR6 WG1 Figure 7.10 : feedback p996 (979) // AR6 WG1 Table 7.10 : feedback p995 (978)_
