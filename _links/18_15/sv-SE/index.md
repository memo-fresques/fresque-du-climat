---
fromCardId: '18'
toCardId: '15'
status: optional
---
När isen smälter, ersätts den vita isen av mörkblått vatten som har en lägre albedo. Albedo är ett mått på ett objekts förmåga att reflektera ljus (ett svart objekt har ett albedo på 0, en spegel har ett albedo på 1 och jorden har ett genomsnittligt albedo på 0,31). Således absorberar jorden mer energi och värms upp. Detta är en förstärkande återkoppling. Denna relation är inte nödvändig, men vissa deltagare som är lite experter kommer att tänka på det.  
_KÄLLOR: AR6 WG1 7.4.2.3: ytalbedo (s970) // AR6 WG1 Figur 7.10: återkoppling s996 (979) // AR6 WG1 Tabell 7.10: återkoppling s995 (978)_
