---
fromCardId: '18'
toCardId: '15'
status: optional
---
Quando il ghiaccio si scioglie, la banchisa bianca lascia spazio a un'acqua blu scuro che ha un'albedo più bassa. L'albedo è la capacità di un corpo di riflettere la luce (un corpo nero ha un'albedo di 0, uno specchio ha un'albedo di 1. La Terra ha un'albedo media di 0,31). Pertanto, la Terra assorbe più energia e si riscalda. Questo è un feedback loop, o un ciclo di retroazione. Questo collegamento non è essenziale, ma alcuni partecipanti esperti potrebbero pensarci. 
_FONTI: AR6 WG1 7.4.2.3: albedo della superficie (p970) // Figura 7.10 di AR6 WG1: feedback p996 (979) // Tabella 7.10 di AR6 WG1: feedback p995 (978)_
