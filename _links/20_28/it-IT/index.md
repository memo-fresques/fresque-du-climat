---
fromCardId: '20'
toCardId: '28'
status: optional
---
In genere, la carta dei vettori di malattia è correlata alla carta della biodiversità terrestre poiché i vettori di malattia sono un sottogruppo della biodiversità, ma può anche essere correlata alle stesse cause della carta della biodiversità, ovvero la perturbazione del ciclo dell'acqua e l'aumento della temperatura.
