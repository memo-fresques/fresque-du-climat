---
fromCardId: '20'
toCardId: '28'
status: optional
---
Kortet 'Sjukdomsvektorer' är i allmänhet kopplad till den kortet 'Landlevande biologisk mångfald' eftersom sjukdomsvektorer är en del av den biologiska mångfalden, men den kan också kopplas till samma orsaker som kortet om biologisk mångfald, det vill säga störningar i vattencykeln och temperaturökning.
