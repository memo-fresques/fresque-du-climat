---
fromCardId: '30'
toCardId: '12'
status: optional
---
Om en brist på regn uppstår medan växter växer, blir det mindre fotosyntes och därför minskad kapacitet av kolsänkan att ta upp koldioxid från atmosfären. I Europa minskade kolsänkor med 18 % under 2018.
