---
fromCardId: '30'
toCardId: '12'
status: optional
---

If a lack of rain occurs while plants are growing, there is less photosynthesis and therefore a reduction in capacity of the carbon sink. In Europe, in 2018, carbon sinks declined by 18%.
