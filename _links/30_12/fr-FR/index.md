---
fromCardId: '30'
toCardId: '12'
status: optional
---
Les sécheresses limitent la photosynthèse, augmentent la mortalité des forêts et promeuvent des feux de forêts diminuant ainsi la viabilité ou la capacité des puits de carbone. Les évènements El niño de grandes ampleurs sont d'ailleurs responsables de fortes variations interannuelles de CO2 dans les puits de carbone des continents.  
_SOURCES: AR6 WG1 Box TS.5 p48 // AR6 WG1 Cross-Chapter Box 5.1 p714 // AR5 WG1 Figure 6.8 p503 // AR6 WG1 Figure 5.6 (b) p707_
