---
fromCardId: '30'
toCardId: '12'
status: optional
---
La siccità limita la fotosintesi, aumenta la mortalità delle foreste e promuove incendi boschivi riducendo così la sopravvivenza o la capacità dei pozzi di carbonio. Gli eventi di grande portata di El Niño sono inoltre responsabili di forti variazioni interannuali di CO2 nei pozzi di carbonio dei continenti. 
_FONTI: AR6 WG1 Box TS.5 p48 // AR6 WG1 Cross-Chapter Box 5.1 p714 // AR5 WG1 Figura 6.8 p503 // AR6 WG1 Figura 5.6 (b) p707_
