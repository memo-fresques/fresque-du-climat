---
fromCardId: '34'
toCardId: 42_v9
status: valid
---
Les catastrophes climatiques peuvent exacerber les inégalités préexistantes en accentuant les disparités économiques, sociales et environnementales entre les différentes populations. Les politiques de gestion des risques, d'adaptation et d'atténuation doivent tenir compte de ces inégalités pour promouvoir une résilience plus équitable. 

**Sources :** _AR6 WG2 B.2.5 p25 (p13)_
