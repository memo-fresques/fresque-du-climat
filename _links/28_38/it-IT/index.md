---
fromCardId: '28'
toCardId: '38'
status: valid
---
I vettori di malattie come le zanzare trasmettono virus tipo quelli responsabili di malaria, Zika, dengue, malattia di Lyme, ecc... 
_FONTI: AR6 WG2 2.5.1.4 p273 (p261) // AR6 WG2 FAQ 2.2 p21_
