---
fromCardId: '28'
toCardId: '38'
status: valid
---
Les vecteurs de maladies comme les moustiques véhiculent des virus comme ceux responsables de la malaria, Zika, la dengue, la maladie de Lyme ....  
_SOURCES: AR6 WG2 2.5.1.4 p273 (p261) // AR6 WG2 FAQ 2.2 p21_
