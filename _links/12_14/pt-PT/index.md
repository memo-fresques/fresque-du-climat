---
fromCardId: '12'
toCardId: '14'
status: invalid
---

O objetivo aqui não é dizer que não há uma relação de causa e efeito entre estas duas cartas, mas alertar para o risco de as confundir. O primeiro (Sumidouros de carbono) diz-nos para onde vai o carbono. O segundo (Orçamento energético) diz-nos para onde vai o excesso de energia na Terra. As duas noções são próximas (em ambos os casos, é uma distribuição), mas não dizem respeito à mesma coisa: de um lado, o carbono, do outro, a energia. O que contribui ainda mais para a confusão é que a atmosfera e o oceano estão presentes em ambos os lados.
