---
fromCardId: '12'
toCardId: '14'
status: invalid
---

Poenget her er ikke å si at det ikke er noen årsakssammenheng mellom disse to kortene, men å advare mot risikoen for å forveksle dem. Det første (Karbonsluk) forteller oss hvor karbonet havner. Det andre (Energibalanse) forteller oss hvor overskuddsenergien på jorden havner. Begge begrepene er nært beslektet (i begge tilfeller er det en fordeling), men de gjelder ikke det samme: det ene dreier seg om karbon, det andre om energi. Det som bidrar enda mer til forvirringen, er at atmosfæren og havet er til nevnt på begge kort.
