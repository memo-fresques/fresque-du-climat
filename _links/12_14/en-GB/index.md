---
fromCardId: '12'
toCardId: '14'
status: invalid
---

The idea here is not to say there is no link between these two cards, but to make sure they are not mixed up. The carbon sinks card tells us where the carbon goes. The energy budget card tells us where the excess energy goes. Both distribute something, but not the same thing. To make it even more confusing, the atmosphere and the ocean are present in both.
