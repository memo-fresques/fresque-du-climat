---
fromCardId: '12'
toCardId: '14'
status: invalid
---
Lo scopo qui non è dire che non c'è alcuna relazione di causa-effetto tra queste due carte, ma di avvertire del rischio di confonderle. La prima (Pozzi di carbonio) ci dice dove va il carbonio. La seconda (Bilancio energetico) ci dice dove va l'energia in eccesso sulla Terra. Le due nozioni sono simili (in entrambi i casi si tratta di una distribuzione), ma non riguardano la stessa cosa: da un lato il carbonio, dall'altro l'energia. Ciò che contribuisce ancora di più a creare confusione è che l'atmosfera e l'oceano sono presenti in entrambi i casi.
