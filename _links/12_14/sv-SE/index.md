---
fromCardId: '12'
toCardId: '14'
status: invalid
---
Syftet här är inte att säga att det inte finns något orsakssamband mellan dessa två kartor, utan att varna för risken att förväxla dem. Den första (Kolsänka), visar vart kolet tar vägen. Den andra (Energibudget) visar vart överskottsenergin på jorden tar vägen. De två begreppen är nära (i båda fallen handlar det om en fördelning), men de gäller inte samma sak: å ena sidan kolet, å andra sidan energin. Det som bidrar ännu mer till förvirringen är att atmosfären och havet finns på båda sidor.
