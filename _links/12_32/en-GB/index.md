---
fromCardId: '12'
toCardId: '32'
status: optional
---

Studies have shown that yields increase with increased CO2, but that the nutrient content of vegetables is reduced as a result, because trace elements are not more abundant when yields increase.
