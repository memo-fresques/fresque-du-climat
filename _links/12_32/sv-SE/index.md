---
fromCardId: '12'
toCardId: '32'
status: optional
---
Studier har visat att skördar ökar med ökad koncentration av koldioxid i atmosfären, men att näringsinnehållet i grönsaker minskar som ett resultat, eftersom spårämnen inte är rikligare när skördarna ökar.
