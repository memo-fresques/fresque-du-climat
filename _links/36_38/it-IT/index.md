---
fromCardId: '36'
toCardId: '38'
status: valid
---
Il corpo umano non può sopportare temperature troppo elevate per lunghi periodi di tempo. Le persone sono particolarmente esposte nelle aree urbane e ancora più a rischio sono gli anziani e i giovani. Un forte calore può causare disidratazione, diminuzione della concentrazione e delle capacità cognitive. Per quanto riguarda i bambini, i pronto soccorso pediatrici ricevono sempre più visite per disturbi legati al caldo. Le ondate di calore aumentano anche il rischio di mortalità, a causa del caldo eccessivo o dell’aumento dell’inquinamento atmosferico come l’ozono. Durante l’estate del 2003, le ondate di calore hanno causato quasi 20.000 morti in Italia e 70.000 in Europa. In più, favoriscono gli incendi (vedi carta relativa).  
_FONTI: AR6 WG2 6.2.2.1 (città) p934 (p922) // AR6 WG2 FAQ 6.2 p1005 (p993) // AR6 SYR Figura 3.2 p38 // AR6 WG2 Figura 6.3 p935 (p923)_
