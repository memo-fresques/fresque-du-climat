---
fromCardId: '36'
toCardId: '38'
status: valid
---
Le corps humain ne peut pas supporter des températures trop élevées sur de grandes durées. Les gens sont particulièrement exposés en milieu urbain.C'est d'autant plus vrai pour les personnes agées et les jeunes. Une forte chaleur est associée avec des deshydratation diminuant la concentration et les capacités cognitives. Chez les enfants, les urgences pédiatriques ont eu de plus en plus de visites pour des maladies liées à la chaleur. Les canicules augmentent aussi le risque de mortalité, soit par des chaleurs trop élevées, soit en augmentant la pollution de l'air comme l'ozone. Pendant l'été 2003, la canicules a causé 15 000 morts en France, 70 000 en Europe. Y'a aussi plus d'incendies (voir carte concernée).  
_SOURCES: AR6 WG2 6.2.2.1 (cities) p934 (p922) // AR6 WG2 FAQ 6.2 p1005 (p993) // AR6 SYR Figure 3.2 p38 // AR6 WG2 Figure 6.3 p935 (p923)_
