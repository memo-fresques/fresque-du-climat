---
fromCardId: '11'
toCardId: '13'
status: valid
---
Du kan känna igen molekylerna för de olika växthusgaserna på kort 13: H2O (röd och vit), CO2 (grå och röd), CH4 (grå och vit) samt lustgas (blå och röd).
