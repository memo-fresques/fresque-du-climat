---
fromCardId: '11'
toCardId: '13'
status: valid
---
Plus la concentration en CO2 atmosphérique est grande et plus le climat se réchauffe.  
_SOURCES: AR6 WG3 TS.3 p11 (p59) // AR6 WG1 Figure SPM.10 p45 (p28)_
