---
fromCardId: '11'
toCardId: '13'
status: valid
---
Jo høyere konsentrasjonen av CO2 i atmosfæren er, desto mer varmes klimaet opp.  
_KILDER: AR6 WG3 TS.3 s11 (s59) // AR6 WG1 Figur SPM.10 s45 (s28)_
