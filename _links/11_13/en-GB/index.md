---
fromCardId: '11'
toCardId: '13'
status: valid
---

You can recognize the molecules of the different GHGs on the card 13 : H2O (red and white), CO2 (grey and red), CH4 (grey and white), nitrious (blue and red).
