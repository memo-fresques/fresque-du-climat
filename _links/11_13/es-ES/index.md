---
fromCardId: '11'
toCardId: '13'
status: valid
---
Cuanto mayor es la concentración de CO2 atmosférico, más cálido se vuelve el clima.  
_SOURCES: AR6 WG3 TS.3 p11 (p59) // AR6 WG1 Figure SPM.10 p45 (p28)_
