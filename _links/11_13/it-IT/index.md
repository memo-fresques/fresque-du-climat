---
fromCardId: '11'
toCardId: '13'
status: valid
---
Maggiore è la concentrazione di CO2 nell'atmosfera, più si riscalda il clima. 
_FONTE: AR6 WG3 TS.3 p11 (p59) // Figura SPM.10 AR6 WG1 p45 (p28)_
