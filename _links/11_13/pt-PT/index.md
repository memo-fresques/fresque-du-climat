---
fromCardId: '11'
toCardId: '13'
status: valid
---
Quanto maior for a concentração de CO2 atmosférico, mais o clima aquece.
_FONTES: AR6 WG3 TS.3 p11 (p59) // AR6 WG1 Figura SPM.10 p45 (p28)_
