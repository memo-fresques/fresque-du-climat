---
fromCardId: '26'
toCardId: '32'
status: valid
---
Les évènements extrêmes peuvent détruire ou baisser les rendements agricoles. C'est le cas par exemple des inondations. De telles inondations ont par exemple couté 4.5 milliards de dollars en 2010 au Pakistan et 572 millions de dollars en 2015 au Myanmar.  
_SOURCES: AR6 WG2 TS.B.2.3 p14 (p48) // AR6 WG2 5.4.1.1 p741 (p729) // AR6 WG2 Table 4.3 p575 (p587)_
