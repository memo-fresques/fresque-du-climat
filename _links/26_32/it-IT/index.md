---
fromCardId: '26'
toCardId: '32'
status: valid
---
Gli eventi estremi possono distruggere o ridurre le rese agricole. È il caso, ad esempio, delle inondazioni. Le inondazioni hanno causato 4,5 miliardi di dollari di danni in Pakistan nel 2010 e 572 milioni di dollari in Myanmar nel 2015. 
_FONTI: AR6 WG2 TS.B.2.3 p14 (p48) // AR6 WG2 5.4.1.1 p741 (p729) // AR6 WG2 Tabella 4.3 p575 (p587)_
