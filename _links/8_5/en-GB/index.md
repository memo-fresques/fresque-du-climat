---
fromCardId: '8'
toCardId: '5'
status: optional
---

Agriculture does not use much fossil fuel, just enough to keep tractors running. Its carbon emissions are high, bu mainly because of methane and nitrous oxide.
