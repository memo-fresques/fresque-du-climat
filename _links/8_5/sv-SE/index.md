---
fromCardId: '8'
toCardId: '5'
status: optional
---
Jordbruket använder inte mycket fossila bränslen. Bara lite bensin för traktorerna. Däremot släpper det ut andra växthusgaser som metan från idisslarnas rapar och risodling eller N2O som kommer från gödsel som används.  
_KÄLLOR: AR6 WG3 TS.3 s17 (s65) // AR6 WG3 Figur TS.6
AR6 WG3 Figur 7.3 s18 (s66)
s769 (s756)_
