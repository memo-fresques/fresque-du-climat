---
fromCardId: '1'
toCardId: '25'
status: optional
---
Par ce lien, on exprime toutes les dégradations que l'Homme est capable d'infliger à la vie terrestre comme la destruction des habitats (déforestation) par exemple. C'est hors-sujet par rapport au changement climatique, mais c'est intéressant de faire le lien quand-même.  
_SOURCES: IPBES SPM.11 p34 // IPBES Figure SPM.2 p31_
