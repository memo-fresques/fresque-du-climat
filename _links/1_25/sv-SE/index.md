---
fromCardId: '1'
toCardId: '25'
status: optional
---
Människor upptar nästan allt tillgängligt utrymme på jorden och lämnar inget utrymme för djur och växter. Detta innebär att naturliga livsmiljöer försvinner och det är den främsta orsaken till förlust av biologisk mångfald i dag, långt före klimatförändringarna.
