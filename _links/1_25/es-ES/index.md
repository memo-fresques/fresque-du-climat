---
fromCardId: '1'
toCardId: '25'
status: optional
---
Esta relación representa todo el impacto que la especie humana inflige sobre a la vida en el planeta tierra, como la destrucción de los habitats (por la deforestación).
