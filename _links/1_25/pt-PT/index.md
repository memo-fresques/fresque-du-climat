---
fromCardId: '1'
toCardId: '25'
status: optional
---
Com esta ligação, ilustramos todas as degradações que o Homem é capaz de infligir à vida terrestre, como a destruição de habitats (Desflorestação), por exemplo. Está fora do contexto das alterações climáticas, mas é interessante fazer a ligação na mesma.  
_FONTES: IPBES SPM.11 p34 // IPBES Figura SPM.2 p31_
