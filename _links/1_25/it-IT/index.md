---
fromCardId: '1'
toCardId: '25'
status: optional
---
Questo collegamento evidenzia tutte le degradazioni che l'umanità è in grado di infliggere alla vita terrestre, come ad esempio la distruzione degli habitat (deforestazione). Questo è fuori tema rispetto al cambiamento climatico, ma è comunque interessante fare il collegamento. 
_FONTI: IPBES SPM.11 p34 // IPBES Figura SPM.2 p31_
