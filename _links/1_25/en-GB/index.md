---
fromCardId: '1'
toCardId: '25'
status: optional
---

Humans occupy almost all available space on Earth, leaving no room for animals and plants. This entails the disappearance of natural habitats and it is the main cause of biodiversity loss today, well ahead of climate change.
