---
fromCardId: '1'
toCardId: '25'
status: optional
---
Ved denne koblingen uttrykkes alle skadene som mennesket er i stand til å påføre livet på jorden, som for eksempel ødeleggelse av habitater. Det har ikke nødvendigvis noe med klimaendringer å gjøre, men det er interessant å gjøre koblingen likevel.  
_KILDER: IPBES SPM.11 s34 // IPBES Figur SPM.2 s31_
