---
fromCardId: '18'
toCardId: '13'
status: invalid
---
Här finns en förväxling med albedoeffekten. Den vita havsisen som smälter lämnar plats för en mycket mörkare yta och den absorberade energin värmer upp jorden. Denna mekanism kallas albedo och har inget att göra med växthuseffekten. Det påverkar de orange pilarna på kort 13 och inte de röda pilarna.
