---
fromCardId: '18'
toCardId: '13'
status: invalid
---
Atención a esta relación que puede crear confusión. El hielo marino blanco que se derrite da lugar a una superficie mucho más oscura y la energía absorbida calienta la tierra. Este meacanismo se llama albedo y no tiene nada que ver con el efecto invernadero. Hay que clarificar que el albedo afecta las flechas naranjas de la carta 13 y  no a las flechas rojas, por lo tanto no va aumentar el efecto invernadero.
