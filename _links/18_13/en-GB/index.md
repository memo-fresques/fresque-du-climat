---
fromCardId: '18'
toCardId: '13'
status: invalid
---

This link would mark confusion with the amplifying effect due to albedo. The white sea ice melts to reveal a darker surface underneath and the extra energy absorbed warms the Earth. This mechanism is called albedo and has nothing to do with the greenhouse effect. It belongs to the orange arrows on card 13, not to the red arrows.
