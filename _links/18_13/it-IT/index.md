---
fromCardId: '18'
toCardId: '13'
status: invalid
---
C'è confusione qui per quanto riguarda l'effetto di amplificazione dovuto all'albedo. La banchisa bianca che si scioglie lascia spazio a una superficie molto più scura e l'energia assorbita riscalda la terra. Questo meccanismo è chiamato albedo e non ha nulla a che fare con l'effetto serra. Questo influenza le frecce arancioni della carta 13 e non le frecce rosse.
