---
fromCardId: '25'
toCardId: '32'
status: valid
---
L'érosion de la biodiversité et notamment la perte de polinisateurs affecte les rendements agricoles. Par exemple, un facteur clé est la diversité des espèces d'abeilles (plus que l'abondance).  
_SOURCES: AR6 WG2 TS.E.4.1 p75 (p109) // AR6 WG2 2.5.4 p279 (p291)_
