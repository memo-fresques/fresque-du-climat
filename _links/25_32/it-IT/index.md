---
fromCardId: '25'
toCardId: '32'
status: valid
---
L'erosione della biodiversità e in particolare la perdita di impollinatori influisce sulle rese agricole. Ad esempio, un fattore chiave è la diversità delle specie di api (più che l'abbondanza). 
_FONTI: AR6 WG2 TS.E.4.1 p75 (p109) // AR6 WG2 2.5.4 p279 (p291)_
