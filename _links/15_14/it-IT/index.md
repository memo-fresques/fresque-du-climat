---
fromCardId: '15'
toCardId: '14'
status: valid
---
L'effetto serra addizionale provoca un forzante radiativo positivo. L'energia in eccesso si distribuisce nell'oceano (93%), nella vegetazione (5%), nel ghiaccio (3%) e nell'atmosfera (1%). Questa energia riscalda le diverse componenti terrestri in cui si distribuisce, aumenta la loro temperatura e, nel caso dei ghiacci, li fa sciogliere. Il testo sul retro della carta 14 non lascia adito a dubbi. Per questo motivo, se si vuole giocare con la versione semplificata, è necessario eliminare entrambe le carte. 
_FONTI: AR6 WG1 TS3.1 (bilancio energetico) p59 (p93) // AR6 WG1 Figura TS.13 (d): bilancio energetico p58_
