---
fromCardId: '15'
toCardId: '14'
status: valid
---
O efeito de estufa adicional provoca um forçamento radiativo positivo. O excesso de energia é distribuído no oceano (93%), na vegetação (5%), no gelo (3%) e na atmosfera (1%). Esta energia aquece os diferentes compartimentos terrestres nos quais é distribuída, aumenta a sua temperatura e, no caso do gelo, derrete-o. O texto no verso da carta 14 não deixa espaço para ambiguidades. É por isso que é necessário remover estas duas cartas ao mesmo tempo se quisermos ter uma versão simplificada.  
_FONTES: AR6 WG1 TS3.1 (balanço energético) p59 (p93) // AR6 WG1 Figura TS.13 (d) : balanço energético p58_
