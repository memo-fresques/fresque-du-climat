---
fromCardId: '15'
toCardId: '14'
status: valid
---
L'effet de serre additionnel provoque un forçage radiatif positif. Le surplus d'énergie se répartie dans l'océan (93%), la végétation (5%), la glace (3%) et l'atmosphère (1%). Cette énergie chauffe les différents compartiments terrestres dans lesquels elle se répartie, augmente leur température, et, dans le cas glaces, les fait fondre. Le texte au verso de la carte 14 ne laisse place à aucune ambiguïté. C'est pour cette raison qu'il faut supprimer ces deux cartes en même temps si on veut avoir une version simplifiée.  
_SOURCES: AR6 WG1 TS3.1 (bilan énergétique) p59 (p93) // AR6 WG1 Figure TS.13 (d) : bilan énergétique p58_
