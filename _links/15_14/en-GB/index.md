---
fromCardId: '15'
toCardId: '14'
status: valid
---

The text behind card n14 allows no ambiguity. Therefore the 2 cards have to be skipped together or not at all.
