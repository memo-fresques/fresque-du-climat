---
fromCardId: '35'
toCardId: '25'
status: optional
---

This is a more minor link, except for specific biodiversity-rich areas.
