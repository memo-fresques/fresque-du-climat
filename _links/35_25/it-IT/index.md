---
fromCardId: '35'
toCardId: '25'
status: optional
---
Gli incendi influenzano il funzionamento e la struttura degli ecosistemi e della biodiversità che ospitano.  
_FONTI: AR6 WG3 7.3.1.4 : Fire Regime changes (cambiamenti del regime degli incendi) p783 (p770)_
