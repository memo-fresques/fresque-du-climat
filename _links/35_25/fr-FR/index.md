---
fromCardId: '35'
toCardId: '25'
status: optional
---
Les incendies affectent le fonctionnement et la structure des écosystèmes et de la biodiversité qu'ils abritent.  
_SOURCES: AR6 WG3 7.3.1.4 : Fire Regime changes p783 (p770)_
