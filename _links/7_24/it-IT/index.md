---
fromCardId: '7'
toCardId: '24'
status: invalid
---
Le emissioni di CO2 che vanno nell'oceano lo acidificano. È anche possibile far partire la freccia dalla carta dei pozzi di carbonio. 
_FONTI: AR6 WG1 Box TS.5 p48 // AR5 WG1 Figura 6.8 p503 // AR6 WG1 Figura SPM.7 (pozzi e scenari) p20_
