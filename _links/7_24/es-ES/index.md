---
fromCardId: '7'
toCardId: '24'
status: invalid
---
Las emisiones de CO2 que van al océano lo acidifican. También podemos hacer que la flecha de esta relación salga de la carta de sumideros de carbono.  
_SOURCES: AR6 WG1 Box TS.5 p48 // AR5 WG1 Figura 6.8 p503 // AR6 WG1 Figura RRP.7 (pozos y escenarios) p20_
