---
fromCardId: '13'
toCardId: '15'
status: valid
---
Den ekstra drivhuseffekten holder mer varme på jorden, den utøver en positiv strålingspådriv. Den forårsaker et overskudd av energi på jorden sammenlignet med 1850-1900, og derfor blir jorden varmere.  
_KILDER: AR6 WG3 TS.3 s11 (s59) // AR6 WG1 Figur 7. : Pådrivstemperatur s979 (962)_
