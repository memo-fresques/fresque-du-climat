---
fromCardId: '13'
toCardId: '15'
status: valid
---
El efecto invernadero adicional hace que se retenga más calor en la Tierra, ejerciendo lo que se conoce como un forzamiento radiativo positivo, provocando un exceso de energía en la Tierra en comparación con 1850-1900 y, como consecuencia, la Tierra se calienta.  
_SOURCES: AR6 WG3 TS.3 p11 (p59) // AR6 WG1 Figure 7. : Température de forçage p979 (962)_
