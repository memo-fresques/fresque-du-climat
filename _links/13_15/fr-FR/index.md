---
fromCardId: '13'
toCardId: '15'
status: valid
---
L'effet de serre additionnel garde plus de chaleur sur Terre, il exerce un forçage radiatif positif. Il occasionne un surplus d'énergie sur Terre par rapport à 1850-1900 et donc la Terre se réchauffe.  
_SOURCES: AR6 WG3 TS.3 p11 (p59) // AR6 WG1 Figure 7. : Température de forçage p979 (962)_
