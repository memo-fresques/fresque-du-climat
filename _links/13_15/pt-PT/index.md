---
fromCardId: '13'
toCardId: '15'
status: valid
---
O efeito de estufa adicional retém mais calor na Terra, exercendo um forçamento radiativo positivo. Tal causa um excesso de energia na Terra em comparação com 1850-1900 e, portanto, a Terra aquece.  
_FONTES: AR6 WG3 TS.3 p11 (p59) // AR6 WG1 Figura 7. : Temperatura de forçamento p979 (962)_
