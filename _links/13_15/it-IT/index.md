---
fromCardId: '13'
toCardId: '15'
status: valid
---
L'effetto serra addizionale trattiene più calore sulla Terra, esercitando un forzante radiativo positivo. Ciò comporta un eccesso di energia sulla Terra rispetto al periodo 1850-1900 e quindi la Terra si riscalda. 
_FONTI: AR6 WG3 TS.3 p11 (p59) // AR6 WG1 Figura 7: Temperature di forzante p979 (962)_
