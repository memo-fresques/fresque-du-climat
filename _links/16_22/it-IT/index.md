---
fromCardId: '16'
toCardId: '22'
status: valid
---
Dal 1900, il livello del mare è aumentato in media di 20 cm. Tra il 1971 e il 2018, circa la metà dell'innalzamento del livello del mare è stata causata dall'espansione termica degli oceani (50,4%). Il secondo contributo più importante è dato dalla fusione dei ghiacciai con il 22,2%, seguito dalla fusione della Groenlandia (12,6%) e dell'Antartide (7,1%). Anche se i ghiacciai contribuiscono molto all'attuale innalzamento del livello del mare, la loro completa fusione aumenterebbe il livello del mare solo di 0,3 m, mentre la Groenlandia e l'Antartide hanno il potenziale di aumentare il livello del mare rispettivamente di 7 m e 58 m. 
_FONTI: AR6 WG1 Box TS.4 p45 // AR6 WG1 Cross-Chapter 9.1, Figura 1: contributo all'aumento del livello dell'acqua p1308 (p1291) // AR6 WG1 Box TS.4, Figura 1: scenari di aumento del livello dell'acqua p46 (p78)_
