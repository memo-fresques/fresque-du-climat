---
fromCardId: '16'
toCardId: '22'
status: valid
---

A melting of 100 gigatons of ice per year is equivalent to about 0.28 mm per year of mean sea level rise. Thus, 15 to 35% of sea level rise is linked to the melting of glaciers, according the IPCC scenarios. 30% to 35% is linked to the expansion of water. The remainder is linked to the melting of the ice caps.
