---
fromCardId: '16'
toCardId: '22'
status: valid
---
Depuis 1900, le niveau des mers est montée en moyenne de 20cm. Entre 1971 et 2018, environ la moitié de la montée des eaux était due à l'expansion thermique des océans (50.4%). La fonte des glaciers est le 2e contributeur avec 22.2% suivi de la fonte du groënland (12.6%), de la fonte de l'Antarctique (7.1%). Bien que les glaciers contribuent beaucoup à la montée des eaux actuelle, leur fonte intégrale ne ferait monter le niveau des eaux que de 0.3m tandis que le Groënland et l'Antarctique ont le potentiel d'augmenter le niveau des eaux de 7m et 58m respectivement.  
_SOURCES: AR6 WG1 Box TS.4 p45 // AR6 WG1 Cross-Chapter 9.1, Figure 1 : contribution à la montée des eaux p1308 (p1291) // AR6 WG1 Box TS.4, Figure 1 : scenarios montée des eaux p46 (p78)_
