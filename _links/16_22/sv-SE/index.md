---
fromCardId: '16'
toCardId: '22'
status: valid
---
En smältning av 100 gigaton is per år motsvarar cirka 0,28 mm per år av medelhavsnivåhöjning. Således är 15–35 % av havsnivåhöjningen kopplad till smältningen av glaciärer, enligt IPCC:s scenarier. 30–35% är kopplat till expansion av vatten. Resten är kopplat till istäckenas smältning.
