---
fromCardId: '20'
toCardId: '25'
status: valid
---
La mancanza di acqua può anche influire direttamente sulle specie che ne dipendono. 
_FONTI: AR6 WG1 4.5.5 p629 (p617) // AR6 WG2 Figura 4.2: schema del ciclo dell'acqua p577 (p565)_
