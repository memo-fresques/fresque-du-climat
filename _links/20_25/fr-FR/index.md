---
fromCardId: '20'
toCardId: '25'
status: valid
---
Le manque d'eau peut aussi affecter directement les espèces qui en dépendent.  
_SOURCES: AR6 WG1 4.5.5 p629 (p617) // AR6 WG2 Figure 4.2 : schéma cycle de l'eau p577 (p565)_
