---
fromCardId: '21'
toCardId: '28'
status: optional
---
I cambiamenti climatici (temperature, siccità, inondazioni) possono portare alla migrazione di specie e vettori di malattie in nuove regioni. A volte, queste malattie sono particolarmente problematiche perché si verificano in regioni dove gli animali e la popolazione non sono immunizzati. Questi cambiamenti possono anche facilitare il ciclo di vita dei patogeni e dei vettori di patogeni favorendone la sopravvivenza, lo sviluppo o il desiderio di pungere! Tuttavia, alcune malattie possono diminuire localmente, come la malaria in alcune regioni dove farà troppo caldo. La carta 28 (vettori di malattie) è generalmente correlata alla carta 25 (Biodiversità terrestre) poiché i vettori di malattie sono un sottogruppo della biodiversità, ma può anche essere correlata alle stesse cause della carta 25, ovvero 20 (Ciclo dell'acqua) e 21 (Temperatura). 
_FONTI: AR6 WG2 FAQ 2.2 p21 // AR6 WG2 2.4.7.2 p244 (p232)_
