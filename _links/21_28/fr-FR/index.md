---
fromCardId: '21'
toCardId: '28'
status: optional
---
Les changements climatiques (températures, sécheresses, innondations) peuvent conduire à la migration d'espèces et de vecteurs de maladies dans de nouvelles régions. Parfois, ces maladies sont particulièrement problématiques car elles surviennent dans des régions où les animaux et la population ne sont pas immunisées. Ces changements peuvent aussi faciliter le cycle de vie des pathogènes et vecteurs de pathogènes favorisant leur survie, leur développement ou leurs envies de piquer!  Notons tout de même que certaines maladies peuvent localement diminuer comme la malaria dans certaines régions où il fera trop chaud. La carte 28 (vecteurs de maladie), est en général reliée à la carte 25 (Biodiversité terrestre) car les vecteurs de maladie sont une sous-partie de la biodiversité, mais on peut aussi la relier aux mêmes causes que la carte 25, cest-à-dire 20 (Cycle de l'eau) et 21 (Température).  
_SOURCES: AR6 WG2 FAQ 2.2 p21 // AR6 WG2 2.4.7.2 p244 (p232)_
