---
fromCardId: '21'
toCardId: '28'
status: optional
---
Kortet 'Sjukdomsbärare' är i allmänhet kopplade till kortet om landlevande biologiska mångfald eftersom sjukdomsbärare är en del av den biologiska mångfalden, men det kan också kopplas till samma orsaker som kortet för biologisk mångfald, det vill säga störningar av vattnets kretslopp och stigande lufttemperaturer.
