---
fromCardId: '21'
toCardId: '28'
status: optional
---
Los cambios climáticos (temperaturas, sequías, inundaciones) pueden llevar a la migración de especies y vectores de enfermedades a nuevas regiones. A veces, estas enfermedades son particularmente problemáticas porque ocurren en regiones donde los animales y la población no están inmunizados. Estos cambios también pueden facilitar el ciclo de vida de los patógenos y vectores de patógenos, favoreciendo su supervivencia, desarrollo o sus ganas de picar. No obstante, cabe señalar que algunas enfermedades pueden disminuir localmente, como la malaria en algunas regiones donde hará demasiado calor. La carta 28 (vectores de enfermedades) está generalmente relacionada con la carta 25 (Biodiversidad terrestre) porque los vectores de enfermedades son una subparte de la biodiversidad, pero también se puede relacionar con las mismas causas que la carta 25, es decir, 20 (Ciclo del agua) y 21 (Temperatura). 
_FUENTES: AR6 WG2 FAQ 2.2 p21 // AR6 WG2 2.4.7.2 p244 (p232)_
