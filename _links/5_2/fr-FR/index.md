---
fromCardId: '5'
toCardId: '2'
status: optional
---
Les énergies fossiles permettent aux activités humaines d'exister telles qu'on les connait. Mais dans la fresque, on place généralement la carte des ressources fossiles en avale des activités humaines. D'ailleurs la carte montre les émissions de CO2 liées à l'usage des ressources fossiles.
