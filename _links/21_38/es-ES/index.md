---
fromCardId: '21'
toCardId: '38'
status: optional
---
Estudios recientes sugieren que los futuros aumentos en las temperaturas medias podrían exponer a las poblaciones en vastas áreas de los trópicos y subtropicales a temperaturas ambientales durante largos períodos cada año que están por encima del umbral de habitabilidad humana.
_FUENTES: AR6 WG2 7.3.2.1 p1112 (p1100)_
