---
fromCardId: '21'
toCardId: '38'
status: optional
---
Studi recenti suggeriscono che i futuri aumenti delle temperature medie potrebbero esporre le popolazioni su vaste aree dei tropici e subtropicali a temperature ambientali oltre la soglia di abitabilità umana per lunghi periodi ogni anno.
_FONTI: AR6 WG2 7.3.2.1 p1112 (p1100)_
