---
fromCardId: '21'
toCardId: '38'
status: optional
---
Des études récentes suggèrent que les augmentations futures à des températures moyennes pourrait exposer les populations sur de vastes zones des tropiques et subtropicaux aux températures ambiantes pendant de longues périodes chaque année qui sont au-delà du seuil d'habitabilité humaine.  
_SOURCES: AR6 WG2 7.3.2.1 p1112 (p1100)_
