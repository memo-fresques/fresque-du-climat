---
fromCardId: '6'
toCardId: '26'
status: optional
---
Vegetationen håller kvar vatten. Att hugga ner träd och ta ner annan växtlighet kan leda till översvämningar.
