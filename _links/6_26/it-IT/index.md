---
fromCardId: '6'
toCardId: '26'
status: optional
---
La deforestazione diminuisce l'evapotraspirazione e le precipitazioni. D'altra parte, aumenta il deflusso e quindi il rischio di esondazioni.  
_FONTI: AR6 WG1 Box TS.6 p54 (p86)_
