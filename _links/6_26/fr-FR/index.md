---
fromCardId: '6'
toCardId: '26'
status: optional
---
La déforestation diminue l'evapotranspiration et les précipitations. D'un autre côté, elle augmente le ruissellement et donc le risque de crues.  
_SOURCES: AR6 WG1 Box TS.6 p54 (p86)_
