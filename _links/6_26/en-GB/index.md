---
fromCardId: '6'
toCardId: '26'
status: optional
---

Vegetation retains water. Cutting it down can lead to flooding.
