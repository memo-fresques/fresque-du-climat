---
fromCardId: '23'
toCardId: '29'
status: valid
---
Gli pteropodi, i coccolitofori e anche i coralli sono colpiti dalle loro difficoltà a calcificare i loro gusci o esoscheletri.  
_FONTI: IBPES 2.2.7.16 p330 // AR6 WG 3.2.3.1 p407 (p395) // https://www.annualreviews.org/doi/abs/10.1146/annurev.marine.010908.163834_
