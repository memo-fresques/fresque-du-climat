---
fromCardId: '23'
toCardId: '29'
status: valid
---
Les ptéropodes, les coccolithophores ou encore les coraux sont affectés par leur difficultés à calcifier leurs coquilles ou exosquelette.  
_SOURCES: IBPES 2.2.7.16 p330 // AR6 WG 3.2.3.1 p407 (p395) // https://www.annualreviews.org/doi/abs/10.1146/annurev.marine.010908.163834_
