---
fromCardId: '26'
toCardId: '28'
status: invalid
---
Attention aux associations d'idées : Crues / marécages / moustiques.
Les crues peuvent amener des situations sanitaires dégradées, mais ce n'est pas de cela qu'on parle quand on évoque les vecteurs de maladie qui se déplacent à cause du changement climatique.  
_SOURCES: AR6 WG2 2.4.7.1 p243 (p231)_
