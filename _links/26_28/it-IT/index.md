---
fromCardId: '26'
toCardId: '28'
status: invalid
---
Attenzione all'associazione di idee: esondazioni-paludi-zanzare.
Le esondazioni possono portare a situazioni sanitarie degradate, ma non è a questo che ci si riferisce quando si parla di vettori di malattie che si spostano a causa del cambiamento climatico.  
_FONTI: AR6 WG2 2.4.7.1 p243 (p231)_
