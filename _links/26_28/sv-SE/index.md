---
fromCardId: '26'
toCardId: '28'
status: invalid
---
Var vaksam över missuppfattningar om översvämningar, träsk, myggor...
Översvämningar kan leda till försämrade sanitära förhållanden, men det är inte det vi pratar om när vi nämner att sjukdomsbärare som sprids på grund av klimatförändringar.  
_KÄLLOR: AR6 WG2 2.4.7.1 s243 (s231)_
