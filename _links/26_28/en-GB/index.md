---
fromCardId: '26'
toCardId: '28'
status: invalid
---

Be wary of common misconceptions about flooding, marshes, mosquitos... Flooding can cause sanitation issues, but that is not what we are talking about here.
