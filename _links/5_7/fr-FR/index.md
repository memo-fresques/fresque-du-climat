---
fromCardId: '5'
toCardId: '7'
status: valid
---
La combustion des ressources fossilles émet du CO2!  
_SOURCES: AR6 WG1 Box TS.5 p48 // AR6 WG1 Figure 5.5 (a) p705_
