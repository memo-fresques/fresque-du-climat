---
fromCardId: '17'
toCardId: '34'
status: valid
---
Les cyclones s’alimentent de l’énergie
des eaux chaudes à la surface de
l’océan. Leur puissance a augmenté à cause du changement climatique.  
_SOURCES: AR6 WG1 TS.2.3 p39 (p71) // https://www.notre-planete.info/terre/risques_naturels/cyclones.php_
