---
fromCardId: '17'
toCardId: '34'
status: valid
---
I cicloni sono alimentati dall'energia delle acque calde sulla superficie dell'oceano. La loro potenza è aumentata a causa del cambiamento climatico.  
_FONTI: AR6 WG1 TS.2.3 p39 (p71) // https://www.notre-planete.info/terre/risques_naturels/cyclones.php_
