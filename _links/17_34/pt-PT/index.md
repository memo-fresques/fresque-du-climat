---
fromCardId: '17'
toCardId: '34'
status: valid
---
Os ciclones extraem a sua energia das águas quentes da superfície do oceano. Devido às alterações climáticas, são cada vez mais fortes.
_FONTES: AR6 WG1 TS.2.3 p39 (p71) // https://www.notre-planete.info/terre/risques_naturels/cyclones.php_
