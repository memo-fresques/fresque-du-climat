---
fromCardId: '31'
toCardId: '32'
status: valid
---
Il 23% delle colture sono irrigate, fornendo il 34% della produzione calorica. Tra queste, il 68% soffre di carenza idrica 1 mese all'anno e il 37% fino a 5 mesi all'anno. Tra il 1983 e il 2009, 454 milioni di ettari hanno subito perdite di rendimento legate alla siccità.  
_FONTI: AR6 WG2 TS.B.4.6 p16 (p50) // AR6 WG2 4.3.1 p584 (p596) // AR6 WG2 Figura 4.20 p621 (p633)_
