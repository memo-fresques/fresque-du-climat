---
title: Sumidouros de Carbono
backDescription: >-
  Metade do CO2 que emitimos todos os anos é absorvido pelos sumidouros de
  carbono: - ¼ pela vegetação (através da fotossíntese) - ¼ pelo oceano. A
  metade restantes (1/2) permanece na atmosfera.
---
"O gráfico original do IPCC representa simultaneamente as emissões de CO2 e os sumidouros de carbono. A Climate Fresk optou por dividi-lo em dois para mostrar, por um lado, de onde vem o CO2 e, por outro, para onde ele vai. É por isso que os dois gráficos são exatamente simétricos: todos os anos, o CO2 emitido pelo Homem deve ir para algum lugar. Todo o CO2 que não é absorvido pelos sumidouros de carbono permanece na atmosfera. O texto no verso do gráfico fornece a distribuição aproximada das taxas de absorção. As últimas estimativas são: 23% para o oceano, 31% para a fotossíntese[^1].
[^1]: _AR6 WG1 Box TS.5 p48 // AR5 WG1 Figure 6.8 p503 (p487)_"
