---
title: Rezerwuary węgla
backDescription: >-
  Połowę emitowanego przez nas rocznie CO2 pochłaniają naturalne rezerwuary
  węgla: - roślinność w procesie fotosyntezy (1/4) - oceany (1/4) Reszta (1/2)
  pozostaje w atmosferze.
---
Oryginalny wykres IPCC przedstawia zarówno emisje CO2, jak i rezerwuary dwutlenku węgla. Mozaika Klimatyczna zdecydowała się podzielić go na dwie części, aby pokazać, skąd CO2 pochodzi i dokąd trafia. Dlatego też dwie karty są symetryczne: każdego roku CO2 emitowany przez ludzkość musi gdzieś trafić. CO2, które nie zostanie pochłonięte przez inne rezerwuary, pozostaje w atmosferze. Tekst z tyłu karty podaje przybliżone wskaźniki absorpcji. Bardziej szczegółowe wartości procentowe to: 27,9% dla oceanu, 28,8% dla fotosyntezy.
