---
title: Úložiště uhlíku
backDescription: >-
  Polovinu CO2, který každoročně vytvoříme, absorbují tzv. propady uhlíku: - 1/4
  absorbuje vegetace (díky fotosyntéze) - 1/4 absorbují oceány

  Druhá polovina zůstává v atmosféře.
---

