---
title: Szénelnyelők
backDescription: >-
  Az általunk évente kibocsátott CO2 felét szénelnyelők kötik meg: - 1/4-ét a
  vegetáció (fotoszintézisen keresztül) - 1/4-ét az ócán A megmaradó másik fele
  (1/2) a légkörben marad.
---

