---
title: Karbono hustulekuak
backDescription: >-
  Karbono hustulekuek, urtean isurtzen dugun CO₂-aren erdia xurgatzen dute: -
  landarediak laurdena (fotosintesiaren bidez) - ozeanoak laurdena -
  Gaineratekoa (erdia), atmosferan gelditzen da.
---

