---
title: Rezervoare naturale de carbon
backDescription: >-
  Jumătate din CO2 pe care îl emitem în fiecare an este absorbit de rezervoarele
  de carbon: - 1/4 de vegetație (prin fotosinteză) - 1/4 de ocean Restul (1/2)
  rămâne în atmosferă.
---

Rezervoare naturale de carbon
