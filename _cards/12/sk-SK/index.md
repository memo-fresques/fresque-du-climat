---
title: Absorbéry uhlíka
backDescription: >-
  Polovica CO2, ktorú vypustíme každý rok, je zachytená absorbérmi uhlíka: - 1/4
  vegetáciou (cez fotosyntézu) - 1/4 oceánmi Zvyšná polovica (1/2) ostáva v
  atmosfére.
---

