---
title: Carbon Sinks
backDescription: >-
  Half of the CO2 we emit every year is absorbed by carbon sinks: - 1/4 by
  vegetation (through photosynthesis) - 1/4 by the ocean The remaining half
  stays in the atmosphere.
---

