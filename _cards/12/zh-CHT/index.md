---
title: 碳匯(Carbon Sink)
backDescription: 每年我們釋放的二氧化碳，有一半會被碳匯(Carbon Sink)吸收(四分之一由植被吸收(光合作用)，四分之一由海洋吸收)，另外一半則會進入大氣層
---

