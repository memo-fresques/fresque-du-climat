---
backDescription: >-
  La moitié du CO2 que nous émettons chaque année est absorbée par les puits de
  carbone : - la végétation pour 1/4 (via la photosynthèse) - l'océan pour 1/4
  Le reste (1/2) reste dans l'atmosphère.
instagramCode: COA0IlZo3zu
title: Trapoù karbon
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_12_puits_de_carbone
youtubeCode: '--H_a7k_sQ8'
---

