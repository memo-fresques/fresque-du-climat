---
title: Kulstofpuljer
backDescription: >-
  Halvdelen af den CO2, vi udleder hvert år, lagres i klodens egne
  kulstofpuljer: -¼ i planter gennem fotosyntese -¼ i oceanerne Den resterende
  halvdel forbliver i atmosfæren.
---

