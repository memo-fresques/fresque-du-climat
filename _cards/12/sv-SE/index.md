---
title: Kolsänkor
backDescription: >-
  Hälften av den koldioxid vi släpper ut årligen aborberas av kolsänkor: - 1/4
  av vegetation via fotosyntes - 1/4 av havet Den resterande halvan stannar i
  atmosfären.
---
"Den ursprungliga IPCC-grafen visar både CO2-utsläpp och kolsänkor. Här i Fresken har detta delts upp på två kort för att visa varifrån CO2 kommer och var det tar vägen. Därför är de två korten exakt symmetriska: varje år måste den CO2 som släpps ut av människan ta vägen någonstans. All CO2 som inte absorberas av kolsänkorna stannar i atmosfären. Texten på baksidan av kortet ger en ungefärlig fördelning av hur mycket som absorberas av olika sänkor. Noggrannare uppskattninga ger: 23% för havet, 31% för fotosyntesen[^1].
[^1]: _AR6 WG1 Box TS.5 s48 // AR5 WG1 Figure 6.8 s503 (s487)"
