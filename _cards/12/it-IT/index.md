---
title: Pozzi di assorbimento di carbonio
backDescription: >-
  Metà della CO2 che emettiamo ogni anno viene assorbita dai pozzi naturali di
  assorbimento di carbonio: 1/4 attraverso la vegetazione (fotosintesi), 1/4
  dall'oceano. Il resto (1/2) rimane nell'atmosfera.
---
Il grafico originale dell'IPCC rappresenta sia le emissioni di CO2 che i pozzi di carbonio. L'Affresco ha scelto di dividerlo in due parti per mostrare da una parte da dove proviene la CO2 e dall'altra dove va. Ecco perché le due carte sono esattamente simmetriche: ogni anno, la CO2 emessa dall'uomo deve andare da qualche parte. Tutta la CO2 che non viene assorbita dai pozzi di carbonio rimane nell'atmosfera. Il testo sul retro della carta fornisce una distribuzione approssimativa dei tassi di assorbimento. Le ultime stime sono: 23% per l'oceano, 31% per la fotosintesi[^1].
[^1]: _AR6 WG1 Box TS.5 p48 // AR5 WG1 Figura 6.8 p503 (p487)_
