---
title: Sumideros de carbono
backDescription: >-
  La mitad del CO2 que emitimos por año permanece en la atmósfera, y la otra
  mitad es absorbida por los sumideros naturales de carbono : - 1/4 por la
  vegetación (vía la fotosíntesis) - 1/4 por el océano.
---

