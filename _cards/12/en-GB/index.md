---
title: Carbon Sinks
backDescription: >-
  Half of the CO2 we emit every year is absorbed by carbon sinks: - 1/4 by
  vegetation via photosynthesis - 1/4 by the oceans The remaining half stays in
  the atmosphere.
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_12_carbon_sinks
youtubeCode: G9dyMMQideU
instagramCode: COA7uJboe2x
---

The original IPCC graph represents both CO2 emissions and carbon sinks. The Climate Fresk has chosen to split it in two to show where CO2 comes from and where it goes. This is why the two cards are symmetrical: every year, the CO2 emitted by humankind has to go somewhere. Any CO2 that is not absorbed by the other carbon sinks remains in the atmosphere. The text at the back of the card gives approximate absorption rates. The more detailed percentages are: 27.9% for the ocean, 28.8% for photosynthesis.
