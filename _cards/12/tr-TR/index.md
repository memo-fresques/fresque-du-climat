---
title: Karbon Yutakları
backDescription: >-
  Her yıl üretilen CO2 emisyonlarının yarısı doğal karbon yutakları tarafından
  tutulur: - 1/4 bitkiler tarafından fotosentez yoluyla - 1/4 okyanuslar
  tarafından Diğer yarısı (1/2) ise atmosferde kalır.
---

