---
title: Spremnici (ponori) ugljika
backDescription: >-
  Polovina CO2 koju godišnje emitiramo sprema se u ponore ugljika: - biljke,
  kroz fotosintezu, spremaju 1/4 - oceani spremaju 1/4

  Preostala polovina ostaje u atmosferi.
---

