---
title: Kohlenstoffsenken
backDescription: >-
  Die Hälfte des CO2, das wir jedes Jahr emittieren, wird von Kohlenstoffsenken
  aufgenommen: - 1/4 von der Vegetation (Photosynthese) - 1/4 vom Ozean Die
  andere Hälfte bleibt in der Atmosphäre.
---

