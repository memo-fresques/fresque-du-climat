---
title: Karbonsluk
backDescription: >-
  Halvparten av alt CO2 vi slipper ut hvert år, absorberes av karbonsluk: - 1/4
  absorberes av vegetasjon (gjennom fotosyntese) - 1/4 absorberes av havet
  Resten (1/2) forblir i atmosfæren.
---
"Det opprinnelige diagrammet fra IPCC viser både CO2-utslipp og karbonlagre. I klimakollasjen er det delt i to for å vise hvor CO2 kommer fra og hvor det går. Derfor er de to kortene nøyaktig symmetriske: hvert år må CO2 som slippes ut av mennesker havne et sted. All CO2 som ikke absorberes av karbonlagrene forblir i atmosfæren. Teksten på baksiden av kortet gir et omtrentlig bilde av hvordan CO2-en fordeler seg. De siste estimatene er: 23% for havet, 31% for fotosyntese[^1].
[^1]: _AR6 WG1 Box TS.5 p48 // AR5 WG1 Figur 6.8 p503 (p487)_"
