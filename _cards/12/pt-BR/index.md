---
title: Sequestros de carbono
backDescription: >-
  Metade do CO2 que emitimos todos os anos é absorvida pelos sequestros de
  carbono: - ¼ pela vegetação (fotossíntese), - ¼ pelo oceano. Os 50% restantes
  permanecem na atmosfera
---

