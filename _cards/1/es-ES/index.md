---
title: Actividades Humanas
backDescription: Aquí es donde todo comienza…
---
Aquí es donde todo empieza ... En el resumen prólogo del sexto informe del IPCC  podemos leer "es inequívoco que las actividades humanas han calentado nuestro clima "[^1]. Podemos agregar que los modelos estiman la participación humana en... 100% (entre 90% y 110%)[^2]. Esta carta puede considerarse como la causa de todas las cartas de sectores los económicos (Industria, El uso de los edificios, Transporte, Agricultura y ganadería), o como la causa agrupada de todas estas cartas (que se agruparan en un gran grupo”). .  
[^1]: _AR6 WG1 Prólogo p6 (pV)_  
[^2]: _AR6 GT1 RRP A.1.3 p22 (p5)_
