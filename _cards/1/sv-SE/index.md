---
title: Mänskliga aktiviteter
backDescription: Allt startar här...
---
Det är här det börjar... Det är också här den 6:e IPCC-rapporten börjar. I förordet kan man läsa att: "the IPCC is now 95 percent certain that human beings are the main cause of current global warming" ("enligt IPCC är det otvetydigt att mänskliga aktiviteter har orsakat global uppvärmning")[^1]. Klimatmodeller uppskattar mänskligt påverkan till ... 100 % (mellan 90 % och 110 %)[^2]. Kortet kan antingen betraktas som orsaken till alla kort som berör den ekonomiska sektorn (industri, användning av byggnader, transporter, jordbruk) eller som en titel för dessa kort (som sedan grupperas tillsammans).  
[^1]: _AR6 WG1 Förord p6 (pV)_  
[^2]: _AR6 WG1 SPM A.1.3 p22 (p5)_
