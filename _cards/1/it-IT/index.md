---
title: Attività umane
backDescription: Qui è dove tutto ha inizio…
---
È da qui che tutto ha inizio....È anche da qui che inizia il sesto rapporto dell'IPCC, nella prefazione del suo riassunto: "non c'è dubbio che le attività umane abbiano riscaldato il nostro clima"[^1]. Si può aggiungere che i modelli stimano l'impatto dell'uomo al... 100% (tra il 90% e il 110%)[^2]. Questa carta può essere considerata sia la causa di tutte le carte dei vari settori economici (Industria, Uso degli edifici, Trasporti, Agricoltura), sia come un titolo per tutte queste carte (che sono quindi raggruppate in un grande "patata").
[^1]: _AR6 WG1 Prefazione p6 (pV)_
[^2]: _AR6 WG1 SPM A.1.3 p22 (p5)_
