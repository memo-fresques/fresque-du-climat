---
backDescription: C'est là que tout commence…
title: Activités humaines
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_1_activit%C3%A9s_humaines
youtubeCode: ZPknjN0hYe8
instagramCode: CMALNkEouzB
---
C'est par là que tout commence....C'est aussi par-là que commence le 6ème rapport du GIEC, dans l'avant-propos de son résumé : "il est sans équivoque que les activités humaines ont réchauffé notre climat"[^1]. On peut ajouter que les modèles estiment l'implication de l'homme à ... 100% (entre 90% et 110%)[^2]. Cette carte peut être considérée soit comme la cause de l'ensemble des cartes secteurs économiques (Industrie, Utilisation des bâtiments, Transport, Agriculture), soit comme un titre de l'ensemble de ces cartes (qui sont alors regroupées dans une grosse “patate“).  
[^1]: _AR6 WG1 Avant Propos p6 (pV)_  
[^2]: _AR6 WG1 SPM A.1.3 p22 (p5)_
