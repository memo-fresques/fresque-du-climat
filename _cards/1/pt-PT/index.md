---
title: Atividades Humanas
backDescription: É aqui que tudo começa...
---
É por aí que tudo começa....É também por aí que começa o 6º relatório do IPCC, no prefácio do seu resumo: "é inequívoco que as Atividades Humanas aqueceram o nosso clima"[^1]. Podemos acrescentar que os modelos estimam a implicação do homem em ... 100% (entre 90% e 110%)[^2]. Esta carta pode ser considerada tanto como a causa de todas os cartas dos setores económicos (Indústria, Utilização dos edifícios, Transportes, Agricultura), ou como um título para todas essas cartas (que podem ser então agrupadas dentro de um grande círculo).  
[^1]: _AR6 WG1 Prefácio p6 (pV)_  
[^2]: _AR6 WG1 SPM A.1.3 p22 (p5)_
