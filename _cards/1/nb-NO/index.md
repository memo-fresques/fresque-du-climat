---
title: Menneskelige aktiviteter
backDescription: Alt starter her...
---
Det er her alt begynner....Det er også her den sjette rapporten fra IPCC begynner, i forordet til sammendraget: "det er utvetydig at menneskelig aktivitet har varmet opp vårt klima"[^1]. Vi kan legge til at modellene anslår menneskets innvirkning til ... 100% (mellom 90% og 110%)[^2]. Dette kortet kan betraktes enten som årsaken til alle de økonomiske sektorkortene (Industri, Bygningsbruk, Transport, Landbruk), eller som en tittel for alle disse kortene (som da er samlet i en stor klynge).  
[^1]: _AR6 WG1 Forord s6 (sV)_  
[^2]: _AR6 WG1 SPM A.1.3 s22 (s5)_
