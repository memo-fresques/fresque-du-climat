---
title: Trasporti
backDescription: >-
  Il settore dei trasporti dipende molto dal petrolio. È responsabile del 15%
  delle emissioni di gas serra.
---
I trasporti rappresentano il 15% delle emissioni di gas serra nel mondo (2019)[^1]. Sembra poco, ma varia molto a seconda dei paesi e degli stili di vita. Nei paesi occidentali, la quota dei trasporti, in particolare quella dell'aviazione, può rappresentare una grande parte dell'impronta di carbonio di ciascuno. Infatti, uno o più voli a lunga percorrenza all'anno costituiscono la maggior parte dell'impronta di carbonio di un individuo[^2]. Il settore dei trasporti ha la particolarità di essere ancora estremamente dipendente dal petrolio e lo sviluppo dell'elettrificazione dei veicoli è una leva importante per decarbonizzare i trasporti[^3].
[^1]: _AR6 WG3 TS.3 p17 (p65) // AR6 WG3 Figura TS.6 p18 (p66)_
[^2]: _Calcolare la propria impronta di carbonio:_ (https://calculator.carbonfootprint.com/calculator.aspx?lang=it)
[^3]: _AR6 WG3 10.3.2 p1082 (p1069)_
