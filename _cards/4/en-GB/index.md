---
title: Transportation
backDescription: >-
  The transportation sector is highly dependent on oil. It accounts for 15% of
  greenhouse gas emissions.
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_4_transportation
youtubeCode: oUx3gsVREKw
instagramCode: CLKOV2JHWQv
---

15% is not much, but it varies a lot depending on the country and lifestyle. In Western countries, the share of transportation, especially air travel, can account for a significant part of people's carbon footprint. If you take one or more long haul flights a year, that's the bulk of your carbon footprint.
