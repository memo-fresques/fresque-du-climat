---
title: Transport
backDescription: >-
  Transportsektoren er veldig avhengig av olje. Den står for 15 % av de totale
  klimagassutslippene.
---
Transport utgjør 15% av klimagassutslippene i verden (2019)[^1]. Det er lite, men det varierer mye avhengig av land og livsstil. I vestlige land kan andelen transport, spesielt fly, utgjøre en stor del av karbonavtrykket til hver enkelt. Hvis du tar en eller noen få langdistanseflyvninger i året, utgjør dette hoveddelen av ditt karbonregnskap[^2]. Transportsektoren er fortsatt ekstremt avhengig av olje, og elektrifisering av kjøretøy er en viktig faktor for å redusere karbonutslippene fra transportsektoren[^3].  
[^1]: _AR6 WG3 TS.3 s17 (s65) // AR6 WG3 Figur TS.6 s18 (s66)_
[^2]: _Beregn ditt karbonavtrykk:_ [agirpourlatransition.ademe.fr](https://agirpourlatransition.ademe.fr/particuliers/conso/conso-responsable/connaissez-vous-votre-empreinte-climat)
[^3]: _AR6 WG3 10.3.2 s1082 (s1069)_
