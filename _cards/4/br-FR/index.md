---
backDescription: >-
  Le secteur du transport est très dépendant du pétrole. Il représente 15% des
  émissions de gaz à effet de serre.
instagramCode: CLKGjM7ofxA
title: Treuzdougen
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_4_transport'
youtubeCode: oMxhN9RlMnA
---

