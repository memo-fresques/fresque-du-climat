---
title: Transporte
backDescription: >-
  O setor dos transportes é muito dependente do petróleo. Representa 15% das
  emissões de gases de efeito estufa (GEE).
---
Os Transportes representam 15% das emissões de gases com efeito de estufa no mundo (2019)[^1]. É pouco, mas varia muito em função dos países e dos estilos de vida. Nos países ocidentais, a parte do transporte, e nomeadamente a do avião, pode representar uma grande parte da pegada de carbono de cada um. Na verdade, se fizermos um ou alguns voos de longo curso por ano, tal irá constituir o grosso da nossa pegada de carbono[^2]. O setor dos transportes tem a particularidade de ser ainda extremamente dependente do petróleo e o crescimento da eletrificação dos veículos é uma estratégia importante para descarbonizar os transportes[^3].  
[^1]: _AR6 WG3 TS.3 p17 (p65) // AR6 WG3 Figure TS.6 p18 (p66)_
[^2]: _Calcular a sua pegada de carbono:_ [agirpourlatransition.ademe.fr](https://agirpourlatransition.ademe.fr/particuliers/conso/conso-responsable/connaissez-vous-votre-empreinte-climat)
[^3]: _AR6 WG3 10.3.2 p1082 (p1069)_
