---
title: Transport
backDescription: >-
  Energiforbruget i transportsektoren er baseret næsten udelukkende på fossile
  brændstoffer. Transport er ansvarlig for 15% af den samlede udledning af
  drivhusgasser.
---

