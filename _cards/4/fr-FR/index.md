---
backDescription: >-
  Le secteur du transport est très dépendant du pétrole. Il représente 15% des
  émissions de gaz à effet de serre.
title: Transport
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_4_transport'
youtubeCode: oMxhN9RlMnA
instagramCode: CLKGjM7ofxA
---
Les transports représentent 15% des émissions de gaz à effet de serre dans le monde (2019)[^1].C'est peu, mais cela varie beaucoup en fonction des pays et des modes de vie. Dans les pays occidentaux, la part du transport, et notamment celle de l'avion, peut représenter une très grosse partie de l'empreinte carbone de chacun. En fait, si vous prenez un ou quelques longs courriers dans l'année, cela constitue l'essentiel de votre bilan carbone[^2]. Le secteur des transport a la particularité d'être encore extrêmement dépendant du pétrole et l'essor de l'électrification des véhicules est un levier majeur pour décarboner les transports[^3].  
[^1]: _AR6 WG3 TS.3 p17 (p65) // AR6 WG3 Figure TS.6 p18 (p66)_
[^2]: _Calculer son empreinte carbone :_ [agirpourlatransition.ademe.fr](https://agirpourlatransition.ademe.fr/particuliers/conso/conso-responsable/connaissez-vous-votre-empreinte-climat)
[^3]: _AR6 WG3 10.3.2 p1082 (p1069)_
