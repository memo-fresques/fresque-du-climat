---
title: Transport
backDescription: >-
  Sektor transportu jest bardzo zależny od ropy naftowej. Odpowiada za 15%
  emisji gazów cieplarnianych.
---
15% to niewiele, ale różni się znacznie w zależności od kraju i stylu życia. W krajach zachodnich udział transportu, zwłaszcza podróży lotniczych, może stanowić znaczną część śladu węglowego. Jeśli odbywasz jeden lub więcej lotów długodystansowych rocznie, jest to większość Twojego śladu węglowego.
