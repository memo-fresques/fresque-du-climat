---
title: Aszályok
backDescription: >-
  A vízkörforgás zavara csökkentheti vagy növelheti a rendszerben jelen levő
  vízmennyiséget. Kisebb vízmennyiség aszályokhoz vezet, melyek valószínűleg
  egyre gyakoribbá válnak a jövőben.
---

