---
title: Torka
backDescription: >-
  Störningar av vattnets kretslopp kan leda till ökade eller minskade mängder
  vatten. När störningarna leder till mindre regnmängder kan torka uppstå. Torka
  kommer sannolikt att bli mer vanligt förekommande i framtiden.
---
Torka är en period av onormalt låg nederbörd pga uteblivet regn eller hög avdunstning. Torka kan variera i intensitet och varaktighet, allt från tillfällig, lokaliserad torka till långvarig, utbredd torka över stora geografiska områden (megatorka). Konsekvenserna av torka kan vara allvarliga och påverka jordbruk, vattenresurser, naturliga ekosystem och hela livet för befolkningen i området. Klimatförändringar kan påverka frekvensen och intensiteten av torka t ex genom att öka avdunstning från marken[1].  
_[1] AR6 WG1 8.2.3.3
AR6 WG1 TS.2.6 p1092 (p1075)
p99 (p82) // AR6 WG1 Tabell TS.2
AR6 WG1 Figur TS.12
AR6 WG1 Box TS.10, Figur 1 p84 (s67)
s100 (sid 83)
s126 (s109)_
