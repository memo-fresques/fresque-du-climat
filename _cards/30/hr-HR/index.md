---
title: Suše
backDescription: >-
  Poremećaj hidrološkog ciklusa može uzrokovati i povećanje i smanjenje
  padalina. Nedostatak kiše može uzrokovati sušu. Suše će u budućnosti vrlo
  vjerojatno postati češće.
---

