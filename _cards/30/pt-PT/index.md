---
title: Secas
backDescription: >-
  A perturbação do ciclo da água pode ocasionar mais ou menos água. Quando esses
  distúrbios provocam diminuição da quantidade de água, verfica-se uma seca.
  Estima-se que as secas serão mais frequentes no futuro.
---
Uma Seca é um período de tempo anormalmente seco, suficientemente longo para causar um grave desequilíbrio hidrológico. As Secas devem-se à falta de chuva e/ou evaporação do solo. As Secas podem variar em intensidade e duração, desde secas temporárias e localizadas, até secas prolongadas e generalizadas em grandes áreas geográficas (megasecas). Os impactos das secas podem ser graves, afectando a agricultura, os recursos hídricos, os ecossistemas naturais e a vida quotidiana das populações. É importante notar que as Alterações Climáticas podem influenciar a frequência e a intensidade das secas, modificando nomeadamente a evaporação do solo[1].  
_[1] AR6 WG1 8.2.3.3
AR6 WG1 TS.2.6 p1092 (p1075)
p99 (p82) // AR6 WG1 Tabela TS.2
AR6 WG1 Figura TS.12
Caixa AR6 WG1 TS.10, Figura 1 p84 (p67)
pág.100 (pág.83)
p126 (p109)_
