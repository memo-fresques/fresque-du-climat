---
title: Droughts
backDescription: >-
  The disruption of the water cycle can bring more water or less water. Less
  water is a drought. Droughts are likely to become more frequent in the future.
---

