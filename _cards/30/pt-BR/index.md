---
title: Secas
backDescription: >-
  A perturbação do ciclo da água pode ocasionar mais água ou menos água. Quando
  essas perturbações levam a menos água, isso é uma seca. Estima-se que as secas
  poderão ser mais frequentes no futuro.
---

