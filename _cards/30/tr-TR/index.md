---
title: Kuraklık
backDescription: >-
  Su döngüsünün bozulması, farklı yerlerde daha az veya daha çok su anlamına
  gelir. Bu bozulmalar daha az suya yol açtığında, bu bir kuraklıktır.
  Kuraklıkların gelecekte daha sık olması muhtemeldir.
---

