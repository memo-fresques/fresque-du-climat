---
title: Sequías
backDescription: >-
  La perturbación del ciclo del agua puede implicar más o menos agua. Menos agua
  se traduce en sequía. Se estima que las sequías serán mucho más frecuentes en
  el futuro.
---

