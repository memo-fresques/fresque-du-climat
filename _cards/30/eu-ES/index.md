---
title: Idorteak
backDescription: >-
  Ur zikloaren asaldurak, ur gehiago edo ur gutiago ekar lezake. Ur eskasiak
  idortea eragiten du. Izan ere, etorkizunean idorteak ugaritu daitezkeela
  aurreikusten da.
---

