---
title: Siccità
backDescription: >-
  Un ciclo idrico perturbato può portare a scarsità o abbondanza d'acqua. Meno
  acqua provoca la siccità. Si prevede che le siccità potrebbero moltiplicarsi
  in futuro.
---
La siccità è un periodo di tempo insolitamente secco e abbastanza lungo da causare un grave squilibrio idrologico. È causata dalla mancanza di pioggia e/o dall'evaporazione del suolo. Gli episodi di siccità possono variare in intensità e durata, da siccità temporanee e localizzate a siccità prolungate ed estese su vaste aree geografiche (mega-siccità). Gli impatti delle siccità possono essere gravi, influenzando l'agricoltura, le risorse idriche, gli ecosistemi naturali e la vita quotidiana delle popolazioni. È importante notare che il cambiamento climatico può influenzare la frequenza e l'intensità delle siccità modificando l'evaporazione del suolo.[1].
_[1] AR6 WG1 8.2.3.3
AR6 WG1 TS.2.6 p1092 (p1075)
p99 (p82) // AR6 WG1 Tabella TS.2
AR6 WG1 Figura TS.12
AR6 WG1 Box TS.10, Figura 1 p84 (p67)
p100 (p83)
p126 (p109)_
