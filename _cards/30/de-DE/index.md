---
title: Dürren
backDescription: >-
  Die Störung des Wasserkreislaufs kann zu einem Überschuss oder einem Mangel an
  Wasser führen. Bei zu wenig Wasser, spricht man von einer Dürre. Laut dem IPCC
  werden Dürreperioden in Zukunft häufiger auftreten.
---

