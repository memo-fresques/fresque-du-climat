---
title: Droogtes
backDescription: >-
  Verstoringen van de waterkringloop kunnen droogte en overstroming veroorzaken.
  Volgens het IPCC (Intergovernmental Panel on Climate Change) zou droogte in de
  toekomst vaker voorkomen.
---
Droogtes zijn periodes met abnormale watertekorten. Bij een meteorologische droogte is er sprake van minder regenval. Bij een landbouwdroogte is de vochtigheid van de grond abnormaal laag, wat invloed heeft op de groei van gewassen. Een mega-droogte is een langdurige en wijdverbreide droogte die veel langer duurt dan normaal (meestal een decennium of meer). Gebrek aan regenval en verdamping uit de grond zijn de oorzaken van droogtes, net als erosie. Op de wereldkaart (de hexagonale vlakken) uit het 6e rapport van de IPCC hebben de regio's met groene vlakken te maken met minder droogte. De regio's met oranje vlakken hebben juist meer droogte. Het aantal puntjes vertegenwoordigt de mate van betrouwbaarheid dat de getoonde verandering het resultaat is van menselijke activiteit.
