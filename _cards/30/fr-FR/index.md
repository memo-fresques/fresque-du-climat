---
backDescription: >-
  La perturbation du cycle de l'eau peut amener plus ou moins d'eau. Moins
  d'eau, c'est une sécheresse. On estime ainsi que les sécheresses pourraient se
  multiplier à l’avenir.
title: Sécheresses
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_30_s%C3%A9cheresses
youtubeCode: EX82KQmQ3yg
---
Une sécheresse est une période de temps anormalement sèche suffisamment longue pour causer un grave déséquilibre hydrologique. Elles sont dues à un manque de pluie et/ou à l'évaporation du sol. Les sécheresses peuvent varier en intensité et en durée, allant de sécheresses temporaires et localisées à des sécheresses prolongées et étendues sur de vastes zones géographiques (méga-secheresses). Les impacts des sécheresses peuvent être graves, affectant l'agriculture, les ressources en eau, les écosystèmes naturels, et la vie quotidienne des populations. Il est important de noter que le changement climatique peut influencer la fréquence et l'intensité des sécheresses en modifiant notamment l'évaporation des sols.[1].  
_[1] AR6 WG1 8.2.3.3
AR6 WG1 TS.2.6 p1092 (p1075)
p99 (p82) // AR6 WG1 Table TS.2
AR6 WG1 Figure TS.12
AR6 WG1 Box TS.10, Figure 1 p84 (p67)
p100 (p83)
p126 (p109)_
