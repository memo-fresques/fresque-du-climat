---
title: Susze
backDescription: >-
  Zaburzenia obiegu wody w przyrodzie mogą wpływać na wzrost lub spadek poziomu
  wód. Niedobór wody powoduje susze. Przewiduje się, że w przyszłości susze będą
  występować coraz częściej.
---
Susza to okres nienormalnego niedoboru wody. Susza meteorologiczna ma miejsce, gdy występuje spadek opadów deszczu, a susza rolnicza ma miejsce, gdy wilgotność gleby spada poniżej normy, wpływając na produkcję roślinną. Mega susza to trwała i powszechna susza, która trwa znacznie dłużej niż normalnie (zwykle dekadę lub dłużej). Brak opadów i parowanie z ziemi są przyczynami suszy, a także erozji gleby.
