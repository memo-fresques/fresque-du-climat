---
title: Tørke
backDescription: >-
  Forstyrrelser i vannets kretsløp kan føre til mer eller mindre vann. Ved
  mindre vann blir det tørke. Tørke vil sannsynligvis skje oftere i fremtiden.
---
En tørke er en unormalt tørr periode som er lang nok til å forårsake en alvorlig hydrologisk ubalanse. De skyldes mangel på regn og/eller fordampning fra jorden. Tørker kan variere i intensitet og varighet, fra midlertidige og lokale tørker til langvarige tørker som strekker seg over store geografiske områder (mega-tørker). Virkningene av tørker kan være alvorlige, og påvirke landbruk, vannressurser, naturlige økosystemer og folks daglige liv. Det er viktig å merke seg at klimaendringer kan påvirke hyppigheten og intensiteten av tørker ved å endre blant annet fordampningen fra jorden.[1].  
_[1] AR6 WG1 8.2.3.3
AR6 WG1 TS.2.6 s1092 (s1075)
s99 (s82) // AR6 WG1 Tabell TS.2
AR6 WG1 Figur TS.12
AR6 WG1 Boks TS.10, Figur 1 s84 (s67)
s100 (s83)
s126 (s109)_
