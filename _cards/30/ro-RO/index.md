---
title: Secete
backDescription: >-
  Un circuit al apei perturbat poate duce la lipsa apei sau la abundența
  acesteia. Când aceste perturbări generează mai puțină apă, provoacă o secetă.
  Se estimează că secetele ar putea crește în viitor.
---

