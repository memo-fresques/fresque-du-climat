---
title: Tørke
backDescription: >-
  Forstyrrelse af vandets kredsløb kan føre til mere eller mindre vand. Når et
  område gennem længere tid ikke får tilstrækkeligt med nedbør, vil tørke opstå.
  Man forventer mere tørke i fremtiden.
---

