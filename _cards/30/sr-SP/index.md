---
title: Suše
backDescription: >-
  Prekid hidrološkog procesa može dovesti do manjka ili viška vode. Manjak vode
  predstavlja sušu. Verovatno će u budućnosti suše postati učestalije.
---

