---
title: Droughts
backDescription: >-
  The disruption of the water cycle can both increase and decrease rainfall. A
  lack of rain can cause drought. Droughts are likely to become more frequent in
  the future.
wikiUrl: 'https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_30_droughts'
youtubeCode: bEuX-UVM-4M
instagramCode: ''
---

A drought is a period of abnormal water shortage. A meteorological drought is when there is a drop in rainfall, and an agricultural drought is when soil moisture drops abnormally, affecting crop production. A mega-drought is a persistent and widespread drought that lasts much longer than normal (usually a decade or more). Lack of rainfall and evaporation from the ground are the causes of droughts, as well as soil erosion.
