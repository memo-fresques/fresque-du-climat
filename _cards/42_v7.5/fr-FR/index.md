---
title: Hydrates de méthane
backDescription: Todo
---
Deux cartes ont été ajoutées après la carte n°40 : ce sont des boucles de rétroactions violentes potentielles, des "bombes climatiques", qui si elles se déclenchent, nous font perdre définitivement le contrôle sur le climat.

Suite à la parution de la première partie de l'AR6 (v8.4), elle a été remplacé par Ralentissement du gulf stream.

Les thermokarst sont de véritables bioréacteurs au cœur du processus de relargage du carbone gelé : Lorsque le pergélisol dégèle, des morceaux de sol se détachent et tombent dans l’eau, apportant nutriments et carbone aux bactéries et au plancton présents dans la mare, qui les dégradent en CO2 (dans les couches d’eau proches de la surface) et en méthane ou CH4 (dans le fond privé d’oxygène de la mare).
