---
title: Metanhydrater
backDescription: Todo
---
To kort ble lagt til etter kort nr. 40: dette er potensielle voldsomme tilbakekoblingssløyfer, "klimabomber", som hvis de utløses, gjør at vi mister kontrollen over klimaet for alltid.

Etter utgivelsen av den første delen av AR6 (v8.4), ble den erstattet av Nedbremsing av Golfstrømmen.

Termokarst kan bli kraftige drivere av frigjøringen av frosset karbon: Når permafrosten tiner, løsner biter av jord og faller i vannet, og tilfører næringsstoffer og karbon til bakteriene og planktonet i dammen, som bryter dem ned til CO2 (i vannlagene nær overflaten) og til metan (i dammens oksygenfrie bunn).
