---
title: Idrati di metano
backDescription: da fare
---
Dopo la carta n° 40 ne sono state aggiunte due: si tratta di potenziali cicli di retroazione violenti, di "bombe climatiche", che se si attivano, ci fanno perdere definitivamente il controllo sul clima.

Dopo la pubblicazione della prima parte dell'AR6 (v8.4), questa carta è stata sostituita da quella del Rallentamento della corrente del Golfo.

I laghi termocarsici sono veri e propri bioreattori al centro del processo di rilascio del carbonio congelato: quando il permafrost si scioglie, pezzi di suolo si staccano e cadono nell'acqua, portando nutrienti e carbonio ai batteri e al plancton presenti nel lago, che li degradano in CO2 (nelle zone d'acqua vicine alla superficie) e in metano o CH4 (nel fondo privo di ossigeno del lago).
