---
title: Bioróżnorodność lądowa
backDescription: >-
  Zmiany temperatury i zaburzenia w obiegu wody w przyrodzie wpływają na rośliny
  i zwierzęta: te przenoszą swoje siedliska lub zanikają (a czasem, chociaż
  rzadko, gwałtownie się rozmnażają).
---
Obecnie różnorodność biologiczna Ziemi jest osłabiana przede wszystkim przez czynniki inne niż zmiany klimatu, takie jak wylesianie, zanikanie naturalnych siedlisk, stosowanie pestycydów i różne formy zanieczyszczenia. Jednak zmiany klimatu w dużej mierze przyczynią się do znikania gatunków w nadchodzących dziesięcioleciach.
