---
backDescription: >-
  Les causes de l’érosion de la biodiversité sont : la destruction des habitats,
  la surexploitation des espèces sauvages, la pollution, le changement
  climatique et l’introduction d’espèces exotiques envahissantes. Le changement
  des conditions de vie des espèces (température, cycle de l’eau) les force à
  migrer et à adapter leur mode de vie.
title: Biodiversité terrestre
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_25_biodiversit%C3%A9_terrestre
youtubeCode: TP8Tu4Gq2CE
---
Le changement climatique est l'une des 5 principales causes de perte de biodiversité. On peut également citer la destruction des habitats, l'exploitation directe, la pollution et les espèces exotiques envahissantes. A l'avenir, le changement climatique risque de devenir une cause encore plus prépondérante[1].  
_[1] IPBES Figure SPM.2
 p27 (p25) // https://www.nature.com/articles/s41467-022-30339-y_
