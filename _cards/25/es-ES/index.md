---
title: Biodiversidad terrestre
backDescription: >-
  Los animales y las plantas se ven afectados por los cambios de temperatura y
  del ciclo del agua: migran, desaparecen o, más raramente, proliferan.
---

