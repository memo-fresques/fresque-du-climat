---
title: Biodiversitet på land
backDescription: >-
  Temperaturstigninger og forstyrrelser af vandets kredsløb har stor betydning
  for dyr og planter. Nogle arter kan migrere, andre forsvinder, og somme tider
  kan en art trives og øges.
---

