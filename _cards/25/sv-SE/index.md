---
title: Biologisk mångfald av landlevande djur
backDescription: >-
  Följande leder till minskad biologisk mångfald: förstörelse av habitat,
  överexploatering av vilda djur, föroreningar, klimatförändringarna och
  införande av invasiva arter. När det sker förändringar i miljön, såsom
  temperaturförändringar och störningar av vattnets kretslopp, tvingas arter att
  migrera och anpassa sig.
---
Klimatförändringar är en av de fem huvudsakliga orsakerna till förlust av biologisk mångfald. Andra framträdande faktorer  är förstörda livsmiljöer (t ex avskogning), direkt exploatering, föroreningar och invasiva arter. I framtiden kan klimatförändringar bli en ännu mer dominerande faktor [1].  
_[1] IPBES-figure SPM.2
 p27 (p25) // https://www.nature.com/articles/s41467-022-30339-y_
