---
title: Biologisk mangfold på land
backDescription: >-
  Dyr og planter påvirkes av temperaturendringer og forstyrrelser i vannets
  kretsløp. De kan migrere, bli utryddet eller, noen få ganger, spre seg.
---
Klimaendringer er en av de 5 viktigste årsakene til tap av biologisk mangfold. Man kan også nevne ødeleggelse av habitater, overbeskatning, forurensning og fremmede arter. I fremtiden kan klimaendringer bli en enda mer fremtredende årsak[1].  
_[1] IPBES Figur SPM.2
 s27 (s25) // https://www.nature.com/articles/s41467-022-30339-y_
