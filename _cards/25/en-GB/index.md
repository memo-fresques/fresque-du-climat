---
title: Terrestrial Biodiversity
backDescription: >-
  The causes of biodiversity loss are: the destruction of habitats,
  overexploitation of wild species, pollution, climate change, and the
  introduction of invasive alien species. When there are changes in the
  environment, like temperature and water availability, species are forced to
  migrate and adapt their way of life.
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_25_terrestrial_biodiversity
youtubeCode: oa5rHgw2EE0
instagramCode: ''
---

Today, the Earth's biodiversity is being undermined above all by factors other than climate change, such as deforestation, disappearance of natural habitats, use of pesticides and various forms of pollution. However, climate change will largely contribute to the disappearance of species in the coming decades.
