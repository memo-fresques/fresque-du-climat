---
title: Biodiversità terrestre
backDescription: >-
  Le cause della perdita di biodiversità sono: distruzione degli habitat,
  sfruttamento eccessivo delle specie selvatiche, inquinamento, cambiamento
  climatico e introduzione di specie esotiche invasive. Il cambiamento nelle
  condizioni di vita (temperatura, ciclo dell'acqua)

  delle specie le costringe a migrare e ad adattare il loro modo di vivere.
---
Il cambiamento climatico è una delle 5 principali cause di perdita di biodiversità. Si possono anche citare la distruzione degli habitat, lo sfruttamento diretto, l'inquinamento e le specie invasive. In futuro, il cambiamento climatico rischia di diventare ancora più predominante[1]. 
_[1] IPBES Figura SPM.2
 p27 (p25) // https://www.nature.com/articles/s41467-022-30339-y_
