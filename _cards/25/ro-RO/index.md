---
title: Biodiversitatea terestră
backDescription: >-
  Animalele și plantele sunt afectate de schimbările de temperatură și de
  perturbarea circuitului apei în natură: ele migrează, mor sau, mai rar,
  proliferează.
---

