---
title: Biodiverzita
backDescription: >-
  Zvířata a rostliny jsou ovlivněny změnami teplot a narušením přirozeného
  koloběhu vody. Mohou migrovat, vyhynout nebo, což je vzácnější, se mohou
  přemnožit.
---

