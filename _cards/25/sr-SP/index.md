---
title: Kopneni biodiverzitet
backDescription: >-
  Promene temperature i prekid hidrološkog ciklusa utiču na životinje i biljke.
  Neke migriraju, neke izumiru, a ređe ove pojave dovode do širenja vrste.
---

