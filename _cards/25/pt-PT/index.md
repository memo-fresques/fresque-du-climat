---
title: Biodiversidade Terrestre
backDescription: >-
  Animais e plantas são afetados pelas mudanças de temperatura e do ciclo da
  água: migram ou desaparecem e, em alguns casos, proliferam.
---
As alterações climáticas são uma das 5 principais causas de perda de biodiversidade. Também podemos citar a destruição de habitats, a sobre-exploração das espécies selvagens, a poluição e introdução de espécies exóticas invasoras. No futuro, as alterações climáticas podem tornar-se uma causa ainda mais predominante[1].  
_[1] IPBES Figura SPM.2
 p27 (p25) // https://www.nature.com/articles/s41467-022-30339-y_
