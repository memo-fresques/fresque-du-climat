---
title: terrestrische Biodiversität
backDescription: >-
  Tiere und Pflanzen sind von den Veränderungen der Temperatur und des
  Wasserkreislaufes stark betroffen. Als Folge versuchen sie sich woanders
  anzusiedeln, verschwinden ganz oder, in seltenen Fällen, vermehren sich.
---

