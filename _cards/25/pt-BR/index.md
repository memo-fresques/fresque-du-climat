---
title: Biodiversidade Terrestre
backDescription: >-
  Animais e plantas são afetados pelas mudanças de temperatura e do ciclo da
  água: migram ou desaparecem e, em alguns casos, proliferam.
---

