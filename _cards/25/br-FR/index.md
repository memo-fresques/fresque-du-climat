---
backDescription: >-
  Les animaux et les plantes sont affectés par les changements de température et
  du cycle de l'eau : ils se déplacent ou disparaissent (ou, plus rarement, ils
  prolifèrent).
instagramCode: ''
title: Bevliesseurted douarel
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_25_biodiversit%C3%A9_terrestre
youtubeCode: TP8Tu4Gq2CE
---

