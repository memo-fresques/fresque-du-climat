---
title: Terrestrial Biodiversity
backDescription: >-
  Animals and plants are affected by the changes in temperature and the
  disruption of the water cycle. They may migrate, become extinct or, more
  rarely, proliferate.
---

