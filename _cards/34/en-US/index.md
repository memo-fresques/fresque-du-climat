---
title: Hurricanes
backDescription: >-
  Hurricanes use energy from warm waters at the ocean surface. Because of global
  warming, they are becoming stronger.
---

