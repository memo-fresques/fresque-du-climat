---
title: Cyklony
backDescription: >-
  Cyklony powstają pod wpływem energii ciepłych wód na powierzchni oceanów. W
  wyniku zmian klimatu przybrały na sile.
---
Cyklonów nie jest więcej z powodu zmian klimatu (przynajmniej nie byliśmy jeszcze w stanie ustalić tego statystycznie), ale są one gwałtowniejsze. Jeśli chodzi o kartę wyższego rzędu, możemy wybrać albo zakłócenie obiegu wody, w tym sensie, że wzrost siły cyklonów jest ilustracją zakłócenia obiegu wody, albo wzrost temperatury wody, ponieważ cyklony żywią się energią ciepłej wody z obszarów międzyzwrotnikowych. Mniej sensu ma ułożenie obu.
