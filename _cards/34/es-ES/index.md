---
title: Ciclones
backDescription: >-
  Los ciclones se nutren de la energía de las aguas calientes de la superficie
  del océano. Su potencia ha aumentado a causa del cambio climático.
---

