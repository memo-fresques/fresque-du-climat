---
title: Cyclones
backDescription: >-
  Cyclones draw on the energy from warm water at the ocean surface. They are
  getting stronger because of global warming.
wikiUrl: 'https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_34_cyclones'
youtubeCode: pgbIRQMCeqc
instagramCode: ''
---

There are not more cyclones because of climate change (at least we have not yet been able to establish this statistically), but they are more violent. For the upstream card, we can choose either the disruption of the water cycle, in the sense that the increase in cyclone power is an illustration of the disruption of the water cycle, or the increase in water temperature, because cyclones feed on the energy of warm water from intertropical areas. It makes less sense to have both.
