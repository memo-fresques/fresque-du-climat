---
title: Cicloni
backDescription: >
  Ciclonii sunt alimentați de energia apelor calde de pe suprafața oceanelor.
  Din cauza încălzirii globale, aceștia devin mai puternici.
---

