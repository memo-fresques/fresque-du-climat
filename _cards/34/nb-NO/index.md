---
title: Sykloner
backDescription: >-
  Sykloner bruker energi fra varmt vann på havets overflate. De blir sterkere på
  grunn av global oppvarming.
---
Sykloner dannes ved å hente energi fra varmt vann og jordens rotasjon (Coriolis-effekten). Det er ikke flere sykloner på grunn av klimaendringer (vi er i hvert fall ennå ikke i stand til å fastslå det statistisk), men vi kan si at de er mer kraftige. For kortet før kan vi velge enten forstyrrelser i vannets kretsløp ( kort 20), i den forstand at økningen i syklonenes styrke illustrerer forstyrrelsen av vannets kretsløp, eller økning i vanntemperatur (kort 17), fordi det ofte sies at sykloner får energi fra det varme vannet i den intertropiske konvergenssonen. Det er mindre logisk å sette velge[1].  
_[1] AR6 WG1 TS.2.3 s39 (s71) // https://www.notre-planete.info/terre/risques_naturels/cyclones.php_
