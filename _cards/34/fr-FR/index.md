---
backDescription: >-
  Les cyclones s’alimentent de l’énergie des eaux chaudes à la surface de
  l’océan. Leur puissance a augmenté à cause du changement climatique.
title: Cyclones
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_34_cyclones'
youtubeCode: YvJcgi3LjDY
---
Les cyclones se forment en tirant leur énergie des eaux chaudes et de la rotation terrestre (effet de Coriolis). Il n'y a pas plus de cyclones à cause du changement climatique, (du moins, on n'est pas encore en mesure de l'établir d'un point de vue statistique), mais on peut dire qu'ils sont plus violents. Pour la carte en amont, on peut choisir soit la perturbation du cycle de l'eau, dans le sens où l'augmentation de la puissance des cyclones est une illustration de la perturbation du cycle de l'eau, soit la hausse de la température de l'eau, car on entend souvent dire que les cyclones s'alimentent de l'énergie des eaux chaudes des zones intertropicales. Il est moins logique de mettre les deux[1].  
_[1] AR6 WG1 TS.2.3 p39 (p71) // https://www.notre-planete.info/terre/risques_naturels/cyclones.php_
