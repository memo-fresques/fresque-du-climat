---
title: Κυκλώνες
backDescription: >-
  Οι κυκλώνες αντλούν ενέργεια από ζεστά νερά της επιφάνειας των ωκεανών. Η
  ισχύς τους έχει αυξηθεί λόγω της κλιματικής αλλαγής.
---

