---
title: Ciclones
backDescription: >-
  Os ciclones usam energia de águas quentes da superfície do oceano. Devido às
  alterações climáticas, são cada vez mais fortes.
---
Os Ciclones são formados extraindo sua energia da água quente e da rotação da Terra (efeito Coriolis). Não há mais ciclones por causa das alterações climáticas (pelo menos, ainda não conseguimos estabelecer isso do ponto de vista estatístico), mas podemos dizer que são mais violentos. Para a carta a montante, podemos escolher entre a Perturbação do ciclo da água, uma vez que o aumento da potência dos ciclones é uma ilustração da perturbação do ciclo da água, ou o Aumento da temperatura da água, porque os ciclones extraem energia das águas quentes das zonas intertropicais. Faz menos sentido colocar ambas[1].  
_[1] AR6 WG1 TS.2.3 p39 (p71) // https://www.notre-planete.info/terre/politiques_naturels/cyclones.php_
