---
title: Cykloner
backDescription: >-
  Cykloner använder sig av energi från varmt vatten på havsytan. De blir allt
  kraftigare på grund av den globala uppvärmningen.
---
Cykloner bildas av energi från varma hav och jordens rotation (Corioliseffekten). Cyklonerna har inte blivit fler på grund av klimatförändringarna (åtminstone kan vi ännu inte fastställa det statistiskt), men de har blivit kraftigare. Kortet kan ligga efter Störningar av vattnets kretslopp (kort 20), eftersom kraftigare cykloner är en konsekvens av störningen i kretsloppet, eller efter Ökning av vattentemperatur (kort 17) då cykloner får energi från det varma vattnet i tropiska områden. Det är mindre logiskt att använda båda korten som orsak till cykloner.[1].  
_[1] AR6 WG1 TS.2.3 s39 (s71) // https://www.notre-planete.info/terre/risques_naturels/cyclones.php_
