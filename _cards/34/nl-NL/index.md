---
title: Cyclonen
backDescription: >-
  Boven warm oceaanwater kan een depressie steeds meer energie verzamelen en zo
  uitgroeien tot een cycloon. De intensiteit van cyclonen kan in de toekomst
  toenemen, doordat het oceaanwater opwarmt.
---
Er zijn vanwege de klimaatverandering niet zozeer meer cyclonen (tenminste, we kunnen dit nog niet statistisch vaststellen), maar ze zijn wel krachtiger. Als oorzaak kunnen we kiezen voor de kaart Verstoring van de watercyclus, omdat de toename van de kracht van cyclonen een uitingsvorm is van de verstoring van de watercyclus, of voor de kaart Verhoging van de watertemperatuur, omdat cyclonen voeden zich met de energie van warm water uit intertropische gebieden (rond de evenaar). Het is niet logisch om ze beide als oorzaak te zien.
