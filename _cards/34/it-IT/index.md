---
title: Cicloni
backDescription: >-
  I cicloni sono alimentati dall'energia delle acque calde sulla superficie
  degli oceani. La loro potenza è aumentata a causa del cambiamento climatico.
---
I cicloni si formano attingendo energia dalle acque calde e dalla rotazione terrestre (forza di Coriolis). Il numero di cicloni non aumenta a causa dei cambiamenti climatici (almeno, non siamo ancora in grado di stabilirlo da un punto di vista statistico), ma possiamo dire che sono più violenti. Per la carta precedente a questa, è possibile scegliere tra la perturbazione del ciclo dell'acqua, nel senso che l'aumento della potenza dei cicloni è un'illustrazione della perturbazione del ciclo dell'acqua, o l'aumento della temperatura dell'acqua, poiché spesso si sente dire che i cicloni si alimentano dell'energia delle acque calde delle zone intertropicali. È meno logico metterle entrambe[1].
_[1] AR6 WG1 TS.2.3 p39 (p71) // https://www.notre-planete.info/terre/risques_naturels/cyclones.php_
