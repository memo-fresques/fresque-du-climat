---
backDescription: >-
  Avec le réchauffement, les animaux migrent. Or, certains sont des vecteurs de
  maladie et peuvent atteindre des zones où les populations ne sont pas
  immunisées contre ces maladies.
instagramCode: CNIKsZ3rnrG
title: Dougerioù kleñved
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_28_vecteurs_de_maladie
youtubeCode: 2jp-vkrqJfQ
---

