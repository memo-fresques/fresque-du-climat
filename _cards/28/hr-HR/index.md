---
title: Vektori zaraznih bolesti
backDescription: >-
  Pojedine životinje prijenosnici su bolesti. Globalno zagrijavanje uzrokuje
  njihove migracije. Može ih tako dovesti u kontakt sa stanovništvom koje nije
  imuno na bolesti koje se prenose.
---

