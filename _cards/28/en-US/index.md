---
title: Vectors of Disease
backDescription: >-
  With global warming, animals migrate. Some of them carry diseases and can
  reach areas where the population is not immunized against these diseases.
---

