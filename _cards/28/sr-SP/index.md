---
title: Vektori zaraznih bolesti
backDescription: >-
  Usled globalnog zagrevanja, životinje migriraju. Neke od njih su prenosioci
  zaraznih bolesti i mogu stići u oblasti gde stanovništvo nije imunizovano
  protiv bolesti koje one prenose.
---

