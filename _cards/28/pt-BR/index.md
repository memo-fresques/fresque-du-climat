---
title: Vetores de Doenças
backDescription: >-
  Com o aquecimento global, os animais migram. Alguns deles carregam doenças e
  atingem áreas onde a população não está imunizada contra essas doenças.
---

