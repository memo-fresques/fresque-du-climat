---
title: Mpitondra aretina
backDescription: >-
  Mifindra toerana ireo biby vokatry ny hafanana. Misy amin'izy ireo anefa
  mitondra aretina ka mety ho tonga amin'ny toerana izay ny olona dia mbola tsy
  manana hery fiarovana amin'ilay aretina.
---

