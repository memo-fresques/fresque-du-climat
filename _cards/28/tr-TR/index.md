---
title: Hastalık vektörleri (taşıyıcıları)
backDescription: >-
  Küresel ısınma ile hayvanlar daha önce bulunmadıkları yerlere göç eder.
  Bazıları hastalık taşır ve bu hastalıklara karşı bağışıklığı bulunmayan
  nüfusların olduğu bölgelere ulaşır.
---

