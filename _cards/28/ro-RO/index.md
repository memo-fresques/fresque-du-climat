---
title: Vectori de boli
backDescription: >-
  Odată cu încălzirea globală, animalele migrează. Unele dintre ele poartă boli
  și pot ajunge în zone în care populația nu este imunizată împotriva acestora.
---

