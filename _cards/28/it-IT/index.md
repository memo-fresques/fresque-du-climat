---
title: Vettori di malattia
backDescription: >-
  Con il riscaldamento, gli animali migrano. Alcuni sono vettori di malattie e
  raggiungono zone in cui le popolazioni non sono immunizzate.
---
Il problema non è tanto la proliferazione dei vettori di malattia quanto il loro spostamento. Questa carta viene idealmente dopo la carta della biodiversità terrestre poiché i vettori di malattia sono un sottogruppo della biodiversità[1]. 
_[1] AR6 WG2 TS.B.1.1 p11 // AR6 WG2 FAQ 2.2 p21_
