---
title: Krankheitsüberträger
backDescription: >-
  Als Reaktion auf die globale Erwärmung versuchen Tiere in kühlere Zonen
  auszuweichen und migrieren. Einige von ihnen tragen Krankheiten in sich und
  werden sich in Gebieten ausbreiten, in denen die Bevölkerung nicht immun ist.
---

