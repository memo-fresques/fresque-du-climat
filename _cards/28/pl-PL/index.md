---
title: Nosiciele chorób
backDescription: >-
  Z powodu ocieplenia zwierzęta migrują. Niektóre z nich przenoszą choroby, na
  które zamieszkałe w innych strefach organizmy nie są uodpornione.
---
Problemem jest nie tyle rozprzestrzenianie się wektorów chorób, co ich przemieszczanie. Ta karta idealnie pojawia się po karcie 25, ponieważ wektory chorób są częścią bioróżnorodności.
