---
title: Eritasun-bektoreak
backDescription: >-
  Berotzearekin, animaliek migratzen dute. Alta, batzuk eritasun-bektoreak dira
  eta eritasun horien aurka biztanleria immunizatua ez den lekuetara hel
  daitezke.
---

