---
title: Vetores de Doenças
backDescription: >-
  Com o aquecimento global, os animais migram. Alguns são portadores de doenças
  e podem atingir áreas onde as populações não são imunes a essas doenças.
---
O problema não é tanto a proliferação dos vetores de doenças, mas sim a sua deslocação. Esta carta vem idealmente após a carta Biodiversidade terrestre, na medida em que os vetores de doenças são uma subparte da biodiversidade[1].  
_[1] AR6 WG2 TS.B.1.1 p11 // AR6 WG2 FAQ 2.2 p21_
