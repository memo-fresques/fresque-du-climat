---
backDescription: >-
  Avec le réchauffement, les animaux migrent. Or, certains sont des vecteurs de
  maladie et peuvent atteindre des zones où les populations ne sont pas
  immunisées contre ces maladies.
title: Vecteurs de maladie
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_28_vecteurs_de_maladie
youtubeCode: 2jp-vkrqJfQ
instagramCode: CNIKsZ3rnrG
---
Le problème n'est tant pas la prolifération des vecteurs de maladie que leur déplacement. Cette carte vient idéalement après la carte Biodiversité terrestre dans la mesure où les vecteurs de maladie sont une sous−partie de la biodiversité[1].  
_[1] AR6 WG2 TS.B.1.1 p11 // AR6 WG2 FAQ 2.2 p21_
