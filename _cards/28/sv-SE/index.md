---
title: Sjukdomsbärare
backDescription: >-
  Den globala uppvärmningen orsakar migration av djurarter. Vissa djur är
  sjukdomsbärare och de kommer att nå områden där befolkningen inte är
  motståndskraftig mot dessa sjukdomar.
---
Problemet med sjukdomsbärare är inte så mycket att de sprider sjukdomar utan att de kan tvingas till nya områden av t ex klimatförändringar eller förstörda habitat och då kan sprida sjukdomar inom en ny känsligare grupp/mijlö. Kort kommer logiskt direkt efter kortet om biologisk mångfald på land eftersom sjukdomsbärare är en del av den biologiska mångfalden[1].
_[1] AR6 WG2 TS.B.1.1 s11 // AR6 WG2 FAQ 2.2 s21_
