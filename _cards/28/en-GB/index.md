---
title: Vectors of Disease
backDescription: >-
  Some animals carry diseases. Global warming causes them to migrate, possibly
  reaching populations that have no immunity against these diseases.
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_28_vectors_of_disease
youtubeCode: Ec6KJpVw_Rc
instagramCode: CNILj5dnO37
---

The problem is not so much the proliferation of vectors of disease as their displacement. This card ideally comes after card 25, as vectors of disease are a sub-part of biodiversity.
