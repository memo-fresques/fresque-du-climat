---
title: Sykdomsvektorer
backDescription: >-
  Dyr migrerer på grunn av global oppvarming. Noen av dem er sykdomsvektorer og
  kan nå områder hvor befolkningen ikke er immun mot disse sykdommene.
---
Problemet er ikke så mye spredningen av sykdomsvektorer som deres forflytning. Dette kortet kommer ideelt etter kortet "Biologisk mangfold på land" ettersom sykdomsvektorer er en underdel av det biologiske mangfoldet[1].  
_[1] AR6 WG2 TS.B.1.1 s11 // AR6 WG2 FAQ 2.2 s21_
