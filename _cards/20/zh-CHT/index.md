---
title: 水循環紊亂
backDescription: >-
  水循環由蒸發產生的水氣、雲、雨、河流等組成。如果水和空氣溫度上升，海洋表面的蒸發也會加劇，會形成更多雲，進而導致更多降雨。如果蒸發作用發生在地面，則會令土壤乾燥。
---

