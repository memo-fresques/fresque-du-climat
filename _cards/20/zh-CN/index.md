---
title: 水循环紊乱
backDescription: ' 水循环由蒸发产生的水汽、云、雨河流等等组成。如果水和空气升温，海洋表面发生的蒸发也会加剧。 这会形成更多的云，进而导致更多的降雨。如果蒸发发生在地表，则会使土壤干燥。'
---

