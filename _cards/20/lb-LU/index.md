---
title: Perturbatioun vum Waasserkreeslaf
backDescription: >-
  Wann d'Waasser an d'Loft méi waarm ginn, da verdonst méi Waasser un der
  Uewerfläch vum Ozean. Dat féiert zu méi Wolleken a méi Reen. Mee wann
  d'Verdonstung u Land stattfënnt, dréchent de Buedem aus.
---

