---
title: Perturbarea Circuitului apei în natură
backDescription: >-
  Evaporarea care are loc la suprafața oceanului crește pe măsură ce apa și
  aerul se încălzesc. Odată cu acest fenomen crește numărul norilor care
  provoacă ploaie. Când evaporarea are loc pe pământ, aceasta provoacă uscarea
  solului.
---

