---
title: Poremećaj hidrološkog ciklusa
backDescription: >-
  Topliji oceani i toplija atmosfera dovode do većeg isparavanja, koje uzrokuje
  stvaranje kišnih oblaka i padaline.

  Toplija zemlja i toplija atmosfera isto tako dovode do većeg isparavanja, koje
  u tom slučaju uzrokuje isušivanje zemlje.
---

