---
title: Perturbazione del ciclo dell'acqua
backDescription: >-
  L'evaporazione che avviene sulla superfice dell'oceano aumenta se l'acqua e
  l'aria si riscaldano. Ciò porta alla formazione di più nuvole che causeranno
  la pioggia. Ma se l'evaporazione si svolge sulla terraferma, il suolo si
  prosciuga.
---
Questa carta è importante. Rappresenta da sola il motivo per cui in passato si parlava di riscaldamento globale e ora invece di cambiamento climatico o di alterazione del clima. L'aumento della temperatura è di per sé un problema, ma si può vedere sull'affresco, alla fine, che la perturbazione del ciclo dell'acqua ha molti più effetti.
