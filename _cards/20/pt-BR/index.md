---
title: Perturbação do ciclo da água
backDescription: >-
  O aumento das temperaturas da agua e da atmosfera sao responsavéis pela
  evaporaçao da agua na superficie do oceano e consequetemente a formaçao de
  nuvens de chuva. O mesmo fenômeno de evaporação acontece na terra e resseca o
  solo.
---

