---
title: Poremećaj hidrološkog ciklusa/kruženja vode
backDescription: >-
  Ukoliko je temperatura okeana i atmosfere viša, isparenja okeana se
  pojačavanju, što donosi više kišnih oblaka i više kiše. Ukoliko se isparavanje
  odigrava na kopnu, tlo se isušuje.
---

