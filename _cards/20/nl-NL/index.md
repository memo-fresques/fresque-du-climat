---
title: De verstoring van de waterkringloop
backDescription: >-
  Wanneer de temperatuur van de atmosfeer en de oceanen stijgt, neemt ook de
  verdamping van warm (zee)water toe, wat leidt tot meer wolken en neerslag.
  Door toenemende verdamping van water uit de bodem kan droogte ontstaan.
---
De kaart is belangrijk. Het laat zien waarom we eerst over opwarming spraken, en nu over klimaatverandering. Temperatuursverhogingen zijn al problematisch van zichzelf, maar je kunt aan het einde van de Climate Fresk zien dat de verstoring van de watercyclus nog veel meer effecten heeft.
