---
title: Su döngüsünün bozulması
backDescription: >-
  Okyanus yüzeyinde gerçekleşen buharlaşma, okyanusların ve atmosferin
  sıcaklığının artmasıyla birlikte daha hızlı bir şekilde gerçekleşir. Bu da
  daha fazla yağmur bulutu demektir. Bu buharlaşma fenomeni toprak ve hava
  arasında da olmaktadır ve toprakların
---

