---
title: Disruption of the Water Cycle
backDescription: >-
  If the oceans and the atmosphere are hotter, the evaporation that takes place
  at the ocean surface increases. This means more rain clouds and more rain. If
  this happens on land, then the soil dries out.
---

