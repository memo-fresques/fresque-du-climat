---
title: Zaburzenia obiegu wody w przyrodzie
backDescription: >-
  Wraz z ocieplaniem się wody i powietrza wzrasta parowanie na powierzchni
  oceanów. W rezultacie tworzy się więcej chmur, z których spada deszcz. To samo
  zjawisko zachodzi na lądzie, ale tam powoduje wysychanie gleby.
---
Ta karta jest ważna. Sama w sobie pokazuje, dlaczego kiedyś mówiliśmy o globalnym ociepleniu, a teraz o zmianie klimatu. Wzrost temperatury jest sam w sobie problemem, ale na Mozaice na końcu widać, że zakłócenie obiegu wody ma znacznie większy wpływ.
