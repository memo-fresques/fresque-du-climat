---
title: Perturbation du cycle de l'eau
backDescription: >-
  L'évaporation qui a lieu à la surface de l'océan augmente si l'eau et l'air se
  réchauffent. Cela fait plus de nuages qui feront ensuite de la pluie. Mais si
  l’évaporation a lieu sur terre, cela assèche le sol.
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_20_perturbation_cycle_eau
youtubeCode: m16bWEIgUnI
instagramCode: CNsNyoWorRv
---

Cette carte est importante. Elle matérialise à elle seule pourquoi on parlait autrefois de réchauffement climatique, puis maintenant de changement climatique, voire de dérèglement climatique. L'augmentation de température est en soi un problème, mais on voit sur la fresque, à la fin, que la perturbation du cycle de l'eau a beaucoup plus d'effets.
