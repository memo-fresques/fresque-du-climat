---
title: Uraren zikloaren asaldurak
backDescription: >-
  Ura eta airea berotzen badira, ozeanoaren azalean sortzen den lurrunketa
  handitzen da. Euria eraginen duten hodei gehiago agertzen dira. Aldiz,
  lurrunketa lurraren azalean gertatzen bada, lurra idortzen da.
---

