---
title: Fikorotanan'ny tsingerin'ny rano
backDescription: >-
  Rehefa mihamafana ny rano sy ny rivotra dia mitombo ihany koa ny entona eo
  ambonin'ny ranomasimbe. Izany dia mahatonga rahona betsaka izay lasa orana avy
  eo. Fa raha ny tany no mamoaka entona dia lasa makina izy.
---

