---
title: Störning av vattnets kretslopp
backDescription: >-
  Varmare hav och en varmare atmosfär leder till mer avdunstning, vilket orsakar
  mer regnmoln följt av mer nederbörd. Men mer avdunstning över land torkar ut
  marken.
---
Det här är ett viktigt kort. Det visar varför man tidigare talade om global uppvärmning, men nu allt oftare om klimatförändringar eller till och med klimatstörningar. Temperaturökningen är i sig ett problem, men som vi ser i slutet av fresken så har störningar i vattnets kretslopp mycket större effekter.
