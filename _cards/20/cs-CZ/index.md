---
title: Narušení koloběhu vody
backDescription: >-
  Jestliže se voda v oceánech a atmosféra ohřívají, zvyšuje se odpařování vody
  nad povrchem oceánů. To znamená více dešťových mraků a více deště. Odpařování
  vody na pevnině má ale za následek vysychání půdy.
---

