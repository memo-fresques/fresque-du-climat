---
title: Disruption of the Water Cycle
backDescription: >-
  Hotter oceans and a hotter atmosphere lead to stronger evaporation, causing
  rainclouds and rainfall. Hotter land and a hotter atmosphere also lead to
  stronger evaporation, this time causing the ground to dry out.
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_20_disruption_water_cycle
youtubeCode: T13hvxFrBfA
instagramCode: CNsTPWcnXgZ
---

This card is important. It alone shows why we used to talk about global warming and now about climate change. Temperature increase is in itself a problem, but you can see on the Fresk at the end that disruption of the water cycle has much more effect.
