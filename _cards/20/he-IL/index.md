---
title: שיבוש מחזור המים
backDescription: >-
  האידוי שמתרחש בפני הים מתגבר ככל שטמפרטורת הים וטמפרטורת האטמוספירה עולות, דבר
  שגורם ליותר ענני גשם. אותה תופעת אידוי קיימת בין הקרקע לאוויר, ואז האדמה
  מתייבשת.
---

