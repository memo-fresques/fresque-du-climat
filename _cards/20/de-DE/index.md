---
title: Störung des Wasserkreislaufes
backDescription: >-
  Die Verdunstung, die an der Wasseroberfläche stattfindet, nimmt mit der
  Erwärmung von Wasser und Luft zu. Dadurch entstehen mehr Regenwolken. Das
  gleiche Verdunstungsphänomen besteht an Land und hat ein Austrocknen des
  Bodens zur Folge.
---

