---
title: Perturbação do ciclo da água
backDescription: >-
  Os oceanos mais quentes e uma atmosfera mais quente levam a uma evaporação
  mais forte, causando nuvens de chuva e precipitação. A terra mais quente e uma
  atmosfera mais quente conduzem também a uma evaporação mais forte, causando
  desta vez a secura do sol
---

Esta carta é importante: sozinha consegue ilustrar porque é que antes falávamos de Aquecimento Global, depois de Alterações Climáticas, e agora até de Desregulação Climática. O aumento da temperatura é por si só um problema, mas vemos no mural, no final, que a perturbação do ciclo da água tem muito mais efeitos.
