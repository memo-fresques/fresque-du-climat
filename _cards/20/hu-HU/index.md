---
title: Vízkörforgás zavara
backDescription: >-
  Ahogy az óceánok és a légkör felmelegszik, úgy az óceánok felszínéről történő
  párolgás mértéke is növekszik. Ez fokozottabb felhőképződéshez és esőhöz
  vezet. Ha a párolgás a szárazföld felett történik, a talaj kiszárad.
---

