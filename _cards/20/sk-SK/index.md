---
title: Narušenie vodného cyklu
backDescription: >-
  Vyparovanie vody z hladiny oceánu sa zvyšuje ak sa zvyšuje teplota vody a
  vzduchu. To spôsobuje väčšie množstvo dažďových oblakov. Ak sa ten istý
  fenomén vyparovania vyskytne na zemi, pôda sa vysúša.
---

