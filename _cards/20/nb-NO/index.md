---
title: Forstyrrelser i vannets kretsløp
backDescription: >-
  Fordampningen av vann fra havoverflaten øker ved økende vann- og
  lufttemperaturer. Dette fører til flere regnskyer og mer regn. Hvis
  temperaturøkningen skjer på land, tørker jorden ut.
---

Dette kortet er viktig. Det viser hvorfor vi tidligere snakket om global oppvarming, og nå om klimaendringer, eller til og med klimaforstyrrelser. Temperaturøkningen er i seg selv et problem, men vi ser på fresken, til slutt, at forstyrrelsen av vannsyklusen har mye større effekter.
