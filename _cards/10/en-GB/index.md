---
title: Aerosols Emissions
backDescription: >-
  Nothing to do with aerosol spray cans. Aerosols are a type of local pollution
  that comes from the incomplete combustion of fossil fuels. They are bad for
  human health and they negatively contribute to radiative forcing, meaning that
  they have a cooling ef
wikiUrl: 'https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_10_aerosols'
youtubeCode: '-RE5I12XKJQ'
instagramCode: COQL_hDoYWX
---

We advise you to skip this card in general unless participants have a good level, have time and you fell comfortable explaining it.

Aerosols are solid or liquid particules so little that their falling speed is close to zero. They prevent the sun's radiati
