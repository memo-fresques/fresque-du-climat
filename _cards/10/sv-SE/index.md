---
title: Aerosoler
backDescription: >-
  Klimatmässigt har detta inget att göra med sprayflaskor. Aerosoler är en form
  av lokal förorening som uppkommer vid ofullständig förbränning av fossila
  bränslen. De har skadlig inverkan på människors hälsa och bidrar negativt till
  strålningsdrivningen, vilket leder till ett svalare klimat.
---
Vi rekommenderar att du tar bort det här kortet om du inte känner dig bekväm med att förklara det, om deltagarna är på lägre nivå i sin förståelse eller om det är ont om tid. 

Aerosoler är små partiklar eller droppar som svävar i luften. De släpps ut vid förbränning av fossila bränslen men de finns också naturligt som ökendamm, vulkanaska eller sot från bränder. 

Aerosoler medför flera olika effekter. De försämrar människors hälsa. Vidare så minskar de uppvärmningen genom att reflektera solstrålning (inte alla aerosoler gör detta, t ex inte sotpartiklar) och slutligen spelar de en roll i molnbildning och därmed för nederbördsmönster[^1].
[^1]: _AR6 WG1 Glossary p2233 (s2216)_
