---
title: Aerosóly
backDescription: >-
  Nemajú nič spoločné s aerosólovými sprejmi. Aerosóly sú lokálnymi
  znečisťovateľmi, ktoré škodia zdraviu a pochádzajú z tých istých tovární a
  výfukových potrubí ako CO2. Negatívne prispievajú ku radiačnému pôsobeniu (tj.
  ochladzujú podnebie).
---

