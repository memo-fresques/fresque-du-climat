---
title: Aerossóis
backDescription: >-
  Nada relacionado com sprays de aerossol. Os aerossóis são poluição de base
  local, proveniente das mesmas fábricas e tubos de escape que emitem o CO2. São
  nocivos à saúde e contribuem negativamente para o Forçamento Radiativo
  (arrefecem o clima).
---
Esta carta deve ser retirada na maioria das vezes, a menos que os participantes tenham um bom nível, tenham tempo e o facilitador domine o assunto.

Aerossóis são pequenas partículas líquidas ou sólidas suspensas no ar. São emitidos pela combustão de combustíveis fósseis, mas também existem no seu estado natural, como poeira do deserto, cinzas vulcânicas ou fuligem de incêndios. 

Os aerossóis têm muitos impactos diferentes. Eles já fazem mal à saúde. Depois arrefecem o clima refletindo os raios solares (nem todos, nem o carbono negro, por exemplo) e finalmente desempenham um papel na formação de nuvens e, portanto, no regime de chuvas[^1].
[^1]: _Glossário AR6 WG1 p2233 (p2216)_
