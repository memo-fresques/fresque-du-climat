---
title: Aerosoli
backDescription: >-
  Nemaju nikakve veze sa aerosolnim limenkama. Aerosoli su vrsta lokalnog
  zagađenja koje potiče od nepotpunog sagorevanja fosilnih goriva. Štetna su za
  ljudsko zdravlje i nepovoljno doprinose zračenju (hlade klimu).
---

