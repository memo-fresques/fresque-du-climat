---
title: Aerosols
backDescription: >-
  Tsy misy hifandraisany amin'ny baomba. Ny "aérosols" dia fahalotoana avy
  amin'ny orinasa ary mitovy amin'ny setroka toy ny CO2. Ratsy amin'ny
  fahasalamana ary manampy amin'ny fihibana "forçage radiatif" (mampangatsiaka
  ny andro izy ireo)
---

