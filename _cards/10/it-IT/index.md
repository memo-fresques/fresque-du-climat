---
title: Aerosol
backDescription: >-
  Niente a che fare con l'aerosol che si usa contro tosse e raffreddore. Gli
  aerosol sono un tipo di inquinamento locale, come il diossido di zolfo, che
  deriva dalla combustione incompleta dei combustibili fossili. Sono nocivi per
  la salute e hanno un contributo negativo sul forzante radiativo (raffreddano
  il clima).
---
Questa carta dovrebbe essere rimossa nella maggior parte dei casi, a meno che i partecipanti non abbiano un buon livello, ci sia tempo sufficiente e chi facilita padroneggi l'argomento.

Gli aerosol sono piccole particelle liquide o solide sospese nell'aria. Sono emessi dalla combustione di combustibili fossili, ma esistono anche allo stato naturale, come le polveri del deserto, le ceneri vulcaniche o il fumo degli incendi. 

Gli aerosol hanno svariati impatti molto diversi. Innanzitutto, sono dannosi per la salute. In secondo luogo, raffreddano il clima riflettendo i raggi del sole (non tutti, ad esempio non il carbonio nero) e infine contribuiscono alla formazione delle nuvole e quindi al regime delle piogge[^1].
[^1]: _AR6 WG1 Glossary p2233 (p2216)_
