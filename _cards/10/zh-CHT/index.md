---
title: 氣溶膠(懸浮微粒)
backDescription: 氣溶膠(懸浮微粒)是一種污染，和二氧化碳一樣來自工廠和車輛的廢氣排放，少部分來自農業和火山爆發，對人體健康有害，也會造成輻射冷卻效應。
---

