---
title: Аерозолі
backDescription: >-
  Не треба сплутувати аерозолі з аерозольними балончиками, фарбами чи спреями.
  Тут йдеться про місцеве забруднення, спричинене неповним згорянням горючих
  корисних копалин. Вони мають негативний вплив на радіаційний баланс (тобто
  охолоджують клімат) але несу
---

