---
title: Aerosols
backDescription: >-
  Deze luchtvervuiling wordt meestal veroorzaakt door industrie en transport en
  in mindere mate door landbouw en bosbranden. Sommige aerosols reflecteren het
  zonlicht en hebben daarom een koelende werking op het klimaat.
---

We adviseren je deze kaart over te slaan tenzij de deelnemers een goed kennisniveau hebben, je genoeg tijd hebt en je je comfortabel voelt om dit uit te leggen. Aerosolen zijn vaste of vloeibare deeltjes die zo klein zijn dat hun valsnelheid bijna nul is. Ze voorkomen dat de straling van de zon de aarde bereikt en zorgen zo voor een afkoelend effect.
