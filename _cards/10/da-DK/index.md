---
title: Aerosoler
backDescription: >-
  Aerosoler har her ikke noget at gøre med spraydåser. De er luftbårne
  partikler, som primært stammer fra ufuldstændig afbrænding af fossile
  brændsler. De forurener luften og udgør et væsentligt sundhedsproblem.
  Aerosolerne ‘skygger for solen’ og har dermed
---

