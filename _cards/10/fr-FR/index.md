---
backDescription: >-
  Rien à voir avec les bombes aérosols. Les aérosols sont une pollution locale
  qui vient de la combustion imparfaite des énergies fossiles. _x000B_Ils sont
  mauvais pour la santé et ils ont par ailleurs une contribution négative au
  forçage radiatif (ils refroidiss
title: Émissions d'aérosols
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_10_a%C3%A9rosols
youtubeCode: XRAKRtOQ_Fg
instagramCode: COQbQy2o6Px
---
Cette carte mérite d'être supprimée la plupart du temps, sauf à ce que les participants aient un bon niveau, qu'ils aient du temps, et que l'animateur maitrise le sujet.

Les aérosols sont de petites particules liquides ou solides en suspension dans l’air. Ils sont émis par la combustion des énergies fossiles mais il en existe également à l’état naturel comme les poussières du désert, les cendres volcaniques ou la suie des incendies. 

Les aérosols ont de nombreux impacts très différents. Déjà ils sont mauvais pour la santé. Ensuite ils refroidissent le climat en réflechissant les rayons du soleil (pas tous, pas le carbone noir par exemple) et enfin ils jouent un rôle dans la formation des nuages et donc dans le régime des pluies[^1].
[^1]: _AR6 WG1 Glossary p2233 (p2216)_
