---
title: Aerozole
backDescription: >-
  Nie mylić ze sprejami. Aerozole to lokalne zanieczyszczenia, takie jak np.
  dwutlenek siarki, które pochodzą z niepełnego spalania paliw kopalnych. Mają
  niekorzystny wpływ na zdrowie, a także przyczyniają się do ujemnego wymuszenia
  radiacyjnego (ochładzają klimat).
---
Radzimy pominąć tę kartę, chyba że uczestnicy są na dobrym poziomie, masz czas i czujesz się komfortowo, wyjaśniając ją.
