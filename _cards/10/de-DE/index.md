---
title: Aerosole
backDescription: >-
  Ein Aerosol ist ein Gas, in dem winzige, feste oder flüssige Teilchen schweben
  (Aerosolpartikel). Aerosole werden freigesetzt bei der unvollständigen
  Verbrennung fossiler Brennstoffe (z.B. in Fabriken), was zu lokaler
  Verschmutzung führt. Sie sind gesundheitsschädlich und wirken sich negativ auf
  den Strahlungsantrieb aus (entgegen dem Treibhauseffekt, abkühlende Wirkung).
---
Wir empfehlen grundsätzlich, diese Karte zu überspringen, es sei denn, die Teilnehmer haben gute Grundkenntnisse, ausreichend Zeit, und Sie fühlen sich wohl dabei, sie zu erklären.
