---
backDescription: >-
  Rien à voir avec les bombes aérosols. Les aérosols sont une pollution locale
  qui vient de la combustion imparfaite des énergies fossiles. _x000B_Ils sont
  mauvais pour la santé et ils ont par ailleurs une contribution négative au
  forçage radiatif (ils refroidiss
instagramCode: COQbQy2o6Px
title: Aerosoloù
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_10_a%C3%A9rosols
youtubeCode: XRAKRtOQ_Fg
---

