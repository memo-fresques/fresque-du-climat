---
title: Aerossóis
backDescription: >-
  Não se trata dos sprays de aerossóis. Os aerossóis são poluição de base local
  proveniente da combustão imperfeita de combustíveis fósseis. São nocivos à
  saúde e contribuem negativamente para o forçamento radiativo ( esfriam o
  clima).
---

