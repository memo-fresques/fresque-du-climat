---
title: Aerosoler
backDescription: >-
  Aerosoler er en type lokal forurensning som kommer fra forbrenning av fossil
  brensel. De er helseskadelige og bidrar negativt til strålingspådrivet (de
  avkjøler klimaet).
---
Dette kortet kan med fordel utelates, med mindre deltakerne har god forhåndskunnskap, god tid, og lederen mestrer emnet.

Aerosoler er små flytende eller faste partikler finfordelt i luften. De slippes ut ved forbrenning av fossile energikilder, men de finnes også naturlig som ørkenstøv, vulkansk aske eller sot fra branner.

Aerosoler har flere ulike effekter. For det første er de dårlige for helsa. Deretter kjøler de ned klimaet ved å reflektere solstrålene (ikke alle, for eksempel ikke sot) og til slutt spiller de en rolle i dannelsen av skyer og dermed i nedbørsmønsteret[^1].
[^1]: _AR6 WG1 Glossary s2233 (s2216)_
