---
title: Aerosols
backDescription: >-
  Nothing to do with aerosol spray cans. Aerosols are a type of local pollution
  that comes from the incomplete combustion of fossil fuels. They are bad for
  human health and they contribute negatively to radiative forcing (they cool
  the climate).
---

