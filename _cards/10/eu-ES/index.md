---
title: Aerosolak
backDescription: >-
  Honek ez du zerikusirik aerosol espraiekin. Aerosolak hurbileko kutsadura
  dira, CO2-a isurtzen duten fabrika eta ihes hodi beretatik datorrena.
  Osasunarentzat kaltegarriak dira eta eragin ezkorra dute erradiazioaren
  bortxaketan (klima hozten dute).
---

