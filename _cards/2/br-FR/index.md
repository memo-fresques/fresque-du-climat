---
backDescription: >-
  L'industrie utilise des énergies fossiles et de l'électricité. Elle représente
  40% des Gaz à Effet de Serre (GES).
instagramCode: CKmDdUKoIkp
title: Greanterezh
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_2_industrie'
youtubeCode: 4LG5GMiCcR0
---

