---
title: Industria
backDescription: >-
  L'industria utilizza combustibili fossili ed elettricità. È responsabile del
  40% delle emissioni di gas serra (GHG).
---
Il settore industriale rappresenta il 34% delle emissioni di gas serra nel mondo (2019)[^1]. L'industria non include solo la produzione di beni di consumo, ma anche l'estrazione di minerali, la costruzione e la gestione dei rifiuti[^2]. L'industria comprende un gran numero di settori industriali diversi, tra cui acciaio, cemento, chimica, alluminio e carta[^3]. Per ridurre le emissioni dell'industria, devono essere utilizzate molte leve: cambio di combustibile, elettrificazione, decarbonizzazione dell'elettricità, economia circolare, materie prime non provenienti da fonti fossili, efficienza energetica, cattura e utilizzo o stoccaggio di carbonio (CCU e CCS)[^4].
[^1]: _AR6 WG3 11.2.2 p1185 (p1172) // AR6 WG3 Figura TS.6 p18 (p66)_
[^2]: _AR6 WG3 11.1.1 p1178 (p1165)_
[^3]: _AR6 WG3 Figura 11.4 // p1186 (p1173) // AR6 WG3 Tabella 11.1 p1188 (p1175)_
[^4]: _AR6 WG3 11.1.1 p1178 (p1165)_
