---
title: Industri
backDescription: >-
  Industrien bruger store mængder energi fra fossile brændstoffer og el. Den er
  ansvarlig for 40% af den samlede udledning af drivhusgasser.
---

