---
title: Industri
backDescription: >-
  Industrien bruker fossil energi og elektrisitet. Den står for 40 % av de
  totale klimagassutslippene.
---
Industrien står for 34 % av klimagassutslippene i verden (2019)[^1]. Industrien omfatter produksjonen av alle forbruksvarer, utvinning av mineraler, bygging og avfallshåndtering[^2]. Industrien inkluderer et svært stort antall forskjellige industrisektorer som stål, sement, kjemi, aluminium og papir[^3]. For å redusere utslippene fra industrien må mange tiltak brukes: drivstoffskifte, elektrifisering, avkarbonisering av elektrisitet, sirkulær økonomi, råvarer som ikke kommer fra fossile energikilder, energieffektivitet, karbonfangst og bruk eller lagring (CCU og CCS)[^4].  
[^1]: _AR6 WG3 11.2.2 s1185 (s1172) // AR6 WG3 Figur TS.6 s18 (s66)_
[^2]: _AR6 WG3 11.1.1 s1178 (s1165)_
[^3]: _AR6 WG3 Figur 11.4 // s1186 (s1173) // AR6 WG3 Tabell 11.1 s1188 (s1175)_
[^4]: _AR6 WG3 11.1.1 s1178 (s1165)_
