---
title: Indústria
backDescription: >-
  A indústria utiliza combustíveis fósseis e eletricidade. Representa 40% das
  emissões de gases de efeito estufa (GEE).
---
A Indústria representa 34% das emissões de gases com efeito estufa em todo o mundo (2019)[^1]. A Indústria abrange não apenas a produção de todos os bens de consumo, mas também a extração mineral, a construção e a gestão de resíduos[^2]. A Indústria reúne um grande número de diferentes setores industriais, incluindo aço, cimento, produtos químicos, alumínio e até produção de papel[^3]. Para reduzir as emissões industriais, muitas soluções devem ser utilizadas: mudança de combustível, eletrificação, descarbonização da eletricidade, economia circular, matérias-primas não provenientes de combustíveis fósseis, eficiência energética, captura e utilização ou armazenamento de carbono (CCU e CCS)[^4] .  
[^1]: _AR6 WG3 11.2.2 p1185 (p1172) // AR6 WG3 Figura TS.6 p18 (p66)_
[^2]: _AR6 WG3 11.1.1 p1178 (p1165)_
[^3]: _AR6 WG3 Figura 11.4 // p1186 (p1173) // AR6 WG3 Tabela 11.1 p1188 (p1175)_
[^4]: _AR6 WG3 11.1.1 p1178 (p1165)_
