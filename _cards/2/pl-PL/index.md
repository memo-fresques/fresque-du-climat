---
title: Przemysł
backDescription: >-
  Przemysł wykorzystuje paliwa kopalne oraz energię elektryczną. Odpowiada za
  40% emisji gazów cieplarnianych.
---
Jest to produkcja wszystkich dóbr konsumpcyjnych. Przemysł składa się z wielu różnych sektorów, z których najbardziej znaczące pod względem emisji gazów cieplarnianych są przemysł papierniczy, cementowy, stalowy, aluminiowy i chemiczny. Aby zmniejszyć emisje z przemysłu, rozwiązaniem jest wydłużenie żywotności produktów i zmniejszenie konsumpcji.
