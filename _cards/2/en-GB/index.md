---
title: Industry
backDescription: >-
  Industry uses fossil fuels and electricity. It accounts for 40% of greenhouse
  gas (GHG) emissions.
wikiUrl: 'https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_2_industry'
youtubeCode: '-Hbo56WQ3zo'
instagramCode: CKmEzYeH590
---

This is the manufacturing of all consumer goods. Industry is comprised of many different sectors, the most prominent in terms of GHG emissions being the paper, cement, steel, aluminium and chemicals industries. In order to reduce emissions from industry, the solution lies in extending the lifespan of products and reducing consumption.
