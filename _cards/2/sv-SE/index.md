---
title: Industri
backDescription: >-
  Industrin använder fossila bränslen och elektricitet. Den står för 40% av
  växthusgasutsläppen.
---
Industrin står för 34 % av de globala växthusgasutsläppen (2019)[^1]. Industrin omfattar inte bara tillverkning av alla konsumtionsvaror utan även utvinning av mineraler, byggande och avfallshantering[^2]. Industrin omfattar ett mycket stort antal olika industrisektorer, inklusive stål, cement, kemi, aluminium och pappersmassa[^3]. För att minska industrins utsläpp måste många åtgärder vidtas: nya bränslen, elektrifiering, fossilfri el, cirkulär ekonomi, råvaror som inte kommer från fossila bränslen, energieffektivitet, koldioxidinfångning och användning eller lagring (CCU och CCS)[^4].  
[^1]: _AR6 WG3 11.2.2 p1185 (p1172) // AR6 WG3 Figure TS.6 p18 (p66)_
[^2]: _AR6 WG3 11.1.1 p1178 (p1165)_
[^3]: _AR6 WG3 Figure 11.4 // p1186 (p1173) // AR6 WG3 Table 11.1 p1188 (p1175)_
[^4]: _AR6 WG3 11.1.1 p1178 (p1165)_
