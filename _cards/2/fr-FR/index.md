---
backDescription: >-
  L'industrie utilise des énergies fossiles et de l'électricité. Elle représente
  40% des Gaz à Effet de Serre (GES).
title: Industrie
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_2_industrie'
youtubeCode: 4LG5GMiCcR0
instagramCode: CKmDdUKoIkp
---
L'industrie représente 34% des émissions de gaz à effet de serre dans le monde (2019)[^1]. L'industrie englobe non seulement la fabrication de l'ensemble des biens de consommation mais également l'extraction de minerais, la construction et la gestion des déchets[^2]. L'industrie regroupe un très grand nombre de secteurs industriels différents dont l'acier, le ciment, la chimie, l'aluminium ou encore la papeterie[^3]. Pour réduire les émissions de l'industrie, de nombreux leviers doivent être utilisés : changement de combustible, électrification, décarbonisation de l’électricité, économie circulaire, matières premières ne provenant pas d'énergies fossiles, efficacité énergétique, captage et utilisation ou stockage du carbone (CCU et CCS)[^4].  
[^1]: _AR6 WG3 11.2.2 p1185 (p1172) // AR6 WG3 Figure TS.6 p18 (p66)_
[^2]: _AR6 WG3 11.1.1 p1178 (p1165)_
[^3]: _AR6 WG3 Figure 11.4 // p1186 (p1173) // AR6 WG3 Table 11.1 p1188 (p1175)_
[^4]: _AR6 WG3 11.1.1 p1178 (p1165)_
