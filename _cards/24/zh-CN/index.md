---
title: 海洋酸化
backDescription: ' 当CO2在海洋这个碳汇中溶解后，一部分会与水化合，形成碳酸（H2CO3)，碳酸不稳定继而分解为氢离子（H+) 及碳酸氢根离子（HCO3-)。当二氧化碳浓度上升后，酸性离子含量增加进而造成海洋的酸化（即PH值降低）。'
---

