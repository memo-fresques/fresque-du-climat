---
backDescription: >-
  Quand le CO2 se dissout dans l'océan, il se transforme en des ions acides
  (H2CO3 puis HCO3-). Cela a pour effet d'acidifier l'océan (le pH baisse).
instagramCode: ''
title: Trenkadur ar meurvor
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_24_acidification_oc%C3%A9an
youtubeCode: XlaLH13Pups
---

