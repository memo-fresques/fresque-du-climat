---
title: Okyselování oceánů
backDescription: >-
  Když se CO2 rozpustí v oceánu, změní se na kyselé ionty (H2CO3 a HCO3-). To má
  za následek okyselování oceánů (klesá hodnota pH).
---

