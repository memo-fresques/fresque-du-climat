---
title: L'acidificazione dell'oceano
backDescription: >-
  Quando la CO2 si discioglie nell'oceano, si trasforma in ioni che causano
  l'acidificazione dell'oceano (il pH diminuisce).
---
L'acidificazione degli oceani a volte viene chiamata "l'altro problema del carbonio". È legata alla dissoluzione di CO2 nell'oceano che reagisce con l'acqua e aumenta la quantità di ioni H+. Sono questi ioni H+ a costituire ciò che chiamiamo acidità. Maggiore la presenza di ioni H+, maggiore è l'acidità (e minore il pH). Si noti che, sebbene si parli di acidificazione dell'oceano, il suo pH rimane alcalino (circa 8, l'acidità è inferiore a 7)[1]. 

CO2 + H2O ⇔ H2CO3 ⇔ H+ + HCO3- ⇔ 2 H+ + CO32– 
_[1] AR6 WG2 3.2.3.1
AR6 WG1 2.3.3.5 p408 (p396)
p374 (p357) // AR6 WG1 Figura 4.8 : scenario pH
AR5 WG1 Box 3.2, Figura 1: distribuzione dei pH p594 (p577)
p311 (p295)_
