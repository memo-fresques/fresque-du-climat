---
title: Versauerung der Ozeane
backDescription: >-
  Wenn sich CO2 im Ozean auflöst, wird es in saure Ionen umgewandelt (H2CO3 und
  HCO3-). Dies führt zur Versauerung der Ozeane (der pH-Wert sinkt).
---

