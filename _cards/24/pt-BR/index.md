---
title: Acidificação dos oceanos
backDescription: >-
  Quando o CO2 se dissolve no oceano, transforma-se em H2CO3 e HCO3- que são
  íons ácidos. O efeito dessa transformação é a acidificação do oceano (o pH
  diminui).
---

