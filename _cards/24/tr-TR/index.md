---
title: Okyanusların asitleşmesi
backDescription: >-
  CO2 okyanuslarda çözündükçe, asit iyonlarına dönüşür (H2CO3 ve HCO3). Bu
  dönüşümün sonucu olarak da okyanuslar asitleşir (pH değeri düşer).
---

