---
title: Acidificação dos oceanos
backDescription: >-
  Quando o CO2 se dissolve no oceano, transforma-se em iões ácidos (H2CO3 e
  HCO3-). O efeito dessa transformação é a acidificação do oceano (o pH
  diminui).
---
"A acidificação do oceano é por vezes chamada de ""o outro problema do carbono"". Está relacionada com a dissolução de CO2 no oceano que reage com a água e aumenta a quantidade de iões H+. São os iões H+ que constituem o que se chama acidez. Quanto mais iões H+ houver, maior é a acidez (e o pH diminui). Note-se que, embora se fale de acidificação do Oceano, o seu pH permanece alcalino (cerca de 8, a acidez é abaixo de 7)[1]. 

CO2 + H2O ⇔ H2CO3 ⇔ H+ + HCO3- ⇔ 2 H+ + CO32– 
_[1] AR6 WG2 3.2.3.1
AR6 WG1 2.3.3.5 p408 (p396)
p374 (p357) // AR6 WG1 Figura 4.8: cenário pH
AR5 WG1 Caixa 3.2, Figura 1: distribuição dos pH p594 (p577)
p311 (p295)_"
