---
title: Havsförsurning
backDescription: >-
  När koldioxid löses upp i haven, omvandlas det till vätekarbonatjoner (H2CO3
  och sedan HCO3-). Detta leder till en försurning av haven (pH-värdet minskar).
---
Försurning av haven kallas ibland för "det andra kolproblemet". Det sker när CO2 löses i havet. CO2 reagerar då  med vatten och mängden H+ joner ökar. Ju fler H+-joner det finns, desto högre surhetsgrad (och ju lägre pH-värde). Haven är fortfarande alkaliskt (pH ca 8), men mindre än tidigare._[1]. 

CO2 + H2O ⇔ H2CO3 ⇔ H+ + HCO3- ⇔ 2 H+ + CO32– 
_[1] AR6 WG2 3.2.3.1
AR6 WG1 2.3.3.5 p408 (p396)
p374 (p357) // AR6 WG1 Figur 4.8: pH-scenario
AR5 WG1 ruta 3.2, figur 1: pH-fördelning p594 (p577)
s311 (s295)_"
