---
title: Havenes forsuring
backDescription: >-
  Når havvand optager CO2, reagerer gassen med H2O og danner syre-ioner (H2CO3
  og HCO3), som får vandets pH-værdi til at falde. Dette medfører en forsuring
  af havene.
---

