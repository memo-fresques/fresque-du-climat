---
title: Ocean Acidification
backDescription: >-
  When CO2 dissolves in the ocean, it turns into acid ions (H2CO3 and HCO3-).
  The effect of this transformation is ocean acidification (the pH decreases).
---

