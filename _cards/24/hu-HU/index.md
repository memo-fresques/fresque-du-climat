---
title: Óceánok savasodása
backDescription: >-
  Amikor a CO2 feloldódik az óceánban, savas ionvegyületekké (H2CO3: szénsav és
  HCO3-: hidrogén-karbonát) alakul. Ezen átalakulás következménye az óceánok
  savasodása (a pH csökken).
---

