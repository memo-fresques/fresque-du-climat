---
title: Acidificación del océano
backDescription: >-
  Cuando el CO2 se disuelve en el océano, se transforma en iones ácidos (el
  H2CO3 pasa a HCO3-). Esto tiene como efecto la acidificación del océano (el pH
  baja).
---

