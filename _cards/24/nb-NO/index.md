---
title: Havforsuring
backDescription: >-
  Når CO2 løses opp i sjøvann dannes det syreioner (H2CO3 og HCO3-). Dette fører
  til havforsuring (pH-verdien går ned).
---
Forsuring av havet kalles noen ganger ""det andre karbonproblemet". Det er knyttet til oppløsningen av CO2 i havet som reagerer med vann og øker mengden H+ ioner. Det er H+ ionene som utgjør det vi kaller surhet. Jo flere H+ ioner, jo høyere surhet (og lavere pH). Merk at selv om vi snakker om forsuring av havet, forblir pH-verdien alkalisk (omtrent 8, surhet er under 7)[1]. 

CO2 + H2O ⇔ H2CO3 ⇔ H+ + HCO3- ⇔ 2 H+ + CO32– 
_[1] AR6 WG2 3.2.3.1
AR6 WG1 2.3.3.5 s408 (s396)
s374 (s357) // AR6 WG1 Figur 4.8: scenario pH
AR5 WG1 Boks 3.2, Figur 1: fordeling av pH s594 (s577)
s311 (s295)_"
