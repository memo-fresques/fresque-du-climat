---
title: Zakiseljavanje oceana
backDescription: >-
  Kad se CO2 otopi u oceanu, pretvara se u ugljičnu kiselinu H2CO3 te
  bikarbonatni ion HCO3-. To povećava kiselost oceana i pH vrijednost pada.
---

