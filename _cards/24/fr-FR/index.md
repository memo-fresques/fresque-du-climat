---
backDescription: >-
  Quand le CO2 se dissout dans l'océan, il se transforme en des ions qui ont
  pour effet d'acidifier l'océan (le pH baisse).
title: Acidification de l'océan
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_24_acidification_oc%C3%A9an
youtubeCode: XlaLH13Pups
---
"L'acidification de l'océan est parfois appelée ""l'autre problème du carbone"". Il est lié à la dissolution de CO2 dans l'océan qui réagit avec l'eau et augmente la quantité d'ions H+. Ce sont les ions H+ qui constituent ce qu'on appelle l'acidité. Plus il y a d'ions H+, plus l'acidité est élevée (et le pH bas). A noter que bien qu'on parle d'acidification de l'Océan, son pH reste alcalin (environ 8, l'acidité c'est en dessous de 7)[1]. 

CO2 + H2O ⇔ H2CO3 ⇔ H+ + HCO3- ⇔ 2 H+ + CO32– 
_[1] AR6 WG2 3.2.3.1
AR6 WG1 2.3.3.5 p408 (p396)
p374 (p357) // AR6 WG1 Figure 4.8 : scenario pH
AR5 WG1 Box 3.2, Figure 1 : répartition des pH p594 (p577)
p311 (p295)_"
