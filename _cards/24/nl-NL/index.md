---
title: Verzuring van oceanen
backDescription: >-
  Wanneer koolstofdioxide (CO2) in water (H2O) terechtkomt, lost het op en vormt
  het waterstofcarbonaat (H2CO3). Dit carbonaat heeft echter zwakke bindingen en
  zal één of twee waterstofionen (H+) afscheiden. Door de hogere concentratie
  waterstofionen daalt
---
De verzuring van de oceanen wordt ook wel "het andere koolstof-probleem" genoemd. Het is niet direct een consequentie van klimaatverandering, maar een andere consequentie van CO2-emissies
