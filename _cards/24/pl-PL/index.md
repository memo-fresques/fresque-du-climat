---
title: Zakwaszenie oceanów
backDescription: >-
  Pochłonięty przez ocean CO2, rozpuszczając się w wodzie zmienia się w jony o
  charakterze kwasowym (H2CO3 i HCO3-). Powoduje to wzrost kwasowości oceanów
  (obniżenie pH).
---
Zakwaszenie oceanów jest czasami określane jako „ten drugi problem węglowy”. Nie jest to ściśła konsekwencja zmian klimatu, ale kolejna konsekwencja emisji CO2.
