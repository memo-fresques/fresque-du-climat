---
title: Ocean Acidification
backDescription: >-
  When CO2 dissolves in the ocean, it turns into acid ions (H2CO3 and HCO3-).
  This makes the oceans more acidic and the pH drops.
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_24_ocean_acidification
youtubeCode: ppZenThTh5c
instagramCode: ''
---

Ocean acidification is sometimes referred to as "the other carbon problem". It is not strictly speaking a consequence of climate change, but another consequence of CO2 emissions.
