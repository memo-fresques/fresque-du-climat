---
title: 海洋酸化
backDescription: 當二氧化碳在海洋中溶解後，會與水分子作用形成多種碳酸(H2CO3，HCO3-)，造成海洋酸化效應(pH酸鹼值降低)
---

