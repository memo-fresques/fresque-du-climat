---
title: Aumento da temperatura da água
backDescription: >-
  O oceano absorve 91% da energia acumulada na Terra. À medida que aquece, a
  água expande.
---
A superfície dos oceanos está a aquecer menos rapidamente do que a Terra (0,88°C versus 1,59°C)[^1].Como é que isso acontece se o oceano absorve 91% da energia em excesso na Terra? [^2].É preciso lembrar que o oceano cobre 71% da superfície da terra e que tem uma profundidade média de 4000m. A atmosfera estende-se a uma altura maior, mas se a trouxéssemos para a mesma densidade da água, ela não mediria mais do que 10m (é por isso que ganhamos uma atmosfera de pressão a cada 10m quando mergulhamos). Além disso, a água tem uma capacidade térmica muito mais elevada do que a maioria dos materiais terrestres. Isso significa que ela pode absorver e armazenar uma quantidade considerável de calor sem que a sua temperatura mude muito. Portanto, mesmo que o oceano absorva uma grande quantidade de energia, o aumento da temperatura da água é relativamente baixo em comparação com o da Terra[^3].A água expande-se muito pouco, mas devido ao enorme volume do oceano, isso é suficiente para ter um impacto significativo no nível do mar. Estima-se que a contribuição para o aumento do nível do mar devido à expansão das suas águas seja de cerca de 50%.[^4].
[^1]: _AR6 WG1 Cross-Section TS.1 p28 (p60) // AR6 WG1 Figure 2.11 (c) p333 (p316)_
[^2]: _AR6 WG1 TS3.1 p59 (p91) // AR6 WG1 Figure TS.13 (d) p58 (p90)_ 
[^3]: _AR5 WG1 FAQ 3.1 p11 (p127)_
[^4]: _AR6 WG1 Box TS.4 p45 // AR6 WG1 Table 9.5 p1306 (p1289) // AR6 WG1 Box 9.1, Figure 1 p1308 (p1291)_
