---
title: Rising Water Temperature
backDescription: >-
  Oceans absorb 91% of the energy accumulated on Earth. The water temperature
  has therefore increased, especially close to the surface. Water expands as it
  warms.
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_17_Rising_Water_Temperatures
youtubeCode: hTmQXSS0lWs
instagramCode: ''
---

The ocean is warming by only about a tenth of a degree at the surface and even less under water. Why so little when it absorbs 91% of the excess energy on Earth? This is because it is much larger than the atmosphere and it has a much greater calorific capacity. To measure this, you need to remember that the ocean covers 71% of the Earth's surface and that it has a depth of 4000 m on average. The atmosphere extends over a greater height, but if brought back to the same density as water, it would only be 10 m thick. (That's why we gain one atmosphere of pressure every 10 m when we dive.) Water expands very little. How can warming the ocean by a tenth of a degree result in a rise in the water level? A first answer is that the ocean is 4000 m deep on average, so a very small expansion is enough to amount to a few centimetres.
