---
title: Porast temperature vode
backDescription: >-
  Okeani apsorbuju 93% energije uskladištene na Zemlji. Zato je temperatura
  okeana porasla, naročito u gornjim slojevima. Voda se zagrevanjem širi.
---

