---
title: Zvýšení teploty vody
backDescription: >-
  Oceány absorbují 93 % energie nahromaděné na Zemi. Jejich teplota se zvýšila
  zejména v horních vrstvách. Ohříváním se plocha vody rozšiřuje.
---

