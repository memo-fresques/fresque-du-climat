---
title: Su sıcaklığındaki artış
backDescription: >-
  Okyanuslar, dünyaya gelen enerjinin %93'ünü toplamaktadır. Dolayısıyla
  okyanusların sıcaklığı, özellikle de üst katmanlarda artmıştır. Su, sıcaklığı
  arttıkça (4°C'den yüksek sıcaklıklarda) genleşir.
---

