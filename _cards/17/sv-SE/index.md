---
title: Ökning av vattentemperaturen
backDescription: >-
  Havet absoberar 91% av energin som ackumulerats på jorden. Vatten expanderar
  när det blir varmare.
---
Havets yta värms upp långsammare än marken (0,88°C mot 1,59°C)[^1]. Hur kommer det sig när havet absorberar hela 91 % av jordens överskottsenergi? [^2]. Det beror dels på att havet har större massa än atmosfären, och dels på att vattens värmekapacitet är hög. Havet täcker 71 % av jordens yta och har ett djup på i genomsnitt 4000m.  Atmosfären sträcker sig över en större höjd, men om vi komprimerar den till samma densitet som vatten skulle den bara mäta 10 meter (det är därför 10 m vatten räknas som en atmosfärs tryck när man dyker). På grund av sin höga värmekapacitet kan vatten lagra mycket värme utan att temperaturen förändras särskilt mycket. Så även om havet absorberar en stor mängd energi, är ökningen av vattentemperaturen relativt liten jämfört med land[^3]. Vatten expanderar ganska lite då det värms upp, men med tanke på havets enorma volym är det tillräckligt för att ha en betydande påverkan på havsnivån. Det uppskattas att havets ökade vattenvolym pga temperaturökning utgör ca 50% av havsnivåhöjningen [^4].  
[^1]: _AR6 WG1 Tvärsnitt TS.1 p28 (p60) // AR6 WG1 Figur 2.11 (c) p333 (p316)_
[^2]: _AR6 WG1 TS3.1 p59 (p91) // AR6 WG1 Figur TS.13 (d) p58 (p90)_ 
[^3]: _AR5 WG1 FAQ 3.1 p11 (p127)_
[^4]: _AR6 WG1 Box TS.4 p45 // AR6 WG1 Table 9.5 p1306 (p1289) // AR6 WG1 Box 9.1, Figure 1 p1308 (p1291)_
