---
title: Creșterea temperaturii apei
backDescription: >-
  Oceanul absoarbe 93% din energia care se acumulează pe Pământ. Pe măsură ce se
  încălzește, apa se dilată.
---

