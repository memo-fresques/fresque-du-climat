---
title: Toename van de watertemperatuur
backDescription: >-
  De zeeën en oceanen absorberen 93% van de energie geaccumuleerd op de aarde.
  De temperatuur van de oceanen stijgt, vooral in de bovenste lagen, en door
  deze stijging zetten de oceanen uit.
---
Oceanen warmen aan de oppervlakte slechts een tiende van een graad op, en onder water nog minder. Waarom maar zo weinig, als ze 91% van de extra energie op aarde opnemen? Dit komt omdat de oceanen veel groter zijn dan de atmosfeer, en een veel grotere warmtecapaciteit hebben. Oceanen bedekken 71% van het oppervlak van de aarde, en zijn gemiddeld 4000m diep. De atmosfeer strekt zich weliswaar uit over een grotere afstand, maar als het teruggebracht zou worden naar de dichtheid van water zou de atmosfeer slechts 10 meter dik zijn. (Daarom neemt de druk met 1 atmosfeer toe als je 10 meter dieper duikt.) Water zet niet veel uit. Hoe kan de opwarming met een tiende van een graad dan toch bijdragen aan de stijging van de waterspiegel? Een deel van het antwoord is dat omdat de oceanen gemiddeld 4000 meter diep zijn, een kleine uitzetting van zo'n enorme hoeveelheid water al snel een paar centimeters stijging geeft. Het uitgebreide antwoord vind je in de Factsheet.
