---
title: Aumento de la temperatura del agua
backDescription: >-
  El océano absorbe 93% de la energía acumulada en el planeta haciendo que la
  temperatura del agua también aumente, especialmente en la superficie. Cuando
  se calienta, el agua se dilata.
---

