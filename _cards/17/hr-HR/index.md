---
title: Povišenje temperature vode
backDescription: >-
  Oceani apsorbiraju 91% energije nakupljene na Zemlji. Temperatura vode stoga
  se povisila, osobito na površini. Voda se zagrijavanjem širi.
---

