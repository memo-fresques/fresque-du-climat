---
backDescription: >-
  L'océan absorbe 91% de l’énergie qui s’accumule sur la Terre. En se
  réchauffant, l’eau se dilate.
title: Hausse de la température de l'eau
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_17_hausse_temp%C3%A9rature_eau
youtubeCode: TRhhK8tJ9ds
---
La surface des océans se réchauffe moins vite que les terres (0.88°C versus 1.59°C)[^1].Comment cela se fait-il alors qu'il absorbe 91% de l'énergie en excès sur la terre ? [^2].Il faut se rappeler que l'océan couvre 71% de la surface de la terre et qu'il a une profondeur de 4000m en moyenne.  L'atmosphère s'étend sur une plus grande hauteur, mais si on le ramenait à la même densité que l'eau, il ne mesurerait plus que 10m (c'est pour ça qu'on gagne un atmosphère de pression tous les 10m quand on fait de la plongée). Mais aussi, l'eau a une capacité thermique beaucoup plus élevée que la plupart des matériaux terrestres. Cela signifie qu'elle peut absorber et stocker une quantité considérable de chaleur sans que sa température ne change beaucoup. Par conséquent, même si l'océan absorbe une grande quantité d'énergie, la hausse de température de l'eau est relativement faible par rapport à celle des terres[^3].L'eau se dilate très peu, mais au vu de l'énorme volume de l'océan, cela suffit à avoir un impact important sur le niveau des mers. On estime que la contribution à la hausse du niveau des mers de la dilatation de ses eaux est d'environ 50%.[^4].  
[^1]: _AR6 WG1 Cross-Section TS.1 p28 (p60) // AR6 WG1 Figure 2.11 (c) p333 (p316)_
[^2]: _AR6 WG1 TS3.1 p59 (p91) // AR6 WG1 Figure TS.13 (d) p58 (p90)_ 
[^3]: _AR5 WG1 FAQ 3.1 p11 (p127)_
[^4]: _AR6 WG1 Box TS.4 p45 // AR6 WG1 Table 9.5 p1306 (p1289) // AR6 WG1 Box 9.1, Figure 1 p1308 (p1291)_
