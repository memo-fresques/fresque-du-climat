---
title: Aumento da temperatura da água
backDescription: >-
  O oceano absorve 93% da energia acumulada na Terra. Ao se aquecer, a água se
  expande.
---

