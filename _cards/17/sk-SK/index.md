---
title: Nárast teploty vody
backDescription: >-
  Oceán absorbuje 93% energie nahromadenej na Zemi. Teplota vody preto narastá a
  tým sa voda rozpína.
---

