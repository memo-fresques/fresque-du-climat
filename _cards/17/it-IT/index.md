---
title: Aumento della temperatura dell'acqua
backDescription: >-
  L'oceano assorbe il 91% dell'energia che si accumula sulla Terra. Quando si
  riscalda, l'acqua si dilata.
---
La superficie degli oceani si sta riscaldando meno velocemente rispetto alla terraferma (0,88°C rispetto a 1,59°C)[^1]. Come mai, nonostante assorba il 91% dell'energia in eccesso sulla Terra? [^2]. Bisogna ricordare che l'oceano copre il 71% della superficie terrestre e ha una profondità media di 4000 m. L'atmosfera si estende su un'altezza maggiore, ma se la si portasse alla stessa densità dell'acqua, misurerebbe solo 10 m (è per questo che si guadagna un'atmosfera di pressione ogni 10 m quando si fanno le immersioni). Inoltre, l'acqua ha una capacità termica molto più elevata rispetto alla maggior parte dei materiali terrestri. Ciò significa che può assorbire e immagazzinare una quantità considerevole di calore senza che la sua temperatura cambi molto. Pertanto, anche se l'oceano assorbe una grande quantità di energia, l'aumento della temperatura dell'acqua è relativamente basso rispetto a quello delle terre[^3]. L'acqua si dilata molto poco, ma dato l'enorme volume dell'oceano, questo è sufficiente per avere un impatto significativo sul livello del mare. Si stima che il contributo all'aumento del livello del mare della dilatazione delle sue acque sia circa del 50%.[^4]. 
[^1]: _AR6 WG1 Cross-Section TS.1 p28 (p60) // AR6 WG1 Figura 2.11 (c) p333 (p316)_
[^2]: _AR6 WG1 TS3.1 p59 (p91) // AR6 WG1 Figura TS.13 (d) p58 (p90)_ 
[^3]: _AR5 WG1 FAQ 3.1 p11 (p127)_
[^4]: _AR6 WG1 Box TS.4 p45 // AR6 WG1 Tabella 9.5 p1306 (p1289) // AR6 WG1 Box 9.1, Figura 1 p1308 (p1291)_
