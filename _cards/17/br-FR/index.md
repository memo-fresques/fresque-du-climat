---
backDescription: >-
  L'océan absorbe 91% de l’énergie qui s’accumule sur la Terre. En se
  réchauffant, l’eau se dilate.
instagramCode: ''
title: Kresk temperadur an dour
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_17_hausse_temp%C3%A9rature_eau
youtubeCode: TRhhK8tJ9ds
---

