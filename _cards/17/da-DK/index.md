---
title: 'Havtemperaturerne øges '
backDescription: >-
  Verdenshavene absorberer det meste af energien der akkumuleres på jorden
  (93%). Det fører til en temperaturstigning, særligt i havenes øverste lag.
  Varmen gør, at havene udvider sig.
---

