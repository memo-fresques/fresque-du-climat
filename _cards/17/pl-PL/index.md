---
title: Wzrost temperatury wody
backDescription: >-
  Oceany absorbują 91% energii, która gromadzi się na powierzchni Ziemi. Woda,
  ogrzewając się, zwiększa swoją objętość.
---
Ocean ociepla się tylko o około jedną dziesiątą stopnia na powierzchni i jeszcze mniej pod wodą. Dlaczego tak mało, skoro pochłania 91% nadmiaru energii na Ziemi? Dzieje się tak dlatego, że jest znacznie większy niż atmosfera i ma znacznie większą pojemność kaloryczną. Aby to zmierzyć, należy pamiętać, że ocean pokrywa 71% powierzchni Ziemi i ma średnią głębokość 4000 m. Atmosfera rozciąga się na większą wysokość, ale gdyby sprowadzić ją do tej samej gęstości co wodę, jej grubość wynosiłaby tylko 10 m (to dlatego podczas nurkowania zyskujemy jedną atmosferę ciśnienia na każde 10 m). Woda rozszerza się w bardzo niewielkim stopniu. W jaki sposób ocieplenie oceanu o jedną dziesiątą stopnia może spowodować wzrost poziomu wody? Pierwszą odpowiedzią jest to, że ocean ma średnio 4000 m głębokości, więc bardzo niewielka ekspansja wystarczy na kilka centymetrów.
