---
title: Væpnede konflikter
backDescription: Denne slutten må vi unngå...
---
Dette er kortet som er ment å plasseres sist, som teksten antyder. Vi kan allerede si at klimaendringer har vært en av årsakene til noen konflikter som i Rwanda eller Syria. I en verden som opplever alle konsekvensene beskrevet i dette spillet, er det vanskelig å forestille seg at man kan unngå væpnede konflikter. I 2007, da FNs klimapanel fikk en Nobelpris, var det Nobels fredspris. Og det er veldig gode grunner til det.
