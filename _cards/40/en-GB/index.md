---
title: Armed Conflicts
backDescription: This is how we shouldn't let it end…
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_40_armed_conflicts
youtubeCode: KzW-8WEXdSI
instagramCode: ''
---

This card is intended to be placed last, as the text suggests. It can already be said that climate change has been one of the causes of some conflicts, such as in Rwanda or Syria. In a world that is suffering from all the consequences described in the game, it is hard to imagine that armed conflicts can be avoided. In 2007, when the IPCC was awarded the Nobel Prize, it was the Nobel Peace Prize. And there are very good reasons for that.
