---
title: Konflikty zbrojne
backDescription: Takiego zakończenia wolelibyśmy uniknąć…
---
Ta karta jest przeznaczona do umieszczenia jako ostatnia, jak sugeruje tekst. Już teraz można powiedzieć, że zmiany klimatu były jedną z przyczyn niektórych konfliktów, takich jak te w Rwandzie czy Syrii. W świecie, który cierpi z powodu wszystkich konsekwencji opisanych w grze, trudno sobie wyobrazić, że można uniknąć konfliktów zbrojnych. W 2007 roku, kiedy IPCC otrzymało Nagrodę Nobla, była to Pokojowa Nagroda Nobla. I są ku temu bardzo dobre powody.
