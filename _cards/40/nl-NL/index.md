---
title: Gewapende conflicten
backDescription: Zo moet het NIET eindigen…
---
Deze kaart dient als laatste geplaatst te worden. Klimaatverandering kan reeds als (mede-)oorzaak gezien worden van conflicten, bijvoorbeeld in Rwanda en Syrië. In een wereld die al lijdt onder alle consequenties die in het Climate Fresk spel aan bod zijn gekomen, is het moeilijk om je voor te stellen dat gewapende conflicten vermeden kunnen worden. In 2007 kreeg het IPCC de Nobelprijs voor de vrede. Daar zijn goede redenen voor. 
