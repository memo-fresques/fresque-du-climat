---
title: Conflitti armati
backDescription: È così che bisognerebbe evitare che finisca…
---
Questa è la carta che andrebbe posizionata per ultima, come suggerisce il testo. È già possibile dire che il cambiamento climatico è stato una delle cause di alcuni conflitti come in Ruanda o in Siria. In un mondo che subisce tutte le conseguenze descritte in questo gioco, è difficile immaginare di poter evitare conflitti armati. Nel 2007, quando l'IPCC ha vinto il Premio Nobel, è stato il Premio Nobel per la Pace. E ci sono ottime ragioni per questo.
