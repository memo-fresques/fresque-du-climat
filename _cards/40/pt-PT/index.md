---
title: Conflitos Armados
backDescription: É o fim que devemos evitar...
---
Esta é a carta que se destina a ser colocada por último, como o texto sugere. Já podemos afirmar que as alterações climáticas foram uma das causas de alguns conflitos, como no Ruanda ou na Síria. Num mundo que sofre todas as consequências descritas neste jogo, é difícil imaginar que se possam evitar os Conflitos Armados. Em 2007, quando o IPCC recebeu o Prémio Nobel, foi o Prémio Nobel da Paz. E há muito boas razões para isso.
