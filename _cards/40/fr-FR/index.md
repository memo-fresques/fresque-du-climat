---
backDescription: C'est comme ça qu'il faudrait éviter que ça finisse…
title: Conflits armés
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_40_conflits_arm%C3%A9s
youtubeCode: _gURpjNvlRc
---
C'est la carte qui est prévue pour être placée en dernier, comme le laisse entendre le texte. On est déjà en mesure de dire que le changement climatique a été l'une des causes de certains conflits comme au Rwanda ou en Syrie. Dans un monde qui subit l'ensemble des conséquences décrites dans ce jeu, il est difficile d'imaginer qu'on puisse éviter des conflits armés. En 2007, quand le GIEC a eu le Prix Nobel, c'était le Prix Nobel de la Paix. Et il y a de très bonnes raisons à ça.
