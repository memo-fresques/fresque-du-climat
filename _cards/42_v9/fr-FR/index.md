---
title: Accroissement des inégalités
backDescription: >-
  Le changement climatique renforce les inégalités sociales et économiques entre
  les individus, entre les pays et entre les générations.

  Pour ne pas aggraver ces inégalités, les politiques climatiques doivent être
  justes et équitables.
---
 
