---
backDescription: >-
  Il cambiamento climatico rafforza le disuguaglianze sociali ed economiche tra

  individui, tra Paesi e tra generazioni. Per evitare di inasprire queste
  disuguaglianze, le politiche climatiche devono essere giuste ed eque.
title: Aumento delle disuguaglianze
---

