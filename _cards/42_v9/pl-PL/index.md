---
title: Wzrost nierówności społecznych
backDescription: >-
  Zmiany klimatu wzmacniają nierówności społeczne i ekonomiczne: między
  jednostkami, między krajami i między pokoleniami. Aby zapobiec pogłębianiu się
  tych nierówności, musimy zapewnić sprawiedliwą i równą politykę klimatyczną.
---

