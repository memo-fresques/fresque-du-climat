---
backDescription: >-
  Klimaendringer forsterker sosiale og økonomiske ulikheter mellom individer,
  mellom land og mellom generasjoner.

  For å ikke forverre disse ulikhetene, må klimapolitikken være rettferdig.
title: Økende ulikheter
---
Gjenstår å skrive.
