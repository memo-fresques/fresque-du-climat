---
backDescription: >-
  Climate change reinforces social and economic inequalities: between
  individuals, between countries, and between generations. To prevent these
  inequalities from getting worse, we must ensure climate policies are fair and
  equitable.
title: Growing Inequality
---

