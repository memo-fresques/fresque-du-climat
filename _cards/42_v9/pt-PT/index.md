---
backDescription: >-
  As alterações climáticas reforçam as desigualdades sociais e económicas entre
  os indivíduos, entre os países e entre as gerações.

  Para evitar o agravamento estas desigualdades, temos que assegurar políticas
  climáticas mais justas e equitativas.
title: Aumento das desigualdades
---

