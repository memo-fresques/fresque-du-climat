---
backDescription: >-
  L'agriculture est responsable de l'émission d'un peu de CO2 et de beaucoup de
  méthane (bovins, rizières), et de protoxyde d'azote (engrais). En tout, c’est
  25% des GES si on y inclut la déforestation induite.
instagramCode: CP8bcU4ItK5
title: Labour-douar
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_8_agriculture'
youtubeCode: BIZ_MNIehjg
---

