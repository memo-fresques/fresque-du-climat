---
title: Земеделие
backDescription: >-
  Земеделието е отговорно за емисиите на малко CO2 и на много метан (говеда,
  оризища) и на азотен протоксид (торове). Като цяло, това са 25% от емисиите на
  парникови газове, ако включим и свързаното обезлесяване.
---

