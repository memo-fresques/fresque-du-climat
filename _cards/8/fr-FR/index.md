---
backDescription: >-
  L'agriculture est responsable de l'émission d'un peu de CO2 et de beaucoup de
  méthane (bovins, rizières), et de protoxyde d'azote (engrais). En tout, c’est
  25% des GES si on y inclut la déforestation induite.
title: Agriculture
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_8_agriculture'
youtubeCode: BIZ_MNIehjg
instagramCode: CP8bcU4ItK5
---
L'agriculture représente 22% des émissions de gaz à effet de serre dans le monde (2019)[^1].L'agriculture utilise très peu d'énergies fossiles, ses émissions de CO2 sont essentiellement liées à la déforestation[^2]. Elle est responsable de 80% de la déforestation[^3]. En effet, il faut des grandes surfaces pour cultiver, surtout pour nourrir les animaux d'élevage[^4].La stabilisation du climat il y a 10 000ans, au début du Néolithique, a permis aux civilisations de se sédentariser et de développer l'agriculture[^5]. L'agriculture a permis de nourrir une population toujours croissante. Mais l'agriculture moderne pose de nombreux problèmes environnementaux: perte d'habitats et substances nocives pour la biodiversité (déforestation et pesticides), eutrophisation des milieux aquatiques (engrais), émissions de gaz à effet de serre (rumination (méthane) et engrais (Protoxyde d'azote)). Cette carte regroupe aussi les activités liées à l'aquaculture (production animale ou végétale en milieu aquatique).  
[^1]: _AR6 WG3 TS.3 p17 (p65) // AR6 WG3 Figure TS.6 p18 (p66)_
[^2]: _AR6 WG3 TS.3 p17 (p65) // AR6 WG3 Figure TS.6 p18 (p66)_
[^3]: _Selon OECD : 81% : https://www.oecd.org/env/indicators-modelling-outlooks/monitoring-land-cover-change.htm p6_
[^4]: _https://www.science.org/doi/10.1126/science.aaq0216_
[^5]: _AR6 WG2 Cross-Chapter Box PALEO // p165 (p152) // https://www.science.org/doi/10.1126/sciadv.adh2458_
