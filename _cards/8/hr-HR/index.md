---
title: Poljoprivreda
backDescription: >-
  Poljoprivreda nije u velikoj mjeri odgovorna za emisije CO2, ali jest za
  emisije velikih količina metana (od stoke i uzgoja riže) kao i dušikovih
  oksida (iz gnojiva).

  Ako joj se pridoda namjerna deforestacija, poljoprivreda je odgovorna za
  sveukupno 25%
---

