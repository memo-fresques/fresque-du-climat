---
title: Rolnictwo
backDescription: >-
  Rolnictwo jest odpowiedzialne za emisje niewielkiej ilości CO2 oraz znacznych
  ilości metanu (bydło, uprawa ryżu) i podtlenku azotu (nawozy). W sumie daje to
  25% gazów cieplarnianych, jeśli wliczyć skutki wylesiania.
---
Rolnictwo zużywa bardzo mało paliw kopalnych w porównaniu do emisji innych gazów cieplarnianych, za które jest odpowiedzialne. Jest ono odpowiedzialne za 80% wylesiania ze względu na duże obszary potrzebne do uprawy roślin, zwłaszcza do karmienia zwierząt hodowlanych. Rolnictwo to działalność człowieka, która rozpoczęła się, gdy tylko klimat się ustabilizował, na początku okresu neolitu 10 000 lat temu, po ostatniego cofania się lodowca, które trwało 10 000 lat. Od tego czasu wpływ działalności człowieka na środowisko jest coraz większy. Gatunki roślin zostały udomowione (dziś udomowiony ryż nie jest już w stanie rozmnażać się bez interwencji człowieka), lasy zostały wycięte, aby rozszerzyć obszary uprawne, pozbawiając gatunki zwierząt ich naturalnego siedliska, a od czasu zielonej rewolucji (zielonej dla rolnictwa, nie dla ekologii) używamy pestycydów i środków szkodliwych dla środowiska i naszego zdrowia.
