---
title: Poljoprivreda
backDescription: >-
  Poljoprivredna delatnost ne dovodi do emisije velike količine CO2, ali je
  odgovorna za emisiju značajnih količina metana (od goveda i pirinčanih polja)
  i azot suboksida (iz đubriva). Sve ukupno, poloprivredna delatnost objašnjava
  25% gasova sa efektom s
---

