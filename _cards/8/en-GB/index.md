---
title: Agriculture
backDescription: >-
  Agriculture does not emit much CO2 but does emit large quantities of methane
  (from cattle and rice paddies) and nitrous oxide (from fertilizers). In all,
  agriculture accounts for 25% of GHGs if we include the induced deforestation.
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_8_agriculture
youtubeCode: ZOTCm-J_YVU
instagramCode: CP8ZdjEIlV3
---

Agriculture uses very little fossil fuel compared to the emissions of other GHGs for which it is responsible. It is responsible for 80% of deforestation because of the large areas needed to grow crops, especially to feed farm animals. Agriculture is a human activity that began as soon as the climate stabilised, at the beginning of the Neolithic period 10,000 years ago, after the last deglaciation, which itself lasted 10,000 years. Since then, the impact of human activity on its environment has been growing. Plant species have been domesticated (today, domesticated rice is no longer able to reproduce without human intervention), forests have been cleared to expand cultivated areas, depriving animal species of their natural habitat, and since the Green Revolution (green for agricultural, not for ecological), we have been using pesticides and inputs that are harmful to the environment and to our health.
