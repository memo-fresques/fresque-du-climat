---
title: Landbouw
backDescription: >-
  De landbouw stoot niet veel CO2 uit, maar is verantwoordelijk voor de
  grootschalige uitstoot van methaan of CH4 (koeien en rijstvelden) en lachgas
  of N2O (meststoffen) . In totaal gaat dit over 25% van de totale hoeveelheid
  BKG als we de extra ontbossing
---
De landbouw gebruikt relatief weinig fossiele brandstoffen, maar is wel verantwoordelijk voor de emissie van andere broeikasgassen. Ontbossing wordt voor 80% door landbouw veroorzaakt, omdat grote gebieden nodig zijn voor het telen van gewassen, met name voor veevoer. Landbouw is ontstaan toen het klimaat zich stabiliseerde. Dit was aan het begin van het Neolithicum (10.000 jaar geleden) na de laatste IJstijd, die zelf 10.000 jaar duurde. Sinds deze landbouwrevolutie is de menselijke invloed op haar omgeving gestaag gegroeid. Plantensoorten zijn gedomesticeerd (gedomesticeerde rijst kan bijvoorbeeld niet meer reproduceren zonder menselijke interventie), bossen zijn verdwenen voor gebiedsontwikkeling, dierensoorten zijn van hun habitat verjaagd, en we zijn begonnen met het gebruiken van pesticides die gevaarlijk zijn voor de natuur en voor onze gezondheid.
