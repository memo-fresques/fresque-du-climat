---
title: Agricoltura
backDescription: >-
  L'agricoltura è responsabile dell'emissione di CO2 (deforestazione), di metano
  (bovini, risaie) e di protossido di azoto (fertilizzanti). In totale, e a
  livello mondiale, l'agricoltura è responsabile del 25% delle emissioni di gas
  serra.
---
L'agricoltura rappresenta il 22% delle emissioni di gas serra nel mondo (2019)[^1]. L'agricoltura utilizza pochissimi combustibili fossili, le sue emissioni di CO2 sono principalmente legate alla deforestazione[^2]. L'agricoltura è responsabile dell'80% della deforestazione[^3]. Infatti, sono necessarie grandi superfici per coltivare, soprattutto per nutrire gli animali da allevamento[^4]. La stabilizzazione del clima 10.000 anni fa, all'inizio del Neolitico, ha permesso alle civiltà di sedentarizzarsi e sviluppare l'agricoltura[^5]. L'agricoltura ha permesso di nutrire una popolazione in costante crescita. Ma l'agricoltura moderna presenta numerosi problemi ambientali: perdita di habitat e sostanze nocive per la biodiversità (deforestazione e pesticidi), eutrofizzazione dei corpi idrici (fertilizzanti), emissioni di gas serra (ruminazione (metano) e fertilizzanti (ossido di azoto)). Questa carta include anche le attività legate all'acquacoltura (produzione animale o vegetale in ambiente acquatico). 
[^1]: _AR6 WG3 TS.3 p17 (p65) // AR6 WG3 Figura TS.6 p18 (p66)_
[^2]: _AR6 WG3 TS.3 p17 (p65) // AR6 WG3 Figura TS.6 p18 (p66)_
[^3]: _Secondo l'OCSE: 81%: https://www.oecd.org/env/indicators-modelling-outlooks/monitoring-land-cover-change.htm p6_
[^4]: _https://www.science.org/doi/10.1126/science.aaq0216_
[^5]: _AR6 WG2 Cross-Chapter Box PALEO // p165 (p152) // https://www.science.org/doi/10.1126/sciadv.adh2458_
