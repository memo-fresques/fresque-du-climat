---
title: Laborantza
backDescription: >-
  Laborantzak CO₂ pixka bat isurtzen du. Aldiz, metano (behiek, irris-alorrek)
  eta oxido nitroso (ongailuek) anitz isurtzen ditu. Horotara, ondorioz sortzen
  den oihan soiltzea kontuan hartzen bada, berotegi-efektuko gasen % 25 da.
---

