---
title: Agricultura
backDescription: >-
  A agricultura é responsável pela emissão de uma pequena parcela de CO2, mas de
  uma grande parcela de metano (da pecuária e arrozais) e óxido nitroso (de
  fertilizantes). No total, representa 25% dos GEE emitidos, se considerarmos
  ainda a desflorestação ind
---
A agricultura representa 22% das emissões de gases com efeito de estufa no mundo (2019)[^1].A agricultura utiliza muito poucos combustíveis fósseis, e as suas emissões de CO2 estão essencialmente ligadas à Desflorestação[^2]. A Agricultura é responsável por 80% da Desflorestação[^3]. De facto, são necessárias grandes superfícies para cultivar, especialmente para alimentar o gado[^4].A estabilização do clima há 10 000 anos, no início do Neolítico, permitiu às civilizações sedentarizarem-se e desenvolverem a agricultura[^5]. A Agricultura permitiu alimentar uma população cada vez maior. Mas a agricultura moderna coloca muitos problemas ambientais: perda de habitats e substâncias nocivas para a biodiversidade (desflorestação e pesticidas), eutrofização dos meios aquáticos (fertilizantes), emissões de gases com efeito de estufa (ruminação (metano) e fertilizantes (óxido nitroso)). Esta carta também agrupa as atividades relacionadas com a aquacultura (produção animal ou vegetal em meio aquático).  
[^1]: _AR6 WG3 TS.3 p17 (p65) // AR6 WG3 Figure TS.6 p18 (p66)_
[^2]: _AR6 WG3 TS.3 p17 (p65) // AR6 WG3 Figure TS.6 p18 (p66)_
[^3]: _Segundo OECD : 81% : https://www.oecd.org/env/indicators-modelling-outlooks/monitoring-land-cover-change.htm p6_
[^4]: _https://www.science.org/doi/10.1126/science.aaq0216_
[^5]: _AR6 WG2 Cross-Chapter Box PALEO // p165 (p152) // https://www.science.org/doi/10.1126/sciadv.adh2458_
