---
title: Zemědělství
backDescription: >-
  Zemědělství neprodukuje vysoké množství CO2, ale je zodpovědné za emise
  velkého množství metanu (krávy, rýžová pole) a oxidu dusného (hnojiva). Ze
  zemědělství pochází celkově 25 % emisí skleníkových plynů, pokud do něj
  zahrneme indukované odlesňování.
---

