---
title: Tarım ve Hayvancılık
backDescription: >-
  Tarım tek başına çok fazla CO2 salımında bulunmaz ancak çeltik alanları ve
  endüstriyel hayvancılık (büyükbaş hayvanlar) metan salımında rol oynar. Aynı
  zamanda tarımda yüksek miktarda kullanılan azot oksit (suni gübre) de sera
  etkisini arttırır. Toplamda,
---

