---
title: Agricultura
backDescription: >-
  A agricultura é responsável pela emissão de uma pequena parcela de CO2, de
  grande parcela de metano (da pecuária e arrozais) e óxido nitroso (de
  fertilizantes). No total, ela representa 25% dos GEE emitidos, se consideramos
  ainda o desmatamento induzido
---

