---
title: Mezőgazdaság
backDescription: >-
  A mezőgazdaság CO2-kibocsátása nem kimagasló, viszont (a tehéntartás és a
  rizsföldek miatt) nagymennyiségű metánkibocsátásért és (a trágyázás miatt)
  nitrogén-oxid kibocsátásért felelős. Összeségében a mezőgazdaság, az
  erdőírtást is beleértve, az üvegházha
---

