---
title: Landbruk
backDescription: >-
  Landbruket slipper ikke ut mye CO2, men står for store metan- (fra kyr og
  rismarker) og nitrogenoksidutslipp (fra gjødsel). Totalt står landbruket for
  25 % av klimagassutslippene hvis vi inkluderer utslippene fra avskoging til
  landbruksformål.
---
Landbruket står for 22% av de globale klimagassutslippene (2019)[^1].Landbruket bruker svært lite fossilt brensel, dets CO2-utslipp er hovedsakelig knyttet til avskoging[^2]. Det er ansvarlig for 80% av avskogingen[^3]. Det kreves store arealer for å dyrke, spesielt for å mate husdyr[^4]. Stabilisering av klimaet for 10 000 år siden, i begynnelsen av neolittisk tid, gjorde det mulig for sivilisasjoner å slå seg ned og utvikle landbruk[^5]. Landbruket har gjort det mulig å brødfø en stadig voksende befolkning. Men moderne landbruk skaper mange miljøproblemer: tap av habitater og skadelige stoffer for biologisk mangfold (avskoging og plantevernmidler), eutrofiering av vannmiljøer (gjødsel), klimagassutslipp (drøvtygging (metan) og gjødsel (lystgass)). Dette kartet inkluderer også aktiviteter knyttet til akvakultur (dyre- eller planteproduksjon i vannmiljøer).
[^1]: _AR6 WG3 TS.3 p17 (p65) // AR6 WG3 Figure TS.6 p18 (p66)_
[^2]: _AR6 WG3 TS.3 p17 (p65) // AR6 WG3 Figure TS.6 p18 (p66)_
[^3]: _Ifølge OECD: 81%: https://www.oecd.org/env/indicators-modelling-outlooks/monitoring-land-cover-change.htm p6_
[^4]: _https://www.science.org/doi/10.1126/science.aaq0216_
[^5]: _AR6 WG2 Cross-Chapter Box PALEO // p165 (p152) // https://www.science.org/doi/10.1126/sciadv.adh2458_
