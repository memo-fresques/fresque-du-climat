---
title: Agriculture
backDescription: >-
  Agriculture does not emit a lot of CO2, but is responsible for the emission of
  large quantities of methane (from cows and rice paddies) and of nitrous oxide
  (from fertilizers). In all, agriculture amounts for 25% of GHGs if we include
  induced deforestati
---

