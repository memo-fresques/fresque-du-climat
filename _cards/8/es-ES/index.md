---
title: Agricultura y ganadería
backDescription: >-
  La agricultura y la ganadería son responsables de las emisiones de una pequeña
  parte de CO2, de mucho metano (rumiantes, arrozales) y de óxido nitroso
  (fertilizantes).
---

