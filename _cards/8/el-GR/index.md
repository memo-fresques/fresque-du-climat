---
title: Γεωργία - Κτηνοτροφία
backDescription: >-
  Η γεωργία και η κτηνοτροφία είναι υπεύθυνες για την εκπομπή ενός μικρού μέρους
  του CO2, μεγάλου ποσοστού μεθανίου (από βοοειδή και ορυζώνες) και του
  υποξειδίου του αζώτου (από λιπάσματα). Συνολικά, μαζί με την αποψίλωση,
  ευθύνονται για το 25 % των εκπομπώ
---

