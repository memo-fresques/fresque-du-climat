---
title: Landbrug
backDescription: >-
  Landbrugssektoren udleder ikke så meget CO2, men bidrager til gengæld med
  store mængder metangas (CH4, fra drøvtyggere og rismarker) og  lattergas (N2O,
  kvælstofgødning). Hvis vi inkluderer skovrydning, bidrager landbruget i alt
  med 25% af de samlede driv
---

