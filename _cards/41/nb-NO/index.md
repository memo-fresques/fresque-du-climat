---
title: Permafrost
backDescription: >-
  Permafrost er fast frossen jord. Smelting av permafrost fører til oppløsing av
  organisk materiale som tidligere ble frosset under jorda. Dette slipper ut
  både metan og CO2 til atmosfæren. Etter 2 grader vil dette mest sannsynligvis
  øke og resultere i en n
---
Permafrost, er områder der bakken er frosset i minst to påfølgende år. Permafrost er en av de positive tilbakekoblingene (ikke positive for klimaet). Når den tiner, brytes det organiske materialet som har akkumulert seg der i tusenvis av år ned av mikroorganismer, og som et resultat frigjøres drivhusgasser, spesielt CO2, men også metan (CH4), som bidrar til ytterligere oppvarming av planeten[1].
_[1] AR6 WG1 Box 5.1 s745 (s728) // AR6 WG1 Figur 5.29 (tilbakekoblinger) s755 (s738) // For mer informasjon: https://youtu.be/6pdJT70UqWo_
