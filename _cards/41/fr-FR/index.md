---
backDescription: >-
  Le permafrost désigne le sol gelé en permanence. On constate qu’il commence à
  dégeler, relâchant dans l’air du CO2 et du méthane, suite à la décomposition
  de la matière organique qui était jusque-là gelée. Cela constitue une boucle
  de rétroaction.
title: Permafrost
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_41_permafrost'
youtubeCode: T_9vgHZkx90
instagramCode: CNaMM10ocNX
---
Le permafrost, ou pergélisol en français, est un sol gelé pendant au moins deux années consécutives.  Le Permafrost fait partie des rétroactions positives (pas positives pour le climat). Lorsqu'il dégèle, la matière organique qui s'y était accumulée pendant des milliers d'années est dégradée par des microogranismes et par conséquent, des gaz à effet de serre notamment du CO2 mais aussi du méthane (CH4) sont libérés, participant au réchauffement additionnel de la planète[1].  
_[1] AR6 WG1 Box 5.1 p745 (p728) // AR6 WG1 Figure 5.29 (feedbacks) p755 (p738) // Pour aller plus loin : https://youtu.be/6pdJT70UqWo_
