---
title: 永冻层中可燃冰
backDescription: ' 永冻层是指永远冻结的土地。我们观察到它开始解冻，将原本存储在地下的可燃冰（甲烷）释放出来。如果气温上升2°C以上，解冻现象会加剧，甲烷的加速释放会极大可能地导致气候失控。'
---

