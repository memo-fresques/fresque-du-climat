---
backDescription: >-
  Le permafrost désigne le sol gelé en permanence. On constate qu’il commence à
  dégeler, relâchant dans l’air du méthane et du CO2, suite à la décomposition
  de la matière organique qui était jusque là gelée. Cela constitue une boucle
  de rétroaction positive
instagramCode: CNaMM10ocNX
title: Permafrost
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_41_permafrost'
youtubeCode: T_9vgHZkx90
---

