---
title: Permafroszt
backDescription: >-
  A permafroszt állandóan fagyott talajt jelent. A permafroszt olvadása olyan
  szerves anyagok bomlásához vezet, melyek azelőtt fagyott állapotban voltak a
  talajban. A folyamat metánt és CO2-ot szabadít a légkörbe. +2°C-on túl szinte
  bizonyos, hogy ez a jele
---

