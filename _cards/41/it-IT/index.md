---
title: Permafrost
backDescription: >-
  Il permafrost indica un suolo perennemente ghiacciato. Si osserva che sta
  cominciando a fondere, rilasciando nell'aria metano e CO2, in seguito alla
  decomposizione della materia

  organica che fino ad ora era rimasta congelata. Tale fenomeno costituisce un
  fenomeno di

  retroazione auto-rinforzante (feedback positivo).
---
Il permafrost, o suolo permanentemente ghiacciato, è un suolo che rimane congelato per almeno due anni consecutivi. Il permafrost fa parte delle retroazioni (feedback) positive (non positive per il clima). Quando si scioglie, la materia organica accumulata per migliaia di anni viene degradata da microrganismi e di conseguenza vengono rilasciati gas serra, in particolare CO2 e metano (CH4), contribuendo al riscaldamento aggiuntivo del pianeta[1]. 
_[1] AR6 WG1 Box 5.1 p745 (p728) // AR6 WG1 Figura 5.29 (feedbacks) p755 (p738) // Per saperne di più: https://youtu.be/6pdJT70UqWo_ (francese), https://www.youtube.com/watch?v=PBOphHc1TAg (italiano)
