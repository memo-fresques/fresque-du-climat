---
title: Permafrost (pergelissolo)
backDescription: >-
  Permafrost refere-se a solo permanentemente congelado. Verifica-se que está a
  começar a descongelar, libertando metano e CO2 no ar em resultado da
  decomposição de matéria orgânica previamente congelada. Isto constitui uma
  retroacção, da mesma forma que os
---
O Permafrost, ou pergelissolo em português, é o solo congelado durante pelo menos dois anos consecutivos. O Permafrost faz parte das retroações positivas (não positivas para o clima). Quando descongela, a matéria orgânica que se acumulou durante milhares de anos é degradada por microrganismos e, consequentemente, são libertados gases com efeito estufa, principalmente CO2, mas também metano (CH4), contribuindo para o aquecimento adicional do planeta[1].
_[1] AR6 WG1 Box 5.1 p745 (p728) // AR6 WG1 Figure 5.29 (feedbacks) p755 (p738) // Para saber mais: https://youtu.be/6pdJT70UqWo_
