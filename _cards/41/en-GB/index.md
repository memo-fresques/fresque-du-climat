---
title: Permafrost
backDescription: >-
  Permafrost is permanently frozen ground. As it thaws, it is releasing CO2 and
  methane into the atmosphere. These gases come from previously frozen organic
  matter which starts to decompose. Melting permafrost creates a feedback loop.
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_41_permafrost
youtubeCode: 0eXsD4C15KQ
instagramCode: CNaOV_FHH2D
---

Permafrost is soil that is permanently frozen for at least two consecutive years. The two last cards to be added to the Fresk after card 40 are potentially violent feedback loops or "climate bombs" which, if triggered, would cause us to lose control over the climate for good. Thermokarst are veritable bioreactors at the heart of the process of releasing frozen carbon: when the permafrost thaws, pieces of soil detach and fall into the water, bringing nutrients and carbon to the bacteria and plankton present in the sea, which degrade them into CO2 (in the water layers near the surface) and methane (CH4 -- in the oxygen-deprived depths).
