---
title: Permafrosta
backDescription: >-
  Permafrosta deitzen da, etengabe izoztua den lurra. Ohartu gara urtzen hasia
  dela, airean metano eta CO₂-a isuriz, ordu arte izoztua zen materia organikoa
  deskonposatzen baita. 2°C baino gehiago emendatzen bada, fenomeno hau bizkortu
  daiteke, klima aldake
---

