---
title: Permafrost
backDescription: >-
  _x000B_Permafrost is permanently frozen ground. Permafrost thawing leads to
  the decomposition of organic matter previously frozen underground, a
  phenomenon that releases methane and CO2 into the atmosphere. Beyond +3.6°F
  (+2°C), it is almost certain this phenom
---

