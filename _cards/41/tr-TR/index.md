---
title: Permafrost
backDescription: >-
  Permafrost sürekli donmuş toprak tabakasına denir. Permafrostun erimeye ve
  erirken de hapsettiği metan gazını havaya salmaya başladığını gözlemliyoruz.
  Bu fenomen hızlanırsa, özellikle +2°C üzerindeki sıcaklık artışında iklim
  değişikliğinin öngörülemez ha
---

