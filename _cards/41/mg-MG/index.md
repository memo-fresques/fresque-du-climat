---
title: Permafrost
backDescription: >-
  Ny "permafrost" dia hanambarana ny tany mivaingana foana. Tsikaritra anefa fa
  manomboka miharava ary manaparitaka "méthane" sy CO2 eny amin'ny rivotra
  satria miha lo ireo singa organika nadrafitra azy ireo. Io trangan-javatra io
  dia miha aingana ary mety
---

"Permafrost"
