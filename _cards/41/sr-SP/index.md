---
title: Permafrost
backDescription: >-
  Permafrost je trajno zaleđeno tlo. Otapanje permafrosta vodi raspadanju
  organskih materija koje su prethodno bile zaleđene pod zemljom, pojava
  prilikom koje se metan i ugljen-dioksid oslobađaju u atmosferu. Na temperaturi
  iznad +2°C, gotovo je sigurno da
---

