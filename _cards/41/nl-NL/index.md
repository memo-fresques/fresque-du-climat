---
title: Permafrost
backDescription: >-
  Permafrost is permanent bevroren ondergrond. Door klimaatverandering dooit er
  elk jaar steeds meer tot dan toe bevroren organisch materiaal en zo komt er
  meer methaan vrij. Boven een temperatuurstijging van 2°C zal dit fenomeen
  waarschijnlijk versnellen,
---
Permafrost is grond die al minstens twee jaar permanent bevroren is. Deze twee laatste kaarten zijn potentieel zeer krachtige feedbackloops of "klimaatbommen." Als ze getriggered worden zorgen ze ervoor dat we de controle over het klimaat definitief kwijtraken. Thermokarst is echt een bioreactor, waarin bevroren koolstof vrijkomt. Als de permafrost ontdooit vallen er stukjes grond in het water. Dit brengt nutriënten en koolstof naar de bacteriën en plankton die in het water leven. De bacteriën en plankton breken het af in CO2 (in het oppervlaktewater) en methaan (CH4, in de zuurstofarme diepten).
