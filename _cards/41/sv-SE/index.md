---
title: Permafrost
backDescription: >-
  Permafrost är permanent frusen mark. Denna mark har börjat tina vilket frigör
  metan som lagrats under marken och som nu når atmosfären. Detta skapar en så
  kallad positiv återkoppling, precis som skogsbränder och smältande havsis.
---

