---
title: Wieczna zmarzlina
backDescription: >-
  Wieczna zmarzlina to stale zamarznięta warstwa gruntu. Zaobserwowano, że
  zaczyna ona rozmarzać. W efekcie zamrożona dotąd materia organiczna zaczyna
  się rozkładać, co powoduje uwalnianie metanu i CO2. Mamy tu do czynienia z
  tzw. sprzężeniem zwrotnym, podobnie jak w przypadku pożaru lasów czy zmiany
  albedo wskutek topnienia morskiej pokrywy lodowej.
---
Wieczna zmarzlina to gleba, która jest trwale zamarznięta przez co najmniej dwa kolejne lata. Dwie ostatnie karty dodane do Mozaiki po karcie 40 to potencjalnie gwałtowne pętle sprzężenia zwrotnego lub „bomby klimatyczne”, których uruchomienie spowodowałoby utratę kontroli nad klimatem na dobre. Termokrasy są prawdziwymi bioreaktorami w sercu procesu uwalniania zamrożonego węgla: kiedy wieczna zmarzlina topnieje, kawałki gleby odrywają się i wpadają do wody, przynosząc składniki odżywcze i węgiel bakteriom i planktonowi obecnemu w morzu, które rozkładają je na CO2 (w warstwach wody przy powierzchni) i metan (CH4 - w pozbawionych tlenu głębinach).
