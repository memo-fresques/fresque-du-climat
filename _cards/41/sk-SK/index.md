---
title: Permafrost
backDescription: >-
  Permafrost je permanentne zamrznutá pôda. Vidíme, že sa začína rozmrazovať,
  čím vypúšťa do vzduchu metán, ktorý bol uskladnený pod zemským povrchom. Ak sa
  tento fenomén zrýchli, je tu vysoký risk, že sa globálne otepľovanie stane
  nepredvídateľným, hlavne
---

