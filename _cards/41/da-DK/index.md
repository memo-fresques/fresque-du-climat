---
title: Permafrost
backDescription: >-
  Permafrost er permanent frossen jord. Når permafrost tør, nedbrydes de
  organiske materialer, der før var frosne. Dette udleder metan og CO2 til
  atmosfæren. Ved opvarmning over 2˚C er det næsten sikkert, at dette fænomen
  vil accelerere, og dermed skabe yd
---

