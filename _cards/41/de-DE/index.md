---
title: Permafrost
backDescription: >-
  Unter Permafrost versteht man dauerhaft gefrorenen Boden. Das Auftauen von
  Permafrost führt zur Zersetzung organischer Materie, die zuvor unter der Erde
  gefroren war. Ein Phänomen, das Methan und CO2 in die Atmosphäre freisetzt.
  Erreichen wir mehr als +2°
---

