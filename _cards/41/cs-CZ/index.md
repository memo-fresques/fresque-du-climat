---
title: Permafrost
backDescription: >-
  Permafrost je trvale zmrzlá půda. Rozmrazování permafrostu vede k rozkladu
  organické hmoty dříve zmrazené pod zemí, což je jev, při kterém se uvolňuje
  metan a CO2 do atmosféry. Při teplotě nad 2 °C je téměř jisté, že se tento jev
  zrychlí a klimatické změn
---

