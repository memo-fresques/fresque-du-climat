---
title: '"Forçage radiatif"'
backDescription: >-
  Ny "forçage radiatif" dia ny fandrefesana ny fitongilanana eo amin'ny angovo
  izay tonga sy miala isan-tsengondra amin'ny tany. 3,1 W/m2 ("Watt par m2") izy
  hoan'ny entona maitso ary 0,8W/m2 kosa hoan'ny "aérosols", izany hoe 2,3W/m2
  izy rehetra.
---

"Forçage radiatif"
