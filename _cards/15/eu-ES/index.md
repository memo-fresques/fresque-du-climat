---
title: Erradiazioaren bortxaketa
backDescription: >-
  Erradiazioaren bortxaketa, segunduro lur gainera heltzen den eta lurretik
  joaten den energiaren arteko desoreka da. Berotegi- efektuak 3,1 W/m² balio
  ditu, espraiek - 0,8 W/m², hots, orotara, 2,3 W/m².
---

