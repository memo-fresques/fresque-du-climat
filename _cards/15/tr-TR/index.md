---
title: Işınımsal zorlama
backDescription: >-
  Işınımsal zorlama, her saniye dünyaya gelen enerji ile dünyadan salınan enerji
  arasındaki (insan faaliyetleri sebebiyle oluşan) farktır. IPCC'de sunulan 5.
  durum raporunda, 2,3 W/m2 (metrekare başına düşen watt) olarak belirtilmiştir.
---

