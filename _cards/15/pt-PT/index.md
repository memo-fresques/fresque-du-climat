---
title: Forçamento Radiativo
backDescription: >-
  O Forçamento Radiativo representa a diferença entre a energia que chega à
  Terra a cada segundo e a energia que é libertada. É avaliado em 2,8 W/m² (Watt
  por metro quadrado), 3,8 W/m² do efeito de estufa e -1 W/m² dos aerossóis.
---
No gráfico principal, vemos as diferentes componentes do forçamento radiativo de 1750 até hoje (em variação em relação à situação de equilíbrio de 1750: forçamento radiativo = 0): na parte superior, os efeitos de aquecimento (forçamento radiativo positivo), na parte inferior, os efeitos de arrefecimento (forçamento radiativo negativo). O efeito de estufa (CO2 + Outros WMGHG (outros GEE) + O3 (ozono) troposférico)) representa um forçamento positivo de 3,8 W/m^2. Portanto, está na parte superior do gráfico. Os aerossóis (Efeito direto + efeito indireto) e os vulcões e a mudança de uso do solo têm um efeito de arrefecimento, e estão, portanto, na parte inferior do gráfico. O gráfico secundário à direita representa o forçamento radiativo ao longo de dois séculos e meio (histórico e projeções). No 6º relatório do IPCC, o forçamento radiativo é de 3,8 W/m² (Watt por m²) para o efeito de estufa e -1 W/m² para os aerossóis, ou seja, 2,8 W/m² no total. Os valores do forçamento em 2100 deram nome aos 5 cenários do IPCC (SSP1-1.9; SSP1-2.6; SSP2-4.5; SSP3-7.0; SSP4-8.5). As cores desses cenários são encontradas nos gráficos dos mapas nº 5, 11, 15, 21, 22 e 24[^1].
[^1]: _AR6 WG1 TS3.1 p59_ // AR6 WG1 Figura TS.9 (d) (forçamento) // p36 // AR5 WG1 Figura 8.18: Forçamento radiativo p979 (962)_
