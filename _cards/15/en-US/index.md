---
title: Radiative Forcing
backDescription: >-
  Radiative forcing represents the difference between the energy that arrives on
  Earth each second and the energy that is released. In the 5th assessment
  report of IPCC, the increase of radiative forcing from human activities since
  1750 is estimated at 2.3
---

