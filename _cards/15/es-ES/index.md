---
title: Forzamiento radiativo
backDescription: >-
  El forzamiento radiativo representa la diferencia entre la energía que llega
  cada segundo a la tierra y la que sale. Equivale a 3,1 W/m² (Watt por m²) para
  el efecto invernadero, y -0,8 W/m² para los aerosoles, es decir 2,3 W/m² en
  total.
---

