---
title: Forçamento Radioativo
backDescription: >-
  O forçamento radiativo representa a diferença entre a energia que chega à
  Terra a cada segundo e a energia por ela liberada. Ele foi estimado a 2,3 W/m²
  (Watt por metro quadrado), sendo 3,1 W/m²  para o efeito estufa e - 0,8 W / m²
  para os aerossóis.
---

