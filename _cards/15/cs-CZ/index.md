---
title: Radiační působení
backDescription: >-
  Radiační působení představuje nerovnováhu (způsobenou lidskou činností) mezi
  energií, která každou sekundu dopadá na Zemi a energií, která se uvolňuje. Dle
  Páté hodnotící zprávy Mezinárodního panelu pro změnu klimatu (IPCC) je
  radiační působení na hodnotě
---

