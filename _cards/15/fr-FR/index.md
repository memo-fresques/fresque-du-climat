---
backDescription: >-
  Le forçage radiatif est la mesure du déséquilibre entre l'énergie qui arrive
  chaque seconde sur terre et celle qui repart. Il vaut 3,8 W/m² (Watt par m²)
  pour l’effet de serre et - 1 W/m² pour les aérosols, soit 2,8 W/m² en tout.
title: Forçage radiatif
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_15_for%C3%A7age_radiatif
youtubeCode: WYpGFCtFuNg
instagramCode: COie1nbIojF
---
Sur le graphique principal, on voit les différentes composantes du forçage radiatif de 1750 à nos jours (en variation par rapport à la situation d'équilibre de 1750 : forçage radiatif = 0) : dans la partie du haut, les effets réchauffant (forçage radiatif positif), dans la partie du bas, les effets refroidissants (forçage radiatif négatif). L'effet de serre (CO2 + Other WMGHG (autres GES) + Trop O3 (ozone troposphérique)) représente un forçage positif de 3,8 W/m^2. Il est donc dans la partie supérieure du graphique. Les aérosols (Effet direct + effet indirect) et les volcans et le changement d'affectation des sols ont un effet refroidissant, et sont donc dans la partie inférieure du graphique. Le graphe secondaire de droite représente le forçage radiatif sur deux siècles et demi (historique et projections). Dans le 6e rapport du GIEC, le forçage radiatif vaut 3,8 W/m² (Watt par m²)
pour l’effet de serre et - 1 W/m² pour les
aérosols, soit 2,8 W/m² en tout.. Les valeurs du forçage en 2100 ont donné leur nom aux 5 scénarios du GIEC (SSP1-1.9 ; SSP1-2.6 ; SSP2-4.5 ; SSP3-7.0 ; SSP4-8.5). On retrouve les couleurs de ces scénarios dans les graphiques des cartes n° 5, 11, 15, 21, 22 et 24[^1].
[^1]: _AR6 WG1 TS3.1 p59_ // AR6 WG1 Figure TS.9 (d) (forçage) // p36 // AR5 WG1 Figure 8.18 : Forçage radiatif p979 (962)_
