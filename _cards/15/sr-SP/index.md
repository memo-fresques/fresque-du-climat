---
title: Balans zračenja
backDescription: >-
  Balans zračenja predstavlja razliku (nastalu kao posledica ljudskog delovanja)
  između energije koja stiže na Zemlju svake sekunde i energije koju Zemlja
  otpušta. U petom izveštaju Međuvladinog panela o klimatskim promenama,
  procenjen je na 2,3 W/m² (Vat p
---

