---
title: Sugárzási kényszer
backDescription: >-
  A sugárzási kényszer a másodpercenként a Földre érkező és a felszabaduló
  energia közötti (az emberek által okozott) különbséget jelenti. Ennek értékét
  az IPCC 5. értékelő jelentésében (Szintézis Jelentés) 2,3 W/m² -ben (Watt per
  négyzetméter) határozták m
---

