---
title: Strålingspådriv
backDescription: >-
  Strålingspådriv måler ubalansen mellom inngående og utgående energi fra jorda
  hvert sekund. I IPCCs femte hovedrapport vurderes strålingspådriv til 2,3 W/m²
  (watt per kvadratmeter).
---
Grafen til venstre viser de forskjellige komponentene av strålingspådriv fra 1750 til i dag (i variasjon i forhold til likevektssituasjonen i 1750: strålingspådriv = 0): i den øvre delen, de med oppvarmende effekt (positivt strålingspådriv), i den nedre delen, de med avkjølende effekt (negativt strålingspådriv). Drivhuseffekten (CO2 + Other WMGHG (andre klimagasser) + Trop O3 (troposfærisk ozon)) representerer et positivt pådriv på 3,8 W/m^2. Den er derfor i den øvre delen av grafen. Aerosoler (direkte effekt + indirekte effekt) og vulkaner og endring i arealbruk har en avkjølende effekt, og er derfor i den nedre delen av grafen. Den mindre grafen til høyre viser strålingspådriv over to og et halvt århundre (historisk og fremskrivninger). I den 6. rapporten fra IPCC er strålingspådrivet 3,8 W/m² (Watt per m²) for drivhuseffekten og -1 W/m² for aerosolene, altså 2,8 W/m² totalt. Verdiene for pådriv i 2100 har gitt navn til de 5 scenariene fra IPCC (SSP1-1.9; SSP1-2.6; SSP2-4.5; SSP3-7.0; SSP4-8.5). Fargene til disse scenariene finnes i grafene til kartene nr. 5, 11, 15, 21, 22 og 24[^1].
[^1]: _AR6 WG1 TS3.1 s59_ // AR6 WG1 Figur TS.9 (d) (pådriv) // s36 // AR5 WG1 Figur 8.18: Strålingspådriv s979 (962)_
