---
backDescription: >-
  Le forçage radiatif est la mesure du déséquilibre entre l'énergie qui arrive
  chaque seconde sur terre et celle qui repart. Il vaut 3,8 W/m² (Watt par m²)
  pour l’effet de serre et - 1 W/m² pour les aérosols, soit 2,8 W/m² en tout.
instagramCode: COie1nbIojF
title: Kemm skinadurel
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_15_for%C3%A7age_radiatif
youtubeCode: WYpGFCtFuNg
---

