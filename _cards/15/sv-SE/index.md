---
title: Strålningsdrivning
backDescription: >-
  Strålningsdrivning är skillnaden mellan mängden energi som tillkommer på
  jorden varje sekund, och den energi som lämnar jorden. Den är beräknad till
  totalt 2.8 W/m² (Watt per kvadratmeter): +3.8 W/m² från växthuseffekten och -1
  W/m² från aerosoler.
---
I den större figuren ser vi de olika komponenter som bidrar till strålningsdrivning, och hur deras bidrag har varierat från 1750 fram till idag (jämfört med 1750 där vi sätter nollnivå för strålningsdrivning).  I den positiva övre delen ser vi de komponenter som bidrar till uppvärmning (positiv strålningsdrivning) och i den nedre, negativa delen visas komponenterna som bidrar till kylning (negativ strålningsdrivning). Växthuseffekten (CO2 + Other WMGHG ("wellmixed green house gases") + tropospheric O3 (troposfäriskt ozon)) orsakar en positiv drivning på 3,8 W/m^2.  Aerosoler (direkt effekt + indirekt effekt), partiklar från vulkanutbrott och förändrad markanvändning har en kylande effekt. Den mindre figuren till höger visar strålningsdrivningen över 250 år (historiska värden och projektioner). I den sjätte IPCC-rapporten uppges strålningsdrivningen till 3,8 W/m² (Watt per m²)
för växthuseffekten och - 1 W/m² för aerosoler, dvs totalt 2,8 W/m². De projicerade värdena för strålningsdrivning år 2100 har gett namn till de 5 IPCC-scenarierna (RCP1-1.9; RCP1-2.6; SSP2-4.5; RCP3-7.0; RCP4-8.5). Vi återfinner dessa scenarier i respektive färg på kort nr 5, 11, 15, 21, 22 och 24[^1].
[^1]: _AR6 WG1 TS3.1 p59_ // AR6 WG1 Figure TS.9 (d) (forcing) // p36 // AR5 WG1 Figure 8.18: Radiative forcing p979 (962)_
