---
title: 辐射强迫
backDescription: >-
  辐射强迫是指地球吸收的能量与辐射回太空的能量之间的差距，一般是以在对流层单位面积的能量差来计算 （单位 为 瓦特/平方米 W
  /m²）。温室效应造成的辐射强迫为 3.1 W /m²，气溶胶为 -0.8 W /m²，即辐射强迫总值为2.3 W /m²。
---

