---
title: Forçage radiatif
backDescription: >-
  De forçage radiatif (et gëtt kee gutt Wuert dofir op Lëtzebuergesch) stellt
  d'Ongläichgewiicht duer tëscht däer Energie, déi all Sekonn op der Äerd
  ukënnt, an däer, déi nees fortgeet. De forçage radiatif sinn 3,1 W/m2 (Watt
  pro Quadratmeter) fir den Zäre
---

