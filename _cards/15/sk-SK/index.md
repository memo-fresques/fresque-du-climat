---
title: Radiačné pôsobenie
backDescription: >-
  Radiačné pôsobenie predstavuje rozdiel medzi energiou, ktorá každú sekundu
  prichádza na zem a energiou, ktorá je uvoľnená. To sa rovná 2,3 W/m² (Watt na
  meter štvorcový) (tj.3,1 W/m² pre skleníkový efekt mínus 0,8 W/m² pre
  aerosóly).
---

