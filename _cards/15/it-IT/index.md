---
title: Forzante radiativo
backDescription: >-
  Il forzante radiativo è la misura dello squilibrio tra l'energia che arriva
  ogni secondo sulla Terra e quella che viene rilasciata. Corrisponde a 3,8 W/m²
  (Watt per m²) per l'effetto serra e -1 W/m² per gli aerosol, cioè 2,8 W/m² in
  tutto.
---
Nel grafico principale sono mostrate le diverse componenti del forzante radiativo dal 1750 a oggi (in variazione rispetto alla situazione di equilibrio del 1750 in cui il forzante radiativo = 0): nella parte superiore gli effetti riscaldanti (forzante radiativo positivo), nella parte inferiore gli effetti raffreddanti (forzante radiativo negativo). L'effetto serra (CO2 + altri gas serra (WMGHG) + ozono (O3) troposferico) rappresenta un forzante positivo di 3,8 W/m². Lo vediamo quindi nella parte superiore del grafico. Gli aerosol (effetto diretto + effetto indiretto), i vulcani e il cambiamento di destinazione d'uso del suolo hanno un effetto raffreddante e si trovano quindi nella parte inferiore del grafico. Il grafico secondario a destra rappresenta il forzante radiativo nell'arco di due secoli e mezzo (dati storici e proiezioni). Nel sesto report dell'IPCC, il forzante radiativo è pari a 3,8 W/m² (Watt per m²) per l'effetto serra e -1 W/m² per gli aerosol, per un totale di 2,8 W/m². I valori del forzante nel 2100 hanno dato il nome ai 5 scenari dell'IPCC (SSP1-1.9; SSP1-2.6; SSP2-4.5; SSP3-7.0; SSP5-8.5). I colori di questi scenari si ritrovano nei grafici delle carte n° 5, 11, 15, 21, 22 e 24 [^1].
[^1]: _AR6 WG1 TS3.1 p59_ // AR6 WG1 Figura TS.9 (d) (forzante) // p36 // AR5 WG1 Figura 8.18: Forzante radiativo p979 (962)_
