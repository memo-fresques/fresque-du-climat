---
title: Strahlungsantrieb
backDescription: >-
  Der Strahlungsantrieb stellt das (vom Menschen verursachte) Ungleichgewicht
  zwischen der Energie, die jede Sekunde auf der Erde ankommt, und der Energie,
  die freigesetzt wird, dar. Er wird mit 2,3 W/m² (Watt pro m²) angegeben: 3,1
  W/m² für den Treibhausef
---

