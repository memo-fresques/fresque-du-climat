---
title: Abrandamento da corrente do Gulf Stream
backDescription: >-
  A circulação termohalina, da qual o Gulf Stream faz parte, poderia abrandar
  devido ao influxo de água doce da Gronelândia derretida. Isto iria
  desregulamentar ainda mais o ciclo da água e reduzir a capacidade do oceano de
  absorver carbono e calor.
---

