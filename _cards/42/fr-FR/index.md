---
title: Ralentissement du gulf stream
backDescription: >-
  La circulation thermohaline dont _x000B_fait partie le Gulf Stream pourrait
  ralentir à cause de l’apport en eau douce de la fonte du Groenland. Cela
  aurait pour effet de déréguler encore plus le cycle de l’eau et de réduire la
  capacité de l’océan à absorber du
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_42_ralentissement_du_gulf_stream
youtubeCode: ''
instagramCode: CRopOMaIBYL
---

La circulation thermohaline, appelée aussi circulation océanique profonde, est la circulation océanique engendrée par les différences de densité de l'eau de mer, à l'origine de courants marins de profondeur. Ces différences de densité proviennent des écarts de température et de salinité des masses d'eau, d'où le terme de thermo — pour température — et halin — pour salinité. L’eau de mer est d’autant plus dense que sa température est basse et sa salinité élevée. Les scientifiques utilisent le terme de MOC pour Meridional Overturning Circulation (Circulation méridienne de retournement). Quand cela concerne uniquement l'atlantique, on parle de l'AMOC (Atlantic Meridional Overturning Circulation). Le Gulf Stream est un courant océanique de surface qui prend sa source entre la Floride et les Bahamas et se dilue dans l'océan Atlantique vers la longitude du Groenland. Il est une des composante de l'AMOC.
