---
title: Methanhydrate
backDescription: >-
  Methanhydrate bestehen aus Methan, das in erstarrtes Wasser eingelagert ist
  und auf dem Meeresboden entlang der Kontinentalabhänge vorkommen. Ab einem
  globalen Temperaturanstieg von +2°C können sie instabil werden.
---

