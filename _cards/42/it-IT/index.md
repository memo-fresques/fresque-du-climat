---
title: Rallentamento della corrente del Golfo
backDescription: >-
  La circolazione termoalina, di cui fa parte la corrente del Golfo, potrebbe
  rallentare a causa dell'afflusso di acqua dolce dovuto allo scioglimento della
  calotta glaciale della Groenlandia. Questo porterebbe a un ulteriore
  squilibrio del ciclo dell'acqua.
---
La circolazione termoalina, anche chiamata circolazione oceanica profonda, è la circolazione oceanica generata dalle differenze di densità dell'acqua di mare, che originano correnti marine di profondità. Queste differenze di densità derivano dalle differenze di temperatura e salinità delle masse d'acqua, da cui il termine termo - per temperatura - e alino - per salinità. L'acqua di mare è tanto più densa quanto più bassa è la sua temperatura e più elevata è la sua salinità. Gli scienziati utilizzano il termine MOC per Meridional Overturning Circulation. Quando si riferisce solo all'Atlantico, si parla di AMOC (Atlantic Meridional Overturning Circulation). La Corrente del Golfo è una corrente oceanica di superficie che ha origine tra la Florida e le Bahamas e si diluisce nell'oceano Atlantico verso la longitudine della Groenlandia. È una delle componenti dell'AMOC.
