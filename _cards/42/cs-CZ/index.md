---
title: Metanové hydráty
backDescription: >-
  Metanové hydráty (neboli metanové klatráty) jsou formou ledu na dně oceánu,
  podél kontinentálních svahů, který zachycuje molekuly metanu. Při teplotě
  vyšší než 2 °C se mohou stát nestabilními.
---

