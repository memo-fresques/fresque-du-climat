---
title: Hydráty metánu
backDescription: >-
  Hydráty (alebo Klatráty) metánu sú formou ľadu na oceánskom dne, pozdĺž
  kontinentálnych svahov, ktoré zachytávajú molekuly metánu. Môžu sa stať
  nestabilnými nad +2°C.
---

