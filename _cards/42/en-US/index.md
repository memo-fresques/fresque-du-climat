---
title: Methane Hydrates
backDescription: >-
  Methane hydrates (or methane clathrates) are a form of ice on the ocean floor,
  along continental slopes, that traps methane molecules. They can become
  unstable above +3.6°F (+2°C) .
---

