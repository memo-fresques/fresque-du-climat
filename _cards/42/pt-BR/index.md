---
title: Hidratos de Metano
backDescription: >-
  Os hidratos de metano (ou clatratos de metano) são uma forma de gelo
  encontrada no fundo do oceano, ao longo das encostas continentais, que prende
  as moléculas de metano. Eles podem ficar instáveis acima de + 2 ° C.
---

