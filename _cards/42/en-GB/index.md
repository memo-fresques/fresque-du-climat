---
title: Weakening Gulf Stream
backDescription: >-
  The Gulf Stream is part of the ocean's thermohaline circulation. It could
  weaken in response to freshwater input from Greenland's melting ice sheet.
  This could disrupt the water cycle even more and reduce the ocean's capacity
  to absorb more carbon and hea
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_42_weakening_gulf_stream
youtubeCode: 0KDUOKv2Qg8
instagramCode: CRohy7Sozrr
---

see n°42.
