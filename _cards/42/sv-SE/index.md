---
title: Försvagad Golfström
backDescription: >-
  Golfströmmen är en del av havets termohalina cirkulation. När Grönlands
  istäcke smälter kan ökningen av sötvatten leda till att golfströmmen
  försvagas. Detta kan störa vattnets kretslopp ännu mer och reducera havets
  kapacitet att absorbera koldioxid och v
---

