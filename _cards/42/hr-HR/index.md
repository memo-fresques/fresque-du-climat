---
title: Slabljenje Golfske struje
backDescription: >-
  Golfska struja dio je sustava oceanske termohalinske cirkulacije. Prijeti joj
  slabljenje zbog slatke vode koja pritječe topljenjem ledenjaka na Grenlandu.
  To slabljenje moglo bi dodatno poremetiti hidrološki ciklus, što bi
  posljedično umanjilo kapacitet o
---

