---
title: Metán-hidrátok
backDescription: >-
  A metán-hidrátok (vagy metán-klatrátok) a jég egy formái az óceánfenéken, a
  kontinentális lejtők mentén, melyek csapdába ejtik a metánmolekulákat. +2°C
  felett instabillá válnak.
---

