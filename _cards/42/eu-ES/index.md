---
title: Metano hidratoak
backDescription: >-
  Metano hidratoak (edo metano klatratoak), itsas hondoan, kontinente ezponden
  luzeran den izotz mota bat da. Metano molekulak bahitzen ditu eta ezegonkor
  bilaka daitezke 2°C-tik gorago.
---

