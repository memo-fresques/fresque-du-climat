---
title: Metanhydrat
backDescription: >-
  Metanhydrat (eller metanklatrat) er en form for is som finnes på havbunnen
  langs kontinentale skråninger, som fanger metan. De kan bli ustabile over
  +2°C.
---

Den termohaline sirkulasjonen, også kalt dypvannssirkulasjon, er havsirkulasjonen som oppstår på grunn av forskjeller i tettheten til sjøvann, som gir opphav til dype havstrømmer. Disse tetthetsforskjellene skyldes variasjoner i temperatur og saltholdighet i vannmassene, derav begrepene termo — for temperatur — og halin — for saltholdighet. Sjøvann er desto tettere jo lavere temperaturen er og jo høyere saliniteten er. Forskere bruker begrepet MOC for Meridional Overturning Circulation (Meridional havsirkulasjon). Når det kun gjelder Atlanterhavet, kalles det AMOC (Atlantic Meridional Overturning Circulation). Golfstrømmen er en overflatestrøm som har sin kilde mellom Florida og Bahamas og bøyer ut i Atlanterhavet mot Grønlands lengdegrad. Den er en del av AMOC.
