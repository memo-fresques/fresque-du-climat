---
title: Metan Hidrat
backDescription: >-
  Metan hidratlar (veya metan klatratlar) okyanus tabanında, kıta yamaçları
  boyunca metan moleküllerini hapseden bir buz türüdür. 2°C'nin üzerindeki
  sıcaklık artışında kararsız olabilirler.
---

