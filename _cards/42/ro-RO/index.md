---
title: Hidrații de metan
backDescription: >-
  Hidrații de metan (sau clatrații de metan) sunt o formă de gheață aflată pe
  fundul oceanului, de-a lungul versanților continentali, care captează
  moleculele de metan. Ele pot deveni instabile la peste + 2 ° C.
---

