---
title: Osłabienie Prądu Zatokowego
backDescription: >-
  Napływ słodkiej wody z topnienia Grenlandii może spowodować spowolnienie
  cyrkulacji termohalinowej, do której zalicza się także Prąd Zatokowy
  (Golfsztrom). Może to spowodować jeszcze większe zaburzenia obiegu wody i
  zmniejszyć zdolność oceanów do pochłaniania dwutlenku węgla i ciepła.
---
zob. nr 42.
