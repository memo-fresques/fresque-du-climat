---
backDescription: >-
  La circulation thermohaline dont _x000B_fait partie le Gulf Stream pourrait
  ralentir à cause de l’apport en eau douce de la fonte du Groenland. Cela
  aurait pour effet de déréguler encore plus le cycle de l’eau et de réduire la
  capacité de l’océan à absorber du
instagramCode: CRopOMaIBYL
title: Hidratoù metan
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_42_ralentissement_du_gulf_stream
youtubeCode: ''
---

