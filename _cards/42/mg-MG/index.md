---
title: '"Hydrates de méthane"'
backDescription: >-
  Ireo "hydrates de méthanes" (na "clathrates de méthane") dia karazana
  ranomandry mipetraka any amin'ny fanambanin'ny ranomasimbe, eny amin'ny
  tahalan'ny tany, izay mitahiry "méthanes". Mety hiova bika izy ireo rehefa
  ambonin'ny +2°C ny hafanana.
---

"Hydrates de méthane"
