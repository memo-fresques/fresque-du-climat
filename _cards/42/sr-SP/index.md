---
title: Hidrat metana
backDescription: >-
  Hidrat metana (ili klatrat metana) je vrste leda sa dna okeana, duž
  kontinentalne padine u kom su zaleđeni molekuli metana. Oni postaju nestabilni
  na temperaturi iznad +2°C.
---

