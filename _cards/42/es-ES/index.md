---
title: Hidratos de metano
backDescription: >-
  Los hidratos de metano (o clatrato de metano) son una forma de hielo que
  existe en el fondo de los océanos, a lo largo de los taludes continentales,
  que retiene moléculas de metano. Pueden volverse inestables por encima de los
  +2°C.
---

