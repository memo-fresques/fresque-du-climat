---
title: Ptéropodes et coccolithophores
backDescription: >-
  Les ptéropodes sont du zooplancton et les coccolithophores du phytoplancton.
  Ces micro-organismes ont une coquille en calcaire.
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_29_pt%C3%A9ropodes_et_coccolithophores
youtubeCode: nzqB3nKTjzc
instagramCode: CLcIL6rI0c8
---

C'est une carte qui fait beaucoup rire à cause des la difficulté à retenir ces noms. Apprenez-les par cœur, comme si c'était une évidence. "Ça fait toujours bien dans les dîners en ville !" ;-)
