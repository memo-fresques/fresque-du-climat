---
backDescription: >-
  Les ptéropodes sont du zooplancton et les coccolithophores du phytoplancton.
  Ces micro-organismes ont une coquille en calcaire.
instagramCode: CLcIL6rI0c8
title: Pteropoded ha kokolitofored
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_29_pt%C3%A9ropodes_et_coccolithophores
youtubeCode: nzqB3nKTjzc
---

