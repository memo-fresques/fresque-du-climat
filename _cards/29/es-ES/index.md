---
title: Pterópodos y cocolitofóridos
backDescription: >-
  Los pterópodos son zooplancton y los cocolitofóridos, fitoplancton. Estos
  microorganismos tienen una concha calcárea.
---

