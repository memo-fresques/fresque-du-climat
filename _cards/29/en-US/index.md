---
title: Pteropods and Coccolithophores
backDescription: >-
  Pteropods are a kind of zooplankton and Coccolithophores a kind of
  phytoplankton. These organisms have a calcified shell.
---

