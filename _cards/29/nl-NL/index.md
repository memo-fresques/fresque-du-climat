---
title: Pteropoda en coccolithoforen
backDescription: >-
  Pteropoda zijn zoöplankton en coccolithophoren zijn fytoplankton. Deze
  organismen hebben een kalkhoudende schaal.
---
Deze kaart leent zich voor grappen omdat met name "coccolithoforen" lastig uit te spreken is ;)
