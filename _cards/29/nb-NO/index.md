---
title: Pteropoder og kalkflagellater
backDescription: >-
  Pteropoder er en type dyreplankton og kalkflagellater er en type
  planteplankton. Disse mikroorganismene har et forkalket skall.
---

Dette er et kort som får mange til å le på grunn av vanskeligheten med å huske disse navnene. Lær dem utenat, som om det var en selvfølge. "Det tar seg godt ut i middagsselskap!" ;-)
