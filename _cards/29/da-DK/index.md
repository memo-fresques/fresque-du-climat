---
title: Pteropoder & Coccolithophorer
backDescription: >-
  Forsuring af havene går primært ud over plankton som Pteropoder (en slags
  dyreplankton) og Coccolithophorer (en slags planteplankton). Begge grupper er
  skal-dannende.
---

