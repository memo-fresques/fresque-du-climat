---
title: Pteropodi e coccolitofori
backDescription: >-
  Gli pteropodi sono zooplancton e i coccolitofori fitoplancton. Questi
  microrganismi hanno un guscio di calcare.
---
Questa è una carta che fa molto ridere a causa della difficoltà di ricordare questi nomi. Imparali a memoria, come se fosse ovvio. "Fa sempre bella figura durante le cene in città!" ;-)
