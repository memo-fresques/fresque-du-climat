---
title: Pterópodes e cocolitóforos
backDescription: >-
  Pterópodes são zooplânctons e cocolitóforos são fitoplânctons. Estes
  organismos possuem uma concha calcária.
---

É uma carta que faz muita gente rir devido à dificuldade em memorizar estes nomes. Podemos aprendê-los de cor, como se fosse óbvio: "Fica sempre bem em jantares com amigos!" ;-)
