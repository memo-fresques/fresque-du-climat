---
title: Rovsimsnäckor och coccoliter
backDescription: >-
  Rovsimsnäckor (eng: Pteropods) är zooplankton och coccoliter (eng:
  coccolithophores) är fytoplankton. Dessa organismer har kalkstensskal.
---

Det här är ett kort som lockar till skratt på grund av svårigheten att komma ihåg dessa namn. Lär dig dem utantill, som om det vore självklart. 
