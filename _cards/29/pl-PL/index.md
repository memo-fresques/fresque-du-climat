---
title: Skrzydłonogi i kokolitofory
backDescription: >-
  Skrzydłonogi to przedstawiciele zooplanktonu, a kokolitofory – fitoplanktonu.
  Te mikroorganizmy wykształcają wapienne szkielety.
---
Wiele żartów na temat kokkolitoforów, ponieważ ich nazwa jest trudna do wymówienia.
