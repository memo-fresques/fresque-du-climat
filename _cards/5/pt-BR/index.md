---
title: Combustíveis Fósseis
backDescription: >-
  Os combustíveis fósseis são carvão, petróleo e gás natural. Eles são usados
  principalmente na construção civil, transportes e indústria. Eles emitem CO2
  quando queimados.
---

