---
title: Fossiele brandstoffen
backDescription: >-
  Onder fossiele brandstoffen vallen onder andere aardolie, aardgas, steenkool
  en bruinkool. De industrie, de bouwsector en de transportsector verbruiken
  allemaal fossiele brandstoffen. Bij de verbranding van deze fossiele
  brandstoffen komt CO2 vrij.
---
Er is vaak discussie over of de kaart Fossiele Brandstoffen voor of na de kaart Menselijke activiteiten hoort. Zoals bij de kip en het ei is hier geen definitief antwoord, dus spendeer hier geen onnodige tijd aan. De grafiek laat de emissies van fossiele brandstoffen zien. In een +2°C scenario moeten deze emissies tot nul gereduceerd zijn in 2070.
