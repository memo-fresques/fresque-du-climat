---
title: Fosszilis energiahordozók
backDescription: >-
  Fosszilis energiahordozónak számít a kőszén, a kőolaj és a földgáz. Ezek nagy
  részben az iparban, az építési- és közlekedési szektorban kerülnek
  felhasználásra. Elégetésük CO2-kibocsátással jár.
---

