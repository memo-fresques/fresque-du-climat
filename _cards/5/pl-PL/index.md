---
title: Paliwa kopalne
backDescription: >-
  Do paliw kopalnych zalicza się węgiel, ropę naftową i gaz ziemny. Znajdują
  zastosowanie głównie w eksploatacji budynków, transporcie i przemyśle. Podczas
  spalania wytwarzają CO2.
---
Często toczy się debata między umieszczeniem kart paliw kopalnych przed lub po działalności człowieka. Podobnie jak w przypadku kury i jajka, nie ma jednoznacznej odpowiedzi. Nie należy tracić na to czasu. Wykres przedstawia emisje paliw kopalnych. W scenariuszu +2°C powinny one osiągnąć zero do 2070 roku.
