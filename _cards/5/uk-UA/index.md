---
title: Горючі корисні копалини або викопне паливо
backDescription: >-
  Викопне паливо - це вугілля, нафта та природний газ. Вони використовуються в
  основному у будівлях, транспорті та промисловості. У процесі згоряння вони
  виділяють СО2, вуглекислий газ.
---

