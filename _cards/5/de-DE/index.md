---
title: Fossile Brennstoffe
backDescription: >-
  Zu den fossilen Brennstoffen gehören Kohle, Öl und Gas. Sie werden
  hauptsächlich in den Bereichen Gebäude, Verkehr und Industrie eingesetzt. Bei
  ihrer Verbrennung wird CO2 ausgestoßen.
---

