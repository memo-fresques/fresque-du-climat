---
title: Fossile Brændstoffer
backDescription: >-
  Fossile brændstoffer er kul, olie og naturgas. Vi bruger dem primært i byggeri
  og boliger, til transport og industri. Afbrænding af fossile brændstoffer
  frigiver CO2.
---

