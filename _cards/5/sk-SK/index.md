---
title: Fosílne palivá
backDescription: >-
  Fosílne palivá sú uhlie, ropa a zemný plyn. Používajú sa hlavne v budovách,
  doprave a priemysle. Vypúšťajú Oxid uhličitý (CO2) počas spaľovania.
---

