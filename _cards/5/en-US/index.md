---
title: Fossil Fuels
backDescription: >-
  Fossil fuels are coal, petroleum and natural gas. They are used mainly for
  transportation, industry and the usage of buildings. They emit CO2 when
  burned.
---

