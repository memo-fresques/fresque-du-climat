---
title: Fossila bränslen
backDescription: >-
  Fossila bränslen är kol, olja och naturgas. De används i byggnader,
  transporter, och industrin. De släpper ut koldioxid när de förbränns.
---

