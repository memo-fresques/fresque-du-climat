---
title: Utilizzo dei combustibili fossili
backDescription: >-
  I combustibili fossili sono: carbone, petrolio e gas. Sono utilizzati
  principalmente negli edifici, nei trasporti e nell'industria. Emettono CO2
  durante la combustione.
---
Si discute spesso se mettere i combustibili fossili prima o dopo le attività umane. È come l’uovo e la gallina: non esiste una risposta giusta. È una questione di dialettica. Non dobbiamo perdere tempo su questo. Il grafico rappresenta le sole emissioni globali di CO2 derivanti dai combustibili fossili. La curva in nero mostra le emissioni passate e, a colori, le proiezioni secondo i 5 scenari studiati dall'IPCC nel 6° rapporto (AR6). Per i 2 scenari blu (SSP1-1.9 e SSP1-2.6), le emissioni previste devono azzerarsi, rispettivamente nel 2060 e nel 2080. I combustibili fossili sono: carbone, petrolio e gas naturale[^1].  
[^1]: _AR6 WG1 Figura SPM.4 p30 (p13)_
