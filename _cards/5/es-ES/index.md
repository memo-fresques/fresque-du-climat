---
title: Combustibles fósiles
backDescription: >-
  Los combustibles fósiles son: el carbón, el petróleo y el gas natural. Cuando
  se queman, emiten CO2. Se utilizan principalmente en los edificios, el
  transporte y la industria.
---
A menudo hay un debate entre si la carta de los combustibles fósiles va antes o después de la carta actividades humanas. Es como el debate del huevo y la gallina: no hay una respuesta correcta. No debemos perder el tiempo con esto, deja que el grupo decida. El gráfico representa las emisiones globales de CO2 procedentes únicamente de combustibles fósiles. La curva de color negro muestra las emisiones pasadas, y los otros colores, representan las proyecciones según los 5 escenarios estudiados por el IPCC en el VI informe (AR6). Se puede ver en los 2 escenarios de color azul (SSP1-1.9 y SSP1-2.6) que se prevee que las emisiones planificadas lleguen a cero, en 2060 y 2080 respectivamente. Las energías fósiles son: carbón, petróleo y gas natural[^1].  
[^1]: _AR6 WG1 Figura RRP.4 p30 (p13)_
