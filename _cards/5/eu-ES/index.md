---
title: Energia fosilak
backDescription: >-
  Ikatza, petrolioa eta gasa dira energia fosilak. Eraikinetan, garraioan eta
  industrian erabiltzen dira gehien. Erretzean CO₂-a isurtzen dute.
---

