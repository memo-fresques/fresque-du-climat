---
title: الوقود الحفري
backDescription: >-
  الوقود الأحفوري هو الفحم والنفط والغاز الطبيعي. يتم استخدامها بشكل أساسي في
  المباني والنقل والصناعة. ينبعث منها ثاني أكسيد الكربون عند الاحتراق.
---

