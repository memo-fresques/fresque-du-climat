---
title: Combustíveis Fósseis
backDescription: >-
  Os combustíveis fósseis são o carvão, o petróleo e o gás natural. São usados
  principalmente em edifícios, transportes e indústria e emitem CO2 durante a
  combustão.
---
Há frequentemente um debate entre colocar o Uso de combustíveis fósseis antes ou depois das Atividades humanas. É como a galinha e o ovo: não há uma resposta certa. É uma questão de dialética. Não se deve perder tempo com isso. O gráfico representa as emissões mundiais de CO2 apenas provenientes dos combustíveis fósseis. A curva em preto mostra as emissões passadas, e a cores, as projeções de acordo com os 5 cenários estudados pelo IPCC no 6º relatório (AR6). Para os 2 cenários azuis (SSP1-1.9 e SSP1-2.6), as emissões previstas devem ser zero, respetivamente em 2060 e 2080. Os combustíveis fósseis são: carvão, petróleo e gás natural[^1].  
[^1]: _AR6 WG1 Figura SPM.4 p30 (p13)_
