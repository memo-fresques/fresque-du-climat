---
title: Fosilní paliva
backDescription: >-
  Mezi fosilní paliva patří uhlí, ropa a zemní plyn. Používají se hlavně ve
  stavebním sektoru, dopravě a průmyslu. Při spalování dochází k produkci CO2.
---

