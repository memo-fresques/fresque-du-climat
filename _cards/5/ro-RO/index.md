---
title: Combustibili fosili
backDescription: >-
  Combustibilii fosili sunt cărbunele, petrolul și gazul. Sunt utilizați în
  principal în clădiri, în transport și în industrie. Aceştia emit CO2 în timpul
  arderii.
---

