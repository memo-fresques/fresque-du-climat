---
title: Fossile brensler
backDescription: >-
  Fossile brensler består av kull, olje, og naturgass. De brukes hovedsakelig i
  bygninger, transport og industri. De avgir CO2 når de brennes.
---
Det oppstår ofte diskusjoner om å sette fossile energikilder før eller etter menneskelige aktiviteter. Det er som høna og egget: det finnes ikke noe riktig svar. Det er et spørsmål om dialektikk. Man bør ikke kaste bort tid på det. Grafen viser de globale CO2-utslippene fra fossile energikilder alene. Den svarte kurven viser tidligere utslipp, og i farger, projeksjonene i henhold til de 5 scenariene studert av IPCC i den 6. rapporten (AR6). For de 2 blå scenariene (SSP1-1.9 og SSP1-2.6), må de forventede utslippene bli null, henholdsvis i 2060 og 2080. Fossile energikilder er: kull, olje og naturgass[^1].  
[^1]: _AR6 WG1 Figur SPM.4 s30 (s13)_
