---
title: Fosil Yakıtlar
backDescription: >-
  Fosil yakıtlar kömür, petrol, ve doğal gazdır. Ulaşımda, binalarda ve
  endüstride kullanılır. Yakıldığında CO2 (karbondiyoksit) açığa çıkarırlar.
---

