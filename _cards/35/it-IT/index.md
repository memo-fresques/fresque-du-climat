---
title: Incendi
backDescription: >-
  Siccità e ondate di calore favoriscono gli incendi, che emettono CO2
  esattamente come la deforestazione. Questo costituisce un fenomeno di
  retroazione auto-rinforzante (feedback positivo).
---
Il cambiamento climatico ha creato un ambiente secco e favorevole alla propagazione degli incendi[1].  
_[1] 
AR6 WG2 2.4.4.2: observed changes in Wildfire (cambiamenti osservati negli incendi) p255 (p243) // AR6 WG2 FAQ 2.3 p24_
