---
title: Forest Fires
backDescription: >-
  Wildfires start and spread more easily during droughts and heatwaves. They
  emit CO2 in the same way as deforestation. This creates a feedback loop.
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_35_forest_fires
youtubeCode: 0hkG0xUIHp8
instagramCode: ''
---

