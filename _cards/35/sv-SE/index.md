---
title: Skogsbränder
backDescription: >-
  Skogsbränder startas och spridas lättare vid torka och värmeböljor. De orsakar
  koldioxidutsläpp på ett liknande sätt som avskogning och utgör en förstärkande
  återkoppling.
---
Klimatförändringarna har skapat en torr miljö som främjar spridningen av bränder[1].  
_[1] 
AR6 WG2 2.4.4.2: Observed Changes in Wildfire p255 (p243) // AR6 WG2 FAQ 2.3 p24_
