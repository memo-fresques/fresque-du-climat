---
title: Skogbranner
backDescription: Skogbranner starter enklere ved tørke og hetebølger.
---
Klimaendringer har skapt et tørt miljø som fremmer spredning av branner[1].  
_[1] 
AR6 WG2 2.4.4.2 : observerte endringer i skogbrann p255 (p243) // AR6 WG2 FAQ 2.3 p24_
