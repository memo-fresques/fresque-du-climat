---
title: Incêndios Florestais
backDescription: >-
  Os incêndios florestais começam mais facilmente durante as secas e ondas de
  calor.
---
As alterações climáticas criaram um ambiente seco e propício à propagação dos Incêndios[1].  
_[1] 
AR6 WG2 2.4.4.2 : observed changes in Wildfire p255 (p243) // AR6 WG2 FAQ 2.3 p24_
