---
backDescription: >-
  Les incendies sont facilités par les sécheresses et les canicules. Ils
  émettent du CO2 au même titre que la déforestation. Ceci constitue une boucle
  de rétroaction.
title: Incendies
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_35_incendies'
youtubeCode: s6uVJt_Mqdc
---
Le changement climatique a créé un environnement sec et propice à la propagation des incendies[1].  
_[1] 
AR6 WG2 2.4.4.2 : observed changes in Wildfire p255 (p243) // AR6 WG2 FAQ 2.3 p24_
