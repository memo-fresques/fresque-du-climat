---
title: Rising Sea Levels
backDescription: >-
  Since 1900, sea levels have risen by 20 cm. This is caused by the thermal
  expansion of ocean waters and the melting of glaciers and ice sheets.
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_22_sea_level_rise
youtubeCode: PwC6YqtuEYU
instagramCode: CM2QZsFH-NM
---

It is important to note that the forecasts for the rise in water levels are very conservative. Some phenomena, understood qualitatively but not quantitatively, are simply not quantified in the IPCC report. This is the case for moulins, for example. Moulins, or glacier mills, are shafts that carry melted water from the surface of a glacier or an ice sheet down to the bedrock. Once the water enters these passages, it lubricates the contact between the bedrock and the ice sheet, making it easier for the glaciers to drift towards the sea. The figures for sea level rise will therefore most likely be revised upwards in future reports. For the US, you can use the Sea Level Rise Viewer to show the extent of the phenomenon.
