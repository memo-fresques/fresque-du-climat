---
title: Stijging van de zeespiegel
backDescription: >-
  De zeespiegel is sinds 1900 met 20 cm gestegen. De zeespiegelstijging is
  veroorzaakt door de uitzetting van zeewater door temperatuurstijging,
  smeltende gletsjers en smeltende ijskappen
---
Het is belangrijk om aan te geven dat de voorspellingen voor de stijging van de zeespiegel zeer conservatief zijn. Sommige fenomenen, die wel kwalitatief maar (nog) niet kwantitatief begrepen worden, zijn simpelweg niet gekwantificeerd in het IPCC-rapport. Dit is bijvoorbeeld het geval voor gletsjermolens. Dit zijn schachten die smeltwater van het oppervlakt van de gletsjer naar de grond leiden. Als het het afgevoerde water op de ondergrond terecht komt vormt dit een glad laagje tussen de grond en het ijs. Dit kan het schuiven van de gletsjer naar de zee versnellen. De cijfers voor de stijging van het zeeniveau zullen daarom zeer waarschijnlijk aangepast worden in toekomstige rapporten. 
