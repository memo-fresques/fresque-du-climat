---
title: Havets vandstand stiger
backDescription: >-
  Globalt er havniveauet steget 20 cm siden år 1900. Havniveau stiger, dels
  fordi smeltende iskapper og gletsjere tilfører mere vand, og dels fordi vandet
  udvider sig, når det varmes op.
---

