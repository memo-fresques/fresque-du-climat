---
title: 海平面上升
backDescription: 自1900年以来，海洋高度上升了20厘米。导致海平面上升的原因是： 1）海水热膨胀，体积增大； 2）冰川融化； 3）冰盖融化。
---

