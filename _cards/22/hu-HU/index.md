---
title: Tengerszint-emelkedés
backDescription: >-
  1900 óta a világtenger szintje 20 cm-t emelkedett. A tengerszint-emelkedést a
  víz hőtágulása és a gleccserek és jégtakarók olvadása okozza.
---

