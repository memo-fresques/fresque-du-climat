---
title: Vzestup hladiny moře
backDescription: >-
  Od roku 1900 se hladina moře zvýšila o 20 cm. Zvýšení hladiny moře je
  způsobeno tepelnou expanzí oceánských vod a táním ledovců a ledových příkrovů.
---

