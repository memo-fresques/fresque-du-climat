---
title: Aumento del nivel del mar
backDescription: >-
  Desde 1900, el nivel del océano ha subido 20 cm. Esto se debe a la dilatación
  del agua, al deshielo de los glaciares y al deshielo de los casquetes polares.
---
El nivel del mar ha aumentado 20 cm desde 1900 y seguirá aumentando durante miles de años. Existe una gran incertidumbre sobre algunos procesos físicos como las inestabilidades de las capas de hielo, especialmente en la Antártida. En el 6º informe, el IPCC añadió una curva de puntos para mostrar la elevación del nivel del mar teniendo en cuenta estos procesos. Aunque no sean probables, no es posible excluir estos escenarios[^1].  
[^1]: AR6 WG1 Box TS.4 p45 (p77) // AR6 WG1 Box TS.4, Figura 1 p46 (p78)_
