---
backDescription: >-
  Depuis 1900, le niveau de l’océan a monté de 20 cm.Cela est dû à la dilatation
  de l'eau, la fonte des glaciers et la fonte des calottes.
instagramCode: CM2P9noInE8
title: Kresk live an dour
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_22_mont%C3%A9e_des_eaux
youtubeCode: iypRDzcMA-o
---

