---
title: Sea Level Rise
backDescription: >-
  Since 1900, sea level has risen by 7.9'' (20 cm). Sea level rise is caused by
  the thermal expansion of ocean waters, and the melting of glaciers and
  continental glaciers.
---

