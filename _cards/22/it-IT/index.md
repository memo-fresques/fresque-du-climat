---
title: Innalzamento del livello dell'acqua
backDescription: >-
  Dal 1900, il livello dell'oceano si è alzato di 20 cm. Ciò è dovuto alla
  dilatazione dell'acqua, allo scioglimento dei ghiacciai e allo scioglimento
  delle calotte glaciali.
---
Il livello del mare è aumentato di 20 cm dal 1900 e continuerà ad aumentare per migliaia di anni. Esiste una grande incertezza su alcuni processi fisici come le instabilità dei ghiacciai, in particolare in Antartide. Nel suo sesto rapporto, l'IPCC ha aggiunto una curva tratteggiata per mostrare l'aumento del livello del mare tenendo conto di questi processi. In mancanza di probabilità, non è possibile escludere questi scenari[^1].
[^1]: AR6 WG1 Box TS.4 p45 (p77) // AR6 WG1 Box TS.4, Figura 1 p46 (p78)_
