---
title: Havsnivåhöjning
backDescription: >-
  Sedan år 1900 havsnivån har stigit med 20 cm. Höjningen av havsnivån orsakas
  av havsvattnets termiska expansion och av smältningen av glaciärer och
  inlandsisar.
---
Havsnivån har stigit med 20 cm sedan 1900 och kommer att fortsätta att stiga i tusentals år. Det råder stor osäkerhet om vissa fysiska processer som instabiliteten hos inlandsisar, särskilt i Antarktis. I den sjätte rapporten lade IPCC till en prickad linje för att visa havsnivåhöjningen med hänsyn till dessa processer, men linjen är alltså behäftad md stor osäkerhet och kan visa sig vara en konservativ uppskattning [^1].  
[^1]: AR6 WG1 Box TS.4 p45 (p77) // AR6 WG1 Box TS.4, Figure 1 p46 (p78)_
