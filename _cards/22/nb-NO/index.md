---
title: Havnivåstigning
backDescription: >-
  Siden 1900 har havnivået steget med 20 cm. Havnivåstigning skyldes
  varmeutvidelse (termisk ekspansjon) av havvannet, og smelting av isbreer og
  innlandsis:
---
Havnivået har steget med 20 cm siden 1900 og vil fortsette å stige i tusenvis av år. Det er stor usikkerhet rundt enkelte fysiske prosesser som iskappenes ustabilitet, spesielt i Antarktis. I den sjette rapporten la IPCC til en stiplet kurve for å vise havnivåstigningen når man tar hensyn til disse prosessene. Selv om de ikke er sannsynlige, er det ikke mulig å utelukke disse scenariene[^1].  
[^1]: AR6 WG1 Box TS.4 p45 (p77) // AR6 WG1 Box TS.4, Figure 1 p46 (p78)_
