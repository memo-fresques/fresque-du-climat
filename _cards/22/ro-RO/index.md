---
title: Creșterea nivelului mării
backDescription: >-
  Din anii 1900, nivelul oceanului a crescut cu 20 cm. Această creștere este
  cauzată de dilatarea apei, de topirea ghețarilor și de topirea calotelor de
  gheață.
---

