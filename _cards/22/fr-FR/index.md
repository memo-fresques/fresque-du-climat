---
backDescription: >-
  Depuis 1900, le niveau de l’océan a monté de 20 cm.Cela est dû à la dilatation
  de l'eau, la fonte des glaciers et la fonte des calottes.
title: Montée des eaux
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_22_mont%C3%A9e_des_eaux
youtubeCode: iypRDzcMA-o
instagramCode: CM2P9noInE8
---
La niveau des mers a augmenté de 20 cm depuis 1900 et va continuer à augmenter pendant des milliers d'années. Il existe une grande incertitude sur certains processus physiques comme les instabilités des calottes glaciaires, notamment en Antarctique. Dans le 6e rapport, le GIEC a ajouté une courbe en pointillé pour montrer l'élévation du niveau des mers en prenant en compte ces processus. A défaut d'être probables, il n'est pas possible d'exclure ces scénarios[^1].  
[^1]: AR6 WG1 Box TS.4 p45 (p77) // AR6 WG1 Box TS.4, Figure 1 p46 (p78)_
