---
title: Zvyšovanie hladiny mora
backDescription: >-
  Od roku 1900 sa hladina mora zvýšila o 20 cm ako dôsledok termálneho
  rozpínania oceánskych vôd, topenia vysokohorských a pevninských ľadovcov.
---

