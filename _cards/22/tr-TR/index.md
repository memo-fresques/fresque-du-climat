---
title: Deniz seviyesinin yükselişi
backDescription: >-
  1900'den beri deniz seviyesi 20 cm yükseldi. Deniz seviyesinin yükselmesi,
  okyanus sularının termal genişlemesi ve kara buzullarının erimesinden
  kaynaklanmaktadır.
---

