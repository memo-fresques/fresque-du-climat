---
title: Porast razine mora
backDescription: >-
  Od 1900. godine razina mora porasla je za 20 cm. Porast uzrokuju: 1) toplinska
  dilatacija (volumno širenje) oceanskih voda, 2) topljenje kopnenih ledenjaka,
  3) topljenje polarnih ledenih kapa.
---

