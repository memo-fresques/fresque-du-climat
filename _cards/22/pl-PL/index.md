---
title: Wzrost poziomu mórz
backDescription: >-
  Od 1900 r. poziom mórz i oceanów podniósł się o 20 cm. Doszło do tego na
  skutek wzrostu objętości wody, topnienia lodowców i topnienia lądolodów.
---
Należy zauważyć, że prognozy dotyczące wzrostu poziomu wody są bardzo konserwatywne. Niektóre zjawiska, rozumiane jakościowo, ale nie ilościowo, po prostu nie zostały określone w raporcie IPCC. Tak jest na przykład w przypadku młynów lodowcowych. Są to szyby, które przenoszą stopioną wodę z powierzchni lodowca lub pokrywy lodowej do podłoża skalnego. Gdy woda dostanie się do tych korytarzy, pokrywa powierzchnię między podłożem skalnym a pokrywą lodową, ułatwiając lodowcom dryfowanie w kierunku morza. Dane dotyczące wzrostu poziomu morza zostaną zatem najprawdopodobniej skorygowane w przyszłych raportach. W przypadku Stanów Zjednoczonych można użyć przeglądarki Sea Level Rise Viewer, aby pokazać zasięg tego zjawiska.
