---
title: Aumento do nível do mar
backDescription: >-
  Desde 1900, o nível do mar subiu 20 cm. Esta subida é causada pela expansão
  térmica das águas oceânicas, pelo derretimento dos glaciares e pela fusão dos
  calotes polares.
---
O nível do mar aumentou 20 cm desde 1900 e continuará a aumentar durante milhares de anos. Existe uma grande incerteza sobre alguns processos físicos, como as instabilidades das calotes polares, especialmente na Antártida. No 6.º relatório, o IPCC adicionou uma linha tracejada para mostrar a elevação do nível do mar, tendo em conta esses processos. Embora não sejam prováveis, não é possível excluir esses cenários[^1].  
[^1]: AR6 WG1 Box TS.4 p45 (p77) // AR6 WG1 Box TS.4, Figura 1 p46 (p78)_
