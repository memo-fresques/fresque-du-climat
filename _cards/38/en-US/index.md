---
title: Human Health
backDescription: >-
  Famines, migration of disease vectors, heat waves and armed conflicts can
  affect human health.
---

