---
title: Salute umana
backDescription: >-
  Carestie, spostamento di vettori di malattie, ondate di calore e conflitti
  armati possono influenzare la salute umana.
---
È una delle carte che può essere posizionata per ultima, come LA conseguenza finale.
