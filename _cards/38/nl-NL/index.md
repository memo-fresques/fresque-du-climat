---
title: Menselijke gezondheid
backDescription: >-
  Hongersnoden, de verschuiving van vectoren voor ziekteoverdracht, hittegolven
  en gewapende conflicten hebben invloed op de menselijke gezondheid.
---
Dit is een van de kaarten die als laatste geplaatst wordt, als een van de ultieme consequenties van klimaatverandering.
