---
title: Sănătatea umană
backDescription: >-
  Foametea, deplasarea vectorilor de boli, valurile de căldură și conflictele
  armate pot afecta sănătatea umană.
---

