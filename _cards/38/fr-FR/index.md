---
title: Santé humaine
backDescription: >-
  Famines, déplacement des vecteurs de maladie, canicules et conflits armés
  peuvent affecter la santé humaine.
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_38_sant%C3%A9_humaine
youtubeCode: TNSNrEz4cGI
instagramCode: ''
---

C'est l'une des cartes qui peut être placée en dernier, comme LA conséquence ultime.
