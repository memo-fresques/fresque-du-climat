---
title: Helse
backDescription: >-
  Hungersnød, endring av smittekilder, hetebølger og væpnede konflikter kan
  påvirke folks helse.
---

Dette er et av kortene som kan plasseres sist, som den YTTERSTE konsekvensen.
