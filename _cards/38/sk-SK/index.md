---
title: Ľudské zdravie
backDescription: >-
  Hlad, premiestňovanie prenášačov chorôb, vlny horúčav a ozbrojené konflikty
  môžu ovplyvniť ľudské zdravie.
---

