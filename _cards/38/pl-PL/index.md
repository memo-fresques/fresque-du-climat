---
title: Zdrowie człowieka
backDescription: >-
  Na zdrowie ludzkie wpływają głód, migracje nosicieli chorób, upały i konflikty
  zbrojne.
---
Jest to jedna z kart, które należy umieścić na ostatnim miejscu, jako jedna z ostatecznych konsekwencji zmian klimatu.
