---
title: Saúde Humana
backDescription: >-
  Fomes, deslocamento de vetores de doenças, ondas de calor e conflitos armados
  podem afetar a saúde humana.
---

Esta é uma das cartas que pode ser colocada por último, como consequência final.
