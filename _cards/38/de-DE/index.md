---
title: Gesundheit des Menschen
backDescription: >-
  Hungersnöte, Krankheitsüberträger, Hitzewellen und bewaffnete Konflikte können
  die menschliche Gesundheit aufs Spiel setzen.
---

