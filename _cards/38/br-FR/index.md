---
backDescription: >-
  Famines, déplacement des vecteurs de maladie, canicules et conflits armés
  peuvent affecter la santé humaine.
instagramCode: ''
title: Yec'hed Mab-den
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_38_sant%C3%A9_humaine
youtubeCode: TNSNrEz4cGI
---

