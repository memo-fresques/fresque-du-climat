---
title: בריאות האדם
backDescription: >-
  רעב, ,תזוזה מרחבית של מפיצי מחלות, גלי חום וסכסוכים חמושים עלולים להשפיע על
  בריאות האדם.
---

