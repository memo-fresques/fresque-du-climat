---
title: Hälsa
backDescription: >-
  Svält, förflyttningar av sjukdomsbärare, värmeböljor och väpnade konflikter
  påverkar människors hälsa.
---

Detta är ett av korten som kan placeras sist, som en av de (eller t o m DEN) slutliga konsekvensen.
