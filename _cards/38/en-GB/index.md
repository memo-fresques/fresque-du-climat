---
title: Human Health
backDescription: >-
  Hunger, new vectors of disease, heatwaves and armed conflicts can have a
  negative effect on human health.
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_38_human_health
youtubeCode: 8Wz5bj7r-dI
instagramCode: ''
---

This is one of the cards to be placed last, as one of the ultimate consequences of climate change.
