---
title: Zatopenie pobrežia
backDescription: >-
  Cyklóny a narušenie počasia prináša vietor (následne vlny) a tlakovú níž.
  Zníženie tlaku o každý 1 hektopascal má za následok zvýšenie hladiny mora o
  1cm. Cyklóny môžu preto spôsobiť zatopenie pobrežných oblastí (alebo pobrežné
  záplavy), ktoré je znásoben
---

