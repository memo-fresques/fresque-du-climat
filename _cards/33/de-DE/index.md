---
title: Überschwemmungen
backDescription: >-
  Der Meeresspiegelanstieg, ausgelöst durch die Klimaerwärmung, kann
  (Küsten-)Überschwemmungen verursachen. Weitere Auslöser sind Zyklone und
  Wirbelstürme, die Wind (und damit Wellen) und Niederdruck mit sich bringen.
  Denn 1 Hektopascal weniger führt zu ein
---

