---
title: Sumersiones marinas
backDescription: >-
  Ciclones y perturbaciones conllevan viento, olas y bajas presiones. Un
  hectopascal menos significa un centímetro más en el nivel del mar, lo que
  puede ocasionar fenómenos de sumersión (inundaciones costeras) que se
  intensifican con el aumento del nivel de
---

