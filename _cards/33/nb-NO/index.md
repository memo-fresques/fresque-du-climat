---
title: Flom og stormflo ved kysten
backDescription: >-
  Sykloner og værforstyrrelser gir vind (derfor bølger) og lavtrykksforhold. En
  endring i lufttrykket på en hectopascal vil føre til en endring i vannstanden
  på om lag en cm. Derfor kan sykloner forårsake stormflo, forsterket av
  havnivåstigning som allerede
---
Ikke forveksle med flom. Oversvømmelser er når vannet fra havet eller havet stiger. Denne stigningen kan være eksepsjonell på grunn av ekstreme klimahendelser, eller permanent på grunn av havnivåstigning[1].  
_[1] AR6 WG1 FAQ 9.2 s54 // AR6 WG1 FAQ 8.2 s48_
