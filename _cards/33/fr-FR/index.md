---
backDescription: >-
  Cyclones et perturbations amènent du vent (donc des vagues) et des basses
  pressions. Or, chaque hectopascal en moins, c’est 1 cm d’eau en plus. Ils
  peuvent donc occasionner des submersions (inondations côtières) qui sont
  aggravées par l’augmentation du ni
title: Submersions
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_33_submersions
youtubeCode: AalZJ4lPh_Q
---
Ne pas confondre avec les crues. Les submersions, c'est l'eau de la mer ou l'océan qui monte. Cette montée peut être exceptionnelle à cause d'événements climatiques extrêmes, ou permanente à cause de la montée des eaux[1].  
_[1] AR6 WG1 FAQ 9.2 p54 // AR6 WG1 FAQ 8.2 p48_
