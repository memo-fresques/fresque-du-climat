---
backDescription: >-
  Cyclones et perturbations amènent du vent (donc des vagues) et des basses
  pressions. Or, chaque hectopascal en moins, c’est 1 cm d’eau en plus. Ils
  peuvent donc occasionner des submersions (inondations côtières) qui sont
  aggravées par l’augmentation du ni
instagramCode: ''
title: Liñvadennoù
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_33_submersions
youtubeCode: AalZJ4lPh_Q
---

