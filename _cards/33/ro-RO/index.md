---
title: Submersiuni în zonele de coastă
backDescription: >-
  Ciclonii și perturbările meteorologice aduc vântul (deci valurile) și presiune
  atmosferică scăzută. Un hectopascal în minus înseamnă o creștere a nivelului
  mării cu 1 cm în plus. Prin urmare, ciclonii pot provoca submersiuni
  (inundaţii în zonele de coastă
---

