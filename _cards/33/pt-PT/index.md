---
title: Submersão Marinha
backDescription: >-
  Ciclones e perturbações climáticas envolvem ventos (e, portanto, ondas) e
  baixas pressões. 1 hectopascal a menos representa um aumento do nível do mar
  de 1 cm. Por conseguinte, os ciclones podem causar submersão marinha
  (inundação costeira) amplificada pe
---
Não confundir com as Inundações. As Submersões Marinhas devem-se à subida da água do mar ou do oceano. Esta subida pode ser excecional devido a eventos climáticos extremos, ou permanente devido à subida das águas[1].  
_[1] AR6 WG1 FAQ 9.2 p54 // AR6 WG1 FAQ 8.2 p48_
