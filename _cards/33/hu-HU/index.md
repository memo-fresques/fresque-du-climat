---
title: Tengeri áradás
backDescription: >-
  A ciklonok és időjárási zavarok szelet (és ezáltal hullámokat) és alacsony
  légnyomási körülményeket hoznak. 1 hektopaszkál légnyomás-csökkenés 1 cm
  tengerszint-emelkedést jelent. Ebből fakadóan a ciklonok tengeri áradáshoz
  (vagy parti áradáshoz) vezethetn
---

