---
title: Sommersioni
backDescription: >-
  Cicloni e perturbazioni portano vento (e di conseguenza onde) e abbassamento
  di pressione. Un hectopascal in meno corrisponde a 1 cm d'acqua in più.
  Possono quindi provocare sommersioni (inondazioni costiere) che sono aggravate
  dall'aumento del livello degli oceani.
---
Da non confondere con le esondazioni. Le sommersioni sono causate dall'acqua del mare o dell'oceano che sale. Questo aumento può essere eccezionale a causa di eventi climatici estremi o permanente a causa dell'innalzamento del livello del mare[1]. 
_[1] AR6 WG1 FAQ 9.2 p54 // AR6 WG1 FAQ 8.2 p48_
