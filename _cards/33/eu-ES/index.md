---
title: Urperatzeak
backDescription: >-
  Zikloiek eta perturbazioek, haizea (beraz uhinak) eta depresioak sortzen
  dituzte. Alta, hektopascal bat gutiago = 1 cm ur gehiago. Ondorioz, itsas
  mailaren gorakadak okertu ditzateken urperatzeak eragin ditzazke (itsas
  bazterren urperatzeak).
---

