---
title: Deniz suyu altında kalma
backDescription: >-
  Kasırgalar ve dengesiz hava koşulları rüzgar (rüzgar nedeniyle dalga) ve düşük
  basınç getirir. 1 hektopaskal daha az basınç, deniz seviyesinin 1 cm
  yükselmesi demektir. Bu nedenle kasırgalar, halihazırda küresel ısınmanın
  neden olduğu deniz seviyesindeki
---

