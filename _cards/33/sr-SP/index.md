---
title: Potapanje priobalja
backDescription: >-
  Cikloni i vremenske nepogode izazivaju vetrove (a onda i talase) i uslove
  niskog atmosferskog pritiska. 1 hektopaskal niže znači 1 cm rasta nivoa mora.
  Tako cikloni mogu izazvati potapanje priobalja (ili priobalne poplave),
  naglašene već postojećim porast
---

