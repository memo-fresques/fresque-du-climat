---
title: Marine Submersion
backDescription: >-
  Cyclones and atmospheric waves bring wind, waves and low pressure conditions.
  A 1 hectopascal pressure drop causes a 1 cm sea level rise. Therefore cyclones
  can cause marine submersions (or coastal flooding), amplified by the sea level
  rise already caused
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_33_marine_submersion
youtubeCode: OuZd3GVgyjg
instagramCode: ''
---

Not to be confused with floods. Marine submersion is seawater or ocean water rising. This rise can be exceptional because of extreme weather events, or permanent because of rising water levels.
