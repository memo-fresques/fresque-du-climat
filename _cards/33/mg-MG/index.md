---
title: Fiakaran'ny ranomasina
backDescription: >-
  Ny rivo-doza sy ny fikorotanan'ny toetry ny andro dia mitondra rivotra (izany
  oe onja) ary mampidina ny tsindry. Ny fihenana "hectopascal" iray dia mitarika
  fiakaran'ny rano 1cm. Izany dia mety hitarika fiakaran'ny ranomasina
  (tondra-drano ny amoron-tsira
---

