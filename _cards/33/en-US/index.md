---
title: Coastal flooding
backDescription: >-
  Hurricanes and weather disturbances cause wind, waves and low pressure
  conditions. A 1 mB (0,03 inHg) pressure decrease causes a 0.4 inch (1 cm) sea
  level rise. Therefore hurricanes can cause coastal flooding (or marine
  submersions), amplified by the sea
---

