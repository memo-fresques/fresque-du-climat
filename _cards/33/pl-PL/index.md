---
title: Zalania terenów nadmorskich
backDescription: >-
  Cyklony i zaburzenia pogodowe przynoszą wiatr (a więc i fale) oraz spadek
  ciśnienia. A każdy hektopaskal mniej oznacza 1 cm wody więcej. Cyklony mogą
  więc powodować zalania terenów nadmorskich, których skala będzie dodatkowo
  potęgowana przez wzrost poziomu oceanów.
---
Nie należy ich mylić z powodziami. Zalania terenów nadmorskich powstają przez podnoszenie się poziomu wody morskiej lub oceanicznej. Wzrost ten może być wyjątkowy z powodu ekstremalnych zjawisk pogodowych lub stały z powodu rosnącego poziomu wody.
