---
title: Potapanja priobalnih područja
backDescription: >-
  Ciklone i atmosferski valovi donose vjetar, valove i niski tlak. Pad tlaka
  zraka od 1 hektopaskala uzrokuje porast razine mora od 1 cm.

  Stoga ciklone mogu izazvati potapanja obalnih područja. Potapanja dodatno
  pogoršava porast razine mora uzrokovan globa
---

