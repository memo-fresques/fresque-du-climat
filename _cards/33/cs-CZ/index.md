---
title: Zaplavování mořem
backDescription: >-
  Cyklóny a změny počasí přinášejí vítr (tedy vlny) a způsobují pokles tlaku.
  Pokles tlaku o 1 hektopascal znamená vzestup hladiny moře o 1 cm. Cyklóny
  proto mohou způsobit pobřežní záplavy, které jsou ještě zesílené vzestupem
  hladiny moře způsobeným globál
---

