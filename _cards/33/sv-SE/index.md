---
title: Kustöversvämningar
backDescription: >-
  Cykloner och andra oväder för med sig vind, vågor och lågtryck. En sänkning av
  lufttrycket med 1 hektopascal (hPa) innebär att havsnivån höjs med en
  centimeter. Därför kan cykloner orsaka kustöversvämningar. Dessa förvärras av
  den globala havsnivåhöjningen.
---
Förväxla inte med översvämningar runt floder och sjöar. Här avses när kustområden ställs under vatten på grund av att havsnivån stiger. Ökningen av havsnivån kan vara kraftig och tillfällig på grund av extrema väderförhållanden, men också ha ett permanent bidrag från stigande havsnivåer[1].  
_[1] AR6 WG1 FAQ 9.2 p54 // AR6 WG1 FAQ 8.2 p48_
