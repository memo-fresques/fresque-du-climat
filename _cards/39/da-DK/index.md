---
title: Klimaflygtninge
backDescription: >-
  Forestil dig du bor et sted, hvor du er blevet beskyttet mod konsekvenserne af
  klimaforandringer. Mon ikke der er mange millioner, der gerne ville dele det
  sted med dig?
---

