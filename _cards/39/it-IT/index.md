---
title: Spostamento di popolazioni
backDescription: >-
  Le conseguenze del cambiamento climatico renderanno inevitabilmente alcuni
  ecosistemi inabitabili per gli uomini e costringeranno le popolazioni a
  spostarsi.
---
Gli impatti del cambiamento climatico, come gli uragani devastanti, le ondate di calore estreme, l'innalzamento del livello del mare e la desertificazione, sono forze trainanti che costringono le comunità a cercare rifugio altrove. Gli abitanti delle zone costiere sono spesso i primi a essere colpiti, con le loro terre che vengono inghiottite dalle acque o devastate da tempeste sempre più violente.

Ad esempio, l'innalzamento del livello del mare mette in pericolo l'esistenza stessa di Kiribati, composta da numerosi atolli bassi. Gli effetti del cambiamento climatico, come l'erosione costiera e la salinizzazione delle terre e delle acque dolci, hanno impatti devastanti sulla vita quotidiana degli abitanti di queste isole. Le case vengono inghiottite dal mare, le colture danneggiate dall'intrusione di acqua salata e l'accesso all'acqua potabile diventa sempre più difficile. Alcuni abitanti hanno già intrapreso azioni per emigrare, anticipando le crescenti sfide poste dal cambiamento climatico[1].
_[1] AR6 WG2 6.2.3.3 p941 (p929)_
