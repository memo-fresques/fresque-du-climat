---
title: Refugiados climáticos
backDescription: >-
  Imagínate vivir en un lugar que milagrosamente no se ha visto afectado por el
  cambio climático. ¡Millones de seres humanos van a querer compartirlo contigo!
---

