---
title: Refugiați climatici
backDescription: >-
  Imaginați-vă că locuiți într-un loc care a fost în mod miraculos cruțat de
  schimbările climatice. Câteva miliarde de ființe umane ar putea dori să
  împartă acest spațiu cu voi.
---

