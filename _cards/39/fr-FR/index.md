---
backDescription: >-
  Imaginez que vous vivez dans un endroit qui est miraculeusement épargné par le
  changement climatique. Quelques milliards d'humains risquent d'avoir très
  envie de le partager avec vous !!!
title: Déplacement de populations
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_39_r%C3%A9fugi%C3%A9s_climatiques
youtubeCode: He2tKBiumfU
---
Les impacts du changement climatique, tels que les ouragans dévastateurs, les vagues de chaleur extrêmes, la montée du niveau de la mer et la désertification, sont autant de forces motrices qui contraignent les communautés à chercher refuge ailleurs. Les habitants des zones côtières sont souvent les premiers touchés, leurs terres étant englouties par les eaux ou ravagées par des tempêtes de plus en plus violentes.

Par exemple, la montée du niveau de la mer met en péril l'existence même de Kiribati, constituée de nombreux atolls bas. Les effets du changement climatique, tels que l'érosion côtière et la salinisation des terres et de l'eau douce, ont des impacts dévastateurs sur la vie quotidienne des habitants de ces îles. Les maisons sont englouties par la mer, les cultures sont endommagées par l'intrusion d'eau salée, et l'accès à l'eau potable devient de plus en plus difficile. Certains habitants ont déjà entrepris des démarches pour émigrer, anticipant les défis croissants posés par le changement climatique[1].  
_[1] AR6 WG2 6.2.3.3 p941 (p929)_
