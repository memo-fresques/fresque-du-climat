---
title: Klimaatvluchtelingen
backDescription: >-
  Stel je voor dat je op een plek woont die op miraculeuze wijze gespaard is
  gebleven van de klimaatverandering. Je zou deze plek waarschijnlijk met een
  paar miljard mensen moeten delen.
---

