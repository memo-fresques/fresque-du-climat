---
title: Klimatičtí uprchlíci
backDescription: >-
  Představte si, že žijete na místě, které zázračně uniklo klimatickým změnám.
  Několik miliard lidských bytostí by s vámi mohlo chtít tento prostor sdílet.
---

