---
title: Climate Refugees
backDescription: >-
  Imagine that you live in a place that has been miraculously spared by climate
  change. Several billions of human beings might want to share this space with
  you.
---

