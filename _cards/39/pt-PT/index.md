---
title: Refugiados climáticos
backDescription: >-
  Imagine que vive num lugar que foi milagrosamente poupado pelas alterações
  climáticas. Vários biliões de seres humanos podem querer compartilhá-lo
  consigo.
---
Os impactos das alterações climáticas, tais como furacões devastadores, ondas de calor extremas, a subida do nível do mar e a desertificação, são forças motrizes que obrigam as comunidades a procurar refúgio noutro lugar. Os habitantes das zonas costeiras são frequentemente os primeiros a ser afetados, com as suas terras a serem engolidas pelas águas ou devastadas por tempestades cada vez mais violentas.

Por exemplo, a subida do nível do mar está a por em risco a própria existência de Kiribati, constituído por muitos atóis baixos. Os efeitos das alterações climáticas, como a erosão costeira e a salinização das terras e da água doce, têm impactos devastadores na vida quotidiana dos habitantes destas ilhas. As casas são engolidas pelo mar, as culturas são danificadas pela intrusão de água salgada e o acesso a água potável torna-se cada vez mais difícil. Alguns habitantes já tomaram medidas para emigrar, antecipando os desafios crescentes colocados pelas alterações climáticas[1].
_[1] AR6 WG2 6.2.3.3 p941 (p929)_
