---
title: Population displacement
backDescription: >-
  Imagine that you live in a place that has been miraculously spared by climate
  change. Several billions of human beings might want to share this space with
  you.
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_39_climate_refugees
youtubeCode: KhWuuoK_-BI
instagramCode: ''
---

