---
title: Klimatske izbjeglice
backDescription: >-
  Zamislite da živite na nekom mjestu koje je nekim čudom pošteđeno klimatskih
  promjena. Nekoliko milijardi ljudi vjerojatno će htjeti podijeliti to mjesto s
  vama.
---

