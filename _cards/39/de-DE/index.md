---
title: Klimaflüchtlinge
backDescription: >-
  Stellen Sie sich vor, Sie leben an einem Ort, der auf wundersame Weise vom
  Klimawandel verschont geblieben ist. Mehrere Milliarden Menschen möchten
  diesen Raum nun mit Ihnen teilen...
---

