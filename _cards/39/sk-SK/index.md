---
title: Klimatickí utečenci
backDescription: >-
  Predstavte si, že žijete na mieste, ktoré bolo zázračne ušetrené od
  klimatických zmien. Je možné, že niekoľko miliárd ľudí bude chcieť s vami
  zdieľať tento priestor!
---

