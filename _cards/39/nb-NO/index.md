---
title: Klimaflyktninger
backDescription: >-
  Tenk deg at du bor på et sted som mirakuløst har blitt spart fra
  klimaendringene. Flere milliarder mennesker vil kanskje dele dette stedet med
  deg.
---
Virkningene av klimaendringer, som ødeleggende orkaner, ekstreme hetebølger, stigende havnivå og ørkenspredning, er drivkrefter som tvinger samfunn til å søke tilflukt andre steder. Innbyggere i kystområder er ofte de første som rammes, da deres land blir oversvømt av vann eller herjet av stadig mer kraftige stormer.

For eksempel truer stigende havnivå selve eksistensen til Kiribati, som består av mange lave atoller. Effektene av klimaendringer, som kysterosjon og salinisering av land og ferskvann, har ødeleggende virkninger på hverdagen til innbyggerne på disse øyene. Hus blir oversvømt av havet, avlinger blir skadet av saltvannsinntrenging, og tilgangen til drikkevann blir stadig dårligere. Noen innbyggere har allerede tatt skritt for å emigrere, i påvente av de økende utfordringene som klimaendringene medfører[1].
_[1] AR6 WG2 6.2.3.3 s941 (s929)_
