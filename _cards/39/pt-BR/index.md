---
title: Refugiados climáticos
backDescription: >-
  Imagine que você vive em um lugar que foi milagrosamente poupado pela mudança
  climática. Vários bilhões de seres humanos podem querer compartilhá-lo com
  você.
---

