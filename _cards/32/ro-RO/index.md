---
title: Scăderea randamentelor agricole
backDescription: >-
  Producția agricolă poate fi afectată de temperatură, secetă, fenomene
  meteorologice extreme, inundații şi submersiuni (de exemplu: Delta Nilului).
---

