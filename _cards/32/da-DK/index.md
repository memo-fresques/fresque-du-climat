---
title: Faldende fødevareforsyninger
backDescription: >-
  Fødevareproduktionen påvirkes af temperaturstigning, tørke, hyppigere ekstremt
  vejr og oversvømmelser fra floder eller i kystnære områder.
---

