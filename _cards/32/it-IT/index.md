---
title: Calo delle rese agricole
backDescription: >-
  La produzione agricola può essere influenzata dalla temperatura, la siccità,
  gli eventi estremi, le esondazioni e le sommersioni (ad esempio: Delta del
  Nilo).
---
Questa è una delle più grandi minacce per l'umanità. Temperature, risorse idriche, eventi climatici estremi, perdita di biodiversità sono tutte possibili cause di una diminuzione della resa agricola. Una pressione sulle risorse alimentari può essere fonte di tensioni e contribuire all'inizio di conflitti come in Ruanda (1994) o in Siria (2011)[1]. 
_[1] AR6 WG2 TS.B.2.3 p14 (p48) // AR6 WG2 Figura TS.3 (b) p12 (p46) // AR6 WG2 Tabella 4.4 AR5 WG2 Figura SPM.2 p8 p19_
