---
title: Spadek plonów rolnych
backDescription: >-
  Na produkcję rolną wpływają zmiany temperatury, susze, zjawiska ekstremalne,
  powodzie i zalania terenów nadmorskich (np. delta Nilu).
---
To jedno z największych zagrożeń dla ludzkości. Spadek plonów w rolnictwie doprowadził już do konfliktów w Rwandzie i Syrii.
