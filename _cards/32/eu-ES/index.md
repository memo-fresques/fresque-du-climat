---
title: Laborantzako etekinen apaltzea
backDescription: >-
  Tenperaturek, idorteek, muturreko gertakariek, uholdeek eta lurren
  urperatzeek, laborantza ekoizpena kaltetu dezake (ad. Niloren delta).
---

