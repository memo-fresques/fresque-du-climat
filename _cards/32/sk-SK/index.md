---
title: Pokles poľnohospodárskej úrody
backDescription: >-
  Produkcia potravín môže byť ovplyvnená teplotou, suchami, extrémnymi výkyvmi
  počasia, záplavami a zatopením pobrežných oblastí (napr. Delta Nílu).
---

