---
title: Minskade skördar
backDescription: >-
  Matproduktion kan påverkas av temperaturförändringar, torka, extremväder,
  översvämningar och kustöversvämningar (t ex i Nildeltat).
---
Detta är ett av de största hoten mot mänskligheten. Temperatur, vattenresurser, extrema klimathändelser och förlust av biologisk mångfald bidrar till minskad jordbruksavkastning. Konkurrens om livsmedelsresurser kan vara en källa till spänningar och bidra till konflikter som t ex i Rwanda (1994) eller Syrien (2011)[1].  
_[1] AR6 WG2 TS.B.2.3 p14 (p48) // AR6 WG2 Figure TS.3 (b) p12 (p46) // AR6 WG2 Table 4.4 AR5 WG2 Figure SPM.2 p8 p19_
