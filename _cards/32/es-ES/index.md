---
title: Disminución de la producción agrícola
backDescription: >-
  La producción agrícola puede verse afectada por las variaciones de
  temperatura, las sequías, los fenómenos extremos, las riadas y las sumersiones
  marinas (por ejemplo, el delta del Nilo).
---

