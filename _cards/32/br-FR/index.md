---
backDescription: >-
  La production agricole peut être affectée par la température, les sécheresses,
  les évènements extrêmes, les inondations et les submersions (ex : delta du
  Nil).
instagramCode: ''
title: Digresk efedusted al labour-douar
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_32_baisse_rendements_agricoles
youtubeCode: nP2YWbNHTsM
---

