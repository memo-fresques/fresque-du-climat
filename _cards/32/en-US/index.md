---
title: Decline in Agricultural Yields
backDescription: >-
  Food production can be affected by temperature, droughts, extreme weather
  events, floods and coastal flooding (e.g. the Nile Delta).
---

