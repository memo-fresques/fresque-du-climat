---
title: Mezőgazdasági hozam csökkenése
backDescription: >-
  Az élelmiszertermelésre hatással lehet a hőmérséklet, az aszályok, a
  szélsőséges időjárási események, árvizek és a tengeri áradás (pl. a Nílus
  Delta).
---

