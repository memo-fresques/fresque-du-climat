---
title: Nedgang i avlingsutbyttet
backDescription: >-
  Matproduksjon kan påvirkes av temperatur, tørke, ekstremvær, flom og stormflo
  (f.eks. Nildeltaet).
---
Dette er en av de største truslene mot menneskeheten. Temperaturer, vannressurser, ekstreme klimahendelser, tap av biologisk mangfold: alle disse er mulige årsaker til nedganger i avlingsutbytte. Et press på matressursene kan være en kilde til spenning og bidra til å starte konflikter som i Rwanda (1994) eller Syria (2011)[1].
_[1] AR6 WG2 TS.B.2.3 s14 (s48) // AR6 WG2 Figur TS.3 (b) s12 (s46) // AR6 WG2 Tabell 4.4 AR5 WG2 Figur SPM.2 s8 s19_
