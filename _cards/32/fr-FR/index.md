---
backDescription: >-
  La production agricole peut être affectée par la température, les sécheresses,
  les évènements extrêmes, les inondations et les submersions (ex : delta du
  Nil).
title: Baisse des rendements agricoles
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_32_baisse_rendements_agricoles
youtubeCode: nP2YWbNHTsM
---
Voici l'une des plus grosses menaces pour l'humanité. Températures, ressources en eau, évènements climatiques extrêmes, perte de biodiversité, autant de cause possible d'une baisse de rendement agricole. Une pression sur les ressources alimentaires peut être une source de tension et participer à l'initiation de conflits comme au Rwanda (1994) ou en Syrie (2011)[1].  
_[1] AR6 WG2 TS.B.2.3 p14 (p48) // AR6 WG2 Figure TS.3 (b) p12 (p46) // AR6 WG2 Table 4.4 AR5 WG2 Figure SPM.2 p8 p19_
