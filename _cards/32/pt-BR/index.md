---
title: Declínio da produção agrícola
backDescription: >-
  A produção de alimentos pode ser afetada pela temperatura, pelas secas, pelos
  eventos climáticos extremos, pelas inundações e submersões marinha (por
  exemplo, o Delta do Nilo).
---

