---
title: Declínio da produção agrícola
backDescription: >-
  A produção de alimentos pode ser afetada pela temperatura, secas, eventos
  climáticos extremos, inundações e submersão marinha (por exemplo, o Delta do
  Nilo).
---
Esta é uma das maiores ameaças para a humanidade. Temperaturas, recursos hídricos, eventos climáticos extremos, perda de biodiversidade, todas estas são possíveis causas para o Declínio da produção agrícola. A pressão sobre os recursos alimentares pode ser uma fonte de tensão e contribuir para o início de Conflitos Armados como no Ruanda (1994) ou na Síria (2011)[1].  
_[1] AR6 WG2 TS.B.2.3 p14 (p48) // AR6 WG2 Figura TS.3 (b) p12 (p46) // AR6 WG2 Tabela 4.4 AR5 WG2 Figura SPM.2 p8 p19_
