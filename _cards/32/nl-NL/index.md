---
title: Daling van de landbouwopbrengsten
backDescription: >-
  Veranderingen in temperatuur, droogtes, extreme weersomstandigheden en
  overstromingen hebben allemaal gevolgen voor de landbouwproductie (bijv. de
  Nijldelta).
---
Dit is een van de grootste bedreigingen voor de mensheid. Verminderde landbouwopbrengsten hebben al tot conflicten geleid in Rwanda en Syrië.
