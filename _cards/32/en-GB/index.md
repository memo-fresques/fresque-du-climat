---
title: Decline in Agricultural Yields
backDescription: >-
  Food production can be affected by temperature, droughts, extreme weather
  events, floods and marine submersion (e.g. the Nile Delta).
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_32_decline_agricultural_yields
youtubeCode: RkVjxqMTQvE
instagramCode: ''
---

This is one of the greatest threats to humanity. Declines in agricultural yields have already led to conflicts in Rwanda and Syria.
