---
title: Indlandsisen smelter
backDescription: >-
  Indlandsis findes på Antarktis og i Grønland. Hvis al denne is smeltede, ville
  det få vandstanden i verdenshavene til at stige med ca. 7 m. for Grønland og
  54 m. for Antarktis. Til sammenligning var mængden af indlandsis under sidste
  istid så stor, at van
---

