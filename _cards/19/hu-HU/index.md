---
title: Jégtakaró olvadása
backDescription: >-
  Szárazföldi jég (vagy jégtakaró) Grönlandon és az Antarktiszon található. Ha
  teljesen elolvadnak, Grönland 7 méterrel, az Antarktisz 54 méterrel emelné meg
  az óceán vízszintjét.
---

