---
backDescription: >-
  Les calottes glaciaires sont le Groënland et l'Antarctique. Si elles fondaient
  intégralement, cela représenterait une augmentation du niveau de la mer de 7m
  pour le Groënland, et de 54m pour l'Antarctique. Durant la dernière ère
  glaciaire, les calottes ét
instagramCode: CPqYtRrI2A-
title: Teuz an togennoù skorn
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_19_fonte_des_calottes_glaciaires
youtubeCode: Btoz2BU9PFo
---

