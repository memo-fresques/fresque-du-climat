---
title: Izotz-kaskoen urtzea
backDescription: >-
  Groenlandia eta Antartika izotz-kaskoak dira. Osoki urtzen balira, itsas maila
  7 metro igoko litzateke Groenlandian, eta 54 metro Antartikan. Azken
  glaziazioan, izotz-kaskoak hain ziren handiak non itsas maila 120 metro gaur
  baino apalago baitzen.
---

