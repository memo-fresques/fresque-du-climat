---
title: Derretimento das calotas polares
backDescription: >-
  Os calotes glaciares situam-se na Gronelândia e na Antártida. Se derretessem
  por completo, provocariam um aumento nos níveis dos oceanos de 7 metros para a
  Gronelândia e 54 metros para a Antártida. Durante a última era glacial, os
  calotes eram tão grandes
---
"As calotes polares, também chamadas de "Inlandsis" ou mantos de gelo, são extensões de gelo com mais de 50 000km²[^1].Estas ilustrações representam o ganho ou a perda de massa das calotes, os iso-contornos representam centímetros de água por ano (cm de água/ano) e são medidos por gravimetria. A azul, o ganho de massa (porque neva mais) e a vermelho as perdas (os glaciares fluem mais rapidamente para o oceano).

A espessura das calotes é da ordem de 3000m[^2].  

[^1]: _https://www.ipcc.ch/site/assets/uploads/2018/02/AR5_WGII_glossary_FR.pdf // https://fr.wikipedia.org/wiki/Inlandsis_
[^2]: _AR6 WG1 TS.2.5 p94 (p77) // AR5 WG1 Figure 4.13 p363 (p347) // AR5 WG1 Figure 4.14 p364 (p348)_
