---
title: Melting Ice Sheets
backDescription: >-
  Greenland and Antarctica are ice sheets (or continental glaciers). If they
  were to completely melt, they will cause the sea level to rise by 7 metres for
  Greenland and 54 metres for Antarctica. During the last ice age, ice sheets
  were so much larger that
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_19_melting_of_ice_sheets
youtubeCode: J-vm_DSJRjc
instagramCode: CPqTvcoIabh
---

These illustrations represent the gain or loss of mass of the caps, indicated in centimetres of water per year (cm of water/year) and measured gravimetrically. In blue the mass gain (because it snows more) and in red the losses (glaciers flow faster towards the ocean).
