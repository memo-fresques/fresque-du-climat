---
title: Smältning av inlandsis
backDescription: >-
  Inlandsis finns på Grönland och på Antarktis. Om de skulle smälta helt och
  hållet skulle de orsaka en höjning av havsnivån med 7 meter för Grönland och
  54 meter för Antarktis. Under den senaste istiden var inlandsisarna så stora
  att havsnivån var 120 m lägre än idag.
---
Inlandsisar är isområden på mer än 50 000 km²[^1]. Bilderna visar hur inlandsisarna (Grönland och Antarktis) ökar eller minskar. Konturlinjerna indikerar centimeter vatten per år (cm vatten/år) och är uppmätta med gravimetri. Blåa områden visar massökning (på grund av ökad mängd snö) och röda visar minskad massa (här rör sig glaciärerna snabbare mot havet än i andra områden).

Inlandsisarna är cirka 3000 m tjocka [^2].  

[^1]: _https://www.ipcc.ch/site/assets/uploads/2018/02/AR5_WGII_glossary_FR.pdf // https://sv.wikipedia.org/wiki/Inlandsis
[^2]: _AR6 WG1 TS.2.5 p94 (p77) // AR5 WG1 Figure 4.13 p363 (p347) // AR5 WG1 Figure 4.14 p364 (p348)_
