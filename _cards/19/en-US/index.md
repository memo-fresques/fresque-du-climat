---
title: Melting of Ice Caps
backDescription: >-
  Continental glaciers (or ice sheets) are in Greenland and Antarctica. If they
  were to melt completely, they would cause the ocean to rise by 23 feet (7
  meters) for Greenland and 177 feet (54 meters) for Antarctica. During the last
  ice age, ice caps were
---

