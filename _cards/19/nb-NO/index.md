---
title: Smelting av innlandsis
backDescription: >-
  Innlandsis er isbreer som dekker et større landområde. I dag er det kun
  Grønland og Antarktis som er dekket av innlandsis. Om de skulle smelte helt
  ville det føre til en havnivåstigning på 7 meter for Grønland og 54 meter for
  Antarktis. I den siste istide
---
"Iskappene, også kalt innlandsis eller isdekker, er isområder på mer enn 50 000 km²[^1].Disse illustrasjonene viser masseøkning eller -tap av iskappene, isokonturene representerer centimeter vann per år (cm vann/år) og målt ved gravimetri. I blått masseøkning (fordi det snør mer) og i rødt tap (isbreene flyter raskere mot havet).

Tykkelsen på iskappene er rundt 3000 m[^2].  

[^1]: _https://www.ipcc.ch/site/assets/uploads/2018/02/AR5_WGII_glossary_FR.pdf // https://fr.wikipedia.org/wiki/Inlandsis_
[^2]: _AR6 WG1 TS.2.5 p94 (p77) // AR5 WG1 Figure 4.13 p363 (p347) // AR5 WG1 Figure 4.14 p364 (p348)_
