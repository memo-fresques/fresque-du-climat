---
title: Topljenje polarnih ledenih kapa
backDescription: >-
  Grenland i Antarktik su polarne ledene kape (dakle, kopneni ledenjaci). Kada
  bi se potpuno otopile, razina mora bi porasla za 7 metara (za Grenland)
  odnosno za 54 metra (za Antarktiku). Tijekom posljednjeg ledenog doba, bile su
  toliko velike da je razina
---

