---
title: Topnienie lądolodów
backDescription: >-
  Lądolody to pokrywy lodowe na Grenlandii i Antarktydzie. Gdyby całość ich masy
  uległa stopnieniu, poziom wód podniósłby się o 7 m w przypadku Grenlandii i o
  54 m w przypadku Antarktydy. W czasie ostatniego zlodowacenia obszar pokryty
  lodem był tak rozległy, że poziom wód znajdował się o 120m niżej niż obecnie.
---
Ilustracje przedstawiają przyrost lub utratę masy czap lodowych, wyrażoną w centymetrach wody na rok (cm wody/rok) i mierzoną grawimetrycznie. Na niebiesko przyrost masy (ponieważ pada więcej śniegu), a na czerwono straty (lodowce płyną szybciej w kierunku oceanu).
