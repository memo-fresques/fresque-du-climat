---
title: Derretimento das calotas polares
backDescription: >-
  As calotas polares estão na Groenlândia e na Antártica. Se elas derreterem
  completamente, o nível do mar aumentaria em 7 metros na Groenlândia e em 54
  metros na Antártica. Durante a última era glacial, as calotas polares eram tão
  grandes que o nível do ma
---

