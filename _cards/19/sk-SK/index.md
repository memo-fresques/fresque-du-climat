---
title: Topenie pevninských ľadovcov
backDescription: >-
  Pevninské ľadovce (alebo ľadovcové štíty) sa nachádzajú v Grónsku a
  Antarktíde. Ak sa úplne roztopia, spôsobia zvýšenie hladiny oceánu o 7m pre
  Grónsko a 54m pre Antarktídu. Počas poslednej doby ľadovej, pevninské ľadovce
  boli tak rozsiahle, že hladina mo
---

