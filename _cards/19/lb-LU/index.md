---
title: Schmëlze vum den Äiskappen
backDescription: >-
  D'Äiskappen sinn a Grönland an an der Antarktis. Wann se ganz géife schmëlzen,
  da géif de Mieresspigel 7 m eropgoen fir Grönland a 54 m fir d'Antarktis. An
  der leschter Äiszäit waren d'Äiskappen esou déck, dass de Mieresspigel 120 m
  méi niddreg war wéi e
---

