---
title: fitsonik'ireo ranomandry ety ambonin'ny tany
backDescription: >-
  Ireo ranomandry ety ambonin'ny tany amin'ny ankapobeny dia i "Groënland" sy
  "l'Antarctique". Raha mitsonoka tanteraka izy ireo dia mahatonga fiakaran'ny
  ranomasina 7m raha i "Groënland ary 54m kosa raha "l'Antarctique". Nandritra
  izay vanim-potoana farany
---

