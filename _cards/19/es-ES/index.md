---
title: Deshielo de casquetes polares
backDescription: >-
  Los casquetes polares están en Groenlandia y en la Antártida. Si se
  derritieran totalmente, representaría una subida del nivel del mar de 7m para
  Groenlandia y 54m para la Antártida. Durante el último periodo glacial, los
  casquetes polares llegaron a una
---

