---
title: Otapanje ledenih pokrivača
backDescription: >-
  Kontinentalni lednici (ledeni pokrivači) se nalaze na Grenlandu i Antarktiku.
  Ako bi se oni potpuno otopili, usledio bi porast nivoa okeana od 7 metara, u
  slučaju Grenlanda, i 54 metra, u slučaju Antarktika. Tokom poslednjeg ledenog
  doba, ledeni pokrivači
---

