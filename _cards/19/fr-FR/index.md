---
backDescription: >-
  Les calottes glaciaires sont le Groënland et l'Antarctique. Si elles fondaient
  intégralement, cela représenterait une augmentation du niveau de la mer de 7m
  pour le Groënland, et de 54m pour l'Antarctique. Durant la dernière ère
  glaciaire, les calottes ét
title: Fonte des calottes glaciaires
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_19_fonte_des_calottes_glaciaires
youtubeCode: Btoz2BU9PFo
instagramCode: CPqYtRrI2A-
---
"Les calottes glaciaires, plutôt appelées Inlandsis ou nappes glaciaires, sont des étendues de glaces de plus de 50 000km²[^1].Ces illustrations représentent le gain ou la perte de masse des calottes, les iso-contours représentent des centimètre d'eau par année (cm d'eau/an) et mesuré par gravimétrie. En bleu le gain de masse (parce qu'il neige davantage) et en rouge les pertes (les glaciers s'écoulent plus vite vers l'océan).

L'épaisseur des calottes est de l'ordre de 3000m[^2].  

[^1]: _https://www.ipcc.ch/site/assets/uploads/2018/02/AR5_WGII_glossary_FR.pdf // https://fr.wikipedia.org/wiki/Inlandsis_
[^2]: _AR6 WG1 TS.2.5 p94 (p77) // AR5 WG1 Figure 4.13 p363 (p347) // AR5 WG1 Figure 4.14 p364 (p348)_
