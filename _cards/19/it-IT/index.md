---
title: Fusione delle calotte glaciali
backDescription: >-
  Le calotte glaciali sono la Groenlandia e l'Antartide. Se si sciogliessero
  completamente, provocherebbero un aumento del livello d'acqua di 7 m e 54 m
  rispettivamente. Durante l'ultima era glaciale, le calotte erano così estese
  che il livello del mare era 120 m più basso rispetto a oggi.
---
Le calotte glaciali, chiamate anche calotte di ghiaccio o ghiaccio continentale, sono estensioni di ghiaccio di oltre 50.000 km² [^1]. Queste illustrazioni rappresentano il guadagno o la perdita di massa delle calotte, le isoipse rappresentano i centimetri d'acqua all'anno (cm d'acqua/anno) e sono misurati tramite gravimetria. In blu il guadagno di massa (perché nevica di più) e in rosso la perdita (i ghiacciai si muovono più velocemente verso l'oceano).

Lo spessore delle calotte è dell'ordine di 3000 m [^2].

[^1]: _https://www.ipcc.ch/report/ar5/syr/ar5-syr-glossary-english/ // https://it.wikipedia.org/wiki/Calotta_di_ghiaccio_
[^2]: _AR6 WG1 TS.2.5 p94 (p77) // AR5 WG1 Figura 4.13 p363 (p347) // AR5 WG1 Figura 4.14 p364 (p348)_
