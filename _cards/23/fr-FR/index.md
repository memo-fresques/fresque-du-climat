---
backDescription: >-
  Si le pH baisse, la formation de calcaire devient plus difficile, notamment
  pour les coquilles.
title: Problèmes de calcification
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_23_probl%C3%A8mes_de_calcification
youtubeCode: A3zQW_C7RMI
---
Suite à la dissolution du CO2 dans l'océan, celui-ci devient plus acide ce qui limite la concentration d'ions carbonates (CO32-) nécessaires à la calcification et donc à la formation de coquilles. Ce mécanisme affecte donc des organismes telles que le plancton qui est à la base de la chaîne alimentaire ou les coraux, qui abritent bon nombre d'espèces[^1].

CO2 + H2O ⇔ H2CO3 ⇔ H+ + HCO3- ⇔ 2 H+ + CO32–  
[^1]: _https://www.frontiersin.org/articles/10.3389/fmars.2021.584445/full_
