---
title: Problemas de calcificación
backDescription: >-
  Si baja el pH, la formación de carbonato cálcico se vuelve más difícil, sobre
  todo para las conchas.
---

