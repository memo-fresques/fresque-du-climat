---
title: Hindered calcification process
backDescription: >-
  When the pH drops, the formation of calcium carbonate (and more specifically,
  of calcified shells) becomes more difficult.
---

