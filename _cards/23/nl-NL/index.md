---
title: Verstoorde verkalking van schelpen en koraalriffen.
backDescription: >-
  Als water verzuurt, bevat het steeds minder calciumcarbonaat. Dit vertraagt de
  kalkvorming van koralen en schelpvormende soorten.
---
De vorming van kalksteen (verkalking) gaat via de volgende chemische reactie: Ca++ + 2HCO3- ⇔ CaCO3 + H2O + CO2. Het vereist de aanwezigheid van bicarbonaat-ionen (HCO3-). De hoeveelheid van deze ionen in water is echter afhankelijk van de zuurgraad (PH): in water zijn normaal de concentraties van kooldioxide, koolzuur, bicarbonaat-ionen en carbonaat in evenwicht: CO2 + H2O ⇔ H2CO3 ⇔ H+ + HCO3- ⇔ 2 H+ + CO32-. Toevoeging van een zuur verschuift dit evenwicht naar de linkerkant van deze vergelijking. Met andere woorden, als de zuurgraad stijgt (dus als de PH daalt), zijn er minder bicarbonaat-ionen, en wordt het moeilijker voor organismen om hun kalksteen schelpen te maken.
