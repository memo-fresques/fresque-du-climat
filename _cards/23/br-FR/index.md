---
backDescription: >-
  Si le pH baisse, la formation de calcaire devient plus difficile, notamment
  pour les coquilles.
instagramCode: ''
title: Kudennoù razadur
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_23_probl%C3%A8mes_de_calcification
youtubeCode: A3zQW_C7RMI
---

