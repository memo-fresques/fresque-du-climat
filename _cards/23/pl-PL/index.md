---
title: Spowolnienie wapnienia
backDescription: >-
  Kiedy pH obniża się, utrudnia to powstawanie wapienia (w szczególności
  wapiennych muszli).
---
Tworzenie się wapienia (wapnienie) następuje w wyniku reakcji chemicznej Ca++ + 2HCO3- ⇔ CaCO3 + H2O + CO2. Wymaga to obecności jonów wodorowęglanowych (HCO3-). Jednak ilość tych jonów w wodzie zależy od pH: w wodzie dwutlenek węgla, kwas węglowy, jony wodorowęglanowe i jony węglanowe są w równowadze, w zależności od pH: CO2 + H2O ⇔ H2CO3 ⇔ H+ + HCO3- ⇔ 2 H+ + CO32-. Dodanie kwasu przesuwa równowagę w lewą stronę równania. Innymi słowy, jeśli pH spada, jest mniej jonów wodorowęglanowych, co utrudnia organizmom budowanie skorup.
