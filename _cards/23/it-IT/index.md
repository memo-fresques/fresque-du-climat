---
title: Problemi di calcificazione
backDescription: >-
  Se il pH diminuisce, la formazione di calcare diventa più difficile, in
  particolare per le conchiglie.
---
Come conseguenza della dissoluzione della CO2 nell'oceano, quest'ultimo diventa più acido, il che limita la concentrazione degli ioni carbonati (CO32-) necessari per la calcificazione e quindi per la formazione di gusci. Questo meccanismo quindi influisce su organismi come il plancton, che è alla base della catena alimentare, o sui coralli, che ospitano molte specie[^1].

CO2 + H2O ⇔ H2CO3 ⇔ H+ + HCO3- ⇔ 2 H+ + CO32–
[^1]: _https://www.frontiersin.org/articles/10.3389/fmars.2021.584445/full_
