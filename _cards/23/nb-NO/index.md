---
title: Forhindret forkalkning
backDescription: >-
  Når pH-verdien går ned, blir dannelsen av kalsiumkarbonat (og videre
  produksjonen av skjell) redusert.
---
Som følge av oppløsningen av CO2 i havet, blir det mer surt, noe som begrenser konsentrasjonen av karbonationer (CO32-) som er nødvendige for kalkdannelse og dermed dannelse av skjell. Denne mekanismen påvirker derfor organismer som plankton, som er grunnlaget for næringskjeder i havet, eller koraller, som huser mange arter[^1].

CO2 + H2O ⇔ H2CO3 ⇔ H+ + HCO3- ⇔ 2 H+ + CO32–  
[^1]: _https://www.frontiersin.org/articles/10.3389/fmars.2021.584445/full_
