---
title: Hindrad förkalkningsprocess
backDescription: >-
  När haven försuras (pH-värdet minskar) blir det svårare att tillverka
  kalciumkarbonat, vilket är viktigt för att bilda skal.
---
När CO2 löses i havsvattnet blir vattnet försurat (får lägre pH-värde), vilket minskar koncentrationen av karbonatjoner (CO32-) som är nödvändiga för att bilda (förkalkning) de skal många havslevande djur har. Försurningen påverkar därför organismer som plankton som utgör basen i havents näringskedja och även koraller som utgör habitat för många arter[^1].

CO2 + H2O ⇔ H2CO3 ⇔ H+ + HCO3- ⇔ 2 H+ + CO32–  
[^1]: _https://www.frontiersin.org/articles/10.3389/fmars.2021.584445/full_
