---
title: Skaldannelse forhindres
backDescription: >-
  Når havene forsures, betyder det, at der er mindre kalk i vandet. Dette bliver
  et problem for skal-dannende havdyr og plankton, der har brug for kalk til at
  danne deres skaller og skeletter.
---

