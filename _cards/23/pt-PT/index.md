---
title: Problemas de calcificação
backDescription: >-
  Se o pH baixar, a formação de exoesqueletos (ou conchas calcárias) torna-se
  mais difícil.
---
Na sequência da dissolução do CO2 no oceano, este torna-se mais ácido, o que limita a concentração de iões carbonatos (CO32-) necessários à calcificação e, portanto, à formação de conchas. Este mecanismo afeta, assim, organismos como o plâncton, que está na base da cadeia alimentar, ou os corais, que abrigam muitas espécies[^1].

CO2 + H2O ⇔ H2CO3 ⇔ H+ + HCO3- ⇔ 2 H+ + CO32–  
[^1]: _https://www.frontiersin.org/articles/10.3389/fmars.2021.584445/full_
