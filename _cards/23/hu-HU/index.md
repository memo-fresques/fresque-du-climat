---
title: Akadályozott kalcifikáció
backDescription: >-
  Amikor a pH csökken, a kálcium-karbonát (különösképpen a meszes kagylóhéjak)
  képződése nehezebbé válik.
---

