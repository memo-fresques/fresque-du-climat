---
title: Kalsiyum birikiminin engellenmesi
backDescription: >-
  pH değeri düştükçe, kalsiyum karbonat (özellikle kalsiyum karbonattan oluşan
  kabukların) oluşumu güçleşir.
---

