---
title: Spomaľovanie kalcifikácie
backDescription: >-
  Keď pH klesne, kalcifikácia (tvorba vápenca a najmä živočíšnych vápenatých
  schránok) sa sťažuje.
---

Spomaľovanie kalcifikácie
