---
title: Koncentration av koldioxid
backDescription: >-
  Ungefär hälften av våra koldioxidutsläpp fångas av naturliga kolsänkor medan
  den andra halvan stannar i atmosfären. Koncentrationen av koldioxid i luften
  har de senaste 150 åren ökat från 280 till 415 ppm (parts per million).
---
CO2-mätningar har pågått sedan 1958 på Hawaii på ön Big Island, på sluttningarna av vulkanen Mauna Loa. De startades av Charles Keeling har ge namn åt den berömda kurvan som är resultatet av hans arbete och som visar säsongsvariationerna i CO2-koncentration i atmosfären. Men trots denna säsongsvariation tog det honom bara två år att visa att den globala CO2-koncentration ökade[^1].
[^1]: _[history.aip.org/climate/co2](https://history.aip.org/climate/co2.htm#SKC_)
