---
title: CO2 koncentráció (ppm)
backDescription: >-
  Körülbelül a CO2-kibocsátás felét természetes szénelnyelők kötik meg, a másik
  fele a légkörben marad. A légköri CO2 koncentráció 280 ppm-ről (parts per
  million=az egy milliomod része) 410 ppm-re emelkedett 150 év alatt.
---

