---
title: Concentrazione di CO2 (ppm)
backDescription: >-
  Una volta che la metà delle nostre emissioni di CO2 è catturata dai pozzi
  naturali di assorbimento di carbonio, l'altra metà rimane nell'atmosfera. La
  concentrazione di CO2 è aumentata da 280 a 420 ppm (parti per milione) in 150
  anni.
---
Le prime misurazioni di CO2 sono state effettuate a partire dal 1958 alle Hawaii, sulle pendici del vulcano Mauna Loa sull'isola di Big Island. Sono state avviate da Charles Keeling, da cui prende il nome la famosa curva che mostra le variazioni stagionali della concentrazione di CO2 nell'atmosfera. Ma oltre a questa variabilità, gli sono bastati solo due anni di misurazioni per dimostrare che la tendenza globale della concentrazione di CO2 era in aumento[^1].
[^1]: _[history.aip.org/climate/co2](https://history.aip.org/climate/co2.htm#SKC_)
