---
title: 二氧化碳(CO2)濃度（ppm百萬分率）
backDescription: >-
  約有一半的二氧化碳排放量是由自然碳匯(Carbon
  Sink)吸收並儲存，另一半則留在大氣中。過去150年間，大氣中的二氧化碳濃度已由280ppm上升至410ppm
---

