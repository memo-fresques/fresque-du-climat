---
title: Concentration of CO2 in parts per million (ppm)
backDescription: >-
  About half of our CO2 emissions are captured by natural carbon sinks. The
  other half stays in the atmosphere. Concentrations of CO2 in the air have
  increased from 280 to 410 ppm (parts per million) over the past 150 years.
---

