---
title: Концентрація вуглекислого газу (ppm)
backDescription: >-
  Приблизно половина наших викидів СО2 нейтралізується природними поглиначами
  вуглецю. Друга половина залижається в атмосфері. Концентрація СО2 в атмосфері
  зросла з 280 до 410 ppm (parts per million, мільйонних часток) за останні 150
  років.
---

