---
title: Habetsaky ny CO2 (ppm)
backDescription: >-
  Rehefa voarain'ny lavaka voajanahary ny atsasaky ny entona CO2 havoakatsika,
  ny an-tsasany kosa dia mijanona eny amin'ny habakabaka. Ny habetsaky ny CO2
  eny amin'ny habakabaka dia nihoatra ny 280 ka hatramin'ny 410 ppm ("parties
  par millions") tao anatin'
---

