---
title: Koncentrácia CO2 (ppm)
backDescription: >-
  Približne polovica našich CO2 emisií je zachytená prírodnými absorbérmi
  (prepadmi) uhlíka. Druhá polovica ostáva v atmosfére. Koncentrácia CO2 v
  atmosfére sa za posledných 150 rokov zvýšila z 280 na 410 ppm (častíc na
  milión).
---

