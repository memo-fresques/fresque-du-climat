---
title: CO2-konsentrasjon (ppm)
backDescription: >-
  Rundt halvparten av våre CO2-utslipp fanges av naturlige karbonsluker. Den
  andre halvparten forblir i atmosfæren; CO2-konsentrasjonen i atmosfæren har
  økt fra 280 til 410 ppm (andeler per million) i løpet av 150 år.
---
"CO2-målinger har funnet sted siden 1958 på Hawaii, på øya Big Island, på skråningene av vulkanen Mauna Loa. De ble startet av Charles Keeling som ga sitt navn til den berømte kurven fra hans arbeid som viser sesongvariasjonene i CO2-konsentrasjonen i atmosfæren. Men i tillegg til denne variasjonen, trengte han bare to år med målinger for å bevise at den globale trenden for CO2-konsentrasjon var økende[^1].
[^1]: _[history.aip.org/climate/co2](https://history.aip.org/climate/co2.htm#SKC_)
