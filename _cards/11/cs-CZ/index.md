---
title: Koncentrace CO2 (ppm)
backDescription: >-
  Přibližně polovinu našich emisí CO2 zachycují přírodní propady uhlíku. Druhá
  polovina zůstává v atmosféře; koncentrace CO2 ve vzduchu se za 150 let zvýšila
  z 280 na 410 ppm (jednotek na milion).
---

