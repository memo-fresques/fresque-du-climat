---
backDescription: >-
  Une fois que la moitié de nos émissions _x000B_de CO2 a été captée par les
  puits naturels, l'autre moitié reste dans l'atmosphère. La concentration en
  CO2 dans l'atmosphère est passée de 280 à 415 ppm (parties par millions) en
  150 ans. Il faut remonter à trois
instagramCode: ''
title: Paoter e CO2 (lbm)
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_11_concentration_en_co2
youtubeCode: rkUfk9-kito
---

