---
title: CO2 konsantrasyonu (ppm)
backDescription: >-
  CO2 emisyonlarının yarısı doğal karbon yutakları tarafından tutulur. Diğer
  yarısı ise atmosferde kalır. Son 50 yılda CO2'nin atmosferdeki konsantrasyonu
  280 ppm'den 410 ppm'e (ppm = her milyondaki partikül miktarı) yükselmiştir.
---

