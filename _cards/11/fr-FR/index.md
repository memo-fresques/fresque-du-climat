---
backDescription: >-
  Une fois que la moitié de nos émissions de CO2 a été captée par les puits
  naturels, l'autre moitié reste dans l'atmosphère. La concentration en CO2 dans
  l'atmosphère est passée de 280 à 420 ppm (parties par millions) en 150 ans.
title: Concentration en CO2 (ppm)
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_11_concentration_en_co2
youtubeCode: rkUfk9-kito
---
"Des mesures de CO2 ont lieu depuis 1958 à Hawaï, sur l'île de Big Island, sur les flans du volcan Mauna Loa. Elles ont été lancées par Charles Keeling qui a donné son nom à la célèbre courbe issue de son travail montrant les variations saisonnières de la concentration en CO2 dans l'atmosphère. Mais en plus de cette variabilité, il ne lui a fallu que deux ans de mesures pour démontrer que la tendance globale de concentration en CO2 était à la hausse[^1].
[^1]: _[history.aip.org/climate/co2](https://history.aip.org/climate/co2.htm#SKC_)
