---
title: Concentración de CO2 (ppm)
backDescription: >-
  Una vez que la mitad de nuestras emisiones de CO2 ha sido captada por los
  sumideros naturales de carbono, la otra mitad permanece en la atmósfera. La
  concentración de CO2 atmosférico ha pasado de 280 a 410 ppm (partes por
  millón) en 150 años.
---

