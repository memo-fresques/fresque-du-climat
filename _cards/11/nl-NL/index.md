---
title: Concentratie van CO2 (ppm)
backDescription: >-
  Ongeveer de helft van de CO2 emissies wordt in diverse koolstofreservoirs
  opgeslagen. De andere helft blijft in de atmosfeer hangen. De concentratie van
  CO2 in de lucht is van 280 ppm naar 410 ppm gestegen.
---
Sinds 1958 worden CO2-metingen gedaan op Big Island in Hawaii, op de flanken van de Mauna Loa-vulkaan. Dit is geïnitieerd door Charles Keeling. In het blauwe scenario (2°C) stijgen de CO2-emissies tot 2040-2050. Daarna dalen de emissies zoveel dat de natuurlijke reservoirs ze niet meer opnemen.
