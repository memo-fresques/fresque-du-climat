---
title: CO₂ kontzentrazioa (ppm)
backDescription: >-
  Putzu naturalek CO₂ isurketen erdia xurgatu ondoan, beste erdia atmosferan
  gelditzen da. 150 urtez, atmosferan kontzentratzen den CO₂ kopurua 280
  ppm-etik (milioikoparte) 410 ppm-era emendatu da.
---

