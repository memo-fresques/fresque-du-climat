---
title: Concentração de CO2 (ppm)
backDescription: >-
  Cerca de metade das nossas emissões de CO2 são capturadas por sumidouros de
  carbono naturais. A outra metade permanece na atmosfera. A concentração de CO2
  no ar aumentou de 280 para 415 ppm (partes por milhão) ao longo dos últimos
  150 anos.
---
"As medições de CO2 têm sido realizadas desde 1958 no Havai, na ilha de Big Island, nas encostas do vulcão Mauna Loa. Foram iniciadas por Charles Keeling, que deu o seu nome à famosa curva resultante do seu trabalho, mostrando as variações sazonais da concentração de CO2 na atmosfera. Mas além desta variabilidade, bastaram-lhe apenas dois anos de medições para demonstrar que a tendência global da concentração de CO2 estava a aumentar[^1].
[^1]: _[history.aip.org/climate/co2](https://history.aip.org/climate/co2.htm#SKC_)
