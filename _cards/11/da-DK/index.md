---
title: CO2-koncentration (ppm)
backDescription: >-
  Ca. halvdelen af de menneskeskabte CO2-udledninger bliver optaget i naturlige
  kulstofpuljer. Resten bliver i atmosfæren. De sidste 150 år er koncentrationen
  af CO2 i atmosfæren steget fra 280 ppm (parts per million) til 410 ppm.
---

