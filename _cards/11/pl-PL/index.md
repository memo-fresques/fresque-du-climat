---
title: Stężenie CO2 (ppm)
backDescription: >-
  O ile połowa naszych emisji CO2 zostaje pochłonięta przez naturalne rezerwuary
  węgla, to druga połowa pozostaje w atmosferze. Stężenie CO2 w atmosferze
  wzrosło z 280 do 415 ppm (liczba części na milion) w ciągu 150 lat. To
  najwyższy poziom w ciągu ostatnich 3 milionów lat.
---
Pomiary CO2 są prowadzone od 1958 roku na Hawajach, na wyspie Hawaiʻi (Big Island), na zboczach wulkanu Mauna Loa. Zostały one zapoczątkowane przez Charlesa Keelinga. W niebieskim scenariuszu (2°C) wzrastają one do 2040-2050 r., a następnie spadają, ponieważ emisje zostaną ograniczone tak bardzo, że naturalne rezerwuary nie będą już ich absorbować.
