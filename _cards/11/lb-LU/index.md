---
title: CO2-Konzentratioun (ppm)
backDescription: >-
  Ronn d'Halschent vun eisem CO2-Ausstouss gëtt vun natierleche Kuelestoffpëtzer
  opgefaang, déi aner Halschent bleift an der Atmosphär. D'CO2-Konzentratioun an
  der Atmosphär ass a 150 Joer vun 280 op 410 ppm eropgaang (parts per million).
---

