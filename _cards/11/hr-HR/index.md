---
title: Koncentracija CO2 (ppm - čestica na milijun)
backDescription: >-
  Otprilike polovina emisija CO2 odlazi u prirodne spremnike ugljika. Preostala
  polovina ostaje u atmosferi. U proteklih 150 godina koncentracija CO2 u zraku
  povećala se s 280 na 415 ppm (čestica na milijun). To je najviša koncentracija
  CO2 u odnosu na bilo
---

