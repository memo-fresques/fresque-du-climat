---
title: CO2-Konzentration (ppm)
backDescription: >-
  Etwa die Hälfte unserer CO2-Emissionen wird von natürlichen Kohlenstoffsenken
  aufgenommen und gespeichert. Die andere Hälfte bleibt in der Atmosphäre; die
  Konzentration von CO2 in der Luft ist innerhalb von 150 Jahren von 280 auf 410
  ppm (parts per million) gestiegen.
---
Seit 1958 werden auf Hawaii, auf Big Island, CO₂-Messungen durchgeführt. Diese wurden von Charles Keeling initiiert. Im blauen Szenario (2°C) steigen die Werte bis 2040-2050 an, danach sinken sie, da die Emissionen bis dahin so stark reduziert werden, dass natürliche Senken sie nicht mehr aufnehmen.
