---
title: Koncentracija CO2 (u ppm)
backDescription: >-
  Oko polovine emisije CO2 se odstrani putem prirodnih ponora ugljenika.
  Preostala polovina ostane u atmosferi; koncentracija CO2 u vazduhu je za 150
  godina porasla sa 280 na 410 ppm.
---

