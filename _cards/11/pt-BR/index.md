---
title: Concentração de CO2 (ppm)
backDescription: >-
  Ainda que a metade de nossas emissões de CO2 seja capturada pelo processo de
  sequestro de carbono, a outra metade permanecerá na atmosfera. A concentração
  de CO2 na atmosfera aumentou de 280 para 410 ppm (partes por milhão) nos
  últimos 150 anos.
---

