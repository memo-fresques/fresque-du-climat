---
title: Concetrația de CO2 (ppm)
backDescription: >-
  Aproximativ jumătate din emisiile noastre de CO2 sunt captate de rezervoarele
  naturale de carbon. Cealaltă jumătate rămâne în atmosferă. Concentrația de CO2
  în atmosferă a crescut de la 280 la 410 ppm (părți pe milion) în 150 de ani.
---

