---
title: Rezerve de apă dulce
backDescription: >-
  Rezervele de apă dulce sunt afectate de schimbările de precipitații și de
  dispariția ghețarilor care joacă un rol important în reglarea debitului de
  apă.
---

