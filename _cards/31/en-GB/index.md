---
title: Freshwater Resources
backDescription: >-
  Freshwater resources are affected by changes in rainfall and by the melting of
  glaciers that regulate the flow of rivers.
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_31_freshwater_resources
youtubeCode: zJEORSe1RQY
instagramCode: ''
---

The biggest issue here is the disappearance of the glaciers. They serve as reservoirs of fresh water in solid form and melt to supply downstream irrigation for crops.
