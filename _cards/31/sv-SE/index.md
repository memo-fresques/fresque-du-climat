---
title: Sötvattentillgångar
backDescription: >-
  Sötvattentillgångar påverkas av förändrad nederbörd och försvinnandet av
  glaciärer, vilka spelar en reglerande roll för flödet i floder och vattendrag.
---
Huvudproblemet här är att glaciärer försvinner. De fungerar som reservoarer för sötvatten i fast form och smälter (särskilt på sommaren när det är varmt, vilket är bra!) vilket gör att man har vatten för odlingar nedströms[1].  
_[1] AR6 WG1 Box TS.6 s53 // SROCC FAQ 2.1, Figur 1 s162_
