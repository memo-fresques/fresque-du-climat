---
title: Recursos de agua dulce
backDescription: >-
  Los recursos de agua dulce se ven afectados por los cambios en las
  precipitaciones y la desaparición de los glaciares, los cuales tienen un papel
  regulador sobre el caudal de los ríos.
---

