---
title: Süßwasserressourcen
backDescription: >-
  Süßwasserressourcen werden durch Niederschlagsänderungen und dem Verschwinden
  von Gletschern beeinflusst, die bei der Regulierung der Flussläufe eine Rolle
  spielen.
---

