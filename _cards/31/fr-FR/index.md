---
backDescription: >-
  Les ressources en eau douce sont affectées par les changements de
  précipitation et la disparition des glaciers qui jouent un rôle régulateur du
  débit des cours d'eau.
title: Ressources en eau douce
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_31_ressources_eau_douce
youtubeCode: JHRnIGIebH0
---
Le gros sujet, c'est la disparition des glaciers. Ils servent de réservoirs d'eau douce sous forme solide et fondent (surtout en été, quand il fait chaud, ça tombe bien !) pour alimenter en aval l'irrigation des cultures[1].  
_[1] AR6 WG1 Box TS.6 p53 // SROCC FAQ 2.1, Figure 1 p162_
