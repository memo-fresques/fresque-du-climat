---
title: Zasoby wód słodkich
backDescription: >-
  Zasoby wód słodkich dotknięte są skutkami zmienności ilości opadów i procesu
  topnienia lodowców, które odgrywają rolę regulatora w natężeniu przepływów.
---
Największym problemem jest zanikanie lodowców. Służą one jako magazyny słodkiej wody w postaci stałej i topnieją, aby zapewnić nawadnianie upraw w dolnym biegu rzeki.
