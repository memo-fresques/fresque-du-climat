---
title: Ferskvands-ressourcer
backDescription: >-
  Ferskvand findes som floder, søer, gletsjere og i grundvandet under jorden.
  Når nedbør ændres eller landis smelter, påvirker det vores
  ferskvands-ressourcer.
---

