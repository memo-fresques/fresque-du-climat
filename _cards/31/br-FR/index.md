---
backDescription: >-
  Les ressources en eau douce sont affectées par les changements de
  précipitation et la disparition des glaciers qui jouent un rôle régulateur du
  débit des cours d'eau.
instagramCode: ''
title: Pourvezioù dour dous
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_31_ressources_eau_douce
youtubeCode: JHRnIGIebH0
---

