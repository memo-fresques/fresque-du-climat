---
title: Risorse di acqua dolce
backDescription: >-
  Le risorse di acqua dolce sono influenzate dai cambiamenti nelle
  precipitazioni e dalla scomparsa dei ghiacciai che svolgono un ruolo
  regolatore della portata dei corsi d'acqua.
---
Il grosso problema è la scomparsa dei ghiacciai. Questi fungono da serbatoi di acqua dolce in forma solida e si sciolgono (soprattutto in estate, quando fa caldo, il che va bene!) per alimentare a valle l'irrigazione dei campi[1]. 
_[1] AR6 WG1 Box TS.6 p53 // SROCC FAQ 2.1, Figura 1 p162_
