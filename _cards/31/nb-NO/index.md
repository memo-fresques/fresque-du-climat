---
title: Ferskvannsressurser
backDescription: >-
  Ferskvannsressurser påvirkes av endringer i nedbør og av at isbreer forsvinner
  (isbreer regulerer vannstrømmen i flere elver).
---
Det store temaet er isbreenes forsvinning. De fungerer som reservoarer for ferskvann i fast form og smelter (spesielt om sommeren, når det er varmt) for å forsyne vanning av avlinger nedstrøms[1].  
_[1] AR6 WG1 Box TS.6 s53 // SROCC FAQ 2.1, Figur 1 s162_
