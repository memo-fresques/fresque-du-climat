---
title: Reservas de Água Doce
backDescription: >-
  As reservas de água doce são afetadas pelas mudanças nas precipitações e pelo
  desaparecimento das geleiras que desempenham um papel regulador no fluxo dos
  rios.
---

