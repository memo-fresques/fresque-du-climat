---
title: Slatkovodni resursi
backDescription: >-
  Na slatkovodne resurse utječu promjene u količini padalina te nestanak
  kopnenih ledenjaka. Kopneni ledenjaci reguliraju riječne tokove.
---

