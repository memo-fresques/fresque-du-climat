---
title: Reservas de Água Doce
backDescription: >-
  As reservas de água doce são afetadas por mudanças na precipitação e pelo
  desaparecimento dos glaciares, que desempenham um papel regulador no fluxo dos
  cursos de água.
---
O grande problema é o desaparecimento dos glaciares. Eles servem como reservatórios de água doce na forma sólida e derretem (especialmente no verão, quando está calor, o que é conveniente!) para alimentar a irrigação das culturas a jusante[1].  
_[1] AR6 WG1 Box TS.6 p53 // SROCC FAQ 2.1, Figura 1 p162_
