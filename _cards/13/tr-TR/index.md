---
title: İlave sera etkisi
backDescription: >-
  Sera etkisi doğal bir fenomendir (aslında öncelikli sera gazı su buharıdır).
  Sera etkisi olmasaydı gezegenimiz 33°C daha soğuk olacak ve hayat bildiğimiz
  haliyle var olamayacaktı. Ancak insan aktivitesine bağlı CO2 ve diğer sera
  gazları doğal sera etkisin
---

