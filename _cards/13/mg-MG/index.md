---
title: Entona maitso fanampiny
backDescription: >-
  Voajanahary ny entona maitso. Ny entona avy amin'ny rano aza ny GES voalohany.
  Raha tsy misy ny entona maitso dia efa nangatsiaka hatrany amin'ny 33°C ny
  planeta. Fa ny CO2 sy ireo entona maitso hafa vokatry ny asa ataon'ny
  olombelona no mampiakatra ny en
---

