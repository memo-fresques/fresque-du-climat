---
title: Zusätzlicher Treibhauseffekt
backDescription: >-
  Der Treibhauseffekt ist ein natürliches Phänomen. (Das erste natürliche
  Treibhausgas ist übrigens Wasserdampf.) Ohne den Treibhauseffekt wäre der
  Planet um 33°C kälter. Aber CO2 und andere THG, die durch menschliche
  Aktivitäten entstehen, verstärken diese
---

