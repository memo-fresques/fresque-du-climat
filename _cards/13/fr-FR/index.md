---
backDescription: >-
  L'effet de serre est naturel. D’ailleurs, le premier GES naturel est la vapeur
  d’eau. Sans l'effet de serre, la planète serait 33°C plus froide. Mais le CO2
  et les autres GES dus aux activités humaines augmentent cet effet de serre
  naturel ce qui réchauffe le climat.
title: Effet de serre additionnel
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_13_effet_de_serre_additionnel
youtubeCode: M_bdkLnLQAE
instagramCode: CJwB3kSImlo
---
Sur cette carte, on voit des flèches de deux couleurs : Le orange pour l'énergie qui vient du soleil (UV, lumière visible et infrarouge de haute fréquence) et celle qui est réfléchie par effet albédo à la même fréquence. L'albédo est la capacité d'un corps à renvoyer la lumière (un corps noir a un albédo de 0, un miroir a un albédo de 1. La terre a un albédo moyen de 0,31). Le rouge pour les infrarouges de basses fréquences, émis par la terre qui est moins chaude que le soleil, ou retenus par l'effet de serre. L'effet de serre repose sur le fait que ce ne soit pas le même rayonnement entrant que le rayonnement sortant. Sur la droite, ""-18°C"", c'est la température qu'il ferait sur terre sans aucune effet de serre. "15°C", c'est la température réelle aujourd'hui. Il faisait "14°C" en 1850, c'est à dire avant que l'activité humaine ne produise cet effet de serre additionnel. En 2020, la température moyenne est d'environ 15.2°C[^1].
[^1]: _AR6 WG1 Figure 1.11 p207 (p190)_
