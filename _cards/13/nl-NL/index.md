---
title: Versterkt broeikaseffect
backDescription: >-
  Het broeikaseffect is een natuurlijk fenomeen (overigens is het belangrijkste
  BKG waterdamp). Zonder broeikaseffect zou de planeet 33°C kouder zijn en zou
  het leven zoals we dat kennen niet mogelijk zijn...Maar CO2 en andere
  broeikasgassen die verband hou
---
Op deze kaart zien we pijlen in twee kleuren. De oranje pijlen laten de energie zien die van de zon komt (uv-straling, zichtbaar licht en hoogfrequente infraroodstraling) en de straling (van diezelfde frequenties) die weerkaatst wordt van het aardoppervlak (het albedo-effect). Albedo is het vermogen van een oppervlakte om licht te reflecteren (een zwart object heeft een albedo van 0, een spiegel heeft een albedo van 1. De aarde heeft een gemiddelde albedo van 0,31). De rode pijlen vertegenwoordigen laagfrequente infraroodstraling, die door de aarde uitgezonden wordt of door het broeikaseffect vastgehouden wordt. Het broeikaseffect komt doordat er meer binnenkomende straling is dan uitgaande straling. Zonder broeikaseffect zou het -18°C zijn op aarde. De gemiddelde temperatuur op aarde is tegenwoordig 15°C. Voor 1950, dus voordat menselijke activiteit bijdroeg aan het broeikaseffect, was het 14°C.
