---
title: Effetto serra addizionale
backDescription: >-
  L'effetto serra è naturale. Infatti, il primo gas serra naturale è il vapore
  acqueo. Senza l'effetto serra, il pianeta sarebbe 33°C più freddo. Tuttavia la
  CO2 e gli altri gas a effetto serra dovuti all'uomo fanno aumentare questo
  effetto naturale riscaldando il clima.
---
Su questa carta si vedono frecce di due colori. Il colore arancione indica l'energia proveniente dal sole (UV, luce visibile e infrarossa ad alta frequenza) e quella riflessa dall'effetto albedo alla stessa frequenza. L'albedo è la capacità di un corpo di riflettere la luce (un corpo nero ha un albedo di 0, uno specchio ha un albedo di 1. La Terra ha un'albedo media di 0,31). Il colore rosso indica i raggi infrarossi a bassa frequenza, emessi dalla Terra che è meno calda del sole, o trattenuti dall'effetto serra. L'effetto serra deriva dal fatto che la radiazione in entrata e quella in uscita sono diverse. A destra, "-18°C" indica la temperatura che ci sarebbe sulla Terra senza alcun effetto serra. "15°C" è la temperatura reale di oggi. Nel 1850, ovvero prima che l'attività umana producesse questo effetto serra aggiuntivo, c'era una temperatura di "14°C". Nel 2020, la temperatura media è di circa 15,2°C [^1].
[^1]: _AR6 WG1 Figura 1.11 p207 (p190)_
