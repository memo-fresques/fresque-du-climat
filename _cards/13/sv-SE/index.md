---
title: Ytterliggare växthuseffekt
backDescription: >-
  Växthuseffekten är ett naturligt fenomen. Den viktigaste av de naturliga
  växthusgaserna är vattenånga. Utan växthusgaser skulle planeten vara 33°C
  kallare. Mänsklliga utsläpp av koldioxid och andra växthusgaser ökar denna
  naturliga effekt, viket leder till global upphettning.
---
Bilden visar pilar i två olika färger. Orange pilar representerar energi från solen (UV, synligt ljus och högfrekvent infraröd strålning) och den som reflekteras av albedoeffekten vid samma frekvens. Albedo är en kropps förmåga att reflektera ljus (en svart kropp har en albedo på 0, en spegel har en albedo på 1. Jorden har en genomsnittlig albedo på 0,31). Röda pilar visar lågfrekvent infraröd strålning som sänds ut av jorden på grund av dess temperatur. En del av denna hålls kvar av växthusgaser. Det ytterligare växthuseffekt vi upplever idag bygger på att den inkommande energin är större än den utgående energin vilket leder till successivt högre temperatur. Termometern till höger visar "-18°C"  - temperaturen det hade varit på jorden utan någon växthuseffekt alls. "15°C" är den verkliga temperaturen idag. År 1850, innan mänsklig aktivitet orsakade denna ytterligare växthuseffekt var temperaturen "14°C". År 2020 var medeltemperaturen runt 15,2°C[^1].
[^1]: _AR6 WG1 Figure 1.11 p207 (s190)_
