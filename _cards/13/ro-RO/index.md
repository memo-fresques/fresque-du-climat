---
title: Efect de seră suplimentar
backDescription: >-
  Efectul de seră este un fenomen natural (de fapt, primele GES sunt vaporii de
  apă). Fără efectul de seră, planeta ar fi cu 33 ° C mai rece. Cu toate
  acestea, CO2 și celelalte gaze cu efect de seră datorate activităţilor umană
  cresc efectul natural de seră
---

