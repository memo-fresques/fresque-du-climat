---
title: Dodatni efekt staklenika
backDescription: >-
  Efekt staklenika prirodna je pojava. Usput rečeno, najjači staklenički plin
  (GHG) je vodena para. Bez efekta staklenika, planet bi bio hladniji za 33°C, a
  život kakav poznajemo ne bi bio moguć.

  Međutim, CO2 i ostali staklenički plinovi (GHG) povezani s
---

