---
title: Efecto invernadero adicional
backDescription: >-
  El efecto invernadero es un fenómeno natural. De hecho, el principal GEI
  natural es el vapor de agua. Sin el efecto invernadero la temperatura del
  planeta sería 33°C más fría. Pero las emisiones de CO2 y los otros GEI
  derivados de las actividades humanas
---
En esta carta, vemos flechas de dos colores: las flechas naranja representan la energía que viene del sol (UV, luz visible e infrarrojo de alta frecuencia) y la energía que es reflejada por efecto albedo a la misma frecuencia. El albedo es el porcentaje de radiación que cualquier superficie refleja respecto a la radiación que incide sobre ella (un cuerpo negro tiene un albedo de 0, un espejo tiene un albedo de 1. La tierra tiene un albedo medio de 0,31). Las flechas de color rojo representan los infrarrojos de bajas frecuencias, emitidos por la tierra que se encuentra a una temperatura más baja que el sol, o los retenidos por el efecto invernadero. El efecto invernadero se basa en el hecho que la radiación entrante no es la misma que la radiación saliente. A la derecha, ""-18°C"", es la temperatura que haría en la tierra si no existiera el efecto invernadero. "15°C", es la temperatura real hoy en día. Hacía "14°C" en 1850, es decir, antes de que la actividad humana produjera este efecto invernadero adicional. En 2020, la temperatura media era de aproximadamente 15,2°C[^1].
[^1]: _AR6 WG1 Figura 1.11 p207 (p190)_
