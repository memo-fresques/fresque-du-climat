---
title: Efeito de estufa adicional
backDescription: >-
  O efeito estufa é um fenómeno natural (a propósito, o principal GEE é o vapor
  de água). Sem efeito de estufa, o planeta seria 33° C mais frio e a vida tal
  como a conhecemos não seria possível. Mas o CO2 e outros GEE inerentes às
  atividades humanas aument
---
Nesta carta, vemos setas de duas cores: Laranja para a energia que vem do sol (UV, luz visível e infravermelho de alta frequência) e aquela que é refletida pelo efeito albedo na mesma frequência. O albedo é a capacidade de um corpo de refletir a luz (um corpo negro tem um albedo de 0, um espelho tem um albedo de 1. A Terra tem um albedo médio de 0,31). Vermelho para os infravermelhos de baixa frequência, emitidos pela Terra, a qual é menos quente que o sol, ou retidos pelo efeito de estufa. O efeito de estufa baseia-se no facto de que a radiação que entra não é a mesma que a radiação que sai. À direita, ""-18°C"", é a temperatura que faria na Terra sem qualquer efeito de estufa. "15°C", é a temperatura real hoje. Era "14°C" em 1850, ou seja, antes das atividades humanas produzirem este efeito de estufa adicional. Em 2020, a temperatura média é de cerca de 15,2°C[^1].
[^1]: _AR6 WG1 Figura 1.11 p207 (p190)_
