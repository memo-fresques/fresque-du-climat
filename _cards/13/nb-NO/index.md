---
title: Forsterket drivhuseffekt
backDescription: >-
  Drivhuseffekten er en naturlig prosess, og den viktigste naturlige klimagassen
  er vanndamp. Uten drivhuseffekten ville planeten vært 33°C kaldere. Men CO2 og
  de andre klimagassene knyttet til menneskelige aktiviteter forsterker den
  naturlige drivhuseffekt
---
På dette kortet ser vi piler i to farger: oransje pil for energien som kommer fra sola (UV, synlig lys og høyfrekvent infrarød) og den som reflekteres med samme frekvens av albedoeffekten. Albedo er et mål på evnen en flate har til å reflektere lys (et svart legeme har en albedo på 0, et speil har en albedo på 1. Jorda har en gjennomsnittlig albedo på 0,31). Rød pil står for lavfrekvent infrarød utsendt av jorda, eller holdt igjen av drivhuseffekten. Årsaken til drivhuseffekten er at strålingen som går ut er ulik den som kommer inn. Til høyre, ""-18°C"", er temperaturen det ville vært på jorda uten noen drivhuseffekt. "15°C" er den faktiske temperaturen i dag. Det var "14°C" i 1850, det vil si før menneskelig aktivitet ga opphav til en ekstra drivhuseffekt. I 2020 er gjennomsnittstemperaturen omtrent 15,2°C[^1].
[^1]: _AR6 WG1 Figur 1.11 s207 (s190)_
