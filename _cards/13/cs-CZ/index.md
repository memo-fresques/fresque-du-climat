---
title: Přídavný skleníkový efekt
backDescription: >-
  Skleníkový efekt je přirozeným jevem (mimochodem největší procento
  skleníkového efektu představuje vodní pára). Bez skleníkového efektu by byla
  planeta o 33 °C chladnější a život takový, jaký ho známe, by nebyl možný. Ale
  CO2 a další skleníkové plyny sou
---

