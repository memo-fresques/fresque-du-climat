---
title: Dodatkowy efekt cieplarniany
backDescription: >-
  Efekt cieplarniany to zjawisko naturalne. Podstawowym gazem cieplarnianym jest
  para wodna. Bez efektu cieplarnianego temperatura powierzchni Ziemi byłaby o
  33°C niższa. Jednak emisje CO2 i innych gazów cieplarnianych, związane z
  działalnością człowieka, zwiększają naturalny efekt cieplarniany.
---
Na tej karcie widzimy strzałki w dwóch kolorach: pomarańczowe strzałki reprezentują energię pochodzącą ze słońca (UV, światło widzialne i podczerwień o wysokiej częstotliwości) oraz tę, która jest odbijana przez efekt albedo o tej samej częstotliwości. Albedo to zdolność ciała do odbijania światła (czarne ciało ma albedo równe 0, lustro ma albedo równe 1, Ziemia ma średnie albedo równe 0,31). Czerwone strzałki reprezentują energię podczerwoną o niskiej częstotliwości, emitowaną przez Ziemię, która jest mniej ciepła niż Słońce, lub zatrzymywaną przez efekt cieplarniany. Efekt cieplarniany opiera się na fakcie, że promieniowanie docierające nie jest takie samo, jak promieniowanie wychodzące. Po prawej stronie, -18°C to temperatura, jaką mielibyśmy na Ziemi bez efektu cieplarnianego, a 15°C to średnia temperatura planety dzisiaj. W 1850 r., czyli zanim działalność człowieka wywołała dodatkowy efekt cieplarniany, wynosiła ona 14°C.
