---
title: אפקט חממה נוסף
backDescription: >-
  אפקט החממה הוא תופעה טבעית (גז החממה העיקרי הוא אדי מים). ללא אפקט החממה, כדור
  הארץ היה קר יותר ב- 33 מעלות צלזיוס. אבל פחמן דו חמצני (CO2) וגזי חממה אחרים
  הקשורים לפעילות אנושית גורמים להגברת אפקט החממה ולכן להתחממות האקלים.
---

