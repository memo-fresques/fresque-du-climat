---
title: Berotegi-efektu gehigarria
backDescription: >-
  Berotegi-efektua naturala da. Izan ere, ur-lurruna da lehen berotegi-efektuko
  gasa. Berotegi-efekturik gabe, lurra 33 °C hotzagoa litzateke. Baina, gizakiak
  sortzen dituen CO₂-a eta berotegi-efektuko beste gasek, berotegi efektu
  naturala areagotzen dute,
---

