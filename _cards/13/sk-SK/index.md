---
title: Dodatočný skleníkový efekt
backDescription: >-
  Skleníkový efekt je prírodným fenoménom a prvým prírodným skleníkovým plynom
  je práve vodná para. Bez skleníkového efektu by bola naša planéta o 33°C
  chladnejšia. Lenže CO2 a ostatné skleníkové plyny zapríčinené ľudskou
  aktivitou zvyšujú prírodný skleníko
---

