---
title: Biodiversità marina
backDescription: >-
  Essendo gli pteropodi e i coccolitofori alla base della catena alimentare, la
  loro scomparsa minaccia tutta la biodiversità marina. La biodiversità marina è
  minacciata dall'aumento di temperatura dell'acqua e dallo sfruttamento
  eccessivo delle risorse ittiche.
---
Il cambiamento climatico è una delle 5 principali cause di perdita di biodiversità. Si possono anche citare la distruzione degli habitat, lo sfruttamento diretto, l'inquinamento e le specie invasive. La pesca eccessiva in particolare è una delle cause principali di perdita di biodiversità marina. Secondo l'IPBES, il 20% delle proteine animali mondiali proviene dalla pesca e 60 milioni di persone sono impiegate in attività di pesca o acquacoltura (2012).[1]. 
_[1] IPBES 2.1.11.1 p169 (p106) // IPBES Figura SPM.2
 p27 (p25) // https://www.nature.com/articles/s41467-022-30339-y_
