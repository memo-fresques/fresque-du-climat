---
title: Biodiversidade Marinha
backDescription: >-
  Como os pterópodes e os cococolitóforos são a base da cadeia alimentar, se
  eles desaparecerem, toda a vida nos oceanos está ameaçada. O aquecimento do
  oceano representa um importante impacto sobre a biodiversidade marinha.
---

