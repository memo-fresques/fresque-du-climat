---
title: Mořská biodiverzita
backDescription: >-
  Pteropodi a kokolitky jsou základem oceánského potravinového řetězce. Pokud
  zmizí, je ohrožena veškerá biodiverzita moří. Oteplování oceánských vod také
  ohrožuje mořskou biodiverzitu.
---

