---
title: Itsas-bioaniztasuna
backDescription: >-
  Pteropodoak eta kokolitoforoak elikakatearen hastapenean daudanez, desagertuz
  gero, itsas-bioaniztasun zinez arriskuan izan daiteke. Uraren berotzeak,
  itsas-bioaniztasunaren ahultze prozesuan zerikusi handia du ere.
---

