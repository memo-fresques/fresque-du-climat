---
title: Morská biodiverzita
backDescription: >-
  Pteropody a coccolithophoridy (vápnitý nanoplanktón) sú základom potravinového
  reťazca v oceáne. Ak vyhynú, bude preto ohrozená celá morská biodiverzita.
  Otepľovanie vody v oceáne taktiež oslabuje morskú biodiverzitu.
---

