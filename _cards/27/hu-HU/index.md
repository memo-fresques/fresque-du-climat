---
title: Tengeri biodiverzitás
backDescription: >-
  A pteropódák és kokkolitofórok az óceáni tápláléklánc legalját képezik. Ebből
  adódóan, ha eltűnnek, a tengeri biodiverzitás egésze veszélybe kerül. A tenger
  biodiverzitását veszélyezteti továbbá az óceáni vizek felmelegedése is.
---

