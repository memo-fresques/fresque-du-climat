---
title: Biodiversitatea marină
backDescription: >-
  Deoarece pteropodele și cocolitoforele se află la baza lanțului alimentar,
  dispariția lor amenință toată biodiversitatea marină. Încălzirea apei pune în
  pericol biodiversitatea marină.
---

