---
title: Morska bioraznolikost
backDescription: >-
  Pteropodi i kokolitofori temelj su morskog hranidbenog lanca. Njihovim bi
  izumiranjem čitava morska bioraznolikost bila ugrožena.

  Zagrijavanje oceanskih voda također ugrožava morsku bioraznolikost.
---

