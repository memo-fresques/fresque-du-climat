---
title: Biodiversidade Marinha
backDescription: >-
  Os pterópodes e os cocolitóforos estão na base da cadeia alimentar, pelo que o
  seu desaparecimento ameaça toda a biodiversidade marinha. O aquecimento da
  água desempenha também um papel importante no enfraquecimento da
  biodiversidade marinha.
---
As alterações climáticas são uma das 5 principais causas de perda de biodiversidade. Também podemos citar a destruição de habitats, a sobre-exploração das espécies selvagens, a poluição e introdução de espécies exóticas invasoras. A sobrepesca, em particular, é uma causa importante de perda de biodiversidade marinha. Segundo o IPBES, 20% das proteínas animais mundiais provêm da pesca e 60 milhões de pessoas trabalham na pesca ou na aquacultura (2012).[1].  
_[1] IPBES 2.1.11.1 p169 (p106) // IPBES Figura SPM.2
 p27 (p25) // https://www.nature.com/articles/s41467-022-30339-y_
