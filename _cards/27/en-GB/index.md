---
title: Marine Biodiversity
backDescription: >-
  Pteropods and coccolithophores are at the base of the ocean food chain. If
  they are driven to extinction, all marine biodiversity will be threatened.

  Warming waters and overfishing also threaten ocean life.
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_27_marine_biodiversity
youtubeCode: 5aL1RA4Q0VU
instagramCode: CPYT_Nmo0r7
---

For the moment, marine biodiversity is more endangered by overfishing than by climate change or acidification. But in the long term, these two phenomena will considerably increase their pressure. The FAO estimates that between 660 and 820 million people worldwide, about 10% of the world's population, are directly or indirectly dependent on fisheries and aquaculture.
