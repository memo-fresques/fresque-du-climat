---
title: Marine Biodiversität
backDescription: >-
  Plankton bildet die Grundlage der Nahrungskette im Ozean. Sein Verschwinden
  bedroht daher die gesamte marine Biodiversität. Auch die Erwärmung des
  Meerwassers bedroht die biologische Vielfalt der Meere.
---

