---
title: Bioróżnorodność morska
backDescription: >-
  Skrzydłonogi i kokolitofory stanowią podstawę łańcucha żywnościowego, dlatego
  ich zanikanie zagraża całej bioróżnorodności morskiej. Wzrost temperatury wody
  także zagraża morskiej bioróżnorodności.
---
W chwili obecnej morska różnorodność biologiczna jest bardziej zagrożona przez przełowienie niż przez zmiany klimatu czy zakwaszenie. Jednak w dłuższej perspektywie te dwa zjawiska znacznie zwiększą swoją presję. Organizacja Narodów Zjednoczonych do spraw Wyżywienia i Rolnictwa szacuje, że od 660 do 820 milionów ludzi na całym świecie, czyli około 10% światowej populacji, jest bezpośrednio lub pośrednio zależnych od rybołówstwa i akwakultury.
