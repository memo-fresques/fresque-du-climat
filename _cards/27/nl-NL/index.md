---
title: Mariene biodiversiteit
backDescription: >-
  Pteropoda en coccolithoforen staan aan de basis van de voedselketen in de
  oceaan. Daarom wordt, als ze verdwijnen, de hele mariene biodiversiteit
  bedreigd. De opwarming van het oceaanwater bedreigt ook de mariene
  biodiversiteit.
---
Op dit moment wordt de biodiversiteit in de zee meer bedreigd voor overbevissing dan door klimaatverandering of verzuring van de oceaan.  Maar op de lange termijn zal de druk op de diversiteit in de zee door deze twee factoren sterk toenemen. De FAO (VN Voedsel- en Landbouworganisatie) schat dat tussen de 660 en 820 miljoen mensen wereldwijd (ongeveer 10% van de wereldbevolking) direct of indirect afhankelijk zijn van visserij en aquacultuur.
