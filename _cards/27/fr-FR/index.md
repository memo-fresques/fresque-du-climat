---
backDescription: >-
  Ptéropodes et coccolithophores étant à la base de la chaîne alimentaire, leur
  disparition menace toute la biodiversité marine. La biodiversité marine est
  fragilisée par le réchauffement de l'eau ainsi que par la surexploitation des
  réserves halieutiques.
title: Biodiversité marine
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_27_biodiversit%C3%A9_marine
youtubeCode: mMMByJIVkhc
instagramCode: CPYXKmZI_lO
---
Le changement climatique est l'une des 5 principales causes de perte de biodiversité. On peut également citer la destruction des habitats, l'exploitation directe, la pollution et les espèces exotiques envahissantes. La surpêche en particulier est une cause majeure de perte de biodiversité marine. Selon l'IPBES, 20% des protéines animales mondiales proviennent de la pêche et 60 millions de personnes sont embauchées dans des pêcheries ou des aquacultures (2012).[1].  
_[1] IPBES 2.1.11.1 p169 (p106) // IPBES Figure SPM.2
 p27 (p25) // https://www.nature.com/articles/s41467-022-30339-y_
