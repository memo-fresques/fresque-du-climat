---
title: Biodiversitet i havene
backDescription: >-
  De mindste former for plankton repræsenterer de nederste led i havets
  fødekæde. Udryddelse af skal-dannende plankton truer det resterende dyreliv i
  oceanerne. Dermed er den globale opvarming af oceanerne en trussel for
  biodiversiteten.
---

