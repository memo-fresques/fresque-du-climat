---
title: Marint biologisk mangfold
backDescription: >-
  Pteropoder og kalkflagellater befinner seg nederst i havets næringkjede. Hvis
  de skulle forsvinne, vil det true alt marint biologisk mangfold. Økning i
  havtemperaturen truer også det biologiske mangfoldet i havet.
---
Klimaendringer er en av de 5 viktigste årsakene til tap av biologisk mangfold. Man kan også nevne ødeleggelse av habitater, overbeskatning, forurensning og fremmede arter. Overfiske er spesielt viktig årsak til tap av marint biologisk mangfold. Ifølge IPBES kommer 20% av verdens animalske proteiner fra fiske, og 60 millioner mennesker er ansatt i fiskerier eller akvakulturer (2012).[1].  
_[1] IPBES 2.1.11.1 s169 (s106) // IPBES Figur SPM.2
 s27 (s25) // https://www.nature.com/articles/s41467-022-30339-y_
