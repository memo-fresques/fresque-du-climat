---
title: Морско биоразнообразие
backDescription: >-
  Птероподите и коколитофорите са в основата на хранителната верига в океана.
  Ако те са доведени до изчезване, то цялото морско биоразнообразие е
  застрашено. Затоплянето на океанската вода също застрашава морското
  биоразнообразие.
---

