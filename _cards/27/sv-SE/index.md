---
title: Marin biologisk mångfald
backDescription: >-
  Rovsimsnäckor (eng: Pteropods) och coccoliter (eng: coccolithophores) utgör
  basen av havets näringskedja. Därför hotas hela den biologiska mångfalden i
  havet om de försvinner. Uppvärmningen av havet och överfiske är även direkta
  hot mot den marina biologiska mångfalden.
---
Klimatförändringar är en av de fem främsta orsakerna till förlust av biologisk mångfald. Andra framträdande faktorer är förstörda livsmiljöer (t ex avskogning), direkt exploatering, föroreningar och invasiva arter. Särskilt överfiske är en viktig orsak till förlust av marin biologisk mångfald. Enligt IPBES kommer 20 % av världens animaliska protein från fiske och 60 miljoner människor är sysselsatta inom fiske eller vattenbruk (t ex mussel- eller algodlingar) (2012).[1].  
_[1] IPBES 2.1.11.1 p169 (p106) // IPBES Figur SPM.2
 p27 (p25) // https://www.nature.com/articles/s41467-022-30339-y_ 

