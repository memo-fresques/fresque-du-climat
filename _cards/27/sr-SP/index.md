---
title: Biološka raznovrsnost mora
backDescription: >-
  Pteropode i kokolitofore su osnova lanca ishrane u okeanima. Ukoliko bi oni
  nestali, celokupan biodiverzitet mora bi bio ugrožen. Zagrevanje voda okeana
  takođe ugrožava biološku raznovrsnost mora.
---

