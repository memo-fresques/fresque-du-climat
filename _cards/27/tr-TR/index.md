---
title: Deniz biyoçeşitliliği
backDescription: >-
  Pteropodlar ve kokolittoforlar okyanustaki besin zincirinin temelini
  oluşturur. Bu nedenle, kaybolurlarsa, tüm deniz biyoçeşitliliği tehdit altında
  olur. Okyanus sularının ısınması, deniz biyoçeşitliliğini de tehdit ediyor.
---

