---
title: Biodiversidad marina
backDescription: >-
  Pterópodos y cocolitofóridos están en la base de la cadena alimentaria. Su
  desaparición amenaza toda la biodiversidad marina. El calentamiento del agua
  también tiene un papel importante en la pérdida de esta biodiversidad.
---

