---
backDescription: >-
  Ptéropodes et coccolithophores étant à la base de la chaîne alimentaire, leur
  disparition menace toute la biodiversité marine. Le réchauffement de l’eau
  joue aussi un rôle important dans la fragilisation de la biodiversité marine.
instagramCode: CPYXKmZI_lO
title: Bevliesseurted mor
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_27_biodiversit%C3%A9_marine
youtubeCode: mMMByJIVkhc
---

