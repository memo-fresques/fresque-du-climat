---
title: Gletscherschmelze
backDescription: >-
  Fast alle Gletscher haben an Masse verloren. Hunderte sind bereits
  verschwunden. Diese Gletscher haben jedoch eine regulierende Funktion bei der
  Versorgung mit Süßwasser.
---

