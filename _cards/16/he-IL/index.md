---
title: המסת קרחונים
backDescription: >-
  כמעט כל הקרחונים איבדו מסה. מאות מהם כבר נעלמו. לקרחונים אלה תפקיד מווסת כמקור
  למים מתוקים.
---

