---
title: Topenie ľadovcov
backDescription: >-
  Takmer všetky ľadovce stratili hmotu. Stovky z nich sa už úplne vytratili.
  Ľadovce hrajú úlohu regulátora ako zdroj sladkej vody.
---

