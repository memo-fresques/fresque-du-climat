---
title: Gletsjerne smelter
backDescription: >-
  Næsten hele klodens ismasse er i tilbagegang, og flere hundrede gletsjere er
  forsvundet. Gletsjerne leverer smeltevand om sommeren, og spiller dermed en
  vigtig rolle i forsyningen af ferskvand.
---

