---
title: Otapanje glečera
backDescription: >-
  Masa gotovo svih glečera se smanjila. Stotine njih je u potpunosti nestalo.
  Glečeri imaju regulatornu funkciju u obezbeđivanju slatke vode.
---

