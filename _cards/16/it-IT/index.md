---
title: Fusione dei ghiacciai
backDescription: >-
  Quasi tutti i ghiacciai hanno perso massa. Centinaia sono già scomparsi.
  Questi ghiacciai hanno un ruolo regolatore nell'approvvigionamento di acqua
  dolce.
---
Qui si parla dei ghiacciai che si trovano sulle montagne. Quasi tutti i ghiacciai hanno perso massa[1]. 
_[1] AR6 WG1 TS2.5 p44 (p76) // SROOC Figura TS.3 p8_
