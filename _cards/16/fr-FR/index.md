---
backDescription: >-
  Presque tous les glaciers ont perdu de la masse. Des centaines ont même déjà
  disparu. Or ces glaciers ont un rôle régulateur sur l'approvisionnement en eau
  douce.
title: Fonte des glaciers
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_16_fonte_des_glaciers
youtubeCode: zRlMGkImeUs
instagramCode: CMkOau-o86_
---
On parle ici des glaciers dans les montagnes. 
Presque tous les glaciers ont
perdu de la masse[1].  
_[1] AR6 WG1 TS2.5 p44 (p76) // SROOC Figure TS.3 p8_
