---
title: Smelting av isbreer
backDescription: >-
  Nesten alle isbreer har mistet masse. Hundrevis av dem har allerede
  forsvunnet. Disse breene spiller en regulerende rolle i ferskvannsforsyningen.
---
Her snakker vi om isbreene i fjellene. 
Nesten alle isbreene har
mistet masse[1].  
_[1] AR6 WG1 TS2.5 s44 (s76) // SROOC Figur TS.3 s8_
