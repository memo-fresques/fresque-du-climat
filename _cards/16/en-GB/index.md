---
title: Melting Glaciers
backDescription: >-
  Almost all glaciers have receded, and hundreds of them have already
  disappeared. Glaciers are important because they regulate and provide
  freshwater.
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_16_melting_of_glaciers
youtubeCode: LmaMIdQN_40
instagramCode: CMkPWXPHx2m
---
Here, we are talking about mountain glaciers. Technically, they are frozen water currents, and therefore have a higher viscosity (thickness) than water.
