---
title: Het smelten van gletsjers
backDescription: >-
  Vrijwel alle gletsjers trekken zich terug en honderden zijn al helemaal
  verdwenen. De gletsjers spelen een belangrijke rol als bron van drinkwater.
---
We spreken hier over berggletsjers. Dat zijn technisch gezien waterstromen die bevroren zijn, waardoor ze een hogere viscositeit (stroperigheid) hebben dan water.
