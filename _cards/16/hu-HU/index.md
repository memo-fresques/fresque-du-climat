---
title: Gleccserolvadás
backDescription: >-
  Majdnem minden gleccser veszített a tömegéből. Közülük több száz már teljesen
  eltűnt. A gleccserek szabályozó szerepet töltenek be az édesvíz
  biztosításában.
---

