---
backDescription: >-
  Presque tous les glaciers ont perdu de la masse. Des centaines ont même déjà
  disparu. Or ces glaciers ont un rôle régulateur sur l'approvisionnement en eau
  douce.
instagramCode: CMkOau-o86_
title: Teuz ar skornegoù
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_16_fonte_des_glaciers
youtubeCode: zRlMGkImeUs
---

