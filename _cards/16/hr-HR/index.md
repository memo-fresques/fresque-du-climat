---
title: Topljenje ledenjaka
backDescription: >-
  Gotovo svi su se ledenjaci povukli, a na stotine ih je već nestalo. Ledenjaci
  su izuzetno važni kao izvori i regulatori pitke vode (slatkovodnih resursa).
---

