---
title: Deshielo de los glaciares
backDescription: >-
  Casi todos los glaciares han perdido una parte de su masa. Incluso cientos de
  ellos han desaparecido. Los glaciares tienen un papel regulador como recurso
  de agua dulce.
---

