---
title: Melting of Glaciers
backDescription: >-
  Almost all glaciers have lost mass. Hundreds of them have already disappeared.
  These glaciers play a regulating role in the provision of fresh water.
---

