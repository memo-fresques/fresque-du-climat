---
title: Smältning av glaciärer
backDescription: >-
  Nästan alla glaciärer har förlorat massa, och hundratals av dem har
  försvunnit. Glaciärer spelar en viktig roll som reglerande källa för
  sötvatten.
---
Kortet handlar om bergsglaciärer (till skillnad från inlandsis?)  Nästan alla glaciärer har minskat i omfång och massa [1].  
_[1] AR6 WG1 TS2.5 p44 (p76) // SROOC Figure TS.3 p8_
