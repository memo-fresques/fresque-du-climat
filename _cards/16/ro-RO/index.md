---
title: Topirea ghețarilor
backDescription: >-
  Aproape toți ghețarii și-au pierdut din masă. Sute chiar au dispărut deja, iar
  acești ghețari au rolul de regulatori în furnizarea apei dulci pe această
  planetă.
---

