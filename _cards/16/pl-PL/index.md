---
title: Topnienie lodowców
backDescription: >-
  Prawie wszystkie lodowce straciły na objętości, a setki nawet zanikły.
  Tymczasem lodowce pełnią rolę regulatora zasobów słodkiej wody.
---
Mówimy tutaj o lodowcach górskich. Technicznie rzecz biorąc, są to zamarznięte prądy wodne, a zatem mają większą lepkość (grubość) niż woda.
