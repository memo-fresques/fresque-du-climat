---
title: Derretimento das Geleiras
backDescription: >-
  Quase todas as geleiras perderam massa. Centenas delas já desapareceram. Essas
  geleiras desempenham um papel regulador como fonte de água doce.
---

