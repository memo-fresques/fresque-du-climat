---
title: Derretimento dos glaciares
backDescription: >-
  Quase todos os glaciares perderam massa. Centenas já desapareceram. Estes
  glaciares desempenham um papel regulador no fornecimento de água doce.
---
Aqui falamos dos glaciares nas montanhas. 
Quase todos os glaciares perderam
massa[1].  
_[1] AR6 WG1 TS2.5 p44 (p76) // SROOC Figura TS.3 p8_
