---
title: Топене на ледниците
backDescription: >-
  Почти всички ледници са загубили маса. Стотици от тях са вече изчезнали.
  Ледниците са важни, тъй като имат регулираща роля и осигуряват прясна вода.
---

