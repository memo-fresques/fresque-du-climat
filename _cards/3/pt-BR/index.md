---
title: Setor da Construção Civil
backDescription: >-
  O setor de construção civil (residencial e comercial) utiliza combustíveis
  fósseis e eletricidade. Ele representa  20% das emissões de gases de efeito
  estufa (GEE).
---

