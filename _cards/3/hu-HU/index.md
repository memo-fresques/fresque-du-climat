---
title: Épülethasználat
backDescription: >-
  Az építési szektor (lakhatási és kereskedelmi használat) fosszilis- és
  villamos energia felhasználása az üvegházhatású gázok kibocsátásának 20%-áért
  felel.
---

