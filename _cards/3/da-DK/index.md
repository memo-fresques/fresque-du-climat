---
title: Boliger og byggeri
backDescription: >-
  Bolig- og byggesektoren (både erhverv og privat) bruger fossile brændstoffer
  og el. Den står for 20% af alle drivhusgasudledninger.
---

