---
title: Building Usage
backDescription: >-
  Buildings (housing and commercial use) use fossil fuels and electricity. They
  account for 20% of greenhouse gas emissions.
---

