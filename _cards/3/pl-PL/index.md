---
title: Eksploatacja budynków
backDescription: >-
  Eksploatacja budynków (mieszkalnych i usługowych) powoduje zużycie paliw
  kopalnych oraz energii elektrycznej. Odpowiada za 20% emisji gazów
  cieplarnianych.
---
Bierzemy pod uwagę użytkowanie budynków, a nie ich konstrukcję (która jest uwzględniona w karcie dotyczącej przemysłu). Obejmuje to ogrzewanie, klimatyzację, oświetlenie, elektronikę itp. Głównym tematem w Europie i USA jest izolacja termiczna budynków. Jeśli chodzi o nowe budownictwo, kluczowe znaczenie ma budowanie dobrze izolowanych budynków. Możliwości są jednak ograniczone, ponieważ standardy dla nowych budynków są znacznie wyższe niż w przeszłości, a tylko niewielka część (1%) budynków jest budowana każdego roku. Wyzwanie jest zatem znacznie większe w przypadku termomodernizacji budynków.
