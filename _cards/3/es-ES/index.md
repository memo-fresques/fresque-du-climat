---
title: Uso de los edificios
backDescription: >-
  El uso de los edificios (vivienda y uso comercial) implica la utilización de
  combustibles fósiles y electricidad. Representa el 20% de los Gases de Efecto
  Invernadero (GEI).
---

