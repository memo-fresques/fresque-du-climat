---
title: Upotreba u zgradama
backDescription: >-
  U sektoru građevine (stambena i komercijalna upotreba) se koriste fosilna
  goriva i električna energija. To objašnjava 20% emisije gasova sa efektom
  staklene bašte.
---

