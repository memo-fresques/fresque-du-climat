---
title: Utilização dos edifícios
backDescription: >-
  O setor da construção civil (habitação e uso comercial) utiliza combustíveis
  fósseis e eletricidade. Representa 20% das emissões de gases de efeito estufa
  (GEE).
---
A Utilização dos Edifícios representa 21% das emissões de gases com efeito de estufa no mundo (2019)[^1]. Falamos aqui da utilização dos edifícios e não da sua construção (a qual se insere no setor da Indústria). Aquecimento, ar condicionado, iluminação, eletrónica, etc. O grande problema, nas nossas latitudes, é o isolamento térmico dos edifícios. No que diz respeito à construção de novos edifícios, é importante construir edifícios bem isolados. Mas o desafio é limitado porque as normas para novas construções são muito mais rigorosas do que no passado e construímos apenas uma pequena parte (1%) dos edifícios a cada ano. O desafio está, portanto, muito mais na renovação térmica dos edifícios.  
[^1]: _AR6 WG3 9.1 p970 (p957) // AR6 WG3 Figure TS.6 p18 (p66)_
