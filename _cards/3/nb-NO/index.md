---
title: Bruk av bygninger
backDescription: >-
  Bygningssektoren (boliger og kommersielle bygg) bruker fossil energi og
  elektrisitet. Den står for 20 % av de totale klimagassutslippene.
---
Bruken av bygninger står for 21 % av klimagassutslippene i verden (2019)[^1]. Vi snakker her om bruken av bygninger og ikke deres konstruksjon (som faller inn under industrisektoren). Oppvarming, klimaanlegg, belysning, elektronikk, osv. Det store temaet, på våre breddegrader, er termisk isolering av bygninger. Når det gjelder nybygg, er det viktig å bygge godt isolerte bygninger. Men utfordringen er begrenset fordi standardene for nybygg er mye strengere enn før, og vi bygger hvert år bare en liten del (1 %) av den allerede eksisterende bygningsmassen. Utfordringen ligger derfor mye mer i termisk renovering av bygninger.  
[^1]: _AR6 WG3 9.1 s970 (s957) // AR6 WG3 Figur TS.6 s18 (s66)_
