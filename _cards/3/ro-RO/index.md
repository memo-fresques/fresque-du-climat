---
title: Utilizarea clădirilor
backDescription: >-
  Utilizarea clădirilor (rezidențiale și comerciale) implică utilizarea
  combustibililor fosili și a electricităţii. Aceasta este responsabilă pentru
  20% din emisiile de gaze cu efect de seră (GES).
---

Utilizarea clădirilor
