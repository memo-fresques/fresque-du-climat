---
title: Uso degli edifici
backDescription: >-
  Gli edifici (abitativi e commerciali) utilizzano combustibili fossili ed
  elettricità. Sono responsabili del 20% delle emissioni di gas serra (GHG).
---
L'utilizzo degli edifici rappresenta il 21% delle emissioni di gas serra nel mondo (2019)[^1]. Si parla qui dell'utilizzo degli edifici e non della loro costruzione (che rientra nel settore dell'industria). Riscaldamento, aria condizionata, illuminazione, elettronica, ecc. La questione principale, alle nostre latitudini, è l'isolamento termico degli edifici. Per quanto riguarda la costruzione di nuovi edifici, è importante che siano termicamente ben isolati. Ma il problema è limitato perché le norme per i nuovi edifici sono molto più restrittive rispetto al passato e ogni anno viene costruita solo una piccola parte (1%) del patrimonio già esistente. La sfida sta quindi molto più nella ristrutturazione termica degli edifici.  
[^1]: _AR6 WG3 9.1 p970 (p957) // AR6 WG3 Figura TS.6 p18 (p66)_
