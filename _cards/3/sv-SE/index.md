---
title: Byggnader
backDescription: >-
  Byggsektorn (boende och kommersiella fastigheter) använder fossila bränslen
  och elektricitet. Den uppgår till 20% av växthusgasutsläppen.
---

