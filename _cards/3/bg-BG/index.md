---
title: Сграден фонд
backDescription: >-
  Употребата на сградния фонд (за жилищни и търговски цели) изисква изкопаеми
  горива и електричество и е отговорна за 20% от емисиите на парникови газове.
---

