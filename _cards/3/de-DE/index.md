---
title: Gebäudenutzung
backDescription: >-
  Die Nutzung von Gebäuden (privates Wohnen und gewerbliche Nutzung) verbraucht
  fossile Brennstoffe und Elektrizität. Sie ist für 20 % der
  Treibhausgasemissionen verantwortlich.
---

