---
title: Gebruik van gebouwen
backDescription: >-
  Gebouwen (woningen en winkels) maken gebruik van fossiele brandstoffen en
  elektriciteit. Deze sector is verantwoordelijk voor 20% van de totale uitstoot
  van broeikasgassen (BKG).
---
Deze kaart betreft de impact van gebouwen tijdens gebruik, niet de bouw ervan (dit valt onder de kaart Industrie). Het gebruik omvat verwarming, koeling, verlichting, electronika, etc. Een belangrijk onderwerp hierbij (zeker in onze regio) is isolatie. Bij nieuwbouw is het essentieel dat gebouwen goed thermisch geïsoleerd worden. De standaarden voor nieuwe gebouwen liggen echter veel hoger dan in het verleden, en bovendien wordt elk jaar slechts 1% van gebouwen nieuw gebouwd. De uitdaging ligt daarom veel meer in het renoveren van bestaande gebouwen.
