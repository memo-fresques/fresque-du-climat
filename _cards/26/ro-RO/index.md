---
title: Inundații fluviale
backDescription: >-
  Perturbarea circuitului apei poate duce la lipsă de apă sau abundența
  acesteia. Prea multă apă poate genera inundații. Dacă solul a fost uscat
  înainte de o secetă, situația este mai gravă, deoarece apa se scurge la
  suprafață și nu este absorbită.
---

