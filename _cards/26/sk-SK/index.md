---
title: Riečne záplavy
backDescription: >-
  Narušenie vodného cyklu môže spôsobiť nárast alebo úbytok vody. Nárast vody
  môže viesť ku riečnym záplavám. Ak je pôda vysušená, situácia sa zhoršuje,
  pretože voda dobre nevsakuje a steká po povrchu.
---

