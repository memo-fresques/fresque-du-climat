---
title: Powodzie
backDescription: >-
  Zaburzenia obiegu wody w przyrodzie mogą wpływać na wzrost lub spadek poziomu
  wód. Przybór wody grozi powodziami (zatopienia lądu). Jeśli gleba została
  mocno wysuszona na skutek suszy, to pogarsza sprawę, gdyż woda nie wchłania
  się, a spływa po powierzchni.
---
Powódź to tymczasowy wzrost poziomu rzeki spowodowany opadami (lokalnymi lub w górę rzeki) lub topnieniem śniegu lub lodu.
