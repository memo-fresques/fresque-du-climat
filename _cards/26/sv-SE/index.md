---
title: Översvämningar
backDescription: >-
  Störningar av vattnets kretslopp kan leda till ökade eller minskade mängder
  vatten. Mer vatten kan leda till översvämningar av floder och vattendrag.
  Urbanisering med mer hårdgjorda ytor, eller en uttorkad jordskorpa, kan
  förvärra konsekvenserna efterson vattnet rinner av istället för att
  absorberas.
---
En översvämning är en höjning av vattennivån i vattendrag på grund av nederbörd eller smältning av snö eller is. En översvämning är tillfällig. Förväxla inte översvämning med höjda havsnivåer, även om dessa i sig kan öka risken för att kustområden tillfälligt hamnar under vatten. 
