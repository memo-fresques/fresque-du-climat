---
title: Árvizek
backDescription: >-
  A vízkörforgás zavara csökkentheti vagy növelheti a rendszerben jelen levő
  vízmennyiséget. Nagyobb vízmennyiség a folyók áradásához vezethet. Ha a talaj
  ki van száradva, a károk még nagyobbak lesznek, mivel a víz egyszerűen
  elfolyik.
---

