---
title: Povodně
backDescription: >-
  Narušení koloběhu vody může znamenat jak více vody, tak méně vody. Více vody
  může vést k záplavám. Pokud je půda vysušena dlouhodobým suchem, situaci to
  ještě zhoršuje, protože půda vodu nezadrží a ta odteče.
---

