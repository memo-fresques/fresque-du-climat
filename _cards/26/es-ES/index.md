---
title: Riadas
backDescription: >-
  La perturbación del ciclo del agua puede implicar más o menos agua. Si tenemos
  más agua, esto puede generar riadas (inundaciones en tierra). Este fenómeno se
  ve acentuado si el suelo ha sufrido una sequía, ya que el agua se escurre.
---

