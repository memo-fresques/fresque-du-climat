---
title: Ur handitzeak
backDescription: >-
  Ur zikloaren asaldurak ur gehiago edo ur gutiago ekar lezake. Ur gehiago
  denean, ur handitzeak gerta daitezke (uholdeak lurretan). Idorteak lurra
  gogortu badu, okerrago da, ura turrustan isurtzen baita.
---

