---
title: Rivieroverstromingen
backDescription: >-
  Verstoring van de waterkringloop kan een tekort of een overvloed aan water
  opleveren. Te veel water kan rivieroverstromingen veroorzaken. Een droge bodem
  maakt het probleem nog erger, omdat het water dan sneller afvloeit.
---
Een overstroming is een tijdelijke verhoging van het waterpeil van een rivier door neerslag (lokaal of stroomopwaarts) of door smeltende sneeuw of ijs. Op de wereldkaart (de hexagonale vlakken) uit het 6e rapport van de IPCC hebben de regio's met groene vlakken te maken met meer neerslag. Het aantal puntjes vertegenwoordigt de mate van betrouwbaarheid dat de getoonde verandering het resultaat is van menselijke activiteit.
