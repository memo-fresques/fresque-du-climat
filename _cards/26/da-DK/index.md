---
title: Oversvømmelser af vandløb
backDescription: >-
  Forstyrrelsen af vandets kredsløb kan føre til mere eller mindre vand. Mere
  nedbør kan føre til oversvømmelser af vandløbene. Men det kan mindre vand
  også, idet tør jord ikke kan opsuge regnen, når den endelig kommer, og derfor
  skyller den væk i stedet.
---

