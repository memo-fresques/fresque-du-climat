---
title: Riječne poplave
backDescription: >-
  Poremećaj hidrološkog ciklusa uzrokuje i porast i pad količine padalina. Više
  kiše može izazvati riječne poplave. Kada je tlo izrazito suho situacija se
  pogoršava jer ono slabije zadržava vodu.
---

