---
backDescription: >-
  La perturbation du cycle de l'eau peut amener plus d'eau ou moins d’eau. Plus
  d'eau, cela peut engendrer des crues (inondations dans les terres). Si le sol
  a été durci par une sécheresse, c'est pire car l'eau ruisselle.
title: Crues
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_26_crues'
youtubeCode: 43Hf5wnz3Wk
---
Une crue est l'élévation du niveau d'un cours d'eau due aux précipitations ou à des fontes de neige ou de glace. Une crue est temporaire. A ne pas confondre avec une submersion (l'océan).
