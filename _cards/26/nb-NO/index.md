---
title: Elveflom
backDescription: >-
  Forstyrrelser i vannets kretsløp kan føre til mer eller mindre vann. Mer vann
  kan føre til flom. Hvis jordoverflaten på forhånd har tørket ut på grunn av
  tørke, kan flommen forverres fordi vannet renner av i stedet for å trekke ned
  i jorda.
---
En flom er en økning i vannstanden i en elv på grunn av nedbør eller snø- eller is-smelting. En flom er midlertidig. Ikke forveksle med en stormflo (havet).
