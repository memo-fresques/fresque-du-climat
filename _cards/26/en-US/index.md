---
title: River Flooding
backDescription: >-
  The disruption of the water cycle can bring more water or less water. More
  water can lead to river flooding. If the soil has been dried out by a drought,
  it makes things worse because the water runs off.
---

