---
title: Inundações
backDescription: >-
  A perturbação do ciclo da água pode ocasionar mais ou menos água. Mais água,
  pode provocar inundações. A gravidade da situação é maior se o solo estiver
  endurecido pela seca, pois a água escorre.
---
Uma inundação é o aumento do nível de um curso de água devido à precipitação ou derretimento de neve ou gelo. Uma inundação é temporária. Não confundir com Submersão marinha (o oceano).
