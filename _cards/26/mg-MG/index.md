---
title: Tondra-drano
backDescription: >-
  Ny fikorotanan'ny tsingerin'ny rano dia mety mahatonga fiakaran'ny rano na
  mampihena azy. Tondraka ny rano rehefa betsaka. Raha lasa mafy be ny tany
  vokatry ny fahamainana dia lasa mikoriana fotsiny ny rano ka izay no tena
  zava-doza.
---

