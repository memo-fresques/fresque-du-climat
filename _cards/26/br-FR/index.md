---
backDescription: >-
  La perturbation du cycle de l'eau peut amener plus d'eau ou moins d’eau. Plus
  d'eau, cela peut engendrer des crues (inondations dans les terres). Si le sol
  a été durci par une sécheresse, c'est pire car l'eau ruisselle.
instagramCode: ''
title: Dourgresk
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_26_crues'
youtubeCode: 43Hf5wnz3Wk
---

