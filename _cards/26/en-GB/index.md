---
title: River Flooding
backDescription: >-
  The disruption of the water cycle can both increase and decrease rainfall.
  More rain can lead to river flooding. If the soil is very dry, it makes
  matters worse because the water runs off it.
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_26_river_flooding
youtubeCode: EgCZs3oqwos
instagramCode: ''
---

A flood is the temporary rise in the level of a river due to precipitation (whether local or upstream) or melting snow or ice.
