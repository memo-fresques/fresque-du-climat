---
title: Hochwasser
backDescription: >-
  Die Störung des Wasserkreislaufs kann in "zu viel" oder "zu wenig" Wasser
  resultieren. Mehr Wasser kann Flussüberschwemmungen zur Folge haben. Ist der
  Boden durch eine Dürre verhärtet, ist dies besonders schlimm, da das Wasser
  einfach abrinnt anstatt einz
---

