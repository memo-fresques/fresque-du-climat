---
title: Seller
backDescription: >-
  Su döngüsünün bozulması daha fazla veya daha az suya neden olabilir. Daha
  fazla su nehir taşmasına neden olur. Eğer topraklar kurak ise, durum daha da
  kötüdür çünkü su emilmeden hızlı bir şekilde akar.
---

