---
title: Esondazioni
backDescription: >-
  Un ciclo idrico perturbato può portare a scarsità o abbondanza d'acqua. Più
  acqua può generare esondazioni. Con l'urbanizzazione, o se il suolo è stato
  precedentemente inaridito dalla siccità, la situazione è peggiore perché
  l'acqua scorre in superficie.
---
Un'esondazione è l'aumento del livello di un corso d'acqua dovuto alle precipitazioni o allo scioglimento di neve o ghiaccio. Un'esondazione è temporanea. Da non confondere con una sommersione (oceano).
