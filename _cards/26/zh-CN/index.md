---
title: 洪水
backDescription: ' 水循环的扰动会造成水量的增减。 水量增加可能导致洪水（陆地洪水泛滥）。 如果土壤由于干旱而变硬，则更糟，因为水会不停流淌。'
---

