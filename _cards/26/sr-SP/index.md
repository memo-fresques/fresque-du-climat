---
title: Plavljenje reka
backDescription: >-
  Prekid hidrološkog procesa može dovesti do manjka ili viška vode. Višak vode
  može dovesti do plavljenja reka. Ukoliko je tlo isušeno sušom, situacija je
  još gora jer voda otiče.
---

