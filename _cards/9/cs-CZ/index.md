---
title: Ostatní skleníkové plyny
backDescription: >-
  CO2 není jediným skleníkovým plynem. Kromě dalších jsou to metan (CH4) a oxid
  dusný (N2O), dva plyny emitované převážně zemědělskými činnostmi.
---

