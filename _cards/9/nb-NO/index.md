---
title: Andre klimagasser
backDescription: >-
  CO2 er ikke den eneste klimagassen. Blant andre er metan (CH4) og
  nitrogenoksid (N2O), to gasser som hovedsakelig avgis fra landbruket.
---
De andre klimagassene beskrevet her er metan og dinitrogenoksid (ofte kalt lystgass). Generelt er en klimagass en gass som har evnen til å absorbere infrarød stråling utsendt av jorden. Det er gasser som har tre eller flere atomer per molekyl, eller to hvis  atomene er ulike (O2, N2... er derfor ikke klimagasser). De viktigste klimagassene: H2O, CO2, CH4, N2O, O3 (troposfærisk ozon), HKFK (som freon), KFK, HFK, CF4, SF6, CF3-SF5[^1]. Fluorgassene (de som inneholder bokstaven F) brukes som kjølemidler (klimaanlegg og kjølekjeder), brannslukkere og i visse industrielle prosesser og forbruksvarer (som noen løsemidler). De forekommer ikke naturlig i atmosfæren. Det frigjøres metan (CH4) under anaerob nedbrytning (det vil si nedbrytning uten oksygen): i vommen til kyr (i vommen ""fordøyer"" bakteriene cellulose som kua ikke kan bryte ned, deretter gulper kua opp dette gresset for å tygge det og svelge det for godt); i rismarker fordi de er dekket av vann, og det organiske materialet under overflaten har ikke tilgang på oksygen når det brytes ned; på søppelfyllinger, når haugene er for store til at oksygen kan nå bunnen av haugen. Metan er også hovedkomponenten i naturgass. Lekkasjer på gassrørledninger slipper derfor ut metan. De viktigste kildene til menneskeskapte utslipp av metan er (tall fra 2008-2017): landbruk og avfall (58%) (inkludert enterisk gjæring og gjødsel (31%), søppelfyllinger (18%), ris (9%)), fossile ressurser (32%) og forbrenning av biomasse og biodrivstoff (8%)[^2]. Lystgass (N2O) skyldes hovedsakelig bruk av nitrogenholdig gjødsel i landbruket, produksjon av dyrefôr og visse kjemiske prosesser, som produksjon av salpetersyre[^3].
[^1]: _AR6 7SM Table 7.SM.7 p16_
[^2]: _AR6 WG1 Table 5.2 : methane budget p720 (p703)_ // AR6 WG1 Figure 5.14 : methane budget p722 (p705)_
[^3]: _AR6 WG1 Table 5.3 : sources N2O 
p729 (p712) // AR6 WG1 Figure 5.17 : Budget N2O p728 (p711)_
