---
title: Andere Treibhausgase
backDescription: >-
  CO2 ist nicht das einzige Treibhausgas. Unter anderem gibt es Methan (CH4) und
  Distickstoffmonoxid (N2O), die beide hauptsächlich durch landwirtschaftliche
  Tätigkeiten entstehen.
---

