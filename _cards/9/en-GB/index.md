---
title: Other GHG Emissions
backDescription: >-
  CO2 is not the only greenhouse gas (GHG). Among others are methane (CH4) and
  nitrous oxide (N2O), both of which mainly come from agricultural activities.
wikiUrl: 'https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_9_other_ghgs'
youtubeCode: LN7lPIUWPuQ
instagramCode: ''
---

The other GHGs described here are methane and nitrous oxide. In fact, there are a few others such as HFCs (refrigerants). Methane is released as soon as there is anaerobic decomposition (i.e. in the absence of oxygen): in a cow’s belly, also known as the rumen, which gives its name to ruminants (in the rumen, bacteria digest the cellulose that the cow cannot metabolise, then the cow regurgitates this grass to chew it again and swallow it for good); in rice fields because they are covered with water, and the organic matter underwater does not receive oxygen when it decomposes; in waste dumps, when the piles are too deep for oxygen to reach the bottom of the pile. Methane is also the main component of natural gas. Leaks on gas pipelines therefore also release methane into the atmosphere. Emissions of nitrous oxide (N2O) are mainly due to the use of agricultural nitrogen fertilizers, the production of animal feed and certain chemical processes, such as the production of nitric acid. There are also fluorinated gases which are used as refrigerants (air conditioning and cold chains), fire extinguishers and in certain industrial processes and consumer goods (such as certain solvents). They are not naturally present in the atmosphere.
