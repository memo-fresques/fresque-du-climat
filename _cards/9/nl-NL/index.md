---
title: Andere broeikasgassen.
backDescription: >-
  CO2 is niet het enige broeikasgas. Twee andere belangrijke broeikasgassen zijn
  methaan (CH4) en stikstofdioxide (N2O) (uitstoot voornamelijk in de landbouw).
---
De andere broeikasgassen die we hier beschrijven zijn methaan, distikstofmonoxide (lachgas, N2O) en HFK's (koelmiddelen). Broeikasgassen zijn gassen die door de aarde uitgezonden infraroodstraling kunnen absorberen. Dit zijn gassen die meer dan drie atomen per molecuul hebben, of twee als het twee verschillende atomen zijn (O2, N2 etc. zijn dus geen broeikasgassen). Naast waterdamp en CO2 zijn de belangrijkste broeikasgassen: CH4 (methaan), N2O (lachgas), O3 (troposferische ozon), (H)CFK's (chloor-fluor-koolwaterstoffen), HFK's (fluor-koolwaterstoffen), CF4 (perfluormethaan), SF6 (zwavelhexafloride), en CF3-SF5. 
Methaan wordt uitgestoten als er anaerobe (zuurstofloze) verbranding plaatsvindt. Bijvoorbeeld in de pens van de koe, waar bacteriën de onverteerbare cellulose van gras omzetten in stoffen die, na herkauwen, wel verteerd kunnen worden. In rijstvelden, die onder water staan, waar het organisch materiaal geen zuurstof kan ontvangen als het verteert. En in afvalbergen waar de hopen te diep zijn voor zuurstof om door te dringen. Methaan is ook de belangrijkste component van aardgas. Lekken van gaspijpleidingen zorgen daarom ook voor methaan in de atmosfeer. 
Emissies van lachgas (N2O) zijn vooral afkomstig van het gebruik van kunstmest in de landbouw, de productie van diervoeding en bepaalde chemische processen, zoals de productie van salpeterzuur. 
