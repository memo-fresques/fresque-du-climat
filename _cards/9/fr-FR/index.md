---
backDescription: >-
  Le CO2 n'est pas le seul Gaz à Effet de Serre. Il y a aussi le méthane (CH4)
  et le protoxyde d'azote (N2O) (qui viennent en très grande partie de
  l'agriculture), ainsi que quelques autres.
title: Émissions d'autres GES
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_9_autres_ges'
youtubeCode: vP51oA5tgos
---
Les autres GES décrits ici sont le méthane et le protoxyde d'azote (ou oxyde nitreux, appelé aussi gaz hilarant). D'une manière générale, un gaz à effet de serre est un gaz qui a la particularité de pouvoir absorber les rayonnements infrarouges émis par la terre. Il s'agit des gaz qui ont plus de trois atomes par molécule, ou deux si ce sont deux atomes différents (ne sont donc pas des GES : O2, N2, ...). Les principaux gaz à effet de serre : H2O, CO2, CH4, N2O, O3 (ozone troposphérique), les HCFC (comme le fréon), les CFC, les HFC, le CF4, le SF6, le CF3-SF5[^1]. Les gaz fluorés (ceux qui contiennent la lettre F) sont utilisés comme réfrigérants (climatisation et chaînes du froid), extincteurs et dans certains procédés industriels et biens de consommation (comme certains dissolvants). Ils ne sont pas naturellement présents dans l’atmosphère.Il y a dégagement de méthane (CH4) dès qu'il y a une décomposition anaérobie (c’est-à-dire sans oxygène) : dans la panse des vaches qu'on appelle aussi le rumen et qui donne son nom aux ruminants (dans le rumen, les bactéries ""digèrent"" la cellulose que la vache ne sait pas métaboliser, puis la vache régurgite cette herbe pour la mâcher et l'avaler pour de bon) ; dans les rizières car elles sont couvertes d'eau, et la matière organique immergée ne reçoit pas d'oxygène quand elle se décompose ; dans les décharges de déchets, quand les tas sont trop grands pour que l'oxygène atteigne le fond du tas. Le méthane est aussi le composant principal du gaz naturel. Les fuites sur les gazoducs relachent donc du méthane.Les principales sources d'émissions anthropiques de méthane sont (chiffres 2008-2017) : l'agriculture et les déchets (58%) (dont la fermentation entérique et le fumier (31%), les décharges (18%), le riz (9%)), les ressources fossiles (32%) et le brûlage de biomasse et de biocarburants (8%)[^2].Le protoxyde d’azote (N2O) est principalement dû à l’utilisation d’engrais azotés agricoles, à la production d’aliments pour bétail et à certains procédés chimiques, comme la production d’acide nitrique[^3].  
[^1]: _AR6 7SM Table 7.SM.7 p16_
[^2]: _AR6 WG1 Table 5.2 : methane budget p720 (p703)_
 // AR6 WG1 Figure 5.14 : methane budget p722 (p705)_  
[^3]: _AR6 WG1 Table 5.3 : sources N2O 
p729 (p712) // AR6 WG1 Figure 5.17 : Budget N2O p728 (p711)_
