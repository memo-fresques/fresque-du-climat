---
title: Beste berotegi-efektuko gasak
backDescription: >-
  CO₂-a ez da berotegi-efektuko gas bakarra. Beste batzuk ere daude, horien
  artean metanoa (CH₄) eta oxido nitrosoa (N₂O), laborantzak gehien sortzen
  dituenak.
---

