---
title: Ostali gasovi sa efektom staklene bašte
backDescription: >-
  CO2 nije jedini gas sa efektom staklene bašte. Među ostalima, tu se ubrajaju
  metan (CH4) i azot suboksid (N2O), dva gasa čija emisija uglavnom potiče iz
  poljoprivredne delatnosti.
---

