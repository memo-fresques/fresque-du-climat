---
title: Alte GES
backDescription: >-
  CO2-ul nu este singurul gaz cu efect de seră. Există, de asemenea, metanul
  (CH4) și oxidul de azot (N2O) care provin în mare parte din activitățile
  agricole.
---

