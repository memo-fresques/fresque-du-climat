---
title: Outros GEE
backDescription: >-
  O CO2 não é o único gás de efeito de estufa. Há também o metano (CH4) e o
  óxido nitroso (N2O) (cujas emissões têm origem sobretudo na agricultura), bem
  como alguns outros.
---

