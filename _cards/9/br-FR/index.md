---
backDescription: >-
  Le CO2 n'est pas le seul Gaz à Effet de Serre. Il y a aussi le méthane (CH4)
  et le protoxyde d'azote (N2O) (qui viennent en très grande partie de
  l'agriculture), ainsi que quelques autres.
instagramCode: ''
title: Gazoù efed ti-gwer all
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_9_autres_ges'
youtubeCode: vP51oA5tgos
---

