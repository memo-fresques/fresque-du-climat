---
title: De andre drivhusgasser
backDescription: >-
  CO2 er ikke den eneste gas, der bidrager til drivhuseffekten. To andre vigtige
  gasser er metangas (CH4) og kvælstofoxid/lattergas (N2O), som primært udledes
  i landbrugssektoren.
---

