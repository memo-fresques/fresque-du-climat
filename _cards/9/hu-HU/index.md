---
title: Egyéb üvegházhatású gázok
backDescription: >-
  A CO2 nem az egyetlen üvegházhatású gáz. Egyebek mellett a metán (CH4) és a
  dinitrogén-oxid (N2O) is annak számít, melyek főként a mezőgazdasági
  tevékenység során kerülnek kibocsátásra.
---

