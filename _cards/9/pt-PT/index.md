---
title: Outros GEE's
backDescription: >-
  O CO2 não é o único gás com efeito de estufa. O metano (CH4) e o óxido nitroso
  (N2O), cujas emissões têm origem sobretudo na agricultura, são alguns dos
  exemplos.
---
Os outros GEE descritos aqui são o metano e o óxido nitroso (ou óxido de diazoto, também chamado de gás hilariante). De uma modo geral, um gás com efeito de estufa é um gás que tem a particularidade de poder absorver a radiação infravermelha emitida pela terra. Trata-se de gases que têm mais de três átomos por molécula, ou dois se forem dois átomos diferentes (portanto, não são GEE: O2, N2, ...). Os principais gases com efeito de estufa: H2O, CO2, CH4, N2O, O3 (ozono troposférico), os HCFC (como o freon), os CFC, os HFC, o CF4, o SF6, o CF3-SF5[^1]. Os gases fluorados (aqueles que contêm a letra F) são utilizados como refrigerantes (ar condicionado e cadeias de frio), extintores e em alguns processos industriais e bens de consumo (como alguns solventes). Não estão naturalmente presentes na atmosfera. Há libertação de metano (CH4) sempre que há uma decomposição anaeróbica (ou seja, sem oxigénio): no rúmen das vacas, que também é chamado de pança e que dá o nome aos ruminantes (no rúmen, as bactérias ""digerem"" a celulose que a vaca não consegue metabolizar; depois a vaca regurgita essa erva para mastigá-la e engoli-la de vez); nos arrozais, porque estão cobertos de água, e a matéria orgânica submersa não recebe oxigénio quando se decompõe; nos aterros de resíduos, quando as pilhas são grandes demais para que o oxigénio atinja o fundo da pilha. O metano é também o componente principal do gás natural. As fugas nos gasodutos libertam, portanto, metano. As principais fontes de emissões antropogénicas de metano são (dados de 2008-2017): a agricultura e os resíduos (58%) (incluindo a fermentação entérica e o estrume (31%), os aterros (18%), o arroz (9%)), os recursos fósseis (32%) e a queima de biomassa e biocombustíveis (8%)[^2]. O óxido nitroso (N2O) é principalmente devido à utilização de fertilizantes azotados agrícolas, à produção de alimentos para gado e a alguns processos químicos, como a produção de ácido nítrico[^3].  
[^1]: _AR6 7SM Table 7.SM.7 p16_
[^2]: _AR6 WG1 Table 5.2 : methane budget p720 (p703)_
 // AR6 WG1 Figure 5.14 : methane budget p722 (p705)_  
[^3]: _AR6 WG1 Table 5.3 : sources N2O 
p729 (p712) // AR6 WG1 Figure 5.17 : Budget N2O p728 (p711)_
