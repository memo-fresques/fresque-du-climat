---
title: Otros Gases de Efecto Invernadero (GEI)
backDescription: >-
  El CO2 no es el único gas de efecto invernadero. Junto a otros gases, el
  metano (CH4) y el óxido nitroso (N2O), procedentes en su mayor parte de la
  agricultura, también contribuyen al efecto invernadero.
---

