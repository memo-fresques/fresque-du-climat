---
title: Emisije ostalih stakleničkih plinova (GHG)
backDescription: >-
  CO2 nije jedini staklenički plin (GHG). Među ostalima su metan (CH4) i dušikov
  oksid (N2O), a oba ta plina dolaze uglavnom od poljoprivrednih djelatnosti.
---

