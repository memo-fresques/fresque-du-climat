---
title: Emissioni di altri gas serra
backDescription: >-
  La CO2 non è l'unico gas serra. Tra i diversi gas serra troviamo anche il
  metano (CH4) ed il protossido di azoto (N2O), entrambi provenienti soprattutto
  dall'agricoltura.
---
Gli altri gas serra descritti qui sono il metano e il protossido di azoto (o ossido nitroso, anche chiamato gas esilarante). In generale, un gas serra è un gas che ha la particolarità di poter assorbire le radiazioni infrarosse emesse dalla Terra. Si tratta di gas che hanno più di tre atomi per molecola, o due se sono due atomi diversi (non sono quindi gas serra: O2, N2, ecc...). I principali gas serra sono: H2O, CO2, CH4, N2O, O3 (ozono troposferico), gli HCFC (come il freon), i CFC, gli HFC, il CF4, il SF6, il CF3-SF5 [^1]. I gas fluorurati (quelli che contengono la lettera F) vengono utilizzati come refrigeranti (aria condizionata e catene del freddo), estintori e in alcuni processi industriali e beni di consumo (come alcuni solventi). Non sono naturalmente presenti nell'atmosfera. Il metano (CH4) viene emesso non appena c'è una decomposizione anaerobica (cioè senza ossigeno): nel rumine delle mucche, che dà il nome ai ruminanti (nel rumine, i batteri "digeriscono" la cellulosa che la mucca non sa metabolizzare, poi la mucca rigurgita questa erba per masticarla e ingoiarla per bene); nelle risaie, perché sono coperte d'acqua e la materia organica immersa non riceve ossigeno quando si decompone; nelle discariche di rifiuti, quando le pile sono troppo grandi perché l'ossigeno raggiunga il fondo della pila. Il metano è anche il componente principale del gas naturale. Le perdite dei gasdotti rilasciano quindi metano. Le principali fonti antropiche di emissione di metano sono (dati 2008-2017): agricoltura e rifiuti (58%) (tra cui fermentazione enterica e letame (31%), discariche (18%), riso (9%)), risorse fossili (32%) e combustione di biomassa e biocarburanti (8%) [^2]. Il protossido di azoto (N2O) è principalmente dovuto all'uso di fertilizzanti azotati in agricoltura, alla produzione di alimenti per il bestiame e ad alcuni processi chimici, come la produzione di acido nitrico [^3]. 
[^1]: _AR6 7SM Tabella 7.SM.7 p16_
[^2]: _AR6 WG1 Tabella 5.2: bilancio del metano p720 (p703)_
 // AR6 WG1 Figura 5.14: bilancio del metano p722 (p705)_
[^3]: _AR6 WG1 Tabella 5.3: fonti N2O 
p729 (p712) // AR6 WG1 Figura 5.17: Bilancio N2O p728 (p711)_
