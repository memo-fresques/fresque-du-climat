---
title: Other GHGs
backDescription: >-
  CO2 is not the only greenhouse gas (GHG). Among others are methane (CH4), and
  nitrous oxide (N2O), two gases mainly emitted by agricultural activities.
---

