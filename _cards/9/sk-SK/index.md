---
title: Ostatné skleníkové plyny
backDescription: >-
  CO2 nie je jediný skleníkový plyn. Medzi inými poznáme taktiež metán (CH4) a
  oxid dusný (N2O), ktoré sú vylučované najmä poľnohospodárskymi aktivitami.
---

