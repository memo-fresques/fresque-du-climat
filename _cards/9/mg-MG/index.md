---
title: Entona maitso hafa
backDescription: >-
  Tsy ny CO2 irery ihany ny entona maitso. Ao ihany koa ny "méthane (CH4)" sy ny
  "protoxyde d'azote (N2O)" (izay avy amin'ny fambolena sy ny fiompiana ny
  ankamaroany) ary ireo entona mainsto hafa
---

