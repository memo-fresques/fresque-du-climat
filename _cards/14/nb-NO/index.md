---
title: Strålingsbalanse
backDescription: >-
  Denne grafen viser hvor energien som samles opp på kloden fra strålingspådriv
  havner: den varmer opp havet, smelter is, forsvinner i jorda og varmer opp
  atmosfæren.
---
På grafen kan vi se flere farger som representerer, fra topp til bunn: i lyseblått, det øvre laget av havet, mellom 0 og 700m. I mørkeblått, det nedre laget av havet, mellom 700m og 2000m. I enda mørkere blått, det nederste laget av havet, under 2000m. I grått, hav- og landis. I oransje, jordskorpen (utenom is). I lilla, atmosfæren. Den stiplede linjen representerer bare totalen. Den vertikale skalaen er i Zetta Joules (10^21 joules)[^1].
[^1]: _AR6 WG1 Figur TS.13 (e) (tvang) s58_
