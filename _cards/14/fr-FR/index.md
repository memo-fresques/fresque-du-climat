---
backDescription: >-
  Ce graphique explique où va l’énergie qui s’accumule sur la terre à cause du
  forçage radiatif : elle réchauffe l’océan, fait fondre la glace, se dissipe
  dans le sol et réchauffe l’atmosphère.
title: Bilan énergétique
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_14_bilan_%C3%A9nerg%C3%A9tique
youtubeCode: lFT5Mx0eK3U
instagramCode: CPGVmtmo0xw
---
Sur le graphique, on peut voir plusieurs couleurs qui représentent, du haut vers le bas : En bleu clair, la couche supérieure de l'océan, entre 0 et 700m. En bleu foncé, la couche inférieure de l'océan, entre 700m et 2000m. En bleu très foncé, la couche inférieure de l'océan, en-dessous de 2000m. En gris, les différentes glaces. En orange, les sols (hors glace). En violet, l'atmosphère. Le pointillé représente simplement le total. L'échelle verticale est en Zetta Joules (10^21 joules)[^1].
[^1]: _AR6 WG1 Figure TS.13 (e) (forçage) p58_
