---
title: Balanço Energético
backDescription: >-
  Esse gráfico explica para onde vai a energia acumulada na Terra devido ao
  forçamento radiativo: ela aquece o oceano, derrete o gelo, se dissipa no solo
  e aquece a atmosfera.
---

