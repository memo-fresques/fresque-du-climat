---
title: Rozpočítanie energie
backDescription: >-
  Tento graf znázorňuje kam sa podeje energia nazbieraná na zemi kvôli
  radiačnému pôsobeniu: otepľuje oceán, roztápa ľad, rozptyľuje sa do pôdy a
  otepľuje atmosféru.
---

