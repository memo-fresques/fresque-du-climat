---
title: Energia mérleg
backDescription: >-
  Ez a grafikon azt magyarázza el, hogy a Földön a sugárzási kényszer hatására
  felhalmozódott energia hol jelenik meg: felmelegíti az óceánt, megolvasztja a
  jeget, szétoszlik a talajban és felmelegíti a légkört.
---

