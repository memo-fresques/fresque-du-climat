---
title: Enerji bütçesi
backDescription: >-
  Bu grafik, ışınımsal zorlama sebebiyle toplanan enerjinin dünya üzerindeki
  dağılımını göstermektedir: Okyanusları ısıtır, buzulları eritir, toprak
  alanlara dağılır ve atmosferi ısıtır
---

