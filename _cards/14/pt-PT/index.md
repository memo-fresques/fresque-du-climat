---
title: Orçamento para Energia
backDescription: >-
  Este gráfico explica os efeitos da energia acumulada na Terra (em função do
  Forçamento Radiativo): ela aquece o oceano, derrete o gelo, é dissipada no
  solo e aquece a atmosfera.
---
No gráfico podemos observar diversas cores que representam, de cima para baixo: A azul claro, a camada superior do oceano, entre 0 e 700m. A azul escuro, a camada inferior do oceano, entre 700m e 2.000m. A azul bem escuro, a camada inferior do oceano, abaixo de 2.000m. A cinza, os diferentes tipos de gelo. A laranja, os solos (excluindo gelo). A roxo, a atmosfera. A linha pontilhada representa simplesmente o total. A escala vertical está em Zetta Joules (10^21 joules)[^1].
[^1]: _AR6 WG1 Figura TS.13 (e) (forçar) p58_
