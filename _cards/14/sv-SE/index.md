---
title: Energibudget
backDescription: >-
  Grafen visar vart den energi som ackumuleras på jorden på grund av
  strålningsdrivning tar vägen: i hav som värms, i isar som smälter, i marken
  och i atmosfären.
---
I figuren visas, uppifrån och ner, i olika färger: i ljusblått, det övre lagret av havet, mellan 0 och 700m djup. I mörkare blått, det nedre lagret av havet, mellan 700m och 2000m. I mycket mörkblått, det nedersta lagret av havet, under 2000m. I grått, de olika isarna. I orange, marken (utan is). I lila, atmosfären. Den streckade linjen representerar helt enkelt totala mängden energi. Den vertikala skalan är i zetta-joule (10^21 joule)[^1].
[^1]: _AR6 WG1 Figure TS.13 (e) (drivning) s58_
