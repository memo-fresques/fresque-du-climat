---
title: Energetska bilanca
backDescription: >-
  Ovaj graf prikazuje kamo odlazi energija koja se nakuplja na Zemlji: -
  zagrijava ocean, - topi led, - upija se u tlo i - zagrijava atmosferu.
---

