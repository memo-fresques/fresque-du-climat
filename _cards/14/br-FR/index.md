---
backDescription: >-
  Ce graphique explique où va l’énergie qui s’accumule sur la terre à cause du
  forçage radiatif : elle réchauffe l’océan, fait fondre la glace, se dissipe
  dans le sol et réchauffe l’atmosphère.
instagramCode: CPGVmtmo0xw
title: Budjed an energiezh
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_14_bilan_%C3%A9nerg%C3%A9tique
youtubeCode: lFT5Mx0eK3U
---

