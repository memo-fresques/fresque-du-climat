---
title: Energia-aurrekontua
backDescription: >-
  Grafiko honek erakusten du, erradiazioaren bortxaketarengatik, lurraren
  gainean metatzen den energia nora doan: ozeanoa berotzen du, izotzak urtzen
  ditu, lurrean barreiatzen da eta atmosfera berotzen du.
---

