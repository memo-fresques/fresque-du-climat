---
title: Енергиен баланс
backDescription: >-
  Тази графика обяснява къде отива енергията, натрупана на Земята, заради
  радиационното усилване: тя затопля океана, топи ледовете, разсейва се в
  почвата и загрява атмосферата.
---

