---
title: Bilans energetyczny
backDescription: >-
  Ten wykres pokazuje, dokąd trafia energia skumulowana na powierzchni Ziemi w
  efekcie wymuszania radiacyjnego: podnosi ona temperaturę oceanów, powoduje
  topnienie pokrywy lodowej, wnika w głąb lądów i ogrzewa atmosferę.
---
Na wykresie można zobaczyć kilka kolorów, które reprezentują, od góry do dołu: w kolorze jasnoniebieskim, górna warstwa oceanu, między 0 a 700m; w kolorze ciemnoniebieskim, dolna warstwa oceanu, między 700m a 2000m; w kolorze białym, różne rodzaje lodu; na pomarańczowo - gleba; na fioletowo - atmosfera. Linia przerywana przedstawia sumę wszystkich elementów.
