---
title: Energy Budget
backDescription: >-
  This graph explains where the energy accumulated on Earth due to radiative
  forcing goes. It warms up the ocean, melts ice, dissipates into the ground and
  warms up the atmosphere.
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_14_energy_budget
youtubeCode: tnMmouVM-Lg
instagramCode: CPGbagkITSV
---

On the graph, you can see several colours that represent, from top to bottom: In light blue, the upper layer of the ocean, between 0 and 700m. In dark blue, the lower layer of the ocean, between 700m and 2000m In white, the different types of ice. In orange, the soil. In purple, the atmosphere. The dotted line represents the total.
