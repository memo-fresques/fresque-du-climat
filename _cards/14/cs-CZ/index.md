---
title: Energetická bilance
backDescription: >-
  Tento graf vysvětluje, co se děje s energií nahromaděnou na Zemi v důsledku
  radiačního působení: ohřívá oceány, způsobuje tání ledu, rozptyluje se do země
  a ohřívá atmosféru.
---

