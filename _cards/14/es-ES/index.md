---
title: Balance energético
backDescription: >-
  Este gráfico explica adónde va la energía acumulada en la Tierra debido al
  forzamiento radiativo: calienta el océano, derrite el hielo, se disipa en el
  suelo y calienta la atmósfera.
---

