---
title: Bilancio energetico
backDescription: >-
  Questo grafico spiega dove va l'energia che si accumula sulla Terra a causa
  del forzante radiativo: riscalda l'oceano, fonde il ghiaccio, si dissipa nel
  terreno e riscalda l'atmosfera.
---
Seul grafico si possono vedere diversi colori che rappresentano, dall'alto verso il basso: in azzurro chiaro, lo strato superiore dell'oceano, tra 0 e 700 m. In blu scuro, lo strato inferiore dell'oceano, tra 700 m e 2000 m. In blu molto scuro, lo strato più basso dell'oceano, al di sotto di 2000 m. In grigio, i diversi ghiacci. In arancione, i suoli (esclusi i ghiacci). In viola, l'atmosfera. La linea tratteggiata rappresenta semplicemente il totale. La scala verticale è in zettajoule (10^21 joule)[^1].
[^1]: _AR6 WG1 Figura TS.13 (e) (forzante radiativo) p58_
