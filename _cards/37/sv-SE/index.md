---
title: Svält
backDescription: >-
  Svält kan uppstå efter minskade skördar samt av reducerad biologisk mångfald i
  havet.
---
Minskad jordbruksavkastning kan leda till undernäring eller till och med svält. 2017 bidrog t ex en torka i Tanzania, Etiopien, Kenya och Somalia 2017 till en betydande livsmedelsosäkerhet[1]. 
_[1] AR6 WG2 Table 4.4 p92 (p580)_
