---
title: Hungersnød
backDescription: >-
  Hungersnød kan forårsakes av nedgang i landbruksutbyttet og tap av marin
  biodiversitet.
---

En nedgang i landbruksavlinger kan føre til underernæring og til og med hungersnød. For eksempel i 2017 bidro en tørke i Tanzania, Etiopia, Kenya og Somalia til betydelig redusert matsikkerhet[1]. 
_[1] AR6 WG2 Tabell 4.4 s592 (s580)_
