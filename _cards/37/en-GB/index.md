---
title: Hunger
backDescription: >-
  Hunger can be caused by lower agricultural yields and by the loss of marine
  biodiversity.
wikiUrl: 'https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_37_hunger'
youtubeCode: 8moQDG78iqY
instagramCode: ''
---

