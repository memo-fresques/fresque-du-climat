---
backDescription: >-
  Les famines peuvent être occasionnées par la baisse des rendements agricoles
  et la réduction de la biodiversité marine.
instagramCode: ''
title: Naonegezhioù
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_37_famine'
youtubeCode: FfyI7LHroFk
---

