---
title: Hungersnød
backDescription: >-
  Fald i høstudbytter og tabet af biodiversitet i havet vil sætte den globale
  fødevareforsyning under pres.
---

