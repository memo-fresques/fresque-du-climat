---
title: Hungersnöte
backDescription: >-
  Hungersnöte können die Folge von niedrigeren landwirtschaftlichen Erträgen und
  einer geringeren biologischen Vielfalt der Meere sein.
---

