---
title: Famines
backDescription: >-
  Famines can be caused by lower agricultural yields and by the reduction of
  marine biodiversity.
---

