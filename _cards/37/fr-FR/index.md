---
backDescription: >-
  Les famines peuvent être occasionnées par la baisse des rendements agricoles
  et la réduction de la biodiversité marine.
title: Famines
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_37_famine'
youtubeCode: FfyI7LHroFk
---

Une baisse des rendements agricoles peuvent conduire à une sous-nutrition voir à des famines. Par exemple en  2017, une sécheresse en Tanzanie, Ethiopie, Kenya et Somalie a contribué à une importante insécurité alimentaire[1].  
_[1] AR6 WG2 Table 4.4 p592 (p580)_
