---
title: Carestie
backDescription: >-
  Le carestie possono essere causate dalla diminuzione delle rese agricole e
  dalla riduzione della biodiversità marina.
---
Un calo delle rese agricole può portare a malnutrizione o addirittura carestie. Ad esempio, nel 2017, una siccità in Tanzania, Etiopia, Kenya e Somalia ha contribuito a una grave insicurezza alimentare[1].
_[1] AR6 WG2 Tabella 4.4 p592 (p580)_
