---
title: Fome
backDescription: >-
  A fome pode ser causada pelo declínio nos rendimentos agrícolas ou pela
  redução da biodiversidade marinha.
---

Uma diminuição dos rendimentos agrícolas pode levar à subnutrição ou mesmo à Fome. Por exemplo, em 2017, uma seca na Tanzânia, Etiópia, Quénia e Somália contribuiu para uma insegurança alimentar significativa[1].  
_[1] AR6 WG2 Tabela 4.4 p592 (p580)_
