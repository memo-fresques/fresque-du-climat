---
backDescription: >-
  On parle ici de la température de l’air au sol, en moyenne sur la surface de
  la Terre. Elle a déjà augmenté de 1,2°C depuis 1900. Selon les scénarios
  d’émissions, elle aura augmenté de 1,5°C à 5°C d’ici 2100. Or, lors de la
  dernière période glaciaire (il
title: Hausse de la température
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_21_hausse_temp%C3%A9rature
youtubeCode: X0zSLQTEHD8
instagramCode: CMSL54coOTz
---
Sur le graphique principal, la courbe noire représente les mesures de température (1850 à 2015). Les courbes en couleur représentent les projections pour les différents scénarios SSP envisagés (2015 à 2100). Les zones claires représentent les incertitudes[^1]. Ce réchauffement est d'origine humaine, c'est ce que montre le graphique secondaire sur lequel sont représentées les mesures de 1850 à 2020 (courbe noire), les données issues des modèles lorsqu'on ne prend pas en compte les facteurs anthropiques (vert), et les données issues des modèles lorsqu'on prend en compte les facteurs anthropiques (orange). Les données obtenues lorsqu'on prend en compte tous les paramètres dans les modèles peuvent prédire de façon satisfaisante les mesures réelles. es modèles estiment l'implication de l'homme à ... 100% (entre 90% et 110%)[^2]. Par convention, on parle de réchauffement par rapport à la moyenne des températures 1850-1900 (appelé ère préindustrielle). Le réchauffement actuel par rapport à cette période est d'environ 1,2°C[^3]. Mais le réchauffement n'est pas homogène, la température augmente plus vite au dessus des terres qu'à la surface des océans et plus rapidement aux pôles[^4]. Les trajectoires actuelles nous conduisent vers un réchauffement de +3.2°C d'ici 2100. Au rythme actuel le réchauffement atteindra 1,5°C entre 2030 et 2050 environ[^5]. Cette carte peut jouer deux rôles. Soit c'est la température de l'air, donc de l'atmosphère. C'est comme ça qu'il faut l'interpréter quand on a gardé les cartes 10, 14 et 15. Dans ce cas, la carte précédente est la 14 (Bilan énergétique). Soit elle représente la température de la terre (et ça tombe bien parce que la définition de la température de la terre, c'est justement la température de l'air, au sol, en moyenne sur la surface de la terre). Dans ce cas, la carte précédente est la 13 (Effet de serre) et on peut faire un lien vers les cartes 16, 17, 18 et 19.  
[^1]: AR6 WG1 Figure SPM.8 (a) p22  
[^2]: AR6 WG1 SPM A.1.3 p22 (p5) // AR6 WG1 Figure SPM.1 p23 (p6)  
[^3]: AR6 WG1 1.4.1 p206 (p189)  
[^4]: AR6 WG1 Cross-Section TS.1 p28 (p60) // AR6 WG1 Figure 2.11 (b et c) p333 (p316)  
[^5]: AR6 WG3 TS.3 p83 (p70) // AR6 WG3 Figure TS.9 p82 (p69)
