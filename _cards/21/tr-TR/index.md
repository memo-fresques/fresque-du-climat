---
title: Hava sıcaklığının yükselmesi
backDescription: >-
  Burada yeryüzündeki ortalama hava sıcaklığından bahsediyoruz. 1900'den beri 1
  °C artmıştır. Senaryolara bağlı olarak ortalama sıcaklık 2100 yılına kadar 2°C
  ile 5°C arasında artabilir. Son buzul çağının sonunda ortalama sıcaklık
  bugünkünden sadece 5 °C da
---

