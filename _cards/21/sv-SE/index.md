---
title: Temperaturökning
backDescription: >-
  Luftens medeltemperatur på jordytan har sedan år 1900 ökat med 1,2°C. Enligt
  utsläppsscenarion för framtiden kommer denna ökning att nå mellan 1,5 och 5°C
  år 2100. Under den senaste istiden för 20 000 år sedan var jordens
  medeltemperatur 5°C lägre än idag och denna temperaturökning tog 10 000 år.
---
Den svarta kurvan till vänster i den stora figuren visar global temperatur mellan år 1850 och 2015. De färgade kurvorna representerar projektioner för de olika RCP-scenarierna fram till 2100. Skuggade områden visar osäkerheten i projektionerna [^1]. I den infällda figuren visas temperaturmätningar mellan 1850 och 2020. Dessa sammanfaller väl med den bruna kurvan, som visar simuleringar från modeller som inkluderar antopogena faktorer. Den gröna kurvan visar simulerad uppvärmning om antopogen påverkan inte tas med. Slutsatsen är att den uppvärmning vi ser beror på antropogen påverkan - enligt modellerna är människans påverkan ... 100% (mellan 90% och 110%)[^2]. Det vedertagna sättet att beskriva temperaturökning utgår från medeltemperaturen 1850-1900 (kallad förindustriell era). Den nuvarande uppvärmningen i förhållande till denna period är cirka 1,2°C[^3]. Men uppvärmningen är inte lika stor överallt - den ökar snabbare över land än över havsytan och snabbare vid polerna[^4]. Det scenario vi just nu verkar följa leder till en uppvärmning på +3,2°C år 2100. I nuvarande takt kommer uppvärmningen att nå 1,5°C mellan 2030 och 2050[^5]. Kortet kan ha två funktioner. Antingen kan vi tänka att kortet visar lufttemperaturen, alltså atmosfären. Så ska kortet tolkas om man har använt alla kort, dvs inkluderat kort 10, 14 och 15. I så fall kommer det här kortet direkt efter kort 14 (energibudget). I den förenklade verionen kan vi tänka att kortet visar jordens temperatur (och det är bra eftersom definitionen av jordens temperatur är just lufttemperaturen, vid marken, i genomsnitt över jordens yta). I så fall är det föregående kortet nr 13 (ytterligare växthuseffekt), men man får sedan länka direkt till kort 16, 17, 18 och 19  (då kort 15 och 14 saknas).
[^1]: AR6 WG1 Figure SPM.8 (a) p22
[^2]: AR6 WG1 SPM A.1.3 p22 (p5) // AR6 WG1 Figure SPM.1 p23 (p6)
[^3]: AR6 WG1 1.4.1 p206 (p189)
[^4]: AR6 WG1 (Technical Summary) Box TS.1 p28 (p60-61) // AR6 WG1 Figur 2.11 (b och c) p333 (p316)
[^5]: AR6 WG3 TS.3 p83 (p70) // AR6 WG3 Figur TS.9 p82 (p69)
