---
title: Fiakaran'ny hafanana
backDescription: >-
  Ny hafanan'ny rivotra eny amin'ny tany no resaka eto. Efa niakatra nanakaiky
  ny 1°C nanomboka ny taona 1900. Araka ny zava-misy, hiakatra 2°C hatramin'ny
  5°C izy io mandra-pahatongan'ny taona 2100. Tamin'ny vanim-potoana farany
  nisian'ny ranomandry betsak
---

