---
title: Tenperaturaren emendatzea
backDescription: >-
  Hemen aipatzen dena, lurrazaleko lurzoruko airearen bataz besteko tenperatura
  da. 1900az geroztik, 1°C inguru emendatu da, eta, hemendik 2100. urtera 2 eta
  5°C artean emendatuko da. Alta, azken glaziazioan (duela 20000 urte), orain
  baino 5°C apalagoa zen.
---

