---
title: Temperature Rise
backDescription: >-
  Here we are referring to the average air temperature above ground. It has
  increased by 1.8°F (1°C) since 1900. Depending on the scenarios, the rise in
  temperature could reach 3.6°F (2°C) to 9°F (5°C) by 2100. At the end of the
  last ice age, the average te
---

