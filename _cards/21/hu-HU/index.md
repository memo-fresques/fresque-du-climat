---
title: Hőmérséklet-emelkedés
backDescription: >-
  Itt a Föld földfelszín feletti átlaghőmérsékletére utalunk. Ez 1°C-al
  emelkedett 1900 óta. A különböző forgatókönyvektől függően, a
  hőmérséklet-emelkedés elérheti a 2 illetve 5°C-ot 2100-ig. Az utolsó
  jégkorszak végén az átlaghőmérséklet 5°C-al alacsonyab
---

