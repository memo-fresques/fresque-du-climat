---
title: Porast temperature
backDescription: >-
  Ovde mislimo na prosečnu temperaturu vazduha iznad tla Zemlje. Od 1900. godine
  temperatura je porasla za 1°C. U zavisnosti od scenarija, dalji rast
  temperature bi mogao dostići od 2°C do 5°C do 2100. godine. Krajem poslednjeg
  ledenog doba, prosečna temper
---

