---
title: Hausse vun der Temperatur
backDescription: >-
  Hei geet et ëm d'Lofttemperatur um Buedem, an der Moyenne op der ganzer Äerd.
  Zënter 1900 ass se scho bal 1 °C eropgaang. Je no Szenario wäert se bis 2100
  ëm 2 bis 5 °C eropgaang sinn. An der läschter Äiszäit, virun 20.000 Joer, war
  se nëmme 5°C méi niddr
---

