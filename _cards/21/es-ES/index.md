---
title: Aumento de la temperatura
backDescription: >-
  Nos referimos aquí a la temperatura del aire a nivel del suelo, como promedio
  de la superficie terrestre. Ha aumentado 1°C desde 1900. Según los diferentes
  escenarios, este aumento será de entre 2°C y 5°C de aquí a 2100. Al final del
  último periodo glacia
---
En el gráfico principal, la curva negra representa las mediciones de temperatura (1850 a 2015). Las curvas de colores representan las proyecciones para los diferentes escenarios SSP considerados (2015 a 2100). Las zonas claras representan las incertidumbres[^1]. Este calentamiento es de origen humano, como muestra el gráfico secundario en el que se representan las mediciones de 1850 a 2020 (curva negra), los datos de los modelos cuando no se tienen en cuenta los factores antropogénicos (verde), y los datos de los modelos cuando se tienen en cuenta los factores antropogénicos (naranja). Los datos obtenidos cuando se tienen en cuenta todos los parámetros en los modelos pueden predecir de manera satisfactoria las mediciones reales. Los modelos estiman la implicación del hombre en ... 100% (entre 90% y 110%)[^2]. Por convención, se habla de calentamiento en relación con la media de las temperaturas de 1850-1900 (llamada era preindustrial). El calentamiento actual en relación con este período es de aproximadamente 1,2°C[^3]. Pero el calentamiento no es homogéneo, la temperatura aumenta más rápido sobre la tierra que en la superficie de los océanos y más rápidamente en los polos[^4]. Las trayectorias actuales nos llevan a un calentamiento de +3,2°C para 2100. Al ritmo actual, el calentamiento alcanzará 1,5°C entre 2030 y 2050 aproximadamente[^5]. Esta carta puede desempeñar dos roles. O bien es la temperatura del aire, es decir, de la atmósfera. Así es como debe interpretarse cuando se han conservado las cartas 10, 14 y 15. En este caso, la carta anterior es la 14 (Balance energético). O bien representa la temperatura de la tierra (y es conveniente porque la definición de la temperatura de la tierra es precisamente la temperatura del aire, en el suelo, en promedio sobre la superficie de la tierra). En este caso, la carta anterior es la 13 (Efecto invernadero) y se puede hacer un enlace con las cartas 16, 17, 18 y 19.  
[^1]: AR6 WG1 Figura SPM.8 (a) p22  
[^2]: AR6 WG1 SPM A.1.3 p22 (p5) // AR6 WG1 Figura SPM.1 p23 (p6)  
[^3]: AR6 WG1 1.4.1 p206 (p189)  
[^4]: AR6 WG1 Sección Transversal TS.1 p28 (p60) // AR6 WG1 Figura 2.11 (b y c) p333 (p316)  
[^5]: AR6 WG3 TS.3 p83 (p70) // AR6 WG3 Figura TS.9 p82 (p69)
