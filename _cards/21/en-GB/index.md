---
title: Rising Temperatures
backDescription: >-
  The average air temperature at the surface of the Earth has increased by 1.2°C
  since 1900. Possible future emission scenarios predict that this increase will
  reach between 2 and 5°C by 2100. During the last ice age 20,000 years ago, the
  average air tempe
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_21_temperature_rise
youtubeCode: nuACnopTT_g
instagramCode: CMSMUwfnokZ
---

This card can play two roles: Either it's the temperature of the air, and therefore of the atmosphere. This is how it should be interpreted when you have kept cards 10, 14 and 15 in the game. In this case, the previous card is 14. Either it represents the temperature of the Earth (and this is good because the definition of the temperature of the Earth is precisely the temperature of the air, at ground level, on average at the surface of the Earth). In this case, the previous card is 13 and we can link to cards 16, 17, 18 and 19. At the current rate of warming, 0.2°C per decade, the warming will reach 1.5°C between 2030 and 2050.
