---
backDescription: >-
  On parle ici de la température de l’air au sol, en moyenne sur la surface de
  la Terre. Elle a déjà augmenté de 1,2°C depuis 1900. Selon les scénarios
  d’émissions, elle aura augmenté de 1,5°C à 5°C d’ici 2100. Or, lors de la
  dernière période glaciaire (il
instagramCode: CMSL54coOTz
title: Kresk an temperadur
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_21_hausse_temp%C3%A9rature
youtubeCode: X0zSLQTEHD8
---

