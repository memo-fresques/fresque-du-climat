---
title: Temperaturøkning
backDescription: >-
  Her snakker vi om det globale gjennomsnittet av lufttemperaturen på jordens
  overflate. Den har økt med 1°C siden 1900. Avhengig av scenariene,
  temperaturøkningen kan nå mellom 2°C og 5°C innen 2100. Mot slutten av siste
  istid var den gjennomsnittlige temp
---
På hovedgrafen viser den svarte kurven temperaturmålingene (1850 til 2015). De fargede kurvene viser  de forskjellige SSP-scenariene som er vurdert (2015 til 2100). De lyse områdene representerer usikkerhetsmarginene[^1].  Oppvarmingen er menneskeskapt, det viser sekundærgrafen som viser målingene fra 1850 til 2020 (svart kurve), data fra modellene når man ikke tar hensyn til menneskelige faktorer (grønn), og data fra modellene når man tar hensyn til menneskelige faktorer (oransje). Dataene som er oppnådd når man tar hensyn til alle parameterne i modellene kan forutsi de faktiske målingene på en tilfredsstillende måte. Modellene anslår menneskets innvirkning til ... 100% (mellom 90% og 110%)[^2]. Det er kutyme å snakke om oppvarming i forhold til gjennomsnittstemperaturen 1850-1900 (kalt førindustriell tid). Den nåværende oppvarmingen i forhold til denne perioden er omtrent 1,2°C[^3]. Men oppvarmingen er ikke homogen, temperaturen øker raskere over land enn på havoverflaten og raskere ved polene[^4]. De nåværende banene fører oss mot en oppvarming på +3,2°C innen 2100. I dagens tempo vil oppvarmingen nå 1,5°C mellom 2030 og 2050[^5]. Dette kortet kan ha to roller. Enten er det lufttemperaturen, altså atmosfæren. Det er slik det skal tolkes når man har beholdt kortene 10, 14 og 15. I så fall er det forrige kortet 14 (Energibalanse). Eller så representerer det jordens temperatur (og det passer bra fordi definisjonen av jordens temperatur er nettopp lufttemperaturen, ved bakken, i gjennomsnitt på jordens overflate). I så fall er det forrige kortet 13 (Drivhuseffekt) og man kan lage en kobling til kortene 16, 17, 18 og 19.  
[^1]: AR6 WG1 Figur SPM.8 (a) s22  
[^2]: AR6 WG1 SPM A.1.3 s22 (s5) // AR6 WG1 Figur SPM.1 s23 (s6)  
[^3]: AR6 WG1 1.4.1 s206 (s189)  
[^4]: AR6 WG1 Tverrsnitt TS.1 s28 (s60) // AR6 WG1 Figur 2.11 (b og c) s333 (s316)  
[^5]: AR6 WG3 TS.3 s83 (s70) // AR6 WG3 Figur TS.9 s82 (s69)
