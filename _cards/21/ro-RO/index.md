---
title: Creșterea temperaturii
backDescription: >-
  Aici ne referim la temperatura medie a aerului de pe suprafața Pământului. Din
  1900, aceasta a crescut deja cu aproape 1 ° C. Conform scenariilor,
  temperatura ar putea crește cu o valoare cuprinsă între 2°- 5° grade până în
  2100. În ultima perioadă glacia
---

