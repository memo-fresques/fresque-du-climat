---
title: Aumento di temperatura
backDescription: >-
  Parliamo qui della temperatura dell'aria al suolo, in media, sulla superficie
  della Terra. È già aumentata di 1,2°C dal 1900. Secondo gli scenari di
  emissioni future, potrà aumentare da 1,5°C a 5°C da qui al 2100. Durante
  l'ultima era glaciale (20.000 anni fa), la temperatura era solamente 5°C più
  bassa rispetto a oggi...e la deglaciazione è durata 10.000 anni!
---
Sul grafico principale, la curva nera rappresenta le misure della temperatura (1850-2015). Le curve colorate rappresentano le proiezioni per i diversi scenari SSP considerati (2015-2100). Le aree chiare rappresentano le incertezze[^1]. Questo riscaldamento è di origine antropica, come mostrato dal grafico secondario in cui sono rappresentati i dati dal 1850 al 2020 (curva nera), i dati dei modelli quando non si considerano i fattori antropici (verde) e i dati dei modelli quando si considerano i fattori antropici (arancione). I dati ottenuti quando si considerano tutti i parametri nei modelli possono prevedere in modo soddisfacente le misure reali. I modelli stimano l'impatto dell'uomo al... 100% (tra il 90% e il 110%)[^2]. Per convenzione, si parla di riscaldamento rispetto alla media delle temperature 1850-1900 (chiamata era preindustriale). Il riscaldamento attuale rispetto a questo periodo è di circa 1,2°C[^3]. Ma il riscaldamento non è omogeneo, la temperatura aumenta più velocemente sulla terraferma che sulla superficie degli oceani e più rapidamente ai poli[^4]. Le attuali traiettorie ci portano verso un riscaldamento di +3,2°C entro il 2100. Al ritmo attuale, il riscaldamento raggiungerà circa 1,5°C tra il 2030 e il 2050[^5]. Questa carta può svolgere due ruoli. Il primo è la temperatura dell'aria, quindi dell'atmosfera. È così che va interpretata sulle carte 10, 14 e 15. In questo caso, la carta precedente è la 14 (Bilancio energetico). Il secondo ruolo è quello della temperatura terrestre (e va bene perché la definizione di temperatura del suolo è proprio la temperatura dell'aria, a terra, in media sulla superficie terrestre). In questo caso, la carta precedente è la 13 (Effetto serra addizionale) e si può fare un collegamento alle carte 16, 17, 18 e 19.  
[^1]: AR6 WG1 Figura SPM.8 (a) p22  
[^2]: AR6 WG1 SPM A.1.3 p22 (p5) // AR6 WG1 Figura SPM.1 p23 (p6)  
[^3]: AR6 WG1 1.4.1 p206 (p189)  
[^4]: AR6 WG1 Cross-Section TS.1 p28 (p60) // AR6 WG1 Figura 2.11 (b e c) p333 (p316)  
[^5]: AR6 WG3 TS.3 p83 (p70) // AR6 WG3 Figura TS.9 p82 (p69)
