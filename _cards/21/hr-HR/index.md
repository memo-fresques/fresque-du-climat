---
title: Povišenje temperature zraka
backDescription: >-
  Prosječna temperatura zraka na površini Zemlje povisila se za 1,2°C od 1900.
  godine. Prema predviđanjima mogućih budućih emisija daljnje povišenje
  temperature iznosit će između 2 i 5°C do 2100. godine.

  Tijekom posljednjeg ledenog doba, prije 20.000 godi
---

