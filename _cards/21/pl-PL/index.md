---
title: Wzrost temperatury
backDescription: >-
  Chodzi tutaj o średnią temperaturę powietrza na powierzchni Ziemi, która od
  1900 r. wzrosła już o blisko 1,2°C . Według przewidywań do 2100 r. może się
  podnieść o 1.5-5°C. W czasie ostatniego zlodowacenia (20 000 lat temu) była
  zaledwie o 5°C stopni niższa od dzisiejszej... a zanik lodowca trwał 10 000
  lat!
---
Ta karta może odgrywać dwie role: albo jest to temperatura powietrza, a więc i atmosfery i tak należy ją interpretować, jeśli w grze pozostały karty 10, 14 i 15 (w tym przypadku poprzednia karta to 14); albo reprezentuje temperaturę Ziemi (i to jest dobrze, ponieważ definicja temperatury Ziemi to właśnie temperatura powietrza na poziomie gruntu, średnio na powierzchni Ziemi; w tym przypadku poprzednia karta to 13 i możemy połączyć ją z kartami 16, 17, 18 i 19). Przy obecnym tempie ocieplenia, 0,2°C na dekadę, ocieplenie osiągnie 1,5°C między 2030 a 2050 rokiem.
