---
title: Zvyšování teploty
backDescription: >-
  Zde máme na mysli průměrnou povrchovou teplotu Země. Od roku 1900 se zvýšila o
  1 °C. Dle různých scénářů by nárůst teploty mohl do roku 2100 dosáhnout 2 °C
  až 5 °C. Na konci poslední doby ledové byla průměrná teplota pouze o 5 °C
  nižší než dnes... a degla
---

