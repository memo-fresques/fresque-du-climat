---
title: Aumento da temperatura do ar
backDescription: >-
  Referimo-nos aqui à temperatura média do ar na superfície da Terra, que
  aumentou 1 °C desde 1900. Dependendo dos diferentes cenários, o aumento da
  temperatura poderá atingir 2 a 5 °C até 2100. No final da última era glacial
  (há 20.000 anos) a temperatura
---
No gráfico principal, a curva preta representa as medições de temperatura (1850 a 2015). As curvas coloridas representam as projeções para os diferentes cenários SSP considerados (2015 a 2100). As áreas claras representam as incertezas[^1]. Este aquecimento é de origem humana, como mostra o gráfico secundário, no qual estão representadas as medições de 1850 a 2020 (curva preta), os dados dos modelos quando não se consideram os fatores antropogénicos (verde) e os dados dos modelos quando se consideram os fatores antropogénicos (laranja). Os dados obtidos quando se consideram todos os parâmetros nos modelos podem prever de forma satisfatória as medições reais. Os modelos estimam a implicação do Homem em ... 100% (entre 90% e 110%)[^2]. Por convenção, fala-se de aquecimento em relação à média das temperaturas de 1850-1900 (chamada era pré-industrial). O aquecimento atual em relação a este período é de cerca de 1,2°C[^3]. Mas o aquecimento não é homogéneo: a temperatura aumenta mais rapidamente sobre a Terra do que à superfície dos oceanos e mais rapidamente nos polos[^4]. As trajetórias atuais conduzem-nos a um aquecimento de +3,2°C até 2100. Ao ritmo atual, o aquecimento atingirá 1,5°C entre 2030 e 2050 aproximadamente[^5]. Este mapa pode desempenhar dois papéis. Ou é a temperatura do ar, ou seja, da atmosfera. É assim que deve ser interpretado quando se mantiveram as cartas 10, 14 e 15. Nesse caso, a carta anterior é a 14 (Orçamento energético). Ou representa a temperatura da Terra (e é conveniente porque a definição de temperatura da Terra é precisamente a temperatura do ar, ao nível do solo, em média na superfície da Terra). Nesse caso, a carta anterior é a 13 (Efeito de estufa adicional) e pode-se fazer uma ligação com as cartas 16, 17, 18 e 19.  
[^1]: AR6 WG1 Figura SPM.8 (a) p22  
[^2]: AR6 WG1 SPM A.1.3 p22 (p5) // AR6 WG1 Figura SPM.1 p23 (p6)  
[^3]: AR6 WG1 1.4.1 p206 (p189)  
[^4]: AR6 WG1 Secção Transversal TS.1 p28 (p60) // AR6 WG1 Figura 2.11 (b e c) p333 (p316)  
[^5]: AR6 WG3 TS.3 p83 (p70) // AR6 WG3 Figura TS.9 p82 (p69)
