---
title: Jorden varmes op
backDescription: >-
  Grafen viser den globale gennemsnitstemperatur på land. Siden år 1900 er den
  steget med 1°C. Afhængig af RCP-scenario (Representative Concentration
  Pathways), kan stigningen i år 2100 ligge et sted mellem 2°C til 5°C. Til
  sammenligning var gennemsnitstemp
---

