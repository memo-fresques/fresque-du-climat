---
title: Schmëlze vum Mieräis
backDescription: >-
  Duerch d'Schmëlze vum Mieräis geet de Mieresspigel net erop (en Äiscube am
  Waasser deet d'Glas net iwwerlafe, wann e schmëlzt). Par contre, wann d'Äis
  schmëlzt, gëtt d'Uewerfläch do méi däischter a reflektéiert d'Sonnestrahle
  manner (Albedo-Effekt).
---

