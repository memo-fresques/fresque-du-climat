---
title: Deshielo de la banquisa
backDescription: >-
  El deshielo de la banquisa (hielo marino) no es la causa de la subida del
  nivel del mar (un cubito de hielo que se derrite en un vaso, no hace que el
  agua desborde el vaso). Sin embargo, este deshielo deja paso a zonas más
  oscuras (el mar) que absorben má
---
El volumen ocupado por el hielo bajo la superficie es exactamente el mismo que el del hielo una vez derretido. Es el principio del empuje de Arquímedes[^1]. Debido a su salinidad, el hielo marino se forma cuando el agua de mar está a una temperatura inferior a -1.8°C[^2]. El hielo marino se forma cuando el agua del mar se congela, por lo que contiene sal, pero la sal es expulsada a medida que se forma el hielo marino. Esto significa que el hielo marino está compuesto de agua dulce relativamente pura, mientras que la región que rodea el hielo marino está constituida por agua de mar salada. Esta característica tiene importantes implicaciones para la circulación oceánica y los ecosistemas marinos en las regiones polares. Se encuentra hielo marino en el Ártico y en la Antártida. El hielo marino ártico sigue un ciclo estacional. Se derrite parcialmente en verano y se vuelve a formar en invierno. Pero desde finales de los años 70, se ha observado una reducción global de su cobertura[^3]. Se estima que para finales del siglo XXI podría derretirse completamente en verano [^4].
[^1]: _[Wikipedia − Empuje de Arquímedes](https://es.wikipedia.org/wiki/Principio_de_Arquímedes)_
[^2]: _https://nsidc.org/learn/parts-cryosphere/sea-ice/science-sea-ice_
[^3]: _AR6 WG1 TS.2.5 p44 (p76)_
[^4]: _AR6 WG1 Figure TS.8 (c) p34 (p66)_
