---
title: Τήξη των θαλάσσιων πάγων
backDescription: >-
  Η τήξη των θαλάσσιων πάγων δεν είναι υπεύθυνη για την άνοδο της στάθμης της
  θάλασσας (ένα παγάκι που λιώνει σε ένα ποτήρι νερό, δεν κάνει το νερό να
  υπερχειλίσει το ποτήρι). Το λιώσιμο των θαλάσσιων πάγων όμως, αφήνει πάντα
  στην θέση του μια λιγότερο αντα
---

