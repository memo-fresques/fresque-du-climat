---
title: Topljenje morskih ledenjaka
backDescription: >-
  Topljenje morskih ledenjaka ne dovodi do rasta razine oceana i mora (kao što
  se zbog kockice leda koja se rastopi u čaši čaša neće preliti).

  Ipak, kada se led otopi, njegovu bjelinu zamjenjuje puno tamnije more koje
  apsorbira više sunčevih zraka.
---

