---
backDescription: >-
  La fonte de la banquise n'est pas responsable de la montée des eaux (un glaçon
  qui fond dans du pastis ne fait pas déborder le verre).
instagramCode: CKUB7UsoVbl
title: Teuz ar morskorneg
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_18_fonte_de_la_banquise
youtubeCode: qtZ3J_HEHuA
---

