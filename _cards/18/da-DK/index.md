---
title: Havisen smelter
backDescription: >-
  Smeltende havis fører IKKE til en vandstandsstigning i havene (ligesom
  vandstanden i dit glas ikke stiger, når en isterning smelter). Når isen
  smelter skabes endnu mere afsmeltning, fordi den mørke havoverflade opsuger
  mere af solens energi, end den hvid
---

