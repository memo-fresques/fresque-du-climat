---
title: Schmelzen von Meereis
backDescription: >-
  Das Schmelzen von Meereis ist nicht für den Anstieg des Meeresspiegels
  verantwortlich (ein schmelzender Eiswürfel in einem Glas bringt dieses nicht
  zum Überlaufen). Beim Schmelzen weicht es jedoch einer dunkleren Oberfläche
  (dem Wasser), die weitaus mehr
---

