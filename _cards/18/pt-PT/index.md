---
title: Derretimento do Gelo Marítimo
backDescription: >-
  O derretimento do gelo marítimo não é responsável pela elevação do nível do
  mar (um cubo de gelo que derrete num copo de água não faz a água transbordar).
  No entanto, ao derreter, dá lugar a uma superfície mais escura (o oceano), que
  absorve mais raios so
---
O volume ocupado pelo gelo sob a superfície é exatamente o mesmo que o do gelo uma vez derretido. Este é o princípio do empuxo de Arquimedes[^1]. Devido à sua salinidade, o gelo marinho forma-se quando a água do mar está a uma temperatura inferior a -1,8°C[^2]. O gelo marinho forma-se quando a água do mar congela, portanto, contém sal, mas o sal é expulso à medida que o gelo se forma. Isso significa que o gelo marinho é composto por água doce relativamente pura, enquanto a região ao redor do gelo marinho é constituída por água do mar mais salgada. Esta característica tem implicações importantes para a circulação oceânica e os ecossistemas marinhos nas regiões polares. O gelo marinho pode ser encontrado no Ártico e na Antártida. O gelo marinho do Ártico segue um ciclo sazonal. Ele derrete parcialmente no verão e forma-se novamente no inverno. Mas, desde o final dos anos 70, observa-se uma redução global da sua cobertura[^3]. Estima-se que ele possa derreter completamente no verão até o final do século[^4].  
[^1]: _[Wikipedia − Empuxo de Arquimedes](https://fr.wikipedia.org/wiki/Pouss%C3%A9e_d%27Archim%C3%A8de)_
[^2]: _https://nsidc.org/learn/parts-cryosphere/sea-ice/science-sea-ice_
[^3]: _AR6 WG1 TS.2.5 p44 (p76)_  
[^4]: _AR6 WG1 Figure TS.8 (c) p34 (p66)_
