---
title: Smelting av sjøis
backDescription: >-
  Smelting av sjøis fører ikke til havnivåstigning (akkurat som en smeltende
  isbit ikke får et glass til å renne over). Men når isen smelter, blir
  overflaten erstattet med det mørke havet, som absorberer flere solstråler enn
  hvit is.
---
Volumet som opptas av isen under overflaten er nøyaktig det samme som volumet av isen når den har smeltet. Dette er prinsippet om Arkimedes' oppdrift[^1]. På grunn av sin salinitet dannes havisen når sjøvann er ved en temperatur under -1,8°C[^2]. Havisen dannes når sjøvann fryser, så den inneholder salt, men saltet blir drevet ut etter hvert som havisen dannes. Dette betyr at sjøisen består av relativt rent ferskvann, mens området rundt havisen består av saltere sjøvann. Denne egenskapen har viktige implikasjoner for havsirkulasjonen og marine økosystemer i polare regioner. Havisen finnes i Arktis og Antarktis. Den arktiske havisen følger en sesongsyklus. Den smelter delvis om sommeren og dannes på nytt om vinteren. Men siden slutten av 1970-tallet har man observert en generell nedgang av arealet dens.[^3]. Det anslås at den kan smelte helt om sommeren innen slutten av århundret[^4].
[^1]: _[Wikipedia − Arkimedes' oppdrift](https://no.wikipedia.org/wiki/Arkimedes%27_lov)_
[^2]: _https://nsidc.org/learn/parts-cryosphere/sea-ice/science-sea-ice_
[^3]: _AR6 WG1 TS.2.5 s44 (s76)_
[^4]: _AR6 WG1 Figur TS.8 (c) s34 (s66)_
