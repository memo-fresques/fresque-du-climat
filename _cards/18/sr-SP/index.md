---
title: Otapanje morskog leda
backDescription: >-
  Otapanjem morskog leda se ne podiže nivo mora (isto kao što se otapanjem
  kockice leda ne preliva čaša s vodom). Međutim, otapanjem led se zamenjuje
  morem, koje je znatno tamnije boje i koje apsorbuje mnogo više sunčevih zraka
  nego beli led.
---

