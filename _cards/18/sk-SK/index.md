---
title: Topenie morského ľadu
backDescription: >-
  Roztápanie morského ľadu nie je zodpovedné za zvyšovanie morskej hladiny (ľad,
  ktorý sa roztopí v pohári vody nespôsobí pretečenie pohára). Roztápaním ale
  ľad vytvára priestor pre oveľa tmavšie povrchy, ktoré absorbujú viac slnečných
  lúčov.
---

