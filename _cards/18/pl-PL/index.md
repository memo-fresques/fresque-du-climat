---
title: Topnienie morskiej pokrywy lodowej
backDescription: >-
  Topnienie lodu morskiego nie wpływa na wzrost poziomu wód w morzach i oceanach
  (kostka lodu, rozpuszczając się w wypełnionej po brzegi szklance wody nie
  spowoduje przelania się zawartości).
---
Dalsze wyjaśnienie, dlaczego topnienie lodu morskiego nie podnosi poziomu morza, Archimedes i tak dalej, można znaleźć na stronie z wyjaśnieniami. Świetne grafiki ilustrujące utratę lodu na Ziemi można znaleźć w artykule ESA.
