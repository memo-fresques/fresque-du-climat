---
title: Melting of Sea Ice
backDescription: >-
  Sea ice melting does not make the sea level rise (just as a melting ice cube
  does not make a glass overflow). However, when it melts, it gives way to the
  much darker sea, which goes on to absorb more sun rays than white ice.
---

