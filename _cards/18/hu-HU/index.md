---
title: Tengeri jég olvadása
backDescription: >-
  A tengeri jég olvadásától a tengerszint nem emelkedik (épp úgy, ahogy egy
  jégkockától sem csordul túl a víz a poháron). Ugyanakkor, ahogy a jég olvad,
  utat enged a sokkal sötétebb színű tengervíznek, amely sokkal több napsugarat
  fog elnyelni, mint a fehé
---

