---
title: Melting Sea Ice
backDescription: >-
  Sea ice melting does not make the sea level rise (just as a melting ice cube
  does not make a glass overflow).
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_18_melting_of_sea_ice
youtubeCode: 3cR2lvo0TJw
instagramCode: CKUGwIaHRrI
---

For a further explanation of why melting sea ice does not raise de sea level, Archimedes and all that, see the explanation page. For some great graphics that illustrate Earth's ice loss, check out this article from ESA.
