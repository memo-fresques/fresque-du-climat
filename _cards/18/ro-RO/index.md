---
title: Topirea banchizelor
backDescription: >-
  Topirea banchizelor nu este responsabilă pentru creșterea nivelului mării (un
  cub de gheață care se topește într-un pahar cu apă nu face ca apa să se
  reverse din el). Cu toate acestea, pe măsură ce gheața din mări se topește ea
  expune mai mult din suprafa
---

