---
title: Tání pobřežního ledu
backDescription: >-
  Tání pobřežního ledu nezvyšuje hladinu moře (stejně jako tající kostka ledu
  nezpůsobí přetečení sklenice). Ovšem tím, jak ubývá led, zvětšují se tmavé
  plochy vody a ty pohlcují mnohem více slunečních paprsků než bílý led.
---

