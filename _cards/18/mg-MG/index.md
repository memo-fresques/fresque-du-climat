---
title: fitsonik'ireo ranomandry eny amin'ny ranomasina
backDescription: >-
  Ny fitsonik'ireo ranomandry eny amin'ny ranomasina dia tsy miteraka
  fiakaran'ny rano (tsy mampihoatra ny taoka ao anaty vera ny fitsonoky ny
  gilasy ao anatiny). Mamela toerana somary maloka izay maka taratry ny
  masoandro kosa anefa ilay izy mitsonoka.
---

