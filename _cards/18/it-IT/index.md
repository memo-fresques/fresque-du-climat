---
title: Fusione della banchisa
backDescription: >-
  Lo scioglimento della banchisa non è responsabile dell'aumento del livello
  dell'acqua (un cubetto di ghiaccio che si scioglie in una bevanda non fa
  traboccare il bicchiere).
---
Il volume occupato dal ghiaccio sotto la superficie è esattamente lo stesso di quello del ghiaccio una volta sciolto. Questo è il principio della spinta di Archimede[^1]. A causa della sua salinità, la banchisa si forma quando l'acqua di mare è a una temperatura inferiore a -1,8°C[^2]. La banchisa si forma quando l'acqua di mare gela e quindi contiene sale, ma il sale viene espulso progressivamente durante la formazione della banchisa. Ciò significa che il ghiaccio marino è composto da acqua dolce relativamente pura, mentre la regione circostante la banchisa è costituita da acqua di mare più salata. Questa caratteristica ha importanti implicazioni per la circolazione oceanica e gli ecosistemi marini nelle regioni polari. La banchisa si trova nell'Artico e in Antartide. La banchisa artica segue un ciclo stagionale: si scioglie parzialmente in estate e si riforma in inverno. Ma dalla fine degli anni '70, si osserva una riduzione globale della sua copertura[^3]. Si stima che potrebbe sciogliersi completamente in estate entro la fine del secolo[^4]. 
[^1]: _[Wikipedia - Principio di Archimede](https://it.wikipedia.org/wiki/Principio_di_Archimede)_
[^2]: _https://nsidc.org/learn/parts-cryosphere/sea-ice/science-sea-ice_
[^3]: _AR6 WG1 TS.2.5 p44 (p76)_
[^4]: _AR6 WG1 Figura TS.8 (c) p34 (p66)_
