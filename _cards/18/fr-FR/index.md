---
backDescription: >-
  La fonte de la banquise n'est pas responsable de la montée des eaux (un glaçon
  qui fond dans du pastis ne fait pas déborder le verre).
title: Fonte de la banquise
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_18_fonte_de_la_banquise
youtubeCode: qtZ3J_HEHuA
instagramCode: CKUB7UsoVbl
---
Le volume occupé par la glace sous la surface est exactement le même que celui de la glace une fois fondue. C'est le principe de la poussée d'Archimède[^1]. De part sa salinité, la banquise se forme lorsque l'eau de mer est à une température inférieure à -1.8°C[^2]. La banquise se forme lorsque l'eau de mer gèle, elle contient donc du sel mais le sel est expulsé au fur et à mesure que la banquise se forme. Cela signifie que la glace de mer est composée d'eau douce relativement pure, tandis que la région entourant la banquise est constituée d'eau de mer plus salée. Cette caractéristique a des implications importantes pour la circulation océanique et les écosystèmes marins dans les régions polaires. On trouve de la banquise en Arctique et en Antarctique.La banquise arctique suit un cycle saisonnier. Elle fond partiellement en été et se reforme en hiver. Mais depuis la fin des années 70, on observe une réduction globale de sa couverture[^3]. On estime qu'elle pourrait fondre intégralement en été d'ici la fin du siècle[^4].  
[^1]: _[Wikipedia − Poussée d’Archimède](https://fr.wikipedia.org/wiki/Pouss%C3%A9e_d%27Archim%C3%A8de)_
[^2]: _https://nsidc.org/learn/parts-cryosphere/sea-ice/science-sea-ice_
[^3]: _AR6 WG1 TS.2.5 p44 (p76)_  
[^4]: _AR6 WG1 Figure TS.8 (c) p34 (p66)_
