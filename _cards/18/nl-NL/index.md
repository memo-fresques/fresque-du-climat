---
title: Smeltend zee-ijs
backDescription: >-
  Het smelten van zee-ijs is niet verantwoordelijk voor de stijging van de
  zeespiegel (een ijsblokje dat smelt in een glas water zorgt er niet voor dat
  het water het glas overstroomt). Maar als zee-ijs smelt, laat het ruimte voor
  een veel donkerder element
---
Het volume dat het ijs onder het oppervlak van het water inneemt, is precies hetzelfde als dat van het ijs nadat het is gesmolten. Dit is het principe van de stuwkracht van Archimedes. Zee-ijs ontstaat als het zeewater gedurende langere tijd een temperatuur onder de -1,8°C heeft. Het zee-ijs is zout als het zich vormt, maar wordt na verloop van tijd minder zout. Zee-ijs onderscheidt zich van de ijskappen doordat het drijvend is. IJsbergen zijn ook drijvend ijs, maar komen van oprukkende ijskappen en zijn dus van zoet water. De uitgestrekte zee-ijsgebieden bevinden zich voornamelijk in het Noordpoolgebied en aan de rand van het Antarctische continent. Eenjarig zee-ijs wordt onderscheiden van meerjarig zee-ijs. Eenjarig zee-ijs vormt zich in de winter en ontdooit in de zomer. Als we het hebben over smeltend zee-ijs, bedoelen we meerjarig zee-ijs.
