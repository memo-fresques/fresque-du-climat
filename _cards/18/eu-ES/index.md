---
title: Bankisaren urtzea
backDescription: >-
  Bankisaren urtzeak ez dakar uren igoera (baso batean urtzen den izotz
  koxkorrak ez du basoa gaindiarazten). Aldiz, urtzean, iguzki izpiak
  zurrupatzen dituzten eremu ilunagoak agerrarazten ditu.
---

