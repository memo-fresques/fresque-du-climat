---
title: Smältning av havsis
backDescription: >-
  Smältning ax havsis bidrar inte till att höja havsnivån (ytnivån i ett glas
  vatten stiger inte när en isbit i glaset smälter).
---
Is flyter i vatten. Isen tränger undan en viss volym vatten under ytan - denna volym vatten väger lika mycket som isblocket. (Arkimedes [^1]) När isen har smält (till vatten) upptar vattnet därför samma volym som den del av isen som var under ytan. Därför höjs inte vattenytan vid smältning. På grund av att havsvatten är salt fryser det vid något lägre temperatur än sötvatten och havsis bildas när havsvattnet har en temperatur under -1,8°C[^2]. Havsis bildas när havsvatten fryser och innehåller salt, men saltet stöts delvis ut när havsisen bildas. Havsis består därför av relativt rent sötvatten, medan området kring ishyllan består av saltare havsvatten. Detta har konsekvenser för havscirkulationen och marina ekosystem i polarområden. Havsis finns i Arktis och Antarktis och följer en säsongscykel. Den smälter delvis på sommaren och fryser till igen på vintern. Sedan slutet av 1970-talet har det dock skett en minskning av mängden havsis[^3]. Man uppskattar att vid slutet av seklet kan havet vid polerna vara helt isfritt på somrarna. 
[^1]: _[Wikipedia − Arkimedes princip](https://sv.wikipedia.org/wiki/Arkimedes_princip)_
[^2]: _https://nsidc.org/learn/parts-cryosphere/sea-ice/science-sea-ice_
[^3]: _AR6 WG1 TS.2.5 p44 (p76)_  
[^4]: _AR6 WG1 Figure TS.8 (c) p34 (p66)_
