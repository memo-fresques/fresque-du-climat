---
title: Ondate di calore
backDescription: >-
  Le ondate di calore sono destinate a diventare più frequenti e più intense. La
  combinazione di temperatura e umidità può portare a condizioni letali per gli
  esseri umani. Il numero di giorni in cui si verificano queste condizioni
  letali è destinato ad aumentare.
---
Un'ondata di calore è un periodo prolungato di temperature estremamente elevate, spesso accompagnato da un forte calore durante il giorno e da temperature minime elevate di notte. In generale, un'ondata di calore viene dichiarata quando le temperature superano significativamente le medie stagionali per un periodo prolungato. La loro frequenza, intensità e durata aumenteranno a causa dei cambiamenti climatici[1]. 
_[1] AR6 WG1 TS.2.6 p50 (p82) // 
AR6 WG1 Box TS.10, Figura 1 
p126 (p109)_
