---
title: Upały
backDescription: Wzrost temperatur pociąga za sobą liczniejsze i silniejsze fale upałów.
---
Fala upałów to zjawisko meteorologiczne polegające na występowaniu nienaturalnie wysokich temperatur powietrza w dzień i w nocy, trwające od kilku dni do kilku tygodni na stosunkowo dużym obszarze.
