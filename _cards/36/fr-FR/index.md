---
backDescription: >-
  Les canicules vont devenir plus fréquentes et plus intenses. La combinaison de
  la température et de l'humidité peut conduire à des conditions mortelles pour
  les êtres humains. On prévoit une augmentation du nombre de jours durant
  lesquels ces conditions mortelles sont réunies.
title: Canicules
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_36_canicules'
youtubeCode: hrfVmTJom30
---
Une canicule, ou vague de chaleur, est une période prolongée de températures extrêmement élevées, souvent accompagnée d'une forte chaleur pendant la journée et de températures minimales élevées la nuit. En général, une canicule est déclarée lorsque les températures dépassent de manière significative les moyennes saisonnières pendant une période prolongée. Leur fréquence, leur intensité et leur durée va augmenter à cause du changement climatique[1].  
_[1] AR6 WG1 TS.2.6 p50 (p82) // 
AR6 WG1 Box TS.10, Figure 1 
p126 (p109)_
