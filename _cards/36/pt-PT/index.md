---
title: Ondas de Calor
backDescription: >-
  Uma das consequências do aumento de temperatura é a multiplicação de ondas de
  calor.
---
Uma Onda de calor, ou vaga de calor, é um período prolongado de temperaturas extremamente elevadas, frequentemente acompanhado de um calor intenso durante o dia e de temperaturas mínimas elevadas à noite. Geralmente, uma Onda de calor é declarada quando as temperaturas excedem significativamente as médias sazonais durante um período prolongado. A sua frequência, intensidade e duração vão aumentar devido às alterações climáticas[1].  
_[1] AR6 WG1 TS.2.6 p50 (p82) // 
AR6 WG1 Box TS.10, Figura 1 
p126 (p109)_
