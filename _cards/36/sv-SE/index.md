---
title: Värmeböljor
backDescription: >-
  Värmeböljor kommer att bli mer frekventa och kraftigare. Kombinationen av höga
  temperaturer och en hög luftfuktighet kan ge upphov till dödliga tillstånd för
  människor. Antalet dagar när dessa vädertillstånd sker kommer att öka.
---
En värmebölja är en långvarig period (några dagar elller veckor) med extremt höga temperaturer: hög temperatur under dagen och en hög lägsta temperatur på natten. I allmänhet utfärdas en varning för värmebölja när temperaturerna avsevärt överstiger säsongens medelvärde under en längre tid. Värmeböljornas frekvens, intensitet och varaktighet kommer att öka på grund av klimatförändringar[1].  
_[1] AR6 WG1 TS.2.6 p50 (p82) // 
AR6 WG1 Box TS.10, figure 1 
p126 (p109)_
