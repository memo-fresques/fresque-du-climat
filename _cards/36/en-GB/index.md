---
title: Heatwaves
backDescription: >-
  Heatwaves are set to become more frequent and more intense. The combination of
  high temperatures and humidity can create deadly conditions for humans. The
  number of days when these fatal weather conditions occur is set to rise. 
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_36_heat_waves
youtubeCode: cw0iqw6jcyc
instagramCode: ''
---
A heat wave is a meteorological phenomenon of abnormally high air temperatures, day and night, lasting from a few days to a few weeks, over a relatively large area.
