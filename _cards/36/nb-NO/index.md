---
title: Hetebølger
backDescription: Temperaturøkning fører blandt annet til flere hetebølger.
---
En hetebølge, eller varmebølge, er en lengre periode med ekstremt høye temperaturer, ofte ledsaget av sterk varme om dagen og høye minimumstemperaturer om natten. Generelt erklæres en hetebølge når temperaturene betydelig overstiger årstidens gjennomsnitt over en lengre periode. Deres frekvens, intensitet og varighet vil øke på grunn av klimaendringer[1].  
_[1] AR6 WG1 TS.2.6 s50 (s82) // 
AR6 WG1 Boks TS.10, Figur 1 
s126 (s109)_
