---
title: Deforestation
backDescription: >-
  Deforestation consists in cutting or burning down trees beyond the forest's
  natural ability to regrow. 80% of deforestation is driven by the expansion of
  agriculture.
---

