---
title: Krčenje šuma (Deforestacija)
backDescription: >-
  Krčenje šuma podrazumeva seču ili spaljivanje drveća u meri koja prevazilazi
  mogućnosti obnavljanja šume. 80% krčenja šuma je povezano sa poljoprivredom.
---

