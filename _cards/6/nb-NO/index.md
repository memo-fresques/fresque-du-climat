---
title: Avskoging
backDescription: >-
  Avskoging er nedhugging eller brenning av skog i et slikt omfang at gjenvekst
  av skogen reduseres eller opphøres. 80% av avskoging er knyttet til landbruk.
---
"Avskoging kan betraktes som en menneskelig aktivitet eller som en konsekvens av jordbruk, eller begge deler. For å avskoge, brukes ofte metoden svedjebruk eller slash-and-burn. Med andre ord, trærne blir hugget og deretter brent, noe som frigjør store mengder CO2 i atmosfæren. Selv om i-land i Nord-Amerika og Europa har forlatt denne metoden, er den fortsatt mye brukt i Amazonas og Sørøst-Asia[^1].  
[^1]: [https://tiikmpublishing.com/proceedings/index.php/iccc/article/view/570](https://tiikmpublishing.com/proceedings/index.php/iccc/article/view/570)
