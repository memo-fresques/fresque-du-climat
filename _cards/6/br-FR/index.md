---
backDescription: >-
  La déforestation consiste à couper ou brûler des arbres au-delà de la capacité
  de renouvellement de la forêt. Elle est liée à 80% à l’agriculture.
instagramCode: CK4FDvDIYXN
title: Digoadañ
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_6_d%C3%A9forestation
youtubeCode: EMi8CNnHVmE
---

