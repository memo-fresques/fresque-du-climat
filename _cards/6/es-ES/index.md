---
title: Deforestación
backDescription: >-
  La deforestación consiste en cortar o quemar árboles superando la capacidad de
  renovación del bosque. El 80% de la deforestación está relacionada con la
  agricultura.
---

