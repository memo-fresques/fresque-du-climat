---
title: Deforestazione
backDescription: >-
  La deforestazione consiste nel tagliare o bruciare alberi oltre la capacità di
  rigenerazione delle foreste. Nell'80% dei casi è legata all'agricoltura. Oltre
  ad emettere CO2, essa priva le specie locali del loro habitat.
---
La deforestazione può essere considerata un'attività umana o una conseguenza dell'agricoltura, o entrambe. Per deforestare, spesso si utilizza il metodo del taglia e brucia (slash and burn). In altre parole, gli alberi vengono tagliati e poi bruciati, rilasciando così grandi quantità di CO2 nell'atmosfera. Sebbene i paesi sviluppati del Nord America e dell'Europa abbiano abbandonato questo metodo, è ancora ampiamente utilizzato in Amazzonia e nel sud-est asiatico[^1].
[^1]: [https://tiikmpublishing.com/proceedings/index.php/iccc/article/view/570](https://tiikmpublishing.com/proceedings/index.php/iccc/article/view/570)
