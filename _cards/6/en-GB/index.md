---
title: Deforestation
backDescription: >-
  Deforestation is defined as cutting down or burning more trees than the forest
  can replace. 80% of deforestation is driven by agricultural expansion. As well
  as emitting CO2, it destroys the natural habitats of many species.
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_6_deforestation
youtubeCode: uq2Ypdj-5Oo
instagramCode: CK4H20wHJEU
---

Deforestation can be seen as a human activity, as a consequence of agriculture, or both. However, the main issue with deforestation is not so much that it destroys carbon sinks, but that it emits CO2 that took decades or centuries to capture. It's a question of flow vs. stock.
