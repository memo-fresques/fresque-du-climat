---
title: Skovrydning
backDescription: >-
  Skovrydning betyder, at man fælder og/eller afbrænder flere træer, end der kan
  nå at vokse op igen. Dermed mister skoven muligheden for at gendanne sig. 80%
  af al skovrydning sker for at skabe plads til landbrug.
---

