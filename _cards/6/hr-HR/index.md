---
title: Deforestacija
backDescription: >-
  Deforestacija je sječa ili spaljivanje drveća iznad mogućnosti obnove šume
  prirodnim putem. Širenje poljoprivrede odgovorno je za 80% deforestacije.
---

