---
title: Desflorestação
backDescription: >-
  A desflorestação consiste no corte ou queima de árvores para além da
  capacidade de renovação natural da floresta. 80% da desflorestação está
  relacionada com a agricultura.
---
"A Desflorestação pode ser considerada uma atividade humana ou uma consequência da agricultura, ou ambas. Para desflorestar, muitas vezes utiliza-se o método de queimada ou slash-and-burn. Ou seja, as árvores são cortadas e depois queimadas, libertando assim grandes quantidades de CO2 na atmosfera. Embora os países desenvolvidos da América do Norte e da Europa tenham abandonado este método, ele continua a ser amplamente utilizado na Amazónia e no sudeste asiático[^1].  
[^1]: [https://tiikmpublishing.com/proceedings/index.php/iccc/article/view/570](https://tiikmpublishing.com/proceedings/index.php/iccc/article/view/570)
