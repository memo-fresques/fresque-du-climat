---
title: Despădurire
backDescription: >-
  Despădurirea constă în îndepărtarea totală a vegetației lemnoase forestiere de
  pe o anumită suprafață, fără a lua în calcul capacitatea de regenerare a
  acesteia. 80% din despăduriri sunt făcute pentru domeniul agricol.
---

