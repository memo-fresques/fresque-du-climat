---
title: Odlesňování
backDescription: >-
  Odlesňování spočívá v kácení nebo vypalování stromů nad rámec schopnosti lesa
  porost obnovit. Za 80 % odlesňování je odpovědné zemědělství.
---

