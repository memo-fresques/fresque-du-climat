---
title: Erdőírtás
backDescription: >-
  Az erdőírtás során a fák kivágása és elégetése olyan mértékű, ami az erdő
  megújulási képességén túlmutat. Az erdőírtás 80%-a mezőgazdasági
  tevékenységhez kapcsolódik.
---

