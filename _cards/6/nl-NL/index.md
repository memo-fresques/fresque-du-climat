---
title: Ontbossing
backDescription: >-
  Ontbossing is het op grote schaal kappen of verbranden van bossen door mensen.
  Bij ontbossing wordt het herstellingsvermogen van het bos overschreden. 80%
  van de ontbossing wordt veroorzaakt door landbouw.
---
Ontbossing kan gezien worden als een voorbeeld van Menselijke Activteiten, als een consequentie van Landbouw, of allebei. Het probleem met ontbossing is niet alleen dat CO2-opslag vernietigd wordt. Het is vooral problematisch dat de CO2 die in decennia of eeuwen opgenomen is, vrijkomt. Van gekapt hout wordt 93% ter plekke verbrand. Het gaat hier om het verschil tussen beschikbaarheid en voorraad, flow versus stock. 
