---
title: Wylesianie
backDescription: >-
  Wylesianie oznacza wycinkę lub wypalanie drzew w tempie szybszym niż proces
  odnawiania się lasu. 80% wylesiania ma związek z rolnictwem.
---
Wylesianie może być postrzegane jako działalność człowieka, jako konsekwencja rolnictwa lub jako jedno i drugie. Jednak głównym problemem związanym z wylesianiem nie jest to, że niszczy ono pochłaniacze dwutlenku węgla, ale to, że emituje CO2, którego wychwycenie zajęło dziesięciolecia lub stulecia. To kwestia przepływu vs. zapasów.
