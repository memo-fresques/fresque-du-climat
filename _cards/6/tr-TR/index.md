---
title: Ormansızlaştırma (Ormanların yok edilmesi)
backDescription: >-
  Ormansızlaştırma ormanların kendilerini yenileyemeyeceği ölçüde ağaçların
  kesilerek veya yakılarak yok edilmesidir. Dünyada ormanların yok olmasının
  sebebi %80 tarımdır.
---

