---
title: Desmatamento
backDescription: >-
  O desmatamento consiste em cortar ou queimar árvores além da capacidade de
  restauração natural da floresta original. 80% do desmatamento está relacionado
  à agricultura.
---

