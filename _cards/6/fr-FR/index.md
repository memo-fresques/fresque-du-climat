---
backDescription: >-
  La déforestation consiste à couper ou brûler des arbres au-delà de la capacité
  de renouvellement de la forêt. Elle est liée à 80% à l’agriculture. En plus
  d'émettre du CO2, elle prive les espèces locales de leur habitat.
title: Déforestation
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_6_d%C3%A9forestation
youtubeCode: EMi8CNnHVmE
instagramCode: CK4FDvDIYXN
---
"La déforestation peut être considérée comme une activité humaine ou comme une conséquence de l'agriculture, ou encore les deux à la fois. Pour déforester, on utilise souvent la méthode du brûlis ou slash-and-burn. Autrement dit, les arbres sont coupés puis brûlés relachant ainsi de grandes quantités de CO2 dans l'atmosphère.  Bien que les pays développés d'Amérique du nord et d'Europe aient abandonnés cette méthode, elle reste largement utilisées en Amazonie et en Asie du sud-est[^1].  
[^1]: [https://tiikmpublishing.com/proceedings/index.php/iccc/article/view/570](https://tiikmpublishing.com/proceedings/index.php/iccc/article/view/570)
