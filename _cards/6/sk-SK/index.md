---
title: Odlesňovanie
backDescription: >-
  Odlesňovanie znamená zrezávanie alebo spaľovanie väčšieho množstva stromov ako
  vie les sám obnoviť. 80% odlesňovania je spojených s poľnohospodárstvom.
---

