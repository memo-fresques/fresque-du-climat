---
title: Emissioni di CO2
backDescription: >-
  La CO2 è il principale gas a effetto serra antropico (cioè emesso dall'uomo).
  Le emissioni di CO2 derivano dall'uso dei combustibili fossili e dalla
  deforestazione.
---
In marrone sono mostrate le emissioni legate alla deforestazione. In grigio sono mostrate le emissioni legate alla combustione di combustibili fossili[^1]. La scala verticale è in Pg C/anno (Peta grammi di carbonio = 10^15 grammi), che è lo stesso di Gt C/anno (Giga tonnellate di carbonio all'anno). Per convertire in CO2, è necessario moltiplicare per 3,67. La particolarità di questa carta è che non c'è una scala temporale. Questo può essere un indizio per associarla alla carta dei pozzi di assorbimento di carbonio. 
[^1]: _AR6 WG3 Figura TS.2 (a) // AR6 WG1 Figura 5.5 (a) p11 (p59) // p705 (p688)_
