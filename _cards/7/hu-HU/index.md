---
title: CO2-kibocsátás
backDescription: >-
  A CO2 (vagy szén-dioxid) kibocsátását tekintve az elsőszámú antropogén (emberi
  tevékenységhez kapcsolódó) üvegházhatású gáz. A kibocsátás a fosszilis
  energiahordozók elégetéséből és az erdőírtásból ered.
---

