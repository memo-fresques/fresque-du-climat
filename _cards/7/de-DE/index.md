---
title: CO2-Emissionen
backDescription: >-
  CO2 (oder Kohlendioxid) ist das erste anthropogene (vom Menschen verursachte)
  Treibhausgas (THG). Diese Emissionen entstehen durch unsere Nutzung fossiler
  Brennstoffe und Entwaldung.
---

