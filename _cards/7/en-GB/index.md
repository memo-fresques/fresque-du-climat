---
title: CO2 Emissions
backDescription: >-
  Carbon dioxide (CO2) is the main anthropogenic greenhouse gas (i.e. caused by
  human activities). CO2 emissions come from burning fossil fuels and
  deforestation.
wikiUrl: >-
  https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_7_co2_emissions
youtubeCode: vA7BJ8f8t8o
instagramCode: CKB_TF4HCFH
---

CO2 sources are witten in a litteral way on this card: Fossil Energy and Deforestation.
