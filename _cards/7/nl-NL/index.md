---
title: CO2-uitstoot
backDescription: >-
  CO2 (of koolstofdioxide) is het belangrijkste antropogene (d.w.z. aan
  menselijke activiteiten gekoppelde) broeikasgas in termen van uitstoot. Deze
  emissies zijn afkomstig van ons gebruik van fossiele brandstoffen
  (verbranding) en ontbossing.
---
Op deze kaart zie je de bijdrage van de twee belangrijkste bronnen van CO2: fossiele energie en ontbossing. Op deze kaart is geen tijdstlijn afgebeeld. De kaart Koolstofreservoirs (met tijdslijn) kan hier in de tweede ronde aangelegd worden. Hiermee wordt duidelijk waar de CO2 terechtkomt. 
