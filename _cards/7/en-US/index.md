---
title: CO2 Emissions
backDescription: >-
  CO2, or carbon dioxide, is the #1 human-made greenhouse gas in terms of
  emissions.

  CO2 emissions come from our use of fossil fuels and from deforestation.
---

