---
title: Emisiile de CO2
backDescription: >-
  CO2 este principalul gaz antropogen cu efect de seră (adică emis de oameni).
  Emisiile de CO2 provin din arderea combustibililor fosili și din despăduri.
---

