---
backDescription: >-
  Le CO2 est le premier gaz à effet de serre anthropique (c'est à dire émis par
  les activités humaines). Les émissions de CO2 viennent de la combustion des
  énergies fossiles et de la déforestation.
title: Émissions de CO2
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_7_%C3%A9missions_de_co2
youtubeCode: ACV16QfNbok
instagramCode: CKCAW03or_e
---
En couleur marron, les émissions liées à la déforestation. En couleur grise, les émissions liées à à la combustion des énergies fossiles[^1]. L'échelle verticale est en Pg C/an (Peta grammes de carbone = 10^15 grammes), ce qui est la même chose que des Gt C /an (Giga tonnes de carbone par an). Pour convertir en CO2, il faut multiplier par 3.67. La particularité de cette carte est qu'il n'y a pas l'échelle de temps. Cela peut servir d'indice pour l'associer à la carte Puit de carbone.  
[^1]: _AR6 WG3 Figure TS.2 (a) // AR6 WG1 Figure 5.5 (a) p11 (p59) // p705 (p688)_
