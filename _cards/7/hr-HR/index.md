---
title: Emisije CO2
backDescription: >-
  CO2 ili ugljikov dioksid je, po emisijama, najvažniji antropogeni staklenički
  plin. (Dakle, proizveden ljudskim djelovanjem).

  Emisije CO2 uzrokovane su izgaranjem fosilnih goriva i deforestacijom.
---

