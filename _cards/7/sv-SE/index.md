---
title: Koldioxidutsläpp
backDescription: >-
  Koldioxid står för den största delen av de antropogena (dvs som uppstår från
  mänsklig aktivitet) utsläppen av växthusgaser. Koldioxidutsläpp uppstår vid
  förbränning av fossila bränslen och avskogning.
---

