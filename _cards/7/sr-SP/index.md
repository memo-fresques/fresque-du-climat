---
title: Emisija CO2
backDescription: >-
  CO2 (ugljen-dioksid) je prvi antropogeni (koji nastaje usled dejstva čoveka)
  gas sa efekom staklene bašte u smislu količine emisije. Takva emisija potiče
  od upotrebe fosilnih goriva i krčenja šuma.
---

