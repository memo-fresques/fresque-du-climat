---
title: Emise CO2
backDescription: >-
  CO2 (neboli oxid uhličitý) je nejvýznamnější antropogenní (tj. spojený s
  lidskou činností) skleníkový plyn z hlediska emisí. Tyto emise vznikají na
  základě využívání fosilních paliv a odlesňování.
---

