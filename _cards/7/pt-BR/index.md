---
title: Emissões CO2
backDescription: >-
  O CO2 é o mais importante gás de efeito estufa de origem antrópica (isto é,
  produzida pelas atividades humanas). As emissões de CO2 têm origem na queima
  de combustíveis fósseis e no desmatamento.
---

