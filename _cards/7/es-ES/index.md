---
title: Emisiones de CO2
backDescription: >-
  El CO2 es el principal GEI antrópico (de origen humano). Las emisiones de CO2
  provienen de la utilización de los combustibles fósiles y de la deforestación.
---
En color marrón vemos las emisiones vinculadas a la deforestación. En color gris, las emisiones ligadas a la combustión de combustibles fósiles[^1]. La escala vertical está en Pg C/año (Peta gramos de carbono = 10^15 gramos), que es lo mismo que Gt C/año (Giga toneladas de carbono por año). Para convertir a CO2, multiplique por 3,67. La particularidad de este mapa es que no existe una escala de tiempo. Esto puede servir como pista para asociarlo con la tarjeta Sumideros de Carbono.  
[^1]: _AR6 WG3 Figura RT.2 (a) // AR6 WG1 Figura 5.5 (a) p11 (p59) // p705 (p688)_
