---
title: CO2 emisyonu (salımı)
backDescription: >-
  CO2 ilk ve en önemli insan aktivitesi kaynaklı sera gazıdır. CO2 salımı,
  insanların fosil yakıt kullanımından kaynaklanır. Aynı zamanda, ormanların yok
  edilmesi de ağaçların gövdelerinde tuttuğu CO2'nin yanarak açığa çıkmasıyla
  sera etkisini arttırıcı rol
---

