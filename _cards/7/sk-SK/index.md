---
title: CO2 emisie
backDescription: >-
  Oxid uhličitý (CO2) je prvý antropogénny skleníkový plyn (tj. zapríčinený
  ľudskou aktivitou). Emisie CO2 pochádzajú zo spaľovania fosílnych palív a
  odlesňovania.
---

