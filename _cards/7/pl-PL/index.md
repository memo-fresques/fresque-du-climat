---
title: Emisje CO2
backDescription: >-
  CO2 jest głównym antropogenicznym (tzn. emitowanym przez człowieka) gazem
  cieplarnianym. Emisje CO2 są następstwem spalania paliw kopalnych oraz
  wylesiania.
---
Źródła CO2 są zapisane w sposób dosłowny na tej karcie: energia z paliw kopalnych i wylesianie.
