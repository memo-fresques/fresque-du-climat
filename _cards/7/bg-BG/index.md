---
title: Емисии CO2
backDescription: >-
  Въглеродният диоксид (CO2) е основният парников газ, свързан с човешки
  дейности. Съответните емисии на въглероден диоксид са резултат от изгарянето
  на изкопаеми горива и от обезлесяването.
---

