---
backDescription: >-
  Le CO2 est le premier gaz à effet de serre anthropique (càd émis par l’homme).
  Les émissions de CO2 viennent de la combustion des énergies fossiles et de la
  déforestation.
instagramCode: CKCAW03or_e
title: Dilaoskadenn CO2
wikiUrl: >-
  https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_7_%C3%A9missions_de_co2
youtubeCode: ACV16QfNbok
---

