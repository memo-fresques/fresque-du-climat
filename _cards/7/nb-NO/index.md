---
title: CO2-utslipp
backDescription: >-
  CO2 (karbondioksid) er den største menneskeskapte klimagassen når det kommer
  til utslipp. Disse utslippene kommer fra forbruket av fossile brensler og
  avskoging.
---
I brunt, utslipp knyttet til avskoging. I grått, utslipp knyttet til forbrenning av fossile brensler[^1]. Den vertikale aksen er i Pg C/år (Peta gram karbon = 10^15 gram), som er det samme som Gt C/år (Giga tonn karbon per år). For å gjøre om til CO2, må man multiplisere med 3.67. Det spesielle med dette kortet er at det ikke er noen tidsskala. Dette kan virke som en ledetråd for å knytte det til karbonsluk-kortet (kort 12).  
[^1]: _AR6 WG3 Figur TS.2 (a) // AR6 WG1 Figur 5.5 (a) s11 (s59) // s705 (s688)_
