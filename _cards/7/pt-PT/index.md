---
title: Emissões de CO2
backDescription: >-
  O CO2 (dióxido de carbono) é o mais importante gás de efeito de estufa de
  origem antrópica (relacionado com as atividades humanas). As emissões de CO2
  têm origem na queima de combustíveis fósseis e na desflorestação.
---
A castanho, as emissões relacionadas com a Desflorestação. A cinzento, as emissões relacionadas com a combustão dos combustíveis fósseis[^1]. A escala vertical está em Pg C/ano (Peta gramas de carbono = 10^15 gramas), o que é o mesmo que Gt C/ano (Giga toneladas de carbono por ano). Para converter em CO2, é necessário multiplicar por 3.67. A particularidade deste gráfico é que não há escala de tempo. Isto pode servir como uma pista para o associar ao gráfico dos Sumidouros de carbono.  
[^1]: _AR6 WG3 Figura TS.2 (a) // AR6 WG1 Figura 5.5 (a) p11 (p59) // p705 (p688)_
